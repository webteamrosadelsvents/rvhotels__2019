<div class="text-center" id="filtro-ofertas-container" data-spy="affix" data-offset-top="1200">
    <section class="filtro-ofertas filtro-ofertas-2017">
        <div class="row">
            <div class="sf-checkbox-wrapper col-md-6 col-xs-12 text-center">
                <i class="fa fa-search"></i> <?php echo __("Ofertas");?>:&nbsp;
                <label><input type="checkbox" value="Hotel" id="tipo-hotel" name="" checked><span class="customcheck"></span> <span class="leyenda"><?php echo __("Hotel");?></span></label><!--
                --><label><input type="checkbox" value="Apartamento" id="tipo-apartamento" name="" checked><span class="customcheck" ></span> <span class="leyenda"><?php echo __("Apartamento");?></span></label>
            </div>
            <?php /* HASTA QUE HAYA ALGO EN APARTAMENTOS DE OFERTA, OCULTAR ESOS DESTINOS */ ?>
            <style>
            option[value="estartit"],
            option[value="club_torrevella"],
            option[value="platjadaro"],
            option[value="Blanes"],
             option[value="escala"] {
                display:none;
            }
            </style>
            <div class="col-md-6 col-xs-12 text-right nowrap">
                <?php echo __("¿Donde?");?>
                <select id="destino" name="destino" class="form-control unaUotra display-inline-block" style="width:70%;margin-left:10px">
                    <?php
                    foreach ($arr_ubicas as $key => $value) {
                    ?>
                    <option value="<?php echo $key;?>"><?php echo $value;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <?php /* no funcionan las categorias - ocultas hasta que pete 
            <!--
            <div class="col-md-4 col-xs-6">
                <select id="categoria" name="categoria" class="form-control">
                    <option value="-1"><?php echo __("Categoria");?></option>
                    <?php
                    foreach ($taxonomies as $taxonomy) {
                    ?>
                    <option value="<?php echo "cat-".$taxonomy->slug;?>"><?php echo $taxonomy->name;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            -->
            */
            ?>
        </div>
    </section>
</div>
