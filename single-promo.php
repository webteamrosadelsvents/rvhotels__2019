<?php
get_header();
$this_post = $post->ID;

$metas          = get_post_meta($post->ID);
$titulo         = get_the_title();
$imagen         = wp_get_attachment_image_src( $metas['_thumbnail_id'][0],"thumbnail" );

$precio         = $metas['custom_precio'][0];
$dto            = $metas['custom_descuento'][0];
$pornoche       = ( ($metas['custom_pornoche'][0]!="0") && ($metas['custom_pornoche'][0]!="") ) ? "<small class='txt-small'>/".__("noche")."</small>" : "";

$esHotel        = (isset($metas['custom_oferta_en_apart']) && $metas['custom_oferta_en_apart'][0]!="-")?"nohotel":"hotel";
$hotel_apart    = descrimina_hotel_apartamento($metas, $esHotel);

$link           = get_custom_link($metas, $post->ID);

$idApartWP      = (isset($metas['custom_oferta_en_apart'][0]) ? $metas['custom_oferta_en_apart'][0] : "" );
$idHotelWP      = (isset($metas['custom_oferta_en_hotel'][0]) ? $metas['custom_oferta_en_hotel'][0] : "" );
$linkApartPromo = get_permalink($idApartWP);

$idAptoNeo      = get_post_meta( $idApartWP, 'custom_bookings', true );
$arrival        = (isset($metas['custom_dtinicio'][0]) ? $metas['custom_dtinicio'][0] : "" );
$departure      = (isset($metas['custom_dtfin'][0])? $metas['custom_dtfin'][0] : "");
$nights         = (isset($metas['custom_noches'][0])? $metas['custom_noches'][0] : "");

$zonaClass      = (isset($metas['custom_oferta_en_zona'][0]) && $metas['custom_oferta_en_zona'][0]!="-") ? "zona-".$metas['custom_oferta_en_zona'][0] : "zona-";
$zonaClass      = sanitize_title($zonaClass,"default");

$hotelClass      = "hotel-".$hotel_apart[0];
$hotelClass      = sanitize_title($hotelClass,"default");

$postID_class    = "promoid-".$post->ID;
$postID_urlencoded = urlencode("?promoid=".$post->ID);

$discountType   = "dto-".get_field("apto_tipodescuento");

$idiomalocal = ICL_LANGUAGE_CODE;

/* NOVEDAD -- CADUCIDAD DE OFERTAS - SIN DESPUBLICAR / PONER EN BORRADOR */

/* USANDO ESTE CAMPO PODEMOS PLANTAR UN MENSAJE DE OFERTA CADUCADA, DESHABILITAR BOTON DE RESERVA Y SEGUIR MOSTRANDO EL ENLACE */

$promoExpired = false; // indicamos si ha expirado

$fechaActual = date("d/m/Y");
// contiene la fecha de caducidad en caso que la tenga
if (isset($metas['custom_promoexpires'][0])) {
	if ($metas['custom_promoexpires'][0] != "") {
    	// en caso que tengamos fecha de caducidad, la utilizamos
		$caducidadPromo = date($metas['custom_promoexpires'][0]);
		if ( strtotime($fechaActual) >= strtotime($caducidadPromo) ) {
			$promoExpired = true;
		}
	} else {
    	// tenemos meta de fecha de caducidad pero esta vacio
		$caducidadPromo = date("d/m/Y", time()+86400);
	}    
	} else {
		// no tenemos meta de caducidad
		$caducidadPromo = date("d/m/Y", time()+86400);
}

?>
<script type="text/javascript">
	console.log('Hoy es <?php echo $fechaActual;?> !)');
	console.log('Promo expires <?php echo $caducidadPromo;?> !)');
<?php if ($promoExpired) { ?>
	console.log('Fecha actual es igual o mayor que la fecha de caducidad');
<?php } ?>
</script>

<?php

/* forfait */

$forfaitonly    = 0; 
$botonforfait   ="";
$llevaforfaitclass =""; //css class para marcar el tipo de oferta por si acaso
$soloforfaitclass ="";  //css class para marcar si es de las que lleva un solo boton
$opcionforfait  = ( ($metas['custom_forfaitcode'][0]!="0") && ($metas['custom_forfaitcode'][0]!="") ) ? $metas['custom_forfaitcode'][0] : "";
$reservarsolohotel ="";
$forfaitinfo="";
$forfaitincludes = ( ($metas['custom_forfaitincludes'][0]) && ($metas['custom_forfaitincludes'][0]!="") ) ? $metas['custom_forfaitincludes'][0] : "";

if ( ($metas['custom_forfaitcode'][0]) && ($metas['custom_forfaitcode'][0]!="") ) {

  $opcionforfait = $metas['custom_forfaitcode'][0];
  $botonforfait = " <a href='".$opcionforfait."' class='btn-reservarforfait uk-button display-inline-block uk-button uk-button-large blanco button-large text-uppercase text-bold'>RESERVAR <span class='txt-conforfait'>CON FORFAIT</span></a>";
  $llevaforfaitclass = " con-forfait";
  $reservarsolohotel = "<span>".__("SOLO HOTEL")."</span>"; // texto para poner en el boton de reserva normal, para distinguirlo del de con forfait
  $forfaitonly    = $metas['custom_forfaitonly'][0]; // checkbox mostrar solo forfait

  if ($forfaitonly==1) {
    $soloforfaitclass = " solo-forfait ";
  }

  // traducir peazo cadena via wpml... el titulo no cal...
  $forfaitinfo="<div class='uk-width-1-1 col-xs-12 margin-top-10 margin-bottom-0 text-center baguettebox'><div class='alert alert-forfait'>".
  $forfaitincludes."<a href='https://www.rvhotels.es/wp-content/uploads/forfaits-instrucciones-".$idiomalocal.".jpg'> <button class='btn btn-xs btn-sm uk-button uk-button-small text-bold' style='margin:3px!important;height:20px;min-height:0;max-height:20px'>+ INFO</button></a></div></div>";
}

// ver includes / single-promo--reservar.php //

// CATEGORIAS DE LA PROMOCION

$taxonomies = get_terms( array(
    'taxonomy' => 'promo_categories',
    'hide_empty' => false,
    "suppress_filters"=>false
) );

$terms = wp_get_post_terms($post->ID,'promo_categories');

$catPromosBadges ="";
$urlPromos =  __("https://www.rvhotels.es/promos/");

foreach ($terms as $term) {
  // al term name le cambiamos los apostrofes de catalan y frances para evitar ruptura de cadena
  $nombreCategoriaAcentos = str_replace("'","&apos;",$term->name);
  $catPromosBadges .= " <a href='".$urlPromos.$term->slug."' class='display-inline-block' title='".__("Mira todas las ofertas de:")." ".$nombreCategoriaAcentos."'><span class='badge margin-bottom-5'>".$nombreCategoriaAcentos."</span></a>";
  //echo "<pre>".print_r($term, true)."</pre>";
}

if (!isset($pornoche)) $pornoche="";
if (!isset($catPromos)) $catPromos="";
if (!isset($zonaClass)) $zonaClass="";
if (!isset($linkApartPromo)) $linkApartPromo="";
if (!isset($discountType)) $discountType="";
if (!isset($postID_class)) $postID_class="";
if (!isset($postID_urlencoded)) $postID_urlencoded="";
if (!isset($promo_oculta)) $promo_oculta="";

// Imagen para descuentos

if (!isset($dto) or ($dto="NO") ) {
        $dto="";
        $dtoImg="";
    } else {
        $dto=str_replace("%", "", $dto);
        $dtoImg='<img class="dto dtoimg" src="/wp-content/themes/rvhotels/images/dtos/dto-'.$dto.'.png" alt="-'.$dto.'%"/>';
}
?>

<script src="https://www.rvhotels.es/wp-content/themes/rvhotels/js/isotope.pkgd.min.js"></script>
<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
<?php include("includes/modal-promo-apto-v2.inc.php");?>

<style type="text/css">
	#clousmi {display:none;}
</style>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/single-oferta.min.css?v=2"/>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/baguetteBox.min.css"/>

<style type="text/css">
	#masinfosup {cursor:pointer}
	#masinfosup+input:checked+div {display: block!important;}
</style>

<section class="bg-brokenwhite">
	<div class="container container-large margin-bottom-0">
		<article class='singleoferta ofertaDetail margin-top-30 <?php echo $llevaforfaitclass." ".$soloforfaitclass;?>'>
			<figure class='promo-header bg-lightblue position-relative'>
				<div class='promo-toolbar uk-position-top' data-spy="affix" data-offset-top="330">
					<div class='container'>
						<?php if ($precio != "0"){ ?>
						<div class='pvpdto pvpfrom text-right uk-position-top-left blanco'>
							<span class='fromtxt'><?php echo __("desde");?></span> <span class='uk-text-bold uk-text-large'><?php echo $precio; ?></span> <strong>€</strong> 
                          	<?php
								echo $pornoche;
                          		echo $dtoImg;
                          	?>
                        </div>
						<?php } ?>
                        <p class='titleagain blanco text-center text-large margin0'><?php the_title(); ?></p>
					</div>
				</div>

				<?php // comprobamos si se ha introducido imagen grande o si usamos la imagen destacada en tamanyo medio
                      
				if ( get_field( "img_big_header" )!="") { ?>
					<img src="<?php the_field( "img_big_header"); ?>" alt="<?php the_title(); ?>" class="img-responsive lazy-img"/>                  
					<?php } else { ?>
						<img data-original="<?php the_post_thumbnail_url('medium'); ?>" alt="<?php the_title(); ?>" class="img-responsive lazy-img"/>
					<?php } ?>
				<figcaption class='title uk-position-bottom-left hotel'>
                	<h1 class="text-center text-bold text-uppercase "><?php the_title(); ?></h1>
                	<h2 class='text-center text-large blanco subtitle margin-top-30 margin-bottom-40 hot-<?php echo sanitize_title_with_dashes($hotel_apart[0]);?>'>
                   		<?php echo __("Ofertas y Escapadas");?> en <?php echo $hotel_apart[1];?> - RV Hotels <?php echo $hotel_apart[0];?> <span class="hotel"><!-- stars --></span>
                	</h2>
                	<div class="row row-small">
						<div class="col-md-6 mobile-center promobread">
							<p class="text-small blanco margin-bottom-0"><strong><a class="blanco" href="<?php echo __("https://www.rvhotels.es/ofertas/");?>"><?php echo __("OFERTAS RV Hotels");?></a></strong> > <?php the_title(); ?></p>
                   		</div>
                  		<div class="col-md-6 text-right mobile-center badges">
                   			<?php // tags de las categorias con links
                       		echo $catPromosBadges;
							?>
						</div>
					</div><!-- -->
				</figcaption>
			</figure>
			<div class='padding30 promo-content margin-bottom-0'>
				<div class='excerpt text-large text-center text-bold darkblue'><?php the_excerpt(); ?></div>
                <?php // AVISO PROMO CADUCADA 
				if ($promoExpired) { ?>
					<div class="alert alert-danger text-center">
                       	<p><strong><?php echo __("¡LO SENTIMOS! ESTA PROMOCIÓN HA CADUCADO!");?></strong></p>
                		<p><?php echo __("Ya no está disponible para su reserva. Disponemos de ofertas similares...");?></p>
                        <p><?php echo __("También puedes apuntarte a nuestra newsletter, y así recibirás avisos de nuestras próximas ofertas.");?></p>
                        <p class="margin-top-10">
                           	<a id="caduc-verotras" href="#" class="btn btn-danger"><?php echo __("OFERTAS SIMILARES");?></a>
                           	<a id="caduc-newsletter" target="_blank" href="//rvhotels.us2.list-manage.com/subscribe/post?u=23b5df66471f0ebe80f216080&id=5b7afe5083" class="btn btn-danger"><?php echo __("APÚNTATE A NUESTRA NEWSLETTER");?></a>
                        </p>
					</div>
				<?php } ?>
				<div class='content text-left'>
					<div class='row'>
						<div class='uk-width-medium-1-3 col-md-4 leftcol'>
							<figure class='margin-bottom-15'>
								<img src="<?php the_post_thumbnail_url('medium'); ?>" data-original="<?php the_post_thumbnail_url('medium'); ?>" alt="<?php the_title(); ?>" class="img-responsive lazy-img" style="width:100%"/>
                            </figure>
						</div>
						<div class='uk-width-medium-2-3 col-md-8 rightcol content'><?php the_content(); ?></div>
						<div class='col-md-12 row-gallery'><div class='uk-width-medium-3-4 container-center minigallerycont margin-top-20'></div></div>
					</div>
				</div>
				<div id='bookingblock' class='bookingblock reservaonline text-center margin-bottom-0'>
					<div class='uk-grid uk-grid-medium row'>
						<?php if ($precio != "0"){ ?>
							<div class='uk-width-medium-1-2 col-md-6'>
								<p class='pvpfrom lightblue text-right mobile-center mobile-margin-remove'>
									<span class='fromtxt'><?php echo __("desde");?></span> 
									<span class='position-relative display-inline-block'><?php echo $dtoImg;?></span>
									<span class='uk-text-bold uk-text-large'><?php echo $precio; ?></span><strong>€</strong>
									<?php echo $pornoche;?>
								</p>
							</div>
    	                    <div class='uk-width-medium-1-2 col-md-6 text-left mobile-center mobile-margin-bottom'>
						<?php } else { ?>
							<div class='uk-width-medium-1-1 col-md-12 text-center mobile-center mobile-margin-bottom'>
						<?php } ?>
							<?php include "includes/single-promo--reservar.php";?>
						</div>
                        <?php echo $forfaitinfo;?>
						<div class='uk-width-1-1 lletrapetita col-xs-12 margin-top-0 margin-bottom-0 text-center'></div>
					</div>
				</div>
				<div class='share'>
					<div class="lyteShare twitter facebook social-inferior text-center margin-extra margin-bottom-0"><?php echo __("¡Nos encanta compartir!");?><br><br><strong><?php echo __("¿Y a tí?");?></strong><br><br> </div>
                        <script type="text/javascript">
                            // boton de whatsapp
                          jQuery('.lyteShare').append('<a style="display:none;transform:scale(1.3)translate(-1px,1px)" href="whatsapp://send?text=<?php the_title(); ?> <?php the_permalink(); ?>"  title="Enviar por Whatsapp" data-action="share/whatsapp/share" class="wa_btn wa_btn_l hidden-sm-up"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAjCAMAAADL21gSAAABoVBMVEUAAAD+/v7+/v7////+/v7////////+/v7+/v7+/v7+/v7+/v7+/v7+/v7////9/f3+/v79/f3////8/Pz+/v7////+/v7////+/v7+/v7////+/v7+/v79/f3////9/f3////////+/v79/f3+/v7+/v7+/v7+/v78/Pz9/f3+/v79/f3+/v7+/v7w8PD+/v78/Pz+/v7+/v4AqFn///8AqVoApFEAnEIAmDoAqlwAoUsAmz8Ap1YAplMAoEkAljYBok0AnkcAnkQAmTwAlzj2/PrY8eQAql0AlDOv48n5/Prt+PLA6NQRrmT0+/fn9u3b8ubG7Nuo38OM17KB0qh7zaFly5xbxpFHwYhUwIY5vH42unottnQasWoWq10AkzAAki7i9OrU8eTM7uC86NKf3b+Z3b+S27mR17KH06thxY9NwosxuXk7uHcxtHAVr2UAiyHg9OnN69jI6dW55MuO2rmh2beH1bB40ah90aZszJ1px5RWxZBewopPvH9MvH4tunxDuHgzt3dAt3YltHAtsmwirmQZrWIPqlwPqVsIpFJDF8oYAAAAM3RSTlMA/vLsDvrTvnHn29iskoh7WlE9MRQH4860saSAdnVjXzQhHBbh38nDt5+bZldLRUMrJgTmVxRAAAACk0lEQVQ4y4XT5XLbUBCGYa+5YU6atMEyt/rEZGaHmZmpzMy96kondiS3mfad8R/7GeuspPVUdDdcE/R6qzvqG1s9Z9fSVYVydK72/lmmx0ewSw6eMLrwt+mwydDOzKej4vzMRMpm7QOVxH8FwOrbX2Y8piixuKm8L9jXvFeBqgHsxMwIz7F4QZJeWyrQ4jI1BOxmFJmJEst8ToKCjmm0zNSyJguKxp8qfvkQoLqyaQ0QtpY0WdSLEQZKbHnSUn0ldJGwohhp9SCV3IvzjhISBVBnCV0G9jKysQjgYUx3kBxfAMjPTDNhSBA46QWsZlXOSVMLoAaGwsB2gufUURAhK7lPZU4C1xnqBN5YP0lbNnqqcq7EfSDIUBCYEzkuPg0gKwpuFOHzaGPIB1owOE43hoHxY9mNBH0YPoa8yBUVa5bErH3wDM+OXEYpeBm6BHyJ2l+ZmwB9XeL4tGKoBs/baAXnGAoB06o9ixEdAZIfM4o6X3i5KOkyZyzkUMVQHfDKtC+SjkXXAGzHxCFgcCKicdI0EGLoFmH9ZPC0GMkCGF4DwR6ZT4yBuhjyE1AUeaai8QlYEREwH4sIeVCvh3Ue2MiUHpeQOBwFayzGL03CPhKrl4B9SS69Rmr8YHwktT4l6uLPJKjbAuW/yi9Gyg+MEyVFN+KckhgBtTlrEAA+iJyTLmjpqLQJouZTNEBICTqvCHJZyZoZzQLu3QsTNhLRxBFnGppmES1iqu9WLVPrcboKzB1/G0+mdr8boiSp0R8zT8C2wOmBF4NTzwhWuUfZseejj/M28TV4XDUR5U5uIU6jwA3XYrJtIVvU3G4IkRWsT3u431PZNRD5atmw/U099fXdTX2eP2slqr7p9/y7O6HG/4jf2t25aw2zopMAAAAASUVORK5CYII=" alt="Whatsapp"></a>');
                          // Move social media after the content in mobile
                          jQuery(document).ready(function($) {
                            $('.mobile .lyteShare').insertAfter('.bloque-booking');
                          });
                          console.log('post id '+<?php echo $this_post;?>);
						</script>
                        <style type="text/css">
							@media (max-width:1023px) {.wa_btn{display:inline-block!important;}}
						</style>
					</div>
				</div>
                <h3 class='uk-text-center masofertastxt margin-bottom-30'></h3>
			</article>
		</div>
	</section>

	<script type="text/javascript">
  		jQuery(document).ready(function($){
			$('.rightcol > img').appendTo('.leftcol').wrap('<figure></figure>');
			$('minigaleria').appendTo('.minigallerycont');

			//duplicar formulario reservar en el toolbar del header de la oferta - CUANDO ES ACINET - form POST
			$('#bookingblock #reservaoferta').clone().appendTo('.promo-toolbar .container').attr({'id':'reservaofertaClon','name':'reservaofertaClon'});
			//duplicar boton bookingbutton
			$('#bookingblock .btn-bookingbutton').clone().appendTo('.promo-toolbar .container').attr({'id':'reservaofertaClon','name':'reservaofertaClon'});

			<?php if ( ($metas['custom_forfaitcode'][0]) && ($metas['custom_forfaitcode'][0]!="") ) {?>
				// En caso de tener abajo dos botones, reserva hotel y reserva forfait,
				// el boton clonado sticky de arriba nos dirigira al pulsar hasta los botones inferiores, para
				// dejar claras ambas opciones
			   $('#reservaofertaClon span').remove(); // borrar del de arriba el solo hotel
			   $('#reservaofertaClon').removeAttr('href').attr('href','#bookingblock'); // eliminar link del de arriba
			   $('#reservaofertaClon').click(function(e){ // scroll hasta los botones inferiores
				 e.preventDefault();
				 $('html, body').stop().animate({
							scrollTop: $('#bookingblock').offset().top - 250
						}, 400);
			   });
   
			<?php if ($forfaitonly==1) {?>
			  // MOSTRAR SOLO EL BOTON DE FORFAIT Y ELIMINAR EL BOTON DE RESERVA NORMAL
			  // COMO HAY DOS CASOS, LINK O FORMULARIO SEGUN MOTOR, BORRAMOS AMBOS
			  $('#bookingblock .btn-bookingbutton, #reservaoferta').remove();

			<?php }?>

			<?php }?>

			$('.promo-toolbar .container .btn-reservaroferta').addClass('btn-reservaroferta2 uk-position-top-right');

			// los tag <small> de la letra pequenya de cada oferta
			// los movemos a nuestra nueva cajita de letra petita
			$('.letrapeque,.lletrapeque,.content small, .content .uk-alert .uk-text-small').appendTo('.lletrapetita').addClass('uk-margin-bottom-remove');

			// anadimos esto en todos los bloques de letra pequenya OFERTAS NO ACUMULABLES

			if ($('#ocultar_plazas').val() == "" || typeof $('#ocultar_plazas').val() === "undefined") {
				$('[lang="es"] .lletrapetita').append('<br><strong class="uk-text-small">*** IMPORTANTE: LAS OFERTAS Y PACKS NO SON ACUMULABLES ENTRE SÍ.</strong>');
				$('[lang="ca"] .lletrapetita').append('<br><strong class="uk-text-small">*** IMPORTANT: LES OFERTES I  PACKS NO SÓN ACUMULABLES ENTRE SÍ.</strong>');
				$('[lang="en"] .lletrapetita').append('<br><strong class="uk-text-small">*** IMPORTANT: PLEASE NOTE THAT THE DISCOUNTS AND OFFERS ARE NOT CUMULATIVE</strong>');
				$('[lang="fr"] .lletrapetita').append('<br><strong class="uk-text-small">*** IMPORTANT: LES OFFRES ET LES PACKS NE SONT PAS ACHUMABLES</strong>');
			}
			// pegatina plazas limitadas en todas las ofertas

			//borrar todas las que hay puestas a mano en algunas ofertas
			$('.content img[src*="plazas-limitadas-"]').remove();
			
			if ($('#ocultar_plazas').val() == "" || typeof $('#ocultar_plazas').val() === "undefined") {
				$('.content p')
				.first()
				.prepend('<img src="//www.rvhotels.es/wp-content/themes/rvhotels/images/plazas-limitadas-<?php echo $idiomalocal;?>.png" \
				width="90" height="auto" alt="Plazas limitadas" class="uk-align-right right"/>');
			}
			
			// eliminar antiguo bloque de reserva y scripts guarros

			$('.content form, .content script, .pastillaPrecio').remove();

			$('.content ul').addClass('checklist');

			// shortcode nota packs - suplementos ham

			/*$('[lang="es"] .nota-suplementos').html('\
				  Para reservar esta oferta, pulse RESERVAR y en el 1er paso del proceso de reserva, pulse el botón verde PACKS, para poder escoger aquí entre las opciones disponibles.\
					Recuerde que los packs ya incluyen el alojamiento en el precio. El tipo de habitación y régimen los podrá escoger en el siguiente paso.\
				  <label id="masinfosup" for="masinfosupc"><i class="uk-icon-plus-circle"></i> <strong>Más información</strong></label>\
				  <input type="checkbox" name="masinfosupc" id="masinfosupc" style="position:absolute;left:-10000px">\
				  <div style="display:none"><hr>\
					<p class="uk-margin-remove">\
					  Escoger primero pack, y a continuación tipo de habitación y régimen. Rogamos verifiquen fechas y número de ocupantes.</p>\
					<img src="http://www.hotelametllamar.com/images/2017/ayuda-packs.jpg" alt="Ayuda visual selección de PACKS"/>\
				  </div>');

			$('[lang="ca"] .nota-suplementos').html('Per reservar aquesta oferta, premi RESERVAR i al 1er pas del procés de reserva, premi el botó verd PACKS,\
					per poder escollir aquí entre les opcions disponibles. Recordi que els packs ja inclouen l\'allotjament en el preu. El tipus d\'habitació i règim \
					els podrà escollir en el següent pas.<label id="masinfosup" for="masinfosupc"><i class="uk-icon-plus-circle"></i> <strong>Més informació</strong></label>\
					<input type="checkbox" name="masinfosupc" id="masinfosupc" style="position:absolute;left:-10000px"><div style="display:none"><hr> \
					<p class="uk-margin-remove"> Escollir primer pack, i a continuació tipus d\'habitació i règim. Preguem verifiquin dates i nombre d\'ocupants.</p>\
					<img src="http://www.hotelametllamar.com/images/2017/ayuda-packs.jpg" alt="Ayuda visual selección de PACKS"/></div>');*/

			$('[lang="en"] .nota-suplementos').html('To book this offer, press BOOK NOW, tnen in the 1st step of the reservation process, press the green PACKS button, to choose from among the available options. Remember that packs already include accommodation in the price. The type of room and regime can choose them in the next step.<label id="masinfosup" for="masinfosupc"><i class="uk-icon-plus-circle"></i> <strong>More information</strong></label><input type="checkbox" name="masinfosupc" id="masinfosupc" style="position:absolute;left:-10000px"><div style="display:none"><hr> <p class="uk-margin-remove"> Choose first PACK, then type of room / board. Please remember to check dates and number of occupants.</p><img src="http://www.hotelametllamar.com/images/2017/ayuda-packs.jpg" alt="Ayuda visual selección de PACKS"/></div>');
			$('[lang="fr"] .nota-suplementos').html('Pour réserver cette offre, cliquez sur RÉSERVER et la 1ère étape du processus de réservation, appuyez sur les PACKS bouton vert, à choisir parmi les options disponibles ici. Rappelez-vous que les packs et comprennent l\'hébergement dans le prix. Le type de chambre et le régime peut choisir à l\'étape suivante.<label id="masinfosup" for="masinfosupc"><i class="uk-icon-plus-circle"></i> <strong>Más información</strong></label><input type="checkbox" name="masinfosupc" id="masinfosupc" style="position:absolute;left:-10000px"><div style="display:none"><hr> <p class="uk-margin-remove"> Choisissez premier pack, puis le type de chambre et pension. S\'il vous plaît vérifier les dates et le nombre d\'occupants.</p><img src="http://www.hotelametllamar.com/images/2017/ayuda-packs.jpg" alt="Ayuda visual selección de PACKS"/></div>');

		  });
	</script>

	<?php

		// CONTENEDOR OFERTAS RELACIONADAS EN LA ZONA
		// relaciones de zonas

		$relatedZona="";

		// relacionar rialp con vall aran para ofrecer mas ofertas de pirineo

		if ($metas['custom_oferta_en_zona'][0]=="valaran") {
		  $relatedZona = "rialp";
		}

		if ($metas['custom_oferta_en_zona'][0]=="rialp") {
		  $relatedZona = "valaran";
		}

		// relacionar vall llobrega con torroella de montgri - estartit

		if ($metas['custom_oferta_en_zona'][0]=="torroella_montgri") {
		  $relatedZona = "vall_llobrega";
		}

		if ($metas['custom_oferta_en_zona'][0]=="vall_llobrega") {
		  $relatedZona = "torroella_montgri";
		}

		$relatedZonaArgs = array(
						"posts_per_page"=>"8",
						"post_type"=>"promo",
						"post_status"=>array('publish'),
						"post__not_in" => array($this_post),
						"meta_query"=>array(
							'relation' => 'OR',
							array(
								"key"=>"custom_oferta_en_zona",
								"value"=>$metas['custom_oferta_en_zona'][0]),
							array(
								"key"=>"custom_oferta_en_zona",
								"value"=>$relatedZona)
						),
						"suppress_filters" => 0
		);

		// query_posts( $args );
		$wp_query = new WP_Query( $relatedZonaArgs );

	?>
	<section class="bg-lightblue" id="otras-ofertas">
    	<div class="container">
    		<?php if ( $wp_query->post_count>1 ) {?>
      			<div class="margin-top-30 margin-bottom-30">
        			<h3 class="blanco text-uppercase text-center"><?php echo __("Otras ofertas en");?> <?php echo $hotel_apart[1];?> <?php echo __("y destinos cercanos");?></h3>
      			</div>
				<section class="container-ofertas padding0 clear">
        			<div class="row-ofertas grid">
          				<?php /* grid sizer y gutter sizer son necesarios para el calculo de la grid masonry */ ?>
          				<div class="grid-sizer"></div>
          				<div class="gutter-sizer"></div>
						<?php
            				$resuls = 0;
            				while ( $wp_query->have_posts() ) : $wp_query->the_post();
								$resuls++;
								$metas          = get_post_meta($post->ID);
								$titulo         = get_the_title();
								$imagen         = wp_get_attachment_image_src( $metas['_thumbnail_id'][0],"thumbnail" );
								$precio         = ($metas['custom_precio'][0]) ? $metas['custom_precio'][0] : "0";
								$dto            = $metas['custom_descuento'][0];
								$pornoche       = ( ($metas['custom_pornoche'][0]!="0") && ($metas['custom_pornoche'][0]!="") ) ? __("noche") : "";

								$esHotel        = (isset($metas['custom_oferta_en_apart']) && $metas['custom_oferta_en_apart'][0]!="-")?"nohotel":"hotel";
								$hotel_apart    = descrimina_hotel_apartamento($metas, $esHotel);

								$link           = get_custom_link($metas, $post->ID);

								$idApartWP      = (isset($metas['custom_oferta_en_apart'][0]) ? $metas['custom_oferta_en_apart'][0] : "" );

								$idAptoNeo      = get_post_meta( $idApartWP, 'custom_bookings', true );
								$arrival        = (isset($metas['custom_dtinicio'][0]) ? $metas['custom_dtinicio'][0] : "" );
								$departure      = (isset($metas['custom_dtfin'][0])? $metas['custom_dtfin'][0] : "");
								$nights         = (isset($metas['custom_noches'][0])? $metas['custom_noches'][0] : "");

								include("includes/minioferta.inc.php");
							endwhile;
						?>
					</div>
				</section> <!-- end row -->
			<?php } ?>
			<nav class="text-center margin-top-30 margin-bottom-30">
				<a class="btn bg-darkblue padding10 blanco hilite phone-block text-uppercase text-bold" href="<?php echo __("https://www.rvhotels.es/ofertas/");?>"><?php echo __("Todas las ofertas");?></a>
			</nav>
			<!--/CONTENEDOR OFERTAS-->  
		</div>
	</section><!-- lightblue -->
	<div class="catswitcher-container text-center"><?php include("includes/catswitcher.inc.php");?></div>
</section>
<script src="<?php bloginfo('template_url'); ?>/js/isotope.behavior.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/lyteShare.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/baguetteBox.min.js"></script>
<script>
  jQuery(document).ready(function($){
    // asegurarse que los links van a target blank
    $('.post-contenido a').attr('target','_blank');

    // si hay minigaleria, modificamos la estructura del widgetkit
    $('.wk-gallery-wall').removeClass().addClass('row row-small mini-gallery baguettebox');
    baguetteBox.run('.baguettebox'); // activar lightbox en galeria
    $('.mini-gallery a').addClass('col-md-2 col-xs-4 mini-gallery');
    $('.mini-gallery a img').css({'margin':'0 0 10px 0','width':'100%'});

      // boton de ver mas ofertas en el alert de oferta caducada
      $('#caduc-verotras').click(function(e){ // scroll hasta los botones inferiores
      e.preventDefault();
      $('html, body').stop().animate({
                  scrollTop: $('#otras-ofertas').offset().top - 250
              }, 400);
      });
  });
</script>
<?php get_footer(); ?>