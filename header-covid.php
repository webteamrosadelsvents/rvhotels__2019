<?php
$expira = trim(date('r', strtotime('+5 month')),"+0000")."GMT";
$phonetest ="";
if (isset($_GET['test'])) {
    $phonetest ="phonetest";
}
?>
<!DOCTYPE html>
<html lang="<?php echo ICL_LANGUAGE_CODE; ?>" class="<?php echo $phonetest;?>">
<head> 

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-53L8QMC');</script>
<!-- End Google Tag Manager -->

<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php wp_title('|','true','right'); ?><?php bloginfo('name'); ?></title>
<meta http-equiv="expires" content="<?php echo $expira;?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="google-site-verification" content="gFEGRxq36Pvy75L_zVdV-lrYoJRlwMpR_rvGeFth5SE" />
<link rel="icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon"/>
<?php wp_head(); 
$actuaLang = ICL_LANGUAGE_CODE;
?>
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/icons/favicon.ico" type="image/x-icon"/>
<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/icons/touch-icon-iphone.png" />
<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url'); ?>/icons/touch-icon-ipad.png" />
<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url'); ?>/icons/touch-icon-iphone-retina.png" />
<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url'); ?>/icons/touch-icon-ipad-retina.png" />
<link href="<?php bloginfo('template_url'); ?>/css/bootstrawesome.min.css" rel="stylesheet" media="all" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>


<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/rvhotels.min.css?v=32"/>


<!-- DATE PICKER -->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/jqueryui.custom.css">
<script type="text/javascript">
	var msg1 		= "<?php _e('Seleccione un hotel, apartamento o destino', 'rvhotels'); ?>";
	var msg2 		= "<?php _e('Selecione un afecha de entrada y de salida', 'rvhotels'); ?>";
	var lang_code 	= "<?php echo ICL_LANGUAGE_CODE; ?>";
</script>

<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<link href="<?php bloginfo('template_url'); ?>/css/ie.css" rel="stylesheet" type="text/css" />
<script src="<?php bloginfo('template_url'); ?>/js/respond.js"></script>
<![endif]-->
</head>
<body <?php body_class($class); ?> id="<?php echo ICL_LANGUAGE_CODE; ?>">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-53L8QMC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<header class="header" id="top" data-spy="affix" data-offset-top="100">
    <a href="<?php bloginfo('url'); ?>" alt="<?php bloginfo('name'); ?>" title="" class="position-absolute logo-header">
			<img src="https://www.rvhotels.es/logo-rvhotels-standalone.png" alt="RV Hotels - Grup Rosa dels Vents"/>
		</a>
    <nav class="main main-nav text-right">
            <span class="hidden-xs"><?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?></span><!--
--><button id="langbtn" class="display-inline-block hidden-xs"><span class="lingua"></span> <i class="fa fa-chevron-down text-small"></i></button><!--
--><button id="hamb" class="display-inline-block"><i class="fa fa-bars"></i></button><!--
--></nav>
</header>
<style>
body, .bg {
    background-color: #fff;
}
</style>
<?php $langos = icl_get_languages();?>
<?php include ('includes/nav-offpage.inc.php');?>

<?php /* selector idioma para desktop, desplegable */?>
<div id="lang_sel_list" class="lang_sel_list_horizontal hidden-xs">
    <ul id="desplelangs">
        <li class="icl-es">
            <?php $sel=($langos['es']['active'])?"lang_sel_sel":"lang_sel_other";?>
            <a href="<?php echo $langos['es']['url']; ?>" title="<?php echo $langos['es']['name']; ?>" class="<?php echo $sel;?>"></a>
        </li>
        <li class="icl-ca">
            <?php $sel=($langos['ca']['active'])?"lang_sel_sel":"lang_sel_other";?>
            <a href="<?php echo $langos['ca']['url']; ?>" title="<?php echo $langos['ca']['name']; ?>" class="<?php echo $sel;?>"></a>
        </li>
        <li class="icl-en">
            <?php $sel=($langos['en']['active'])?"lang_sel_sel":"lang_sel_other";?>
            <a href="<?php echo $langos['en']['url']; ?>" title="<?php echo $langos['en']['name']; ?>" class="<?php echo $sel;?>"></a>
        </li>
        <li class="icl-fr">
            <?php $sel=($langos['fr']['active'])?"lang_sel_sel":"lang_sel_other";?>
            <a href="<?php echo $langos['fr']['url']; ?>" title="<?php echo $langos['fr']['name']; ?>" class="<?php echo $sel;?>"></a>
        </li>
    </ul>
</div>
<script>
    jQuery(document).ready(function($){
        $('#langbtn').click(function(){
            $('#desplelangs').toggleClass('shown');
        });
        $('.slideshow,.home-subheading,.bg,.carousel-inner').click(function(){
            $('#desplelangs').removeClass('shown');
        });
        $('#hamb').click(function(){
            $('.offpage').addClass('offpaged');
        });
        $('.close-overlay').click(function(){
            $('.offpage').removeClass('offpaged');
        });
    });
</script>