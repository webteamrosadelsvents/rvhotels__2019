<!-- Modal Suscripción -->
<div class="modal fade" id="mSuscribe" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title"><?php _e('<strong>Suscríbete</strong> a nuestro boletín', 'rvhotels'); ?></h2>
			</div>
			<div class="modal-body">
				<p><?php _e('¿Quieres estar al día de nuestras ofertas y novedades? Apúntate a nuestra newsletter y recibirás ofertas exclusivas.', 'rvhotels'); ?></p>
				<!-- Begin MailChimp Signup Form -->
				<link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
				<div id="mc_embed_signup">
					<form action="https://rvhotels.us2.list-manage1.com/subscribe/post?u=23b5df66471f0ebe80f216080&amp;id=5b7afe5083" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" novalidate>
						<div class="mc-field-group">
							<label for="mce-EMAIL">
								<?php _e('Email', 'rvhotels'); ?> <span class="asterisk">*</span>
							</label>
							<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
						</div>
						<div class="mc-field-group">
							<label for="mce-FNAME">
								<?php _e('Nombre', 'rvhotels'); ?> <span class="asterisk">*</span>
							</label>
							<input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
						</div>
						<div class="mc-field-group">
							<label for="mce-LNAME">
								<?php _e('Apellidos', 'rvhotels'); ?> <span class="asterisk">*</span>
							</label>
							<input type="text" value="" name="LNAME" class="required" id="mce-LNAME">
						</div>
						<div id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
						</div>    
						<div style="position: absolute; left: -5000px;">
							<input type="text" name="b_23b5df66471f0ebe80f216080_5b7afe5083" value="">
						</div>
						<div class="clearfix">
							<div class="indicates-required">
								<span class="asterisk">*</span> <?php _e('indica campo requerido', 'rvhotels'); ?>
							</div>
							<input type="submit" value="<?php _e('Suscribirse a la lista', 'rvhotels'); ?>" name="subscribir" id="mc-embedded-subscribe" class="button">
						</div>
					</form>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
//$.noConflict();
	var fnames = new Array();var ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';
	try {
		var jqueryLoaded=jQuery;
		jqueryLoaded=true;
	} catch(err) {
		var jqueryLoaded=false;
	}
	var head= document.getElementsByTagName('head')[0];
	if (!jqueryLoaded) {
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = '//ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js';
		head.appendChild(script);
		if (script.readyState && script.onload!==null){
			script.onreadystatechange= function () {
				  if (this.readyState == 'complete') mce_preload_check();
			}    
		}
	}

	var err_style = '';
	try{
		err_style = mc_custom_error_style;
	} catch(e){
		err_style = '#mc_embed_signup input.mce_inline_error{border-color:#6B0505;} #mc_embed_signup div.mce_inline_error{margin: 0 0 1em 0; padding: 5px 10px; background-color:#6B0505; font-weight: bold; z-index: 1; color:#fff;}';
	}
	var head= document.getElementsByTagName('head')[0];
	var style= document.createElement('style');
	style.type= 'text/css';
	if (style.styleSheet) {
	  style.styleSheet.cssText = err_style;
	} else {
	  style.appendChild(document.createTextNode(err_style));
	}
	head.appendChild(style);
	setTimeout('mce_preload_check();', 250);

	var mce_preload_checks = 0;
	function mce_preload_check(){
		if (mce_preload_checks>40) return;
		mce_preload_checks++;
		try {
			var jqueryLoaded=jQuery;
		} catch(err) {
			setTimeout('mce_preload_check();', 250);
			return;
		}
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = 'https://downloads.mailchimp.com/js/jquery.form-n-validate.js';
		head.appendChild(script);
		try {
			var validatorLoaded=jQuery("#fake-form").validate({});
		} catch(err) {
			setTimeout('mce_preload_check();', 250);
			return;
		}
		mce_init_form();
	}
	function mce_init_form(){
		jQuery(document).ready( function(jQuery) {
		  var options = { errorClass: 'mce_inline_error', errorElement: 'div', onkeyup: function(){}, onfocusout:function(){}, onblur:function(){}  };
		  var mce_validator = jQuery("#mc-embedded-subscribe-form").validate(options);
		  jQuery("#mc-embedded-subscribe-form").unbind('submit');//remove the validator so we can get into beforeSubmit on the ajaxform, which then calls the validator
		  options = { url: 'https://rvhotels.us2.list-manage1.com/subscribe/post-json?u=23b5df66471f0ebe80f216080&id=5b7afe5083&c=?', type: 'GET', dataType: 'json', contentType: "application/json; charset=utf-8",
						beforeSubmit: function(){
						   jQuery('#mce_tmp_error_msg').remove();
							jQuery('.datefield','#mc_embed_signup').each(
								function(){
									var txt = 'filled';
									var fields = new Array();
									var i = 0;
									jQuery(':text', this).each(
										function(){
											fields[i] = this;
											i++;
										});
									jQuery(':hidden', this).each(
										function(){
											var bday = false;
											if (fields.length == 2){
												bday = true;
												fields[2] = {'value':1970};//trick birthdays into having years
											}
											if ( fields[0].value=='MM' && fields[1].value=='DD' && (fields[2].value=='YYYY' || (bday && fields[2].value==1970) ) ){
												this.value = '';
											} else if ( fields[0].value=='' && fields[1].value=='' && (fields[2].value=='' || (bday && fields[2].value==1970) ) ){
												this.value = '';
											} else {
												if (/\[day\]/.test(fields[0].name)){
													this.value = fields[1].value+'/'+fields[0].value+'/'+fields[2].value;                                            
												} else {
													this.value = fields[0].value+'/'+fields[1].value+'/'+fields[2].value;
												}
											}
										});
								});
							jQuery('.phonefield-us','#mc_embed_signup').each(
								function(){
									var fields = new Array();
									var i = 0;
									jQuery(':text', this).each(
										function(){
											fields[i] = this;
											i++;
										});
									jQuery(':hidden', this).each(
										function(){
											if ( fields[0].value.length != 3 || fields[1].value.length!=3 || fields[2].value.length!=4 ){
												this.value = '';
											} else {
												this.value = 'filled';
											}
										});
								});
							return mce_validator.form();
						}, 
						success: mce_success_cb
					};
		  jQuery('#mc-embedded-subscribe-form').ajaxForm(options);
		  /*
	 * Translated default messages for the jQuery validation plugin.
	 * Locale: ES
	 */
	jQuery.extend(jQuery.validator.messages, {
	  required: "Este campo es obligatorio.",
	  remote: "Por favor, rellena este campo.",
	  email: "Por favor, escribe una dirección de correo válida",
	  url: "Por favor, escribe una URL válida.",
	  date: "Por favor, escribe una fecha válida.",
	  dateISO: "Por favor, escribe una fecha (ISO) válida.",
	  number: "Por favor, escribe un número entero válido.",
	  digits: "Por favor, escribe sólo dígitos.",
	  creditcard: "Por favor, escribe un número de tarjeta válido.",
	  equalTo: "Por favor, escribe el mismo valor de nuevo.",
	  accept: "Por favor, escribe un valor con una extensión aceptada.",
	  maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
	  minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
	  rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
	  range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
	  max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
	  min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
	});
		  
		});
	}
	function mce_success_cb(resp){
		jQuery('#mce-success-response').hide();
		jQuery('#mce-error-response').hide();
		if (resp.result=="success"){
			jQuery('#mce-'+resp.result+'-response').show();
			jQuery('#mce-'+resp.result+'-response').html(resp.msg);
			jQuery('#mc-embedded-subscribe-form').each(function(){
				this.reset();
			});
		} else {
			var index = -1;
			var msg;
			try {
				var parts = resp.msg.split(' - ',2);
				if (parts[1]==undefined){
					msg = resp.msg;
				} else {
					i = parseInt(parts[0]);
					if (i.toString() == parts[0]){
						index = parts[0];
						msg = parts[1];
					} else {
						index = -1;
						msg = resp.msg;
					}
				}
			} catch(e){
				index = -1;
				msg = resp.msg;
			}
			try{
				if (index== -1){
					jQuery('#mce-'+resp.result+'-response').show();
					jQuery('#mce-'+resp.result+'-response').html(msg);            
				} else {
					err_id = 'mce_tmp_error_msg';
					html = '<div id="'+err_id+'" style="'+err_style+'"> '+msg+'</div>';
					
					var input_id = '#mc_embed_signup';
					var f = jQuery(input_id);
					if (ftypes[index]=='address'){
						input_id = '#mce-'+fnames[index]+'-addr1';
						f = jQuery(input_id).parent().parent().get(0);
					} else if (ftypes[index]=='date'){
						input_id = '#mce-'+fnames[index]+'-month';
						f = jQuery(input_id).parent().parent().get(0);
					} else {
						input_id = '#mce-'+fnames[index];
						f = jQuery().parent(input_id).get(0);
					}
					if (f){
						jQuery(f).append(html);
						jQuery(input_id).focus();
					} else {
						jQuery('#mce-'+resp.result+'-response').show();
						jQuery('#mce-'+resp.result+'-response').html(msg);
					}
				}
			} catch(e){
				jQuery('#mce-'+resp.result+'-response').show();
				jQuery('#mce-'+resp.result+'-response').html(msg);
			}
		}
	}
</script>