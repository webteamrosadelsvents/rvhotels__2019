    function decorateBookingEngineLinks()
    {
        /* EACH NEO LINKS */
        var neobookingsMark = 'bookings.rvhotels';
        var neobookingsThumbsMark = 'thumbs';
        var links = document.getElementsByTagName('a');
        var href;

        for (var i = 0; i < links.length; i++)
        {
            /* CHECK ENGINE LINKS */
            if(links[i].getAttribute('href'))
            {
                href = links[i].getAttribute('href');

                if (href.indexOf(neobookingsMark) != -1 && href.indexOf(neobookingsThumbsMark) == -1)
                {
                    /* SET LISTENERS */
                    addListener(links[i], 'mousedown', decorateMe);
                    addListener(links[i], 'keydown', decorateMe);
                }
            }
        }

        /* DECORATE LINKS */
        function decorateMe(event)
        {
            event = event || window.event;
            var target = event.target || event.srcElement;

            /* CHECK IF TARGET IS IMAGE AND OVERWRITE */
            if (target instanceof HTMLImageElement || target instanceof HTMLDivElement)
            {
                target = target.parentNode;
            }

            if (target && target.href)
            {   
                ga('linker:decorate', target);
            }
        }

        /* CROSS BROWSER LISTENERS */
        function addListener(element, type, callback)
        {
            if (element.addEventListener)
            {
                element.addEventListener(type, callback);   
            } 
            else if (element.attachEvent) 
            {
                element.attachEvent('on' + type, callback);
            }
        }
    }

    function decorateBookingEngineForms()
    {
        /* EACH FORMS */
        var neobookingsMark = 'bookings.rvhotels';
        var forms = document.getElementsByTagName('form');
        var action;

        for (var i=0; i<forms.length; i++)
        {
            /* SET ENGINE FORMS */
            if(forms[i].getAttribute('action'))
            {
                action = forms[i].getAttribute('action');
                if (action.indexOf(neobookingsMark)!=-1)
                {
                    /* SET LISTENERS */
                    addListener(forms[i], 'submit', decorateForm);
                }
            }
        }

        /* DECORATE FORMS */
        function decorateForm(event)
        {
            event = event || window.event;
            var target = event.target || event.srcElement;
        
            if (target && target.action)
            {
                ga('linker:decorate', target);
            }
        }

        /* CROSS BROWSER LISTENERS */
        function addListener(element, type, callback)
        {
            if (element.addEventListener)
            {
                element.addEventListener(type, callback);
            }
            else if (element.attachEvent)
            {
                element.attachEvent('on' + type, callback);
            }
        }
    }

    var array_ga = new Array();

    // parametros,          IDMotor / UA Analitics / Adwords 
    // hoteles
    array_ga[0]  = new Array(-1, "UA-1651542-16", "1058507870");                        // rvhotels
    array_ga[1]  = new Array("ametlla_mar", "UA-1651542-4", "1058507870");              // Ametlla
    array_ga[2]  = new Array("uW9ZG0$rN9GYUeoyru6HCA", "UA-1651542-46", "1058507870");  // Ses arrels BOOKINGS
//  array_ga[2]  = new Array("sesarrelsdirect", "UA-1651542-46", "1058507870");         // OLD Ses arrels THEBOOKIGBUTTON
//  array_ga[3]  = new Array("palaulomiradordirect", "UA-1651542-45", "1058507870");    // OLD Palau lo mirador THEBOOKIGBUTTON 
    array_ga[3]  = new Array("PuBUcheCderU257gChVsCQ", "UA-1651542-45", "1058507870");  // Palau lo mirador
    array_ga[4]  = new Array("el_condes", "UA-1651542-76", "1058507870");               // Hotel condes
//  array_ga[30] = new Array("hotelcolldirect", "UA-1651542-78", "1058507870");         // OLD Hotel Coll THEBOOKIGBUTTON (GR92)
    array_ga[35] = new Array("yfRd3N7ZTegDF8GQDv3Acg", "UA-1651542-78", "1058507870");  // Hotel GR92
    array_ga[36] = new Array("XNVTCpSQgfuZflMqdx4crQ", "UA-1651542-79", "1058507870");  // Hotel TUCA
    array_ga[37] = new Array("5hTbF9la6YChNpzlNXVhdg", "UA-107596064-1", "1058507870"); // Hotel ORRI
    array_ga[38] = new Array("9dJUYhOduTmRnuxypTJrTw", "UA-112881849-1", "1058507870"); // Hotel NIEVES MAR
    array_ga[39] = new Array("1635", "UA-1651542-80", "1058507870"); // Hotel Sea Club Menorca
    array_ga[40] = new Array("dSa8BOwU4hSLoD%24LMjsmdA", "UA-1651542-80", "1058507870"); // Hotel NAUTIC PARK
    array_ga[41] = new Array("PVS5%24aaAmBkCflVpGn456Q", "UA-1651542-80", "1058507870"); // Hotel Golf Costa Brava

    

    // apartamentos
    array_ga[5]  = new Array("09TBW0yNIOlq6D5MEtC5:g", "UA-1651542-16", "1058507870");  // Port Canigó - Roses
    array_ga[6]  = new Array("rCbMLYXxqKeBlEU6kncM2A", "UA-1651542-16", "1058507870");  // Tropik - Estartit
    array_ga[7]  = new Array("vjn48pOF0POqu1hrjKPwIw", "UA-1651542-16", "1058507870");  // Club Torrevella - Estartit
    array_ga[32] = new Array("vjn48pOF0POqu1hrjKPwIw", "UA-1651542-16", "1058507870"); // Club Torrevella Heemsteede
    array_ga[33] = new Array("vjn48pOF0POqu1hrjKPwIw", "UA-1651542-16", "1058507870"); // Club Torrevella CASA Heemsteede
    //array_ga[34] = new Array("vjn48pOF0POqu1hrjKPwIw", "UA-1651542-16", "1058507870"); // Club Torrevella CASA Breda
    array_ga[8]  = new Array("eYVu60xZ7lnynKuvwjikpQ", "UA-1651542-16", "1058507870");  // Els Salats - Estartit
    array_ga[9]  = new Array("DUnSYoEAPqrUTSFvJZsedg", "UA-1651542-16", "1058507870");  // La Pineda - Estartit
    array_ga[10] = new Array("ayxcCg6qLL9bYwPk3NlwwA", "UA-1651542-16", "1058507870");  // Dúplex Bonsol - Estartit
    array_ga[11] = new Array("vEJv3WZAtPPF5pjQd2bVEg", "UA-1651542-16", "1058507870");  // Del Sol - Estartit
    array_ga[12] = new Array("K6D5t2t2Dt7w4cLc5Stw8A", "UA-1651542-16", "1058507870");  // Villas Piscis - Estartit
    array_ga[13] = new Array("$icLZVsQq26x:YOanTKdpg", "UA-1651542-16", "1058507870");  // Treumal Park - Platja d'Aro
    array_ga[14] = new Array("45kejIr14Zmpk4ZaGjRD7w", "UA-1651542-16", "1058507870");  // Can Torrents - Segur de Calafell/Cunit
    array_ga[15] = new Array("cUNzzHldmGNR:mnbm70Tbg", "UA-1651542-16", "1058507870");  // Alcoceber - Al Andalus
    array_ga[16] = new Array("KDVIld90cYJjkt6AH4HECQ", "UA-1651542-16", "1058507870");  // Estartit Confort - Estartit
    array_ga[17] = new Array("sqi12sOP6J$KvD24j:93XQ", "UA-1651542-16", "1058507870");  // RV Europa Lotus - Blanes
    array_ga[18] = new Array("hCtZgc4dcaFgnqP70Le$ag", "UA-1651542-16", "1058507870");  // RV Ses Illes - Blanes
    array_ga[19] = new Array("6R45s5KF1L80zScnin:H7w", "UA-1651542-16", "1058507870");  // RV Villa de Madrid - Blanes
    array_ga[28] = new Array("QhkTGk3EnoAYNXRPTOorUQ", "UA-1651542-16", "1058507870");  // PROVENZAL
    array_ga[29] = new Array("AapYTkQi6OwLy7GoTfXM$w", "UA-1651542-16", "1058507870");  // QUIJOTE
    array_ga[30] = new Array("GWIKKmYGVupI:8hjrsU1wg", "UA-1651542-16", "1058507870");  // BENELUX NOVEDAD 2017

    // destinos rv apts
    array_ga[20] = new Array("alce", "UA-1651542-16", "1058507870");                    // rv apts alcoceber
    array_ga[21] = new Array("rial", "UA-1651542-16", "1058507870");                    // rv apts rialp
    array_ga[22] = new Array("amet", "UA-1651542-16", "1058507870");                    // rv apts ametlla
    array_ga[23] = new Array("blan", "UA-1651542-16", "1058507870");                    // rv apts blanes
    array_ga[24] = new Array("lest", "UA-1651542-16", "1058507870");                    // rv apts lestartit
    array_ga[25] = new Array("pltj", "UA-1651542-16", "1058507870");                    // rv apts platjadaro
    array_ga[26] = new Array("vall", "UA-1651542-16", "1058507870");                    // rv apts vallobrega
    array_ga[27] = new Array("torr", "UA-1651542-16", "1058507870");                    // rv apts torroella
    array_ga[31] = new Array("esca", "UA-1651542-78", "1058507870");                    // rv apts lescala - costabrava - generico para provezal lescala


    var release = "+0d";
    var today = new Date();
    var tomorrow = new Date();
    jQuery(document).ready(function($) {

        // tabs buscador dispo home

        function tabsBooking() {
            $('.tab-hoteles, .tab-aptos').removeClass('activa');
            $('.bookingError').addClass('hidden');
        }


        $('#tab-hoteles').click(function(){
            //crearDatePickerArrival(window.release); // default hoteles
            tabsBooking();
            $(this).addClass('activa');
            $('#combo-hoteles').removeClass('hidden');
            $('#combo-aptos').addClass('hidden');
        });

        $('#tab-aptos').click(function(){
            //crearDatePickerArrival("+0d"); // default hoteles
            tabsBooking();
            $(this).addClass('activa');
            $('#combo-aptos').removeClass('hidden');
            $('#combo-hoteles').addClass('hidden');
        });

        // calendar ui checkin checkout
        window.tomorrow.setDate(window.tomorrow.getDate() + 1);

        $( "#arrival:not([type='hidden'])" ).datepicker({
            minDate: window.release,
            maxDate: "+1Y",
            showOn: "focus",
            onClose: clearEndDate
        });

        $("#arrival:not([type='hidden'])").datepicker("refresh");

        //usar solo cuando haya cambios de release
        function crearDatePickerArrival (mindates) { //"+1d"
            $( "#arrival:not([type='hidden'])" ).datepicker("option", "minDate", mindates)
                           .datepicker("setDate", (mindates == window.release ? window.tomorrow : window.today))
                           .datepicker("refresh");

            console.log('creadoarrival con min dates' + mindates);
        }

        //crearDatePickerArrival (release); // default release 0

        $( "#arrival:not([type='hidden'])" ).datepicker("option", "minDate", "+0d")
                                            .datepicker("setDate", window.today)
                                            .datepicker("refresh");

        //usar solo cuando haya cambios de release                    
        /*
        $( ".buscador-aptos-container #arrival,\
            .buscadordispo-apart #arrival" ).datepicker("option", "minDate", "+0d")
                                            .datepicker("setDate", window.today)
                                            .datepicker("refresh");
        */

        

        $( "#departure:not([type='hidden'])" ).datepicker({
            showOn: "focus", 
            beforeShow: setMinDateForEndDate
        });

/*        var today = new Date();
        var tomorrow = new Date();

        tomorrow.setDate(tomorrow.getDate() + 1);
        $('#arrival').datepicker('setDate', tomorrow);
        $("#arrival").datepicker("refresh");

        $( ".buscador-aptos-container #arrival,\
            .buscadordispo-apart #arrival" ).datepicker('setDate', today);
        $( ".buscador-aptos-container #arrival,\
            .buscadordispo-apart #arrival" ).datepicker("refresh");

*/

        tomorrow.setDate(tomorrow.getDate() + 1);
        $("#departure:not([type='hidden'])").datepicker('setDate', tomorrow);
        $("#departure:not([type='hidden'])").datepicker("refresh");

        var checkInDate = $('#arrival');
        var checkOutDate = $('#departure');

        function setMinDateForEndDate() {
            var d = checkInDate.datepicker('getDate', '+1d');
            d.setDate(d.getDate()+1);
            if (d) return { minDate: d }
        }

        function clearEndDate(dateText, inst) {
            tomorrow = new Date(checkInDate.datepicker('getDate'));
            tomorrow.setDate(tomorrow.getDate() + 1);
            checkOutDate.datepicker("setDate", tomorrow);
        }

        $(document).on("change", ".apt", function () {
           $(".hot").val(-1);
           $(".dest").val(-1);
        });

        $(document).on("change", ".hot", function () {
           $(".apt").val(-1);
           $(".dest").val(-1);
        });

        $(document).on("change", ".dest", function () {
           $(".apt").val(-1);
           $(".hot").val(-1);
        });


        $(document).on("click", "#form-buscador .btn", function (event) {
            
            $hotel =$(".hot").val();  //option:selected
            $apartamento =$(".apt").val();  //option:selected
                console.log($apartamento);
            $destino =$(".dest").val();  //option:selected
            $id = ($hotel!=-1?$hotel:($apartamento!=-1?$apartamento:($destino!=-1?$destino:-1)));

            $arrival=$("#arrival").val();
            $departure=$("#departure").val();

            if($hotel==-1 && $apartamento==-1 && $destino==-1){
                $('.error-zona').removeClass('hidden');
                return;
            }
            if($arrival=="" || $departure==""){
                $('.error-fecha').removeClass('hidden');
                return;
            }

            $base_url = "https://bookings.rvhotels.es/";

            // isMiniBuscador, el buscador individual de dispo en la ficha de apartamento
            // Comprobamos que no sea el caso ya que este se alimenta solo

            if (typeof isMiniBuscador == 'undefined') {

                if ($hotel!=-1)
                    $url=$base_url + lang_code + "/step-1?id=" + array_ga[$hotel][0];

                if ($apartamento!=-1)
                    $url=$base_url  + lang_code + "/step-1?id=" + array_ga[$apartamento][0];
                
                if ($destino!=-1)
                    $url=$base_url + lang_code + "/?zone="+array_ga[$destino][0];

                // LOS 3 INMUEBLES QUE SE RESERVAN VIA BOOKING DOT COM

                //if (($apartamento=="32") || ($apartamento=="33") || ($apartamento=="34")) {
                if (($apartamento=="32") || ($apartamento=="33")) {

                    var BookingComCheckInDate = $arrival.split("/"); // 24/03/2017" DD MM YY
                    var BookingComCheckOutDate = $departure.split("/"); // 24/03/2017" DD MM YY
                    var BookingComDates = "checkin_monthday="+
                                    BookingComCheckInDate[0]+
                                    "&checkin_year_month="+
                                    BookingComCheckInDate[2]+"-"+BookingComCheckInDate[1]+
                                    "&checkout_monthday="+
                                    BookingComCheckOutDate[0]+
                                    "&checkout_year_month="+
                                    BookingComCheckOutDate[2]+"-"+BookingComCheckOutDate[1];
                }

                if ($apartamento=="32") { // Club Torrevella heemsteede - booking.com
                    $url="https://www.booking.com/hotel/es/apartamentos-heemstede.html"
                    //$url="https://www.booking.com/hotel/es/apartamentos-heemstede.html?aid=330843;"+BookingComDates+";lang="+lang_code;

                }

                if ($apartamento=="33") { // Club Torrevella CASA Heemsteede - booking.com - FUNCIONA
                    $url="https://www.booking.com/hotel/es/casa-heemstede.html"
                    //$url="https://www.booking.com/hotel/es/casa-heemstede.html?aid=330843;"+BookingComDates+";lang="+lang_code;
                }

                //if ($apartamento=="34") { // Club Torrevella CASA Heemsteede - booking.com
                //  $url="http://www.booking.com/hotel/es/casa-breda.html?aid=330843;"+BookingComDates+";lang="+lang_code;
                //}
                console.log($url);
            } else {
                $url=$base_url + lang_code + "/step-1?id=" + bookingCode;
                for (var i = 0; i <= array_ga.length; i++) {
                    if (array_ga[i][0]==bookingCode) {
                        $id = i;
                        break;
                    }
                };
            }

            // por id
            // jQuery("#analytics").val(array_ga[$id][1]);
            // jQuery("#adwords").val(array_ga[$id][2]);

            // por name
            $("[name=analytics]").val(array_ga[$id][1]);
            $("[name=adwords]").val(array_ga[$id][2]);

            $("#form-buscador").attr("action", $url);
            $("#form-buscador").attr('method','post');
            //alert(jQuery(".hot").val());
            //           return false;
                            
            //              "el_condes"                  "rial"
            /*if(($(".hot").val()=="4" || $(".dest").val()=="21")){
                gateway="";
                switch(jQuery(".hot").val()){
                    case "4": // "el_condes"
                        gateway = jQuery("#gateway_condes").val();
                        break;
                }
                $("#gateway").val(gateway);
                $("#form-buscador").attr("action","https://www.faciltef.com/acinet2/ShoppingCenter/Search");
                $("#checkin").val($("#arrival").val());
                $("#checkout").val($("#departure").val());

            }*/



            //            "ametlla_mar"                "nieves mar"					"tuca"							"condes"                        "orri"                          "palau"                     "gr92"                     "sesarrels"                    "menorca"             "nautic"                "hotelgolf" 
            if((jQuery(".hot").val()=="1") || (jQuery(".hot").val()=="38") || (jQuery(".hot").val()=="36") || (jQuery(".hot").val()=="4") || (jQuery(".hot").val()=="37") || ($(".hot").val()=="3") || ($(".hot").val()=="35") /*|| ($(".hot").val()=="2")*/ || ($(".hot").val()=="39") || ($(".hot").val()=="40") || ($(".hot").val()=="41")){ // AÑADIDOS PALAU, GR92, SEACLUBMO Y SESARRELS A BOOKINGS
                gateway="";
                switch(jQuery(".hot").val()){
                    case "1": // "ametlla_mar"
                        gateway = jQuery("#gateway_ametlla").val();
                        break;
                    case "4": // "condes"
                        gateway = jQuery("#gateway_condes").val();
                        break;
                    case "38": // "nieves mar"
                        gateway = jQuery("#gateway_nievesmar").val();
                        break;
                    case "36": // "tuca"
                        gateway = jQuery("#gateway_tuca").val();
                        break;
                    case "37": // "orri"
                        gateway = jQuery("#gateway_orri").val();
                        break;
                    case "3": // "palau"
                        gateway = jQuery("#gateway_palau").val();
                        break;                        
                    case "35": // "gr92"
                        gateway = jQuery("#gateway_gr92").val();
                        break;
                    case "2": // "sesarrels"
                        gateway = jQuery("#gateway_sesarrels").val();
                        break;         
                    case "39": // "menorca"
                        gateway = jQuery("#gateway_menorca").val();
                        break;   
                    case "40": // "nautic"
                        gateway = jQuery("#gateway_nautic").val();
                        break;  
                    case "41": // "hotelgolf"
                        gateway = jQuery("#gateway_hotelgolf").val();
                        break;                                                                                                                       
                }
                $("#gateway").val(gateway);
                $("#form-buscador").attr("action","https://bookings.rvhotels.es/"+lang_code+"/step-1?id="+gateway);
                $("#checkin").val($("#arrival").val());
                $("#checkout").val($("#departure").val());
            }

            // BOOKINGBUTTON SITEMINDER
            //      "ses arrels"
            //if( ($(".hot").val()=="2")){
            //               ses arrels,                palau                   hotel gr92                 
            // if( ($(".hot").val()=="2") || ($(".hot").val()=="3") || ($(".hot").val()=="30")){ // OLD CODE CON THEBOOKINGBUTTON
                //console.log('Hotel selected: '+$(".hot").val());
                //$url = "https://app.thebookingbutton.com/properties/"+array_ga[$id][0]+"?locale="+lang_code+"&check_in_date="+$("#arrival").val()+"&check_out_date="+$("#departure").val();
                //window.location.href = $url; // IMPORTANTE no abrir este metodo en ventana nueva, se consideraria POPUP y bloqueo de navegador
            //}

            // NO TENGO MUY CLARO EL POR QUE DE ESTE BLOQUE
            // PARECE QUE SE QUIERA QUE SI SE BUSCA POR TORROELLA, SE MUESTRE EL PALAU SÍ O SÍ
            // (EN LUGAR DEL GR92, O AMBOS, ESTO ULTIMO, NO SE PUEDE)
            // DE TODOS MODOS ES BOOKINGBUTTON Y SOLO NEOBOOKINGS PERMITE BUSQUEDA DE DISPO MULTIPLE POR ZONA
            // LO DEJO COMO ESTA
            //
            // if( ($(".hot").val()=="3") || ($(".dest").val()=="27")){ //COMENTO PALAU LO MIRADOR
            if( ($(".dest").val()=="27")){                
                $("#form-buscador").attr('method','get');

                $("#check_in_date").val($("#arrival").val());
                $("#check_out_date").val($("#departure").val());
                $("#form-buscador").attr("action","https://bookings.rvhotels.es/ca/step-1?id=PuBUcheCderU257gChVsCQ&analytics=UA-1651542-45");
                //    $("#form-buscador").attr("action","https://app.thebookingbutton.com/properties/palaulomiradordirect?locale="+lang_code); // ANTIGUA URL MOTOR
                //    https://app.thebookingbutton.com/properties/palaulomiradordirect?locale=es&check_in_date=30%2F01%2F2016&check_out_date=31%2F01%2F2016
            }

            // POR ULTIMO SI HOTEL NO ES NINGUNO DE BOOKINGBUTTON, HAZ UN SUBMIT NORMAL
            // IMPORTANTE HAY QUE PONER TODOS DE NUEVO
            //        ses arrels                    palau                   hotel gr92
            // if( ($(".hot").val()!="2") && ($(".hot").val()!="3") && ($(".hot").val()!="30")){ // OLD CODE PALAU Y GR92 PARA THEBOOKINGBUTTON
            //      ses arrels
            //if( ($(".hot").val()!="2")){                
                $("#form-buscador").submit();
            //}
            //            return false;
        });
    });