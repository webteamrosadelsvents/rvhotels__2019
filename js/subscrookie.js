var CookieSubscribe = {
	duracionDias: 365,
	anios: 1,

	setearCookie: function _setearCookie( valor ) {
		jQuery.cookie( 'cookie_subscribe', 1, { expires: CookieSubscribe.duracionDias * CookieSubscribe.anios, path: '/' } );
	},


	leerCookie: function _leerCookie() {
		laCookie = parseInt( jQuery.cookie( 'cookie_subscribe' ) );
		return laCookie;
	},


	inicio: function _inicio( setup ) {
		laCookie = this.leerCookie();
		if(isNaN(laCookie)){
			this.setearCookie();
		}
	}


};