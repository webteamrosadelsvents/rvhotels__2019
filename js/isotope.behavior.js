jQuery(function($){

    var $grid;

    var filtros="";

    function initmasonry() {
      $grid = $('.grid').isotope({
        getSortData: {
                default: '[data-count]',
                euros: '.euros parseInt'
            },
        sortBy: 'default',
        sortAscending: true,
        filter: filtros,
        "itemSelector": ".minioferta",
        "gutter": 0,
        "columnWidth": ".grid-sizer",
        "percentPosition": false,
        "transitionDuration": '0.8s'
      });

      $('.lazy-img:not([src])').lazyload({
            failure_limit : 6,
            effect : "fadeIn"
      });

    }

    function masonry_update() {     
        $('.grid').imagesLoaded(function(){
            $('.grid').isotope();
        });
    }

    function ordenarPrecio() {     
        $('.grid').isotope({
            sortBy: 'euros',
            sortAscending: true
        });
    }

    $('.fa-search').click(function() {
        ordenarPrecio();
        console.log('clica');
    });

    initmasonry();

    $('.lazy-img').load(function() {
        masonry_update();
    });

    window.onscroll = function(ev) {
        if ( (window.innerHeight + window.scrollY) >= (document.body.scrollHeight - 450) ) {
            jQuery('.grid').isotope();
        }
    }
    
});