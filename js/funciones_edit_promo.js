jQuery( document ).ready(function($) {

	function vrfy_chekos(){
		if( $("#custom_show").is(':checked') ){
			$("#custom_feat").removeAttr("disabled");	
		}else{
			$("#custom_feat").attr("disabled", "disabled");
			$("#custom_feat").attr('checked', false);			
		}
		if($("#custom_oferta_en_apart").val()!="-"){
			$("#custom_feat_dest").attr('checked', false);	
			$("#custom_feat_dest").attr("disabled", "disabled");
		}
		//alert($("#custom_oferta_en_apart").val()+" - "+$("#custom_oferta_en_hotel").val());
	}

	$(".rwmb-select").change(function (){
		switch (this.id){
			case "custom_oferta_en_apart":
				$("#custom_oferta_en_hotel").val("-");
				$("#custom_feat_dest").attr('checked', false);	
				$("#custom_feat_dest").attr("disabled", "disabled");
				break;
			case "custom_oferta_en_hotel":
				$("#custom_oferta_en_apart").val("-");
				$("#custom_feat_dest").removeAttr("disabled");		
				break;
		}
	});

	$("#custom_show").change(function (){
		vrfy_chekos();
	});
	vrfy_chekos();
});