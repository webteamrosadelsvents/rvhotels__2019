<?php
/*
Template Name: Contacto
*/
?>
<?php get_header(); ?>
<?php if (isset($_GET['edificio'])) {
	$edificio = $_GET['edificio'];
	$edificio = "Ref: ".urldecode($edificio);
}
?>
<style>
	span.wpcf7-not-valid-tip {
	    color: #fff;
	    font-size: 11px;
	    font-weight: normal;
	    display: block;
	    text-transform: uppercase;
	    text-align: center;
	    background: tomato;
	    line-height: 12px;
	    padding: 3px 2px;
	    margin: 0px -26px;
	}
	div.wpcf7-validation-errors {
		border:0;
		color: #fff;
		background: tomato;
		font-size: 13px;
		line-height:125%;
		text-align: center;
		font-weight:bold;
	}
	#asunto {
		filter:brightness(.85)contrast(1.2);
		padding-left:10px!important;
		text-overflow:ellipsis;
		cursor:pointer;
	}
	.wpcf7-validates-as-required {
		background-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAJCAYAAAALpr0TAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAHtJREFUeNpi/P//PwMamA/EH4C4EFmQhQETKGARY2DCIgky7QC6Rkag1SDGfSB+AMQbkDSBNDhA+YwsUIFEIJaHKgiAik0A4gVA/BCIBRhAnkHC/UC8H4jvA3E9shyyooT/EBCAxsZQuB5qEox/HyqGoRCEBXCwGQACDAAjW5O2iTXK2AAAAABJRU5ErkJggg==')!important;background-position:95% center!important;background-repeat:no-repeat!important;
	}
	:focus::-webkit-input-placeholder {
	color:    rgba(255,255,255,.3);
	}
	:focus:-moz-placeholder {
	color:    rgba(255,255,255,.3);
	}
	:focus:-ms-input-placeholder {
	color:    rgba(255,255,255,.3);
	}

	@media(min-width:980px) {
		.modal-body {margin-left:30px;}
		.border-left-sep:before {
			content:"";
			border-left:2px solid rgba(0,0,0,0.1);
			position:absolute;
			left:-30px;
			min-height:100%;
		}
	}

	#modal-asunto ul {
		list-style:none;
		padding-left:0;
	}

	#modal-asunto li a:hover {
		text-decoration:underline!important;
	}

	#modal-asunto h6 {
		text-transform:uppercase;
	}

	#modal-asunto label {
		margin:0!important;
		cursor:pointer;
		padding:9px 0!important;
		display:inline-block!important;
	}

</style>
<section class="bg bghotels2016">
<div class="container clearfix">

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php the_content();?>

    <?php endwhile; ?>
    <?php else : ?>
 
        <h2><?php _e('No encontrado', 'rvhotels'); ?></h2>
 
    <?php endif; ?>
       
</div>
</div><!--.container-->
</section>
<script>
jQuery(document).ready(function($){
	$('#asunto').val('<?php echo $edificio;?>');

	$('#asunto').attr({'readonly':'readonly','title':'<?php echo __("ASUNTO");?>'});

	$('#asunto').click(function(){
		$('#modal-asunto').modal();
	});

	$('#modal-asunto a').click(function(e){
		e.preventDefault();
		var nuevoasunto=$(this).text();
		$('#asunto').val('Ref: '+nuevoasunto);
		$('#modal-asunto').modal('hide');
	});
});
</script>

	<div class="modal fade" id="modal-asunto">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content ">
	      <div class="modal-header">
	      	<button type="button" class="fa fa-times close-modal blanco bg-darkblue position-absolute" data-dismiss="modal" aria-label="Close"></button>
	        <h5 class="modal-title"><?php echo __("SELECCIONE EL MOTIVO DE SU CONSULTA");?></h5>
	        <p><?php echo __("Queremos ofrecerle el mejor servicio posible, y para ello es necesario que escoja uno de los siguientes temas en relación a sus dudas.");?></p>
	        
	      </div>
	      <div class="modal-body">
	        <div class="row row-small">
	        	<div class="col-md-4 col-xs-12">
					<h6 class="text-bold"><?php echo __("Hoteles");?></h6>
					<ul class="sf-checkbox-wrapper">
						<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Hotels Ametlla Mar</label></a></li>
						<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Hotels Condes del Pallars</label></a></li>
						<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Hotels GR 92</label></a></li>
						<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Hotels Nieves Mar</label></a></li>						
						<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Hotels Palau lo Mirador</label></a></li>
						<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Hotels Ses Arrels</label></a></li>
						<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Hotels TUCA</label></a></li>						
						<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Hotels ORRI</label></a></li>						
						<br>
						<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span><?php echo __("Consulta sobre hoteles en general");?></label></a></li>
						
					</ul>
	        	</div>

	        	<div class="col-md-4 col-xs-6">
	        		<div class="border-left-sep position-relative">
	        		<h6 class="text-bold"><?php echo __("Apartamentos");?></h6>
	        		<ul class="sf-checkbox-wrapper">
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Benelux</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Club Torrevella</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Club Torrevella - Casa Breda</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Club Torrevella - Casa Heemsteede</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Club Torrevella - Heemsteede</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Del Sol</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Dúplex Bonsol</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Els Salats</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Estartit Confort</label></a></li>
	        			<br>
	        			<li class="nowrap"><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span><?php echo __("Consulta sobre apartamentos en general");?></label></a></li>
					</ul>
					</div>
	        	</div>
	        	<div class="col-md-4 col-xs-6">
	        		<h6 class="">&nbsp;</h6>
	        		<ul class="sf-checkbox-wrapper">
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV La Pineda</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Lotus</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Provenzal</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Quijote</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Ses Illes</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Treumal Park</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Tropik</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Villa de Madrid</label></a></li>
	        			<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span>RV Villas Piscis</label></a></li>
					</ul>
	        	</div>
	        	<div class="col-md-12 col-xs-12 margin-top-20">
	        		<hr style="min-width:100%">
	        	</div>
	        	

	        	<div class="col-md-4 col-xs-12">
	        	
	        	<h6 class="text-bold"><?php echo __("Otros");?>...</h6>
	        	<ul class="sf-checkbox-wrapper">
	        		<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span><?php echo __("Funcionamiento/soporte página web");?></label></a></li>
	        		<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span><?php echo __("Problemas con proceso de reserva");?></label></a></li>
	        	</ul>
	        	</div>

	        	<div class="col-md-4 col-xs-12">
	        	
	        	<h6 class="hidden-xs">&nbsp;</h6>
	        	<ul class="sf-checkbox-wrapper">
	        		<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span><?php echo __("OFERTAS RV Hotels");?></label></a></li>
	        	</ul>
	        	</div>

	        	<div class="col-md-4 col-xs-12">

	        	<h6 class="hidden-xs">&nbsp;</h6>
	        	<ul class="sf-checkbox-wrapper">
	        		<li><a href="#"><label><input type="checkbox" class="hidden"><span class="customcheck"></span><?php echo __("OTROS motivos de consulta");?></label></a></li>
	        	</ul>
	        	</div>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>


<?php get_footer(); ?>