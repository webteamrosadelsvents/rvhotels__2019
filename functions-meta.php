<?php

/*= CUSTOM POSTS
-----------------*/
// -- HOTEL -- //
function cp_hotel() {
	$labels = array(
		'name'               => __( 'Hoteles'),
		'singular_name'      => __( 'Hotel'),
		'add_new'            => __( 'Nuevo Hotel'),
		'add_new_item'       => __( 'Nuevo Hotel' ),
		'edit_item'          => __( 'Edita Hotel' ),
		'new_item'           => __( 'Nuevo Hotel' ),
		'all_items'          => __( 'Todos los hoteles' ),
		'view_item'          => __( 'Ver Hotel' ),
		'search_items'       => __( 'Buscar Hoteles' ),
		'not_found'          => __( 'No se encontraron hoteles' ),
		'not_found_in_trash' => __( 'No se encontraron hoteles en la papelera' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Hoteles'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Nuestros hoteles',
		'public'        => true,
		'menu_position' => 3,
		'supports'      => array( 'title', 'thumbnail','excerpt','custom-fields'),
		'has_archive'   => true,
		'show_in_rest' => true
	);
	register_post_type( 'hotel', $args );	
}
add_action( 'init', 'cp_hotel' );


// -- APARTAMENTO -- //
function cp_apartamento() {
	$labels = array(
		'name'               => __( 'Apartamentos'),
		'singular_name'      => __( 'Apartamento'),
		'add_new'            => __( 'Nuevo Apartamento'),
		'add_new_item'       => __( 'Nuevo Apartamento' ),
		'edit_item'          => __( 'Edita Apartamento' ),
		'new_item'           => __( 'Nuevo Apartamento' ),
		'all_items'          => __( 'Todos los apart.' ),
		'view_item'          => __( 'Ver Apartamento' ),
		'search_items'       => __( 'Buscar Apartamentos' ),
		'not_found'          => __( 'No se encontraron apartamentos' ),
		'not_found_in_trash' => __( 'No se encontraron apartamentos en la papelera' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Apartamentos'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Nuestros apartamentos',
		'public'        => true,
		'menu_position' => 4,
//		'supports'      => array( 'title', 'thumbnail','excerpt','custom-fields'),
		'supports'      => array( 'title', 'thumbnail'),
//		'rewrite' => array( 'slug' => 'nuestros-apartamentos', 'with_front' => true ),
		'has_archive'   => true,
		'show_in_rest' => true
	);
	register_post_type( 'apartamento', $args );	
}
add_action( 'init', 'cp_apartamento' );



// -- PROMOCION -- //
function cp_promocion() {
	$labels = array(
		'name'               => __( 'Promociones'),
		'singular_name'      => __( 'Promocion'),
		'add_new'            => __( 'Nuevo Promocion'),
		'add_new_item'       => __( 'Nuevo Promocion' ),
		'edit_item'          => __( 'Edita Promocion' ),
		'new_item'           => __( 'Nueva Promocion' ),
		'all_items'          => __( 'Todas las Promociones' ),
		'view_item'          => __( 'Ver Promocion' ),
		'search_items'       => __( 'Buscar Promociones' ),
		'not_found'          => __( 'No se encontraron promociones' ),
		'not_found_in_trash' => __( 'No se encontraron promociones en la papelera' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Promociones'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Nuestras Promociones',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt'),
		'has_archive'   => true,
		'show_in_rest' => true
	);
	register_post_type( 'promo', $args );
	flush_rewrite_rules();

	$labels = array(
		'name' => 'Categoria promocion',
		'singular_name' => 'Categoria promocion',
		'menu_name' => 'Categoria promocion',
		'all_items' => 'todas',
		'edit_item' => 'Editar',
		'view_item' => 'Ver',
		'update_item' => 'Actualizar',
		'add_new_item' => 'Añadir',
		'new_item_name' => 'Nueva',
		'parent_item' => 'Categoría superior',
		'parent_item_colon' =>  'Categoría superior',
		'search_items' => 'Buscar',
		'popular_items' => 'Populares',
		'separate_items_with_commas' => 'Separa las etiquetas con comas.',
		'add_or_remove_items' => 'Añadir o quitar',
		'choose_from_most_used' => 'Elige',
		'not_found' => 'No se han encontrado',
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => false,
		'show_admin_column' => true,
		'hierarchical' 	=> true,
		'_builtin'		=> true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'promos/%promo_categories%'),
		'has_archive' => true,
		'sort' => false,
		'show_in_rest' => true
	);
	register_taxonomy( 'promo_categories', array( 'promo' ) , $args );
	flush_rewrite_rules();

}
add_action( 'init', 'cp_promocion' );

// cosa rara de raul, para las urls de las promos, no se aun como solucionarlo...
function niapa_solution($wp_rewrite) {
    $rules = array();
    // get all custom taxonomies
    $taxonomies = get_taxonomies(array('_builtin' => false), 'objects');
    $wp_rewrite->extra_permastructs['promo_categories']['struct'] = "promos/%promo_categories%";
    // get all custom post types
    $post_types = get_post_types(array('public' => true, '_builtin' => false), 'objects');
}
add_filter('generate_rewrite_rules', 'niapa_solution');




function promo_categorie_add_new_meta_field($term) {
	$t_id = $term->term_id; 
	$term_meta = get_option( "taxonomy_$t_id" ); 
	?>
	<tr class="form-field">
		<th scope="row">
			<label for="term_meta['meta_tit']"><?php _ex( 'Meta Title', 'term meta_tit' ); ?></label>
		</th>
		<td><input name="term_meta[meta_tit]" id="term_meta[meta_tit]" type="text" value="<?php echo esc_attr( $term_meta['meta_tit'] ) ? esc_attr( $term_meta['meta_tit'] ) : ''; ?>" size="40" aria-required="false" /></td>
	</tr>
	<tr class="form-field">
		<th scope="row">
			<label for="term_meta['meta_des']"><?php _ex( 'Meta Description', 'term meta_des' ); ?></label>
		</th>
		<td><input name="term_meta[meta_des]" id="term_meta[meta_des]" type="text" value="<?php echo esc_attr( $term_meta['meta_des'] ) ? esc_attr( $term_meta['meta_des'] ) : ''; ?>" size="160" aria-required="false" /></td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top">
			<label for="term_meta['custom_term_class']">SUBTÍTULO de la categoría </label>
		</th>
		<td>
			<input type="text" name="term_meta[promoCategoryTagline]" id="term_meta[promoCategoryTagline]" value="<?php echo esc_attr( $term_meta['promoCategoryTagline'] ) ? esc_attr( $term_meta['promoCategoryTagline'] ) : ''; ?>">
			<p class="description">Si no pones nada siempre bajo el título pondrá ofertas de hoteles y apartamentos..</p>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top">
			<label for="term_meta['custom_term_class']">ID de la categoría (para css modif.)</label>
		</th>
		<td>
			<input type="text" name="term_meta[custom_term_class]" id="term_meta[custom_term_class]" value="<?php echo esc_attr( $term_meta['custom_term_class'] ) ? esc_attr( $term_meta['custom_term_class'] ) : ''; ?>">
			<p class="description">A través de este ID se cargan las imagenes e iconos via CSS.</p>
		</td>
	</tr>
	<tr class="form-field">
		<th scope="row" valign="top">
			<label for="term_meta['term_large_img_url']">Imagen Categoria</label>
		</th>
		<td>
			<input type="text" name="term_meta[term_large_img_url]" id="term_meta[term_large_img_url]" value="<?php echo esc_attr( $term_meta['term_large_img_url'] ) ? esc_attr( $term_meta['term_large_img_url'] ) : ''; ?>">
			<p class="description">Imagen que se mostrara en la ficha categoria (se han de subir en la carpeta <em>/images</em> de la raiz del tema).</p>
		</td>
	</tr>

	
	
<?php
}
add_action( 'promo_categories_edit_form_fields', 'promo_categorie_add_new_meta_field', 10, 2 );




function save_taxonomy_custom_meta( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		update_option( "taxonomy_$t_id", $term_meta );
	}
}  
add_action( 'edited_promo_categories', 'save_taxonomy_custom_meta', 10, 2 );



/*= META BOXES
-----------------*/
function RV_register_meta_boxes( $meta_boxes )
{

	$prefix = 'custom_';
	global $arr_ubicas;

	// HOTELES
	$meta_boxes[] = array(
		'id' => 'meta-hotel',
		'title' => 'Informaci&oacute;n Adicional',
		'pages' => array( 'hotel'), 
		'context' => 'advanced', // normal, advanced, side 
		'priority' => 'high', // high, low
		'autosave' => true,
		'fields' => array(
			array(
				'name'=> 'Cerrado, no mostrar',  
				'desc'  => "Marcar si no queremos que se vea en la web, aunque este publicado.",  
				'id'    => "{$prefix}chapado",  
				'type'  => "checkbox",
			),
			array(
				'name'=> 'Nuevo',  
				'desc'  => "Es un hotel nuevo.",  
				'id'    => "{$prefix}new",  
				'type'  => "checkbox",
			),
			array(
				'name'=> 'URL',  
				'desc'  => "Link a la web del hotels",  
				'id'    => "{$prefix}URL",  
				'type'  => "text",  
			),
			array(
				'name'=> 'URL seccion ofertas',  
				'desc'  => "Link a la seccion ofertas de la web de hoteles",  
				'id'    => "{$prefix}URL_ofertas",  
				'type'  => "text",  
			),
			array(
				'name'=> 'Tel&eacute;fono',  
				'desc'  => "Tel&eacute;fono de contacto.",  
				'id'    => "{$prefix}tel",  
				'type'  => "text",  
			),
			array(
				'name'=> 'Direcci&oacute;n',  
				'desc'  => "Direcci&oacute;n f&iacute;sica.",  
				'id'    => "{$prefix}direc",  
				'type'  => "textarea",  
			),
			/*
			array(
				'name'=> 'Destacado',  
				'desc'  => "Hotel destacado.",  
				'id'    => "{$prefix}feat",  
				'type'  => "checkbox",
			),
			*/
			array(
				'name'=> 'Zona',  
				'desc'  => "Ubicación.",  
				'id'    => "{$prefix}zona",  
				'type'  => "select",  
				'options' => $arr_ubicas,
			),
			array(
				'name'=> 'Mar o Monte?',  
				'desc'  => "Que si es de mar o de montaña",  
				'id'    => "{$prefix}maromon",  
				'type'  => "select",  
				'options' => array (  
					'mar' => "Mar, cerca playa",
					'mont' => "Montaña, cerca pistas esquí"
				)
			),
			array(
				'name'=> 'Orden en listados',  
				'desc'  => "Numero entero para orden",  
				'id'    => "{$prefix}ordenhotel",  
				'type'  => "text",  
			),
			array(
				'name'=> 'Encanto',  
				'desc'  => "Tiene encanto, es cuqui, este hotel? (Palau, Ses Arrels...)",  
				'id'    => "{$prefix}conencanto",  
				'type'  => "checkbox",
			),
			array(
				'name'=> 'Estrellas',  
				'desc'  => "Estrellas del hotel.",  
				'id'    => "{$prefix}star",  
				'type'  => "select",  
				'options' => array (  
					'5' => __('5'),
					'4' => __('4'),
					'3' => __('3'),
					'2' => __('2'),
					'1' => __('1'),
				)
			),
			/*
			array(
				'name'=> 'Codigo bookings',  
				'desc'  => "Codigo bookings",  
				'id'    => "{$prefix}bookings",  
				'type'  => "text",
			),
			*/
			array(
				'name' => "Logo",
				'id'   => "{$prefix}logo",
				'desc' => "El logo del hotel",
				'type' => 'image_advanced',
			),
			array(
				'name'=> 'Slide',  
				'desc'  => "Copy paste del codigo, a saco.<br />Donde deberian ir las imagenes (el src del img) poner [img_path_en_plantilla]/images/ficha_condes_03.jpg]",  
				'id'    => "{$prefix}slide",  
				'type'  => "textarea",  
			),
			array(
				'name' => "Servicios disponibles",
				'id'   => "{$prefix}capabilities",
				'desc' => "una imagen con los iconos de servicios disponibles",
				'type' => 'image_advanced',
			),
			array(
				'name'	=> 'Mapa',  
				'desc'  => "Url pal link del mapa del maps",  
				'id'    => "{$prefix}url_mapa",  
				'type'  => "text",  
				'size'	=> "65%",
			),
			array(
				'name' => "Imagen mapa",
				'id'   => "{$prefix}img_mapa",
				'desc' => "La imagen mapa",
				'type' => 'image_advanced',
			),
		),
	);
	
	// APARTAMENTOS
	
	$meta_boxes[] = array(
		'id' => 'meta-apto',
		'title' => 'Informaci&oacute;n del apartamento',
		'pages' => array( 'apartamento'), 
		'context' => 'advanced', // normal, advanced, side 
		'priority' => 'high', // high, low
		'autosave' => true,
		'fields' => array(
			array(
				'name'=> 'Nuevo',  
				'desc'  => "Es un apartamento nuevo.",  
				'id'    => "{$prefix}new",  
				'type'  => "checkbox",
			),
			/*
			array(
				'name'=> 'Destacado',  
				'desc'  => "Apartamento destacado.",  
				'id'    => "{$prefix}feat",  
				'type'  => "checkbox",
			),
			array(
				'name'=> 'URL',  
				'desc'  => "Link a la ficha.",  
				'id'    => "{$prefix}URL",  
				'type'  => "text",  
			),
			*/
			array(
				'name'=> 'Codigo bookings',  
				'desc'  => "Codigo NEObookings",  
				'id'    => "{$prefix}bookings",  
				'type'  => "text",
			),
			array(
				'name'=> 'Zona',  
				'desc'  => "Ubicación.",  
				'id'    => "{$prefix}zona",  
				'type'  => "select",  
				'options' => $arr_ubicas,
			),
			/*
			array(
				'name'=> 'Pie Foto',  
				'desc'  => "Texto que saldrá bajo la foto en el listado aparts",  
				'id'    => "{$prefix}piefoto",  
				'type'  => "text",  
			),
			*/
			array(
				'name'=> 'Slide principal',  
				'desc'  => "Copy paste del codigo, a saco.<br />Donde deberian ir las imagenes (el src del img) poner [img_path_en_plantilla]/images/ficha_condes_03.jpg]",  
				'id'    => "{$prefix}slide",  
				'type'  => "textarea",  
			),
			array(
				'name'	=> 'Contacto',  
				'desc'  => "Copy paste del html del contacto",  
				'id'    => "{$prefix}contacto",  
				'type'  => "textarea",  
			),
			array(
				'name'	=> 'Destaca por',  
				'desc'  => "Texto del bloque destaca por (HTML)",  
				'id'    => "{$prefix}destaca_por",  
				'type'  => "textarea",
			),
			array(
				'name'	=> 'Descripcion',  
				'desc'  => "Texto del bloque descripcion",  
				'id'    => "{$prefix}descripcion",  
				'type'  => "textarea",  
			),
			array(
				'name'	=> 'Mapa',  
				'desc'  => "Url pal link del mapa del maps",  
				'id'    => "{$prefix}url_mapa",  
				'type'  => "text",  
				'size'	=> "65%",
			),
			array(
				'name' => "Imagen mapa",
				'id'   => "{$prefix}apart_img_mapa",
				'desc' => "La imagen mapa",
				'type' => 'image_advanced',
			),
			array(
				'name'=>'Tipología 1',
				'type'=>'heading',
			),
			array(
				'name'	=> 'Titulo',  
				'id'    => "{$prefix}titulo_tipo_1",  
				'type'  => "text",  
				'size'	=> "65%",
			),
			array(
				'name'	=> 'Descripcion',  
				'id'    => "{$prefix}descripcion_tipo_1",  
				'type'  => "textarea",
			),
			array(
				'name'	=> 'Slideshow',  
				'id'    => "{$prefix}slide_tipo_1",  
				'type'  => "textarea",
			),
			array(
				'name'=>'Tipología 2',
				'type'=>'heading',
			),
			array(
				'name'	=> 'Titulo',  
				'id'    => "{$prefix}titulo_tipo_2",  
				'type'  => "text",  
				'size'	=> "65%",
			),
			array(
				'name'	=> 'Descripcion',  
				'id'    => "{$prefix}descripcion_tipo_2",  
				'type'  => "textarea",
			),
			array(
				'name'	=> 'Slideshow',  
				'id'    => "{$prefix}slide_tipo_2",  
				'type'  => "textarea",
			),
		),
		
	);
	
	// PROMOCIONES OFERTAS

	$aparts 	= 	get_posts( array("post_type"=>"apartamento","posts_per_page"=>-1,"suppress_filters"=>0) );
	$def_aparts =	array ( '-' => __('Seleccionar') );
//	$idsAptoNeo  = array();
	foreach ($aparts as $key => $value) {
		$idioma = apply_filters( 'wpml_post_language_details', NULL, $value->ID );
		
		if ($idioma["language_code"] == ICL_LANGUAGE_CODE) {
			$def_aparts[$value->ID]=$value->post_title;

//			$metasApto = get_post_meta($value->ID);
//			$idsAptoNeo[$value->ID] = $metasApto['custom_bookings'][0];

		}
	}
//	$idsAptoNeo = array_unique($idsAptoNeo);
	$def_aparts = array_unique($def_aparts);

	$hotels 	= 	get_posts( array("post_type"=>"hotel","posts_per_page"=>-1,"suppress_filters"=>0) );
	$def_hotels =	array ( '-' => __('Seleccionar') ); //$resultado = array_unique($entrada);
	foreach ($hotels as $key => $value) {
		$idioma = apply_filters( 'wpml_post_language_details', NULL, $value->ID );
		if ($idioma["language_code"] == ICL_LANGUAGE_CODE) {
			$def_hotels[$value->ID]=$value->post_title;
		}
	}
	$def_hotels = array_unique($def_hotels);

	
	$def_ubicas =	array ();
	foreach ($arr_ubicas as $key => $value) {
		$def_ubicas[$key]=$value;
	}

	$meta_boxes[] = array(
		'id' => 'meta-promo',
		'title' => 'COFIGURACION DE LA PROMOCION - FUNCIONAL PARA TODOS ESTABLECIMIENTOS',
		'pages' => array( 'promo'), 
		'context' => 'normal', // normal, advanced, side 
		'priority' => 'high', // high, low
		'autosave' => true,
		'fields' => array(
			array(
				'name'=> 'Apartamento',  
				'desc'  => "",  
				'id'    => "{$prefix}oferta_en_apart",  
				'type'  => "select", 
				'options' => $def_aparts
			),
			array(
				'name'=> 'Hotel',  
				'desc'  => "",  
				'id'    => "{$prefix}oferta_en_hotel",  // custom_oferta_en_hotel
				'type'  => "select", 
				'options' => $def_hotels
			),
			array(
				'name'=> 'Zona',  
				'desc'  => "",  
				'id'    => "{$prefix}oferta_en_zona",  //
				'type'  => "select", 
				'options' => $def_ubicas
			),
			array(
				'name'=> 'Precio',  
				'desc'  => "Número entero o decimal, cuidado si usas puntos o comas, ¡Unifica!",  
				'id'    => "{$prefix}precio",  
				'type'  => "text",  
			),
			array(
				'name'=> 'Precio por noche',  
				'desc'  => "¿Indicar que es PRECIO POR NOCHE?",
				'id'    => "{$prefix}pornoche",  
				'type'  => "checkbox",  
			),
			array(
				'name'=> 'Descuento',  
				'desc'  => "DEJAR EN BLANCO SI NO PROCEDE - Pegatina descuento junto al precio (8,10,12,15,20,25) ",  
				'id'    => "{$prefix}descuento", 
				'type'  => "text",
				/*
				'type'  => "select",
				'options' => ["",8,10,12,15,20,25]
				*/
			),
			array(
				'name' => 'Fecha inicio',
				'id'   => "{$prefix}dtinicio",
				'type' => 'date',
				'js_options' => array(
					'appendText'      => 'Fecha check in, si es abierta, dejar fecha actual',
					'dateFormat'      => 'dd/mm/yy',
					'changeMonth'     => true,
					'changeYear'      => true,
					'showButtonPanel' => true,
				),
				'std' => date('m/d/y')
			),
			array(
				'name' => 'Fecha fin',
				'id'   => "{$prefix}dtfin",
				'type' => 'date',
				'js_options' => array(
					'appendText'      => 'Fecha check out, opcional si indicamos a continuacion noches',
					'dateFormat'      => 'dd/mm/yy',
					'changeMonth'     => true,
					'changeYear'      => true,
					'showButtonPanel' => true,
				)
			),
			array(
				'name' => 'Noches',
				'desc'  => "Numero de noches, opcional si indicas fecha check out. Sólo NEOBOOKINGS", 
				'id'   => "{$prefix}noches",
				'type' => 'text'
			),
			array(
				'name' => 'OPCION FORFAIT PAQUETE',
				'desc'  => "Pegar codigo de NEOBOOKINGS si existe la opcion. En la vista de la oferta, el botón reservar pasará a ser RESERVAR SOLO HOTEL, y junto a este, aparecerá el botón RESERVAR CON FORFAIT", 
				'id'   => "{$prefix}forfaitcode",
				'type' => 'text'
			),
			array(
				'name'=> 'Mostrar sólo la opción RESERVAR CON FORFAIT',  
				'desc'  => "En este caso se mostrará el botón de reservar con forfait, y el genérico de reservar sólo hotel quedará oculto",  
				'id'    => "{$prefix}forfaitonly",
				'type'  => "checkbox",
			),
			array(
				'name'=> 'Texto explicativo QUE INCLUYE LA OPCION FORFAIT',  
				'desc'  => "Se mostrara en una pastilla bajo el boton de forfait y sobre el resto de letra pequeña",  
				'id'    => "{$prefix}forfaitincludes",
				'type'  => "textarea",
			),
			array(
				'name'=>'COMO SE VE ESTA OFERTA EN WWW.RVHOTELS.ES -> <strong>SOLO OFERTAS DE HOTEL ¡OJO!</strong>',
				'type'=>'heading',
			),
			array(
				'name'=> 'Oferta visible en web www.rvhotels.es',  
				'desc'  => "Se muestra en esta web",  
				'id'    => "{$prefix}show",
				'type'  => "checkbox",
			),
			array(
				'name'=> 'Oferta DESTACADA EN LA HOME de www.rvhotels.es',  
				'desc'  => "Destacada en esta web",  
				'id'    => "{$prefix}feat",  
				'type'  => "checkbox",
			),
			/*array(
				'name'=> 'Texto sobre imagen',  
				'desc'  => "El texto que aperecera en el listado de ofertas, sobre la imagen.",  
				'id'    => "{$prefix}texto_en_imagen",  
				'type'  => "text",
			),*/
			array(
				'name' => 'NOVEDAD Oferta vigente hasta',
				'desc'   => "La oferta que ya no este vigente se podra ver, pero no reservarse. Podemos aprovechar igualmente para sugerir otras ofertas o la inscripción al newsletter. Esto es totalmente distinto en funcionalidad al POST EXPIRATOR de la derecha, que despublica la oferta y la convierte en una pantalla de error 404.<br><br>
				Este comportamiento afecta solo a la vista de la oferta en www.rvhotels.es, la oferta en la web del hotel destino ya no será visible. Evidentemente, lo recomendado será dejar la oferta publicada, no borrarla ni dejarla como borrador...",
				'id'   => "{$prefix}promoexpires",
				'type' => 'date',
				'js_options' => array(
					'dateFormat'      => 'dd/mm/yy',
					'changeMonth'     => true,
					'changeYear'      => true,
					'showButtonPanel' => true,
				),
			),
			array(
				'name'=> 'URL oferta',  
				'desc'  => "DEJAR EN BLANCO excepto si queremos que la minitarjetita de la oferta esté linkada a otra web externa<br>Dejándo esta casilla en blanco el clic te lleva a la oferta en esta misma web (recuerda que las ofertas de hotel se publican en rvhotels y en la web del hotel. <br>La principal ventaja de mostrar la oferta en RV Hotels es que puedes mostrar más ofertas parecidas en otros hoteles cercanos o similares, <br>ej. Condes->Tuca y Orri, Palau->Ses Arrels, etc)",  
				'id'    => "{$prefix}oferta_en_hotel_url",  
				'type'  => "text",  
			),
			array(
				'name'=>'CÓMO DEBE VERSE LA OFERTA EN LA WEB DE SU HOTEL (SÓLO HOTELES, EXCEPTO PALAU)',
				'type'=>'heading',
			),
			array(
				'name'=> 'Destacada en la HOME de su web',  
				'desc'  => "Oferta destacada en la web destino (solo para hoteles)",  
				'id'    => "{$prefix}feat_dest",  
				'type'  => "checkbox",
			),/*
			array(
				'name'=> 'Texto extra',  
				'desc'  => "Un texto que apareceera en la web del hotel al lado del precio",  
				'id'    => "{$prefix}texto_precio_extra",  
				'type'  => "text",
			),*/
			array(
				'name'=>'OPCION GLOBAL TANTO RV HOTELS COMO WEB DEL HOTEL',
				'type'=>'heading',
			),
			array(
				'name'=> 'Oferta oculta',  
				'desc'  => "NO mostrar la oferta en el frontend, ésta podrá estar PUBLICADA pero NO SERÁ VISIBLE en los listados.",  
				'id'    => "{$prefix}ofertaoculta",  
				'type'  => "checkbox",
			),
		),
	);


	return $meta_boxes;
}



function my_scripts_method($hook) {
	global $post;
	if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
		if ( 'promo' === $post->post_type ) {     
			wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri() . '/js/funciones_edit_promo.js'	);
		}
	}
}


add_filter( 'rwmb_meta_boxes', 'RV_register_meta_boxes');
add_action( 'admin_enqueue_scripts', 'my_scripts_method' );


/* ENABLE CUSTOM POST META IN API REST */

/* Vamos a hacer que el meta custom_oferta_en_hotel, que nos devuelve el id del hotel, salga en las respuestas json */

/* UPDATE: USASREMOS EL PLUGIN REST API CONTROLLER QUE PROPORCIONA UN UI */

/* osea, que salga cuando pidamos https://www.rvhotels.es/wp-json/wp/v2/promo */

/* https://torquemag.io/2015/07/working-with-post-meta-data-using-the-wordpress-rest-api/ */

/*
function slug_get_post_meta_cb( $object, $field_name, $request ) {
    return get_post_meta( $object[ 'id' ], $field_name );
}
 
function slug_update_post_meta_cb( $value, $object, $field_name ) {
    return update_post_meta( $object[ 'id' ], $field_name, $value );
}

add_action( 'rest_api_init', function() {
	register_api_field(
		'promo',
		'custom_oferta_en_hotel',
		array(
		   'get_callback'    => 'slug_get_post_meta_cb',
		   'update_callback' => 'slug_update_post_meta_cb',
		   'schema'          => null,
		)
	);

	register_api_field(
		'promo',
		'custom_precio',
		array(
		   'get_callback'    => 'slug_get_post_meta_cb',
		   'update_callback' => 'slug_update_post_meta_cb',
		   'schema'          => null,
		)
	);
 
});
*/

/* PERMITE EL USO DE FILTROS EN EL ENDPOINT DEL API REST */

/* LO USAREMOS PARA FILTRAR OFERTAS DEL HOTEL */

function add_query_vars_filter( $vars ){
	$vars = array_merge( $vars, array( 'meta_key', 'meta_value', 'meta_compare','meta_query','tax_query' ) );
	return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

?>