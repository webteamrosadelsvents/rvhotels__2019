	<h1>Entorno de Desarrollo</h1>
	<div class="modal fade" id="finalModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?php echo  __("Confirmación de suscripción");?></h4>
				</div>
				<div class="modal-body">
					<p><?php echo  __("Gracias por suscribirse a nuestra lista de mailing. A partir de ahora recibirá correos con ofertas y packs en su buzón de correo electrónico")?>.</p>
				</div>
				<!--
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
				-->
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<?php
	if($_GET['subscribe']=="ok"){
	?>
	<script>
	jQuery(document).ready(function (){
		jQuery('#finalModal').modal();
	});
	</script>
	<?php
	}
	if(!is_mobile()){
	?>	
	<div class="modal fade" role="dialog" aria-labelledby="gridSystemModalLabel" aria-hidden="true" id="suscribeModal">
		<div class="modal-dialog">
			<div class="modal-content fondoNews">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">					
					<form action="http://rvhotels.us2.list-manage.com/subscribe/post?u=23b5df66471f0ebe80f216080&amp;id=5b7afe5083" method="post" target="_blank" novalidate>
				        <div class="mc-field-group row">
				        	<div class="col-lg-12 col-md-12 col-centered">
				            	<h3><?php echo  __("¡Suscribete y recibe nuestras mejores ofertas!");?></h3>
				        	</div>
				            <div class="col-lg-8 col-md-8 col-centered">
				                <input type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Escribe tu e-mail." style="width:100%;" >
				          		<br />
				                <button type="submit" value="" name="subscribe"  class="btn btn-primary buscar" style="width:100px;"><i class="fa fa-angle-double-right"></i>
				            </div>
				        </div>
				    </form>
				</div>
					<!--
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
					-->
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<script>
	jQuery(document).ready(function (){

		var hayCookie = jQuery.cookie( 'cookie_subscribe' );
		if(isNaN(hayCookie)){
			jQuery('#suscribeModal').modal();
		}
		CookieSubscribe.inicio({
			ajaxCallback: "<?php echo home_url(); ?>/wp-admin/admin-ajax.php",
		});
	});
	</script>
	<?php
	}
	?>
	<script src="<?php bloginfo('template_url'); ?>/js/subscrookie.js" type="text/javascript"></script>
	<footer class="footer_2015">
		<div class="container clearfix">
			<div class="row new_footer">
				<aside class="footer first padding5 col-xs-12 col-sm-4">
					<div id="text_icl-2" class="widget widget_text_icl"><h3 class="widgettitle">RV Hotels </h3>        
						<div class="textwidget">
							<?php echo __('
							<a href="http://www.hotelpalaulomirador.com/es/" target="_blank">RV Hotel Palau lo Mirador</a><br>
							<a href="http://www.hotelsesarrels.es/" target="_blank"> RV Hotel Ses Arrels</a><br>
							<a href="http://www.hotelametllamar.com/" target="_blank">RV Hotel Ametlla Mar</a><br>
							<a href="http://www.hotelcondesdelpallars.com/" target="_blank">RV Hotel Condes del Pallars</a>
							');?>
						</div>
					</div>				
				</aside>

				<aside class="footer second padding5 col-xs-12 col-sm-4">
					<div id="text_icl-3" class="widget widget_text_icl"><h3 class="widgettitle">Grup RV</h3>        
						<div class="textwidget">
							<?php echo __('
							<a href="https://www.rosadelsvents.es/es/" target="_blank">Rosa dels Vents</a><br>
							<a href="http://www.urbanyhostels.com/es/" target="_blank"> Urbany Hostels</a><br>
							<a href="http://www.cursosidiomas.com/" target="_blank">Cursos Idiomas</a>
							');?>
						</div>
					</div>				
				</aside>
				
				<aside class="footer third padding5 col-xs-12 col-sm-4">
				<?php if ( is_active_sidebar( 'footer-third-widget-area' ) ) : ?>
						<?php dynamic_sidebar( 'footer-third-widget-area' ); ?>
				<?php endif; ?>
					<div class="newsletter padding5"><?php include("mailchimper.php");?></div>
				</aside>		
			</div>
			</div>

			<div class="copy">
			<div class="container">
			<p><strong>Copyright &copy;</strong>  <a href="https://www.rvhotels.es">RV Hotels</a> - <?php icl_link_to_element(485,'page'); ?> - <?php icl_link_to_element(497,'page'); ?></p>
			</div>
		</div>
	</footer>
	<?php //	include("suscribe.php");?>
	<?php wp_footer(); ?>
	<script>

		function decorateBookingEngineLinks()
		{
			/* EACH NEO LINKS */
			var neobookingsMark = 'bookings.rvhotels';
			var neobookingsThumbsMark = 'thumbs';
			var links = document.getElementsByTagName('a');
			var href;

			for (var i = 0; i < links.length; i++)
			{
				/* CHECK ENGINE LINKS */
				if(links[i].getAttribute('href'))
				{
					href = links[i].getAttribute('href');
					if (href.indexOf(neobookingsMark) != -1 && href.indexOf(neobookingsThumbsMark) == -1)
					{
						/* SET LISTENERS */
						addListener(links[i], 'mousedown', decorateMe);
						addListener(links[i], 'keydown', decorateMe);
					}
				}
			}

			/* DECORATE LINKS */
			function decorateMe(event)
			{
				event = event || window.event;
				var target = event.target || event.srcElement;

				/* CHECK IF TARGET IS IMAGE AND OVERWRITE */
				if (target instanceof HTMLImageElement || target instanceof HTMLDivElement)
				{
					target = target.parentNode;
				}

				if (target && target.href)
				{	
					ga('linker:decorate', target);
				}
			}

			/* CROSS BROWSER LISTENERS */
			function addListener(element, type, callback)
			{
				if (element.addEventListener)
				{
					element.addEventListener(type, callback);	
				} 
				else if (element.attachEvent) 
				{
					element.attachEvent('on' + type, callback);
				}
			}
		}

		function decorateBookingEngineForms()
		{
			/* EACH FORMS */
			var neobookingsMark = 'bookings.rvhotels';
			var forms = document.getElementsByTagName('form');
			var action;

			for (var i=0; i<forms.length; i++)
			{
				/* SET ENGINE FORMS */
				if(forms[i].getAttribute('action'))
				{
					action = forms[i].getAttribute('action');
					if (action.indexOf(neobookingsMark)!=-1)
					{
						/* SET LISTENERS */
						addListener(forms[i], 'submit', decorateForm);
					}
				}
			}

			/* DECORATE FORMS */
			function decorateForm(event)
			{
				event = event || window.event;
				var target = event.target || event.srcElement;
			
				if (target && target.action)
				{
					ga('linker:decorate', target);
				}
			}

			/* CROSS BROWSER LISTENERS */
			function addListener(element, type, callback)
			{
				if (element.addEventListener)
				{
					element.addEventListener(type, callback);
				}
				else if (element.attachEvent)
				{
					element.attachEvent('on' + type, callback);
				}
			}
		}

		/* INIT */
		window.onload = function()
		{
			decorateBookingEngineLinks();
			decorateBookingEngineForms();
		};

		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-1651542-16', 'auto');
		ga('require', 'linker');
		ga('send', 'pageview');
	</script>
</body>
</html>