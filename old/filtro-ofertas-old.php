<style>
option[value="alcoceber"],
option[value="segur_de_calafell"],
option[value="roses"] {
    display:none!important;
}
</style>
<div class="container dispo-container">
    <div class="filtro-ofertas clearfix">
        <!--TITLE-->
        <div class="padding0 col-md-3 col-sm-3 col-xs-12">
            <h5><?php echo __("Filtrar ofertas");?>:</h5>
        </div>
        <!--/TITLE-->
        <!--CATEGORIA-->
        <form action="" id="filtro-form" method="POST">
            <div class="padding5 col-md-2 col-sm-2 col-xs-12">
                <div class="padding5 col-md-11 col-sm-11 col-xs-11">
                    <select id="categoria" name="categoria" class="form-control">
                        <option value="-1"><?php echo __("Categoria");?></option>
                        <?php
                        foreach ($taxonomies as $taxonomy) {
                            $selecto=(isset($queried_object->term_id) && $queried_object->term_id==$taxonomy->term_id)?"selected":"";
                        ?>
                        <option value="<?php echo $taxonomy->term_id;?>" <?php echo $selecto;?>><?php echo $taxonomy->name;?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="selector-icon padding0 col-md-1 col-sm-1 col-xs-1">
                    <i class="fa fa-tags"></i>
                </div>
            </div>
            <!--/CATEGORIA-->
            <!--DESTINO-->
            <div class="padding5 col-md-2 col-sm-2 col-xs-12">
                <div class="padding5 col-md-11 col-sm-11 col-xs-11">
                    <select id="destino" name="destino" class="form-control unaUotra">
                        <?php
                        foreach ($arr_ubicas as $key => $value) {
                            $selecto_dest=(isset($_POST['destino']) && $_POST['destino']==$key)?"selected":"";
                        ?>
                        <option value="<?php echo $key;?>" <?php echo $selecto_dest;?>><?php echo $value;?></option>
                        <?php
                        }
                        /*
                        <option value="blan">Blanes - Costa Brava</option>
                        <option value="lest">L'Estartit - Costa Brava</option>
                        <option value="pltj">Platja d'Aro - Costa Brava</option>
                        <option value="alce">Alcocéber - Costa Brava</option>
                        <option value="rial">Rialp - Pirineo de Lleida</option>
                        <option value="vall">Vall Llobrega - Costa Brava</option>
                        <option value="amet">L'ametlla de Mar - Costa Dorada</option>
                        <option value="torr">Torroella de Montgri - Costa Brava</option>
                        */
                        ?>
                        
                    </select>
                </div>
                <div class="selector-icon padding0 col-md-1 col-sm-1 col-xs-1">
                    <i class="fa fa-location-arrow"></i>
                </div>
            </div>
            <!--/DESTINO-->
            <!--APARTAMENTO-->
            <div class="padding5 col-md-2 col-sm-2 col-xs-12">
                <div class="padding5 col-md-11 col-sm-11 col-xs-11">
                    <select name="form_apartamento" id="form_apartamento" class="form-control unaUotra">
                        <option value="-1"><?php echo __("Apartamento");?></option>
                        <?php
                        foreach ($aparts as $apart) {
                            $selecto_apart=(isset($_POST['form_apartamento']) && $_POST['form_apartamento']==$apart->ID)?"selected":"";
                        ?>
                        <option value="<?php echo $apart->ID;?>" <?php echo $selecto_apart;?>>RV <?php echo $apart->post_title;?></option>                
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="selector-icon padding0 col-md-1 col-sm-1 col-xs-1">
                    <i class="fa fa-home"></i>
                </div>
            </div>
            <!--/APARTAMENTO-->
            <!--HOTEL-->
            <div class="padding5 col-md-2 col-sm-2 col-xs-12">
                <div class="padding5 col-md-11 col-sm-11 col-xs-11">
                    <select id="form_hotel" name="form_hotel" class="form-control unaUotra">
                        <option value="-1"><?php echo __("Hotel");?></option>
                        <?php
                        foreach ($hotels as $hotel) {
                            $selecto_hot=(isset($_POST['form_hotel']) && $_POST['form_hotel']==$hotel->ID)?"selected":"";
                        ?>
                        <option value="<?php echo $hotel->ID;?>" <?php echo $selecto_hot;?>>RV Hotels <?php echo $hotel->post_title;?></option>                
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="selector-icon padding0 col-md-1 col-sm-1 col-xs-1">
                    <i class="fa fa-hospital-o"></i>
                </div>
            </div>
            <!--/HOTEL-->
            <!--BOTON-->
            <div class="form-group padding5 col-md-1 col-sm-1 col-xs-12">
                <div class="padding5 col-md-10 col-sm-10">
                    <button type="button" class="btn btn-primary buscar"><i class="fa fa-angle-double-right"></i></button>
                </div>    
            </div>
        <!--/BOTON-->
        </form>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){

    //  ACCIONES AL CAMBIAR LOS SELECTORES  DE destino, apatamento Y hotel
        $("select.unaUotra").change(function(){
            $seleccionada=$(this);
            $("select.unaUotra").not($seleccionada).val(-1);
//            console.log($seleccionada.attr("id"));
        });

    //  ACCIONES AL DARLE AL SUBMIT
        $cat    = $("#categoria");
        $dest   = $("#destino");
        $apart  = $("#apartamento");
        $hot    = $("#hotel");
        $form   = $("#filtro-form");
        $("button.buscar").click(function(event){
            if($cat.val()==-1){
                alert("<?php echo __('Seleccione una categoria para filtrar');?>");
                event.preventDefault();
            }else{
                $.ajax({
                    url: "https://www.rvhotels.es/wp-content/themes/rvhotels/aj_filtro_promos.php",
                    data: {'cat_id':$cat.val(),'dest':$dest.val(),'apart':$apart.val(),'hot':$hot.val(),'lang':'<?php echo ICL_LANGUAGE_CODE;?>'},
                    type: "post",
                }).success(function(response) {
                     $form.attr("action",response);
                     $form.submit();
                });
            }
        });
    });
</script>