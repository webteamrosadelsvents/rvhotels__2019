<!-- sidebar-social -->
<aside id="social">

<ul>
<li class="fb"><a href="https://www.facebook.com/RVHotels" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
<li class="tw"><a href="https://twitter.com/Rv_hotels" title="Twitter"  target="_blank"><i class="fa fa-twitter"></i></a></li>
<li class="sb"><a href="javascript:return none;" data-toggle="modal" data-target="#mSuscribe" title="Suscribir"><i class="fa fa-envelope-o"></i></a></li>
</ul>

</aside>