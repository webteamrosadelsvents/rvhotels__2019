<?php 
get_header();
$zona           = $arr_ubicas[get_post_meta($post->ID,"custom_zona",1)];
$slideshow      = print_slideshow($post->ID);
$contacto       = get_post_meta($post->ID,"custom_contacto",1);
$destacaPor     = get_post_meta($post->ID,"custom_destaca_por",1);
$descripcion    = get_post_meta($post->ID,"custom_descripcion",1);
$img_mapa       = wp_get_attachment_image_src(get_post_meta($post->ID,"custom_apart_img_mapa",1),"full");
$bookingCode    = get_post_meta($post->ID,"custom_bookings",1);
$tipologias     = get_tipologias($post->ID);
/*
foreach ($tipologias as $value) {
    echo "<pre>".print_r($value, true)."</pre>";
}
*/
?>
<section class="bg bgaparts">
<div class="container clearfix"> 
<!-- end row --> 
    <article>
        <div class="col-md-12 col-sm-12 col-xs-12 padding20">
            <div class="fichahotel novedad padding0 col-md-12 col-sm-12 col-xs-12">
                <div class="titlehotel col-md-12 col-sm-12 col-xs-12">
                    <h2><span class="rvhotel">RV </span> <?php the_title();?></h2><h3> <?php echo __("Apartamentos en");?> <?php echo $zona;?></h3>    
                </div>
                <!--/MENU APARTAMENTO-->  
                <div class="col-md-4 col-sm-3 col-xs-12 menu-apart padding0">
                    <div class="col-md-12 col-sm-12 col-xs-4 menu-apart-descripcion">
                        <a href="#01_descripcion"><h4><?php echo __("Descripción");?></h4></a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-4 menu-apart-tipo">
                        <a href="#02_tipologia"><h4><?php echo __("Tipología");?></h4></a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-4 menu-apart-ofertas">
                        <a href="#03_ofertas"><h4><?php echo __("Ofertas");?></h4></a>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-4 hidden-xs atencion">
                      <?php echo $contacto;?>
                    </div> 
                </div>
                <!--/MENU APARTAMENTO-->
                <!--SLIDE APARTAMENTO-->
                <div class="col-md-8 col-sm-9 col-xs-12 container-slide-ficha padding0">
                    <?php echo $slideshow;?>
                </div>
                <!--/SLIDE APARTAMENTO-->
                <!--DESCRIPCION APARTAMENTO-->
                <div class="col-md-12 descripcion clear padding0" id="01_descripcion">
                    <!--DESTACA POR-->
                    <div class="col-md-4 col-sm-3 col-xs-12 menu-apart padding0">
                        <div class="col-md-12 hidden-xs apart-destaca">
                            <?php echo $destacaPor;?>
                        </div> 
                    </div>
                    <!--/DESTACA POR-->
                    <div class="col-md-8 col-sm-9 col-xs-12 padding0">
                        <!--ICONOS SERVICIOS-->
                        <!--
                        <div class="col-md-12 container-apart-icons">
                            <img src="<?php echo get_bloginfo('template_url');?>/images/apart_icons.png" class="img-responsive"> 
                        </div>
                        -->
                        <!--/ICONOS SERVICIOS-->
                        <!--SEPARADOR-->
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <!--/SEPARADOR-->
                        <!--TEXTO DESCRIPTIVO-->    
                        <div class="col-md-12 apart-descripcion">
                            <?php echo $descripcion;?>
                        </div>
                        <!--/TEXTO DESCRIPTIVO-->  
                    </div>    
                </div>
                <!--/DESCRIPCION APARTAMENTO-->
                <!--TIPOLOGIA APARTAMENTOS-->
                <div class="col-md-12 col-sm-12 col-xs-12 tipologia clear padding0" id="02_tipologia">
                    <!--MAPA-->
                    <div class="col-md-4 container-apart-map col-sm-4 col-xs-12 hidden-xs padding0">
                        <a href="<?php echo get_post_meta($post->ID,"custom_url_mapa",1);?>" target="_blank">
                          <img src="<?php echo $img_mapa[0];?>" class="img-responsive">
                        </a>
                    </div>
                    <!--/MAPA-->
                    <!--TIPOLOGIA 1-->
                    <?php
                    foreach ($tipologias as $tipologia) {
                    ?>
                    <!--TIPOLOGIA 2-->
                    <div class="col-md-4 tipo2 col-sm-4 col-xs-12">
                        <h5><i class="fa fa-users"></i> <?php echo $tipologia['titulo'];?></h5>
                        <div class="col-md-12 padding0 container-slide-apart-tipo">
                            <!--SLIDE TIPO 2-->
                            <?php echo $tipologia['slide'];?>
                            <!--/SLIDE TIPO 2-->
                        </div> 
                        <p><?php echo $tipologia['descripcion'];?></p>   
                    </div>
                    <!--/TIPOLOGIA 2-->
                    <?php
                    }
                    ?>
                </div>
                <!--/TIPOLOGIA APARTAMENTOS-->
                <!--SECCION OFERTAS-->
                <div class="col-md-12 col-sm-12 col-xs-12 ofertas clear padding0" id="03_ofertas">
                    <div class="col-md-12 col-sm-12 col-xs-12 padding5">
                        <h2><span class="rvhotel"><?php echo __("OFERTAS");?> RV</span> <?php the_title();?></h2>
                        <p><?php echo __("Puedes ver todas las ofertas de nuestros hoteles y apartamentos en el apartado <strong>Ofertas</strong>");?>.</p>
                    </div>
                    <!--CONTENEDOR OFERTAS-->
                    <div class="col-md-12 padding0 container-apart-ofertas">
                        <?php
                        $args = array(
                                        "posts_per_page"=>"4",
                                        "post_type"=>"promo",
                                        "post_status"=>array('publish'),
                                        "meta_query"=>array(
                                                            array(
                                                                "key"=>"custom_oferta_en_apart",
                                                                "value"=>$post->ID)
                                                            ));
                        query_posts( $args );
                        
                        while( have_posts()) : the_post();
                            $metas          = get_post_meta($post->ID);
                            $titulo         = get_the_title();
                            $imagen         = wp_get_attachment_image_src( $metas['_thumbnail_id'][0],"thumbnail" );
                            $precio         = ($metas['custom_precio'][0])?$metas['custom_precio'][0]:"0";
                            $hotel_apart    = descrimina_hotel_apartamento($metas,"nohotel");
                            $link           = get_custom_link($metas, $post->ID);
            //                echo "<pre>".print_r($hotel_apart,true)."</pre>";
                        ?>
                    <!--OFERTA-->
                        <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                            <div class="oferta">
                                <div class="col-md-12 titulo">
                                    <?php echo $titulo;?>
                                </div>
                                <a href="<?php echo $link;?>" target="_blank">
                                    <img src="<?php echo $imagen[0];?>" class="overflow">
                                </a>
                                <div class="col-md-12 padding5 descripcion">
                                    <?php echo get_the_excerpt();?>
                                </div>
                                <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                                    <p class="hotel">RV <?php echo $hotel_apart[0];?></p>
                                    <p class="destino"><?php echo $hotel_apart[1];?></p>
                                </div>
                                <a href="<?php echo $link;?>" target="_blank">
                                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                                        <p class="precio"><?php echo __("desde");?><br/><span class="euros"> <?php echo $precio;?>€</span></p>
                                    </div>
                                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                                        <i class="fa fa-angle-double-right"></i>
                                    </div>  
                                </a>
                            </div>  
                            </a>
                        </div>
                    <?php endwhile; ?>
                        <!--/OFERTA-->
                    </div>
                    <!--/CONTENEDOR OFERTAS-->   
                </div>
                <!--SECCION OFERTAS-->
            </div>   
        </div>
    </article>
</div><!--.container-->
</section>
<?php get_footer(); ?>