<?php  
get_header();

/* BEGIN MOBILE SLIDESHOW WIDGETKIT THUMBNAILS */
?>
<style>
@media (max-width:767px) {
    .wk-gallery-showcase .wk-slideset .next,
    .wk-gallery-showcase .wk-slideset .prev {
        display:none!important;
    }
    .wk-gallery-showcase .wk-slideset>div {
        padding:0!important;
    }
    .wk-gallery-showcase .wk-slideset .sets {
        height:auto!important;
        position:static!important;
    }
    .wk-slideset .set {
        width:auto!important;
        height:auto!important;
        display:inline-block!important;
        transform:none!important;
        text-align:left;
    }
    .wk-slideset .set>li {
        position:static!important;
        display:inline-block!important;
        line-height:0;
        margin:0 1px;
        width:19.4%; /*12.2% desktop */
    }
    .wk-slideset .set>li.active {
        opacity:.7;
    }
    .wk-gallery-showcase .wk-slideset .set>li>div {
        display:inline-block!important;
    }
    .wk-slideset img {
        width:100%!important;
        height:auto!important;
    }
    .wk-gallery-showcase .wk-slideset .set>li+li>div {
        margin:0;
    }
    .wk-gallery-showcase .wk-slideset .set:hover>li>div{
        opacity:1;
    }

    .mobile .fichahotel {
        margin:0 -15px;
    }
}   
</style>
<script>
    jQuery(document).ready(function($){
        $('.mobile .sets').append('<ul class="set final-set"></ul>');
        $('.mobile .set li').appendTo('.final-set');
        $('.mobile .final-set').removeClass('set');
        $('.mobile .set').remove();
        $('.mobile .final-set').addClass('set');
    });
</script>

<?php


$apartamentoActual = $post->ID;
$zonaRel        = get_post_meta($post->ID,"custom_zona",1);
$zona           = $arr_ubicas[$zonaRel];
$slideshow      = print_slideshow($post->ID);
$contacto       = get_post_meta($post->ID,"custom_contacto",1);
$destacaPor     = get_post_meta($post->ID,"custom_destaca_por",1);
$descripcion    = get_post_meta($post->ID,"custom_descripcion",1);
$img_mapa       = wp_get_attachment_image_src(get_post_meta($post->ID,"custom_apart_img_mapa",1),"full");
$bookingCode    = get_post_meta($post->ID,"custom_bookings",1);
$tipologias     = get_tipologias($post->ID);
$nombreApto     = $post->post_title;
$slugTitleClass = strtolower($nombreApto);
$slugTitleClass = str_replace(" ", "-", $slugTitleClass);
$apartamentoGet = urlencode($nombreApto);
$aidiom         = ICL_LANGUAGE_CODE;

// proceso el importe:
global $precioDesde;
$precioDesde = get_field('apt_preciodesde');
$precioDesde = number_format($precioDesde , 2 , ".", "");
?>
<style>.menuseccion a {outline:none;}</style>
<!-- sticky nav mobile inferior -->
<nav class="sticky-nav-mobile-inf bg-yellowgold darkblue">
    <ul class="">
        <li class="b1-7"><a class="display-block  text-center darkblue" href="#01_descripcion"><i class="fa fa-info"></i> <span><?php echo __("Descripción");?></span></a></li><!--

        --><li class="b1-7"><a class="display-block  text-center darkblue" href="#02_tipologia"><i class="fa fa-home"></i> <span><?php echo __("Alojamiento");?></span></a></li><!--

        --><li class="b1-7"><a class="display-block  text-center darkblue" href="#apart-servicios"><i class="fa fa-check"></i> <span><?php echo __("Servicios");?></span></a></li><!--

        --><li class="b1-7"><a class="display-block  text-center darkblue" href="#apart-mapa"><i class="fa fa-map-marker"></i> <span><?php echo __("Mapa");?></span></a></li><!--

        --><li class="b1-7"><a class="display-block  text-center darkblue" href="#03_ofertas"><i class="fa fa-star"></i> <span><?php echo __("Ofertas");?></span></a></li><!--
        --><li class="b2-7"><a href="#dispo" class="btn buscar-dispo-apart buscar-dispo-apart-scroll hilite bg-lightblue-plus blanco btn-book-mobile"><?php echo __("RESERVAR");?></a></li><!--
                            --></ul>
</nav>


<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDWRq4pfhuas4Mbb_hP-JC58dmCq9-8Cjw&extension=.js'></script> 
<section class="bg-lightblue sticky-nav hidden-xs" data-spy="affix" data-offset-top="400">
    <div class="container container-streched clearfix">

            <div class="row">

                <div class="col-md-8 col-sm-8 col-xs-12 nav-clonada">

                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="#dispo" class="btn buscar-dispo-apart buscar-dispo-apart-scroll bg-yellowgold padding10 blanco hilite"><?php echo __("RESERVAR");?></a>
                </div>

            </div>

    </div>

</section>



<section class="bg bgaparts">
    <div class="container clearfix"> 
        
        <div class="breadcrumbs padding10 blanco" typeof="BreadcrumbList" vocab="http://schema.org/">
            <?php if(function_exists('bcn_display'))
            {
                bcn_display();
            }?>
        </div>

        <article class="padding20">
            <div class="col-md-8 col-sm-8 col-xs-12">

                <nav class="menuseccion menuseccionapart hidden-xs">
                <ul class="">
                    <li class="activo"><a class="display-block fg-blanco text-center nav-01" href="#01_descripcion"><?php echo __("Descripción");?></a></li><!--

                    --><li class=""><a class="display-block fg-blanco text-center nav-02" href="#02_tipologia"><?php echo __("Alojamiento");?></a></li><!--

                    --><li class=""><a class="display-block fg-blanco text-center nav-03" href="#apart-servicios"><?php echo __("Servicios");?></a></li><!--

                    --><li class=""><a class="display-block fg-blanco text-center nav-04" href="#apart-mapa"><?php echo __("Mapa");?></a></li><!--

                    --><li class=""><a class="display-block fg-blanco text-center nav-05" href="#03_ofertas"><?php echo __("Ofertas");?></a></li><!--
                    --></ul>
                </nav>
              
                <div class="fichahotel ficha-apartamento novedad margin-bottom-20 position-relative">

                    <div class="titlehotel">
                        <h1 class="h1apt"><span class="rvhotel">RV </span> <?php the_title();?><br>
                        <span class="zonaapt"><?php echo __("Apartamentos en")." ".$zona;?></span></h1>
                    </div>

                    <div class="container-slide-ficha">

                        <?php if( get_field('slideshow_widgetkit_num_id') ) {?>

                            <?php $slidiD = get_field('slideshow_widgetkit_num_id'); ?>

                            <?php echo do_shortcode('[widgetkit id="'.$slidiD.'"]'); ?>
    
                        <?php } else { ?>

                        <?php echo $slideshow;?>

                        <?php }?>

                    </div>

                    <!--DESCRIPCION APARTAMENTO-->
                    <div class="descripcion clear padding15 padding-bottom-0" id="01_descripcion">

                        <h3 class="uppercase darkblue margin-bottom-15"><?php echo __("Descripción general");?></h3>

                        <?php echo $descripcion; ?>

                        <hr class="fullwidth">
  
                    </div>
                    <!--/DESCRIPCION APARTAMENTO-->
                    <!--TIPOLOGIA APARTAMENTOS-->
                    <div class=" clear padding15" id="02_tipologia">

                        <h3 class="uppercase darkblue margin-bottom-15"><?php echo __("Alojamiento y tipologías");?></h3>
                        
                        <!--TIPOLOGIA 1 y 2 -->
                        <?php
                        foreach ($tipologias as $tipologia) {
                        ?>
                        <div class="instalaciones-servicios">
                            <h4 class="lightblue"><i class="fa fa-users"></i> <?php echo $tipologia['titulo'];?></h4>
                            <p class="padding20"><?php echo $tipologia['descripcion'];?></p>   
                        </div>
                        <!--/TIPOLOGIA 1 y 2 -->
                        <?php
                        }
                        ?>
                        <!-- TIPOLOGIA ADICIONAL -->
                        <?php if( get_field('tipologias_adicionales') ): ?>
                            <div class="instalaciones-servicios">
                                <?php the_field('tipologias_adicionales');?>
                            </div>
                        <?php endif;?>

                        <hr class="fullwidth">

                    </div>
                    <!--/TIPOLOGIA APARTAMENTOS-->

                    <div class=" clear padding15" id="apart-servicios">

                        <h3 class="uppercase darkblue margin-bottom-15"><?php echo __("Servicios destacados" , "APARTAMENTOS BACKEND" );?></h3>

                        <div class="destacapor padding20 padding-top-0">
                            <?php echo $destacaPor;?>
                        </div>

                        <?php if( get_field('servicios_obligatorios') ): ?>

                            <h4 class="lightblue"><?php echo __("Servicios obligatorios o incluidos" , "APARTAMENTOS BACKEND");?></h4>
                            <div class="destacapor opcionales padding20">
                            <?php the_field('servicios_obligatorios'); ?>
                            </div>

                        <?php endif;?>


                        <?php if( get_field('servicios_opcionales') ): ?>

                            <h4 class="lightblue"><?php echo __("Servicios opcionales" , "APARTAMENTOS BACKEND");?></h4>
                            
                            <div class="destacapor opcionales padding20">
                            <?php the_field('servicios_opcionales'); ?>
                            </div>

                        <?php endif;?>

                        <?php if( get_field('tener_en_cuenta') ): ?>

                            <h4 class="lightblue"><?php echo __("A tener en cuenta" , "APARTAMENTOS BACKEND");?></h4>
                            
                            <div class="destacapor tenerencuenta padding20">
                                <?php the_field('tener_en_cuenta'); ?>
                            </div>

                        <?php endif;?>

                        <?php if( get_field('numreg') ): ?>
                            
                            <div id="registre-turistic" class="alert alert-info text-center numreg">
                                <strong><?php echo __("Número registro turístico");?></strong>: <?php the_field('numreg'); ?>
                            </div>

                        <?php endif;?>

                        

                        <hr class="fullwidth">

                    </div>


                    <!-- BLOQUE UBICACION, 2 CASOS POSIBLES DE DESCRIPTIVO -->
                

                    <div class=" clear padding15" id="apart-mapa">
                        <h3 class="uppercase darkblue margin-bottom-15"><?php echo __("Mapa e información");?></h3>
                    

                    <?php if( get_field('diferente_direccion') ) {?>

                        <!-- CASO 1: DIFERENTES DIRECCIONES RECEPCION E INMUEBLE -->

                        <div class="alert padding10 bg-yellowgold almostblack text-center">
                            <i class="fa fa-exclamation-circle"></i> <strong><?php echo __("ATENCIÓN: El alojamiento y la recepción/recogida de llaves están en distintos lugares.");?></strong>
                        </div>

                        <h4 class="lightblue margin-bottom-10"><?php echo __("Dirección de recepción y recogida de llaves");?></h4>

                        <div class="alert padding10 bg-darkblue fg-blanco caja-direccion-llaves">
                            <i class="fa fa-key yellowgold"></i>&nbsp; <?php the_field('direccion_de_recepcion');?>
                        </div>

                        <h4 class="lightblue margin-bottom-10"><?php echo __("Dirección del inmueble");?></h4>

                        <div class="alert padding10 bg-lightblue-plus fg-blanco caja-direccion-inmueble">
                            <i class="fa fa-home"></i>&nbsp; <?php the_field('direccion_del_inmueble');?>
                        </div>

                    <?php } else { ?>

                        <!-- CASO 2: UNA SOLA DIRECCION RECEPCION E INMUEBLE -->

                        <h4 class="lightblue margin-bottom-10"><i class="fa fa-home"></i> <i class="fa fa-key"></i>&nbsp; <?php echo __("Recepción, recogida de llaves y ubicación del inmueble");?></h4>

                        <div class="alert padding10 bg-lightblue fg-blanco caja-direccion-llaves">
                            <?php the_field('direccion_de_recepcion');?>
                        </div>

                    <?php } ?>

                            <div class="row">

                                <div class="col-md-6 col-xs-12">

                                    <?php if( get_field('hora_llegada') ): ?>

                                        <h4 class="lightblue margin-bottom-10"><i class="fa fa-clock-o"></i> <?php echo __("Horarios de entrada y salida" , "APARTAMENTOS BACKEND");?>:</h4>

                                        <p><strong><?php echo __("Hora de llegada");?>:</strong> <?php the_field('hora_llegada'); ?></p>

                                    <?php endif;?>

                                    <?php if( get_field('hora_salida') ): ?>

                                        <p><strong><?php echo __("Hora de salida");?>:</strong> <?php the_field('hora_salida'); ?></p>

                                    <?php endif;?>

                                </div>

                                <div class="col-md-6 col-xs-12">

                                    <h4 class="lightblue margin-bottom-10"><i class="fa fa-euro"></i> <?php echo __("Desembolso en día de llegada" , "APARTAMENTOS BACKEND");?>:</h4>

                                    <p><i class="fa fa-check"></i> <strong><?php echo __("Depósito");?>:</strong> <?php the_field('deposito_euros'); ?>€ <?php if( get_field('en_efectivo') ) /*echo __("(En efectivo)")*/;?></p>

                                    <?php if( get_field('deposito_extra_jovenes') ): ?>

                                        <p><i class="fa fa-check"></i> <strong><?php echo __("Depósito jóvenes");?>:</strong> <?php the_field('deposito_extra_jovenes'); ?>€ <?php echo __("/persona");?></p>

                                    <?php endif;?>

                                    <?php

                                    // TASA TURISTICA GLOBAL 
                                    $tasaTuristica = "0.99";

                                    // TASA TURISTICA SOLO BENELUX 
                                    if ( ($apartamentoActual==3784) || ($apartamentoActual==3789) || ($apartamentoActual==3791) || ($apartamentoActual==3794) ) {
                                        $tasaTuristica = "0.50"; // SOLO APARTAMENTOS BENELUX 
                                    }
									
                                    // TASA TURISTICA SOLO TREUMAL PARK 
                                    if ( ($apartamentoActual==25) || ($apartamentoActual==361) || ($apartamentoActual==362) || ($apartamentoActual==363) ) {
                                        $tasaTuristica = "0.50"; // SOLO APARTAMENTOS TREUMAL PARK 
                                    }

                                    ?>

                                    <p><i class="fa fa-check"></i> <strong><?php echo __("Tasa turística");?>:</strong> <?php echo $tasaTuristica;?>€ <?php echo __("persona/noche");?></p>
                                    <p><i class="fa fa-check"></i> <strong><?php echo __("Servicios opcionales escogidos");?></strong></p>

                                </div>

                            </div>

                    </div>

                    


                    
                    <!--MAPA-->                        
                         
                       <!-- <script> 
                            google.maps.event.addDomListener(window, 'load', init);
                            var map;
                            function init() {
                                var mapOptions = {
                                    center: new google.maps.LatLng(<?php the_field('coordenadas_gps_recepcion');?>), // centro en el apartamento
                                    zoom: 11,
                                    zoomControl: true,
                                    zoomControlOptions: {
                                        style: google.maps.ZoomControlStyle.DEFAULT,
                                    },
                                    disableDoubleClickZoom: true,
                                    mapTypeControl: true,
                                    mapTypeControlOptions: {
                                        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                                    },
                                    scaleControl: true,
                                    scrollwheel: true,
                                    panControl: true,
                                    streetViewControl: true,
                                    draggable : true,
                                    overviewMapControl: true,
                                    overviewMapControlOptions: {
                                        opened: false,
                                    },
                                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                                    styles: [{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"simplified"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"hue":"#f49935"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"hue":"#fad959"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#a1cdfc"},{"saturation":30},{"lightness":49}]}],
                                }
                                var mapElement = document.getElementById('mapartamento');
                                var map = new google.maps.Map(mapElement, mapOptions);

                                <?php if( get_field('diferente_direccion') ) {?>

                                var locations = [
                                    [
                                    '<span class="almostblack"><?php echo __("Recepción / Entrega de llaves");?><br>RV</span> <span class="lightblue"><?php the_title();?></span>',
                                    '<?php the_field('direccion_de_recepcion');?>',
                                    'tel',
                                    'email',
                                    'url',
                                    <?php the_field('coordenadas_gps_recepcion');?>,
                                    'https://www.rvhotels.es/wp-content/themes/rvhotels/images/icon_key.png'
                                    ],
                                    [
                                    '<span class="almostblack"><?php echo __("Alojamiento");?><br>RV</span> <span class="lightblue"><?php the_title();?></span>',
                                    '<?php the_field('direccion_del_inmueble');?>',
                                    'tel',
                                    'email',
                                    'url',
                                    <?php the_field('coordenadas_gps_inmueble');?>,
                                    'https://www.rvhotels.es/wp-content/themes/rvhotels/images/icon_home.png'
                                    ]
                                ];

                                <?php } else { ?>

                                var locations = [
                                    [
                                    '<span class="almostblack"><?php echo __("Apartamentos, recepción y entrega de llaves");?><br>RV</span> <span class="lightblue"><?php the_title();?></span>',
                                    '<?php the_field('direccion_de_recepcion');?>',
                                    'tel',
                                    'email',
                                    'url',
                                    <?php the_field('coordenadas_gps_recepcion');?>,
                                    'https://www.rvhotels.es/wp-content/themes/rvhotels/images/icon_home.png'
                                    ]
                                ];

                                <?php } ?>


                                for (i = 0; i < locations.length; i++) {
                                    if (locations[i][1] =='undefined'){ description ='';} else { description = locations[i][1];}
                                    if (locations[i][2] =='undefined'){ telephone ='';} else { telephone = locations[i][2];}
                                    if (locations[i][3] =='undefined'){ email ='';} else { email = locations[i][3];}
                                   if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
                                   if (locations[i][7] =='undefined'){ markericon ='';} else { markericon = locations[i][7];}
                                    marker = new google.maps.Marker({
                                        icon: markericon,
                                        position: new google.maps.LatLng(locations[i][5], locations[i][6]),
                                        map: map,
                                        title: locations[i][0],
                                        desc: description,
                                        tel: telephone,
                                        email: email,
                                        web: web
                                    });
                        link = '';            bindInfoWindow(marker, map, locations[i][0], description, telephone, email, web, link);
                             }
                         function bindInfoWindow(marker, map, title, desc, telephone, email, web, link) {
                              var infoWindowVisible = (function () {
                                      var currentlyVisible = false;
                                      return function (visible) {
                                          if (visible !== undefined) {
                                              currentlyVisible = visible;
                                          }
                                          return currentlyVisible;
                                       };
                                   }());
                                   iw = new google.maps.InfoWindow();
                                   google.maps.event.addListener(marker, 'click', function() {
                                       if (infoWindowVisible()) {
                                           iw.close();
                                           infoWindowVisible(false);
                                       } else {
                                           var html= "<div style='color:#000;background-color:#fff;padding:5px;width:150px;'>"+
                                           "<h4 class=''>"+title+"</h4>"+
                                           "<p>"+desc+"</p>"+
                                           "</div>";
                                           iw = new google.maps.InfoWindow({content:html});
                                           iw.open(map,marker);
                                           infoWindowVisible(true);
                                       }
                                });
                                google.maps.event.addListener(iw, 'closeclick', function () {
                                    infoWindowVisible(false);
                                });
                         }   var waypts = [];
                              directionsService = new google.maps.DirectionsService();
                              directionsDisplay = new google.maps.DirectionsRenderer({
                                  suppressMarkers: true
                              });
                              if (locations.length > 1){
                                  for (var i = 0; i < locations.length; i++) {
                                      waypts.push({
                                          location:new google.maps.LatLng(locations[i][5], locations[i][6]),
                                          stopover:true
                                      }); 
                                  };
                                  var request = {
                                      origin: new google.maps.LatLng(locations[0][5], locations[0][6]),
                                      destination: new google.maps.LatLng(locations[locations.length - 1][5], locations[locations.length - 1][6]),
                                      waypoints: waypts,
                                      optimizeWaypoints: true,
                                      provideRouteAlternatives: true,
                                      travelMode: google.maps.DirectionsTravelMode.DRIVING
                                  };
                                  directionsService.route(request, function(response, status) {
                                      if (status == google.maps.DirectionsStatus.OK) {
                                          polylineOptions = {
                                              strokeColor: '#3498db',
                                              strokeWeight: '2'
                                          }
                                          directionsDisplay.setOptions({
                                              polylineOptions: polylineOptions
                                          });
                                          directionsDisplay.setDirections(response);
                                      }
                                  });
                                  directionsDisplay.setMap(map);
                               }
                           

                        }
                        </script>

                        <div id='mapartamento'></div>
                        -->
                    <!--/MAPA-->

                    <div class="container-map">
                        <a href="<?php echo get_post_meta($post->ID,"custom_url_mapa",1);?>" target="_blank"> 
                        <img class="lazy-img" width="100%" height="auto" data-original="<?php echo $img_mapa[0];?>" class="img-responsive" style="max-height:375px"></a>  
                    </div> 

                    <!--SECCION OFERTAS-->
                    <div class="clear padding15" id="03_ofertas"> <!-- col-md-12 col-sm-12 col-xs-12 ofertas clear padding0 contenedor-ofertas -->
                        <h3 class="uppercase margin-bottom-15">
                            <span class="darkblue"><?php echo __("OFERTAS");?> RV </span>
                            <span class="lightblue"><?php the_title(); ?></span>
                        </h3>



                        <!--CONTENEDOR OFERTAS-->
                            <div class="row row-small container-apart-ofertas">
                                <?php

                                $args = array(
                                                "posts_per_page"=>"6",
                                                "post_type"=>"promo",
                                                "post_status"=>array('publish'),
                                                "meta_query"=>array(
                                                                    array(
                                                                        "key"=>"custom_oferta_en_apart",
                                                                        "value"=>$post->ID)
                                                ),
                                                "suppress_filters" => 0
                                );

                                //query_posts( $args );
                                $wp_query = new WP_Query( $args ); // ambos funcionan ok


                                $mostrar_ofertas = true;
                                if (isset($_GET["origen"])) {
                                    if ($_GET["origen"]=="reminder") {
                                        // si llegamos a esta ficha desde un email reminder de neobookings
                                        // NO mostraremos las ofertas
                                        $mostrar_ofertas = false;
                                    }
                                }


                                if ( $wp_query->have_posts() && $mostrar_ofertas  ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();
                                    $metas          = get_post_meta($post->ID);
                                    $titulo         = get_the_title();
                                    $imagen         = wp_get_attachment_image_src( $metas['_thumbnail_id'][0],"thumbnail" );
                                    $precio         = ($metas['custom_precio'][0]) ? $metas['custom_precio'][0] : "0";
                                    $dto         = $metas['custom_descuento'][0];
                                    $pornoche       = ( ($metas['custom_pornoche'][0]!="0") && ($metas['custom_pornoche'][0]!="") ) ? __("noche") : "";

                                    $esHotel        = (isset($metas['custom_oferta_en_apart']) && $metas['custom_oferta_en_apart'][0]!="-")?"nohotel":"hotel";
                                    $hotel_apart    = descrimina_hotel_apartamento($metas, $esHotel);

                                    $link           = get_custom_link($metas, $post->ID);

                                    $idApartWP      = (isset($metas['custom_oferta_en_apart'][0]) ? $metas['custom_oferta_en_apart'][0] : "" );

                                    $idAptoNeo      = get_post_meta( $idApartWP, 'custom_bookings', true );
                                    $arrival        = (isset($metas['custom_dtinicio'][0]) ? $metas['custom_dtinicio'][0] : "" );
                                    $departure      = (isset($metas['custom_dtfin'][0])? $metas['custom_dtfin'][0] : "");
                                    $nights         = (isset($metas['custom_noches'][0])? $metas['custom_noches'][0] : "");
                                    

                                    //echo "<pre class='pornoche uk-hidden'>".print_r($metas['custom_pornoche'][0],true)."</pre>";
                                
                                    include("includes/minioferta.inc.php");

                                    endwhile;

                                else :?>
                                <!--/OFERTA-->
                                <div class="padding5">
                                    <p class="text-center"><strong><?php echo __("Actualmente no hay ofertas especiales para este inmueble.");?> </strong></p>
                                </div>
                            <?php endif; ?>
                            </div>
                            <hr class="fullwidth margin-top-15 margin-bottom-15">
                            <nav class="text-center">
                                <a class="btn bg-lightblue padding10 blanco hilite phone-block text-uppercase text-bold" href="<?php echo __("https://www.rvhotels.es/promos/especial-apartamento/");?>"><?php echo __("Más ofertas de apartamentos");?></a>
                                    <span class="">&nbsp;</span>
                                <a class="btn bg-darkblue padding10 blanco hilite phone-block text-uppercase text-bold" href="<?php echo __("https://www.rvhotels.es/ofertas/");?>"><?php echo __("Todas las ofertas");?></a>
                            </nav>
                            <?php
                            /*
                            echo "<pre class='uk-hidden hidden'>".print_r($wp_query, true)."</pre>"; 
                            */
                            ?>
                        <!--/CONTENEDOR OFERTAS-->   
                    </div>
                    <!--SECCION OFERTAS-->
                </div>   
            </div>

            <aside class="col-md-4 col-sm-4 col-xs-12 saidbar">
                <?php require_once('includes/buscadordispo-apart.php'); ?>

                <?php require_once('includes/widget-info.php'); ?>

                <?php require_once('includes/related-apartments.php'); ?>

                <div class="affix-tools-apt" > <!-- data-spy="affix" data-offset-top="1200" data-offset-bottom="200"  -->

                    <div class="masinfoaux text-center text-large text-bold blanco">
                     <a href="<?php echo __("https://www.rvhotels.es/info-contacto/")."?edificio=".$apartamentoGet;?>" title="<?php echo __("REALIZAR UNA CONSULTA");?>">
                        <i class="fa fa-info-circle"></i> <span class="hidden-xs"><?php echo __("PEDIR INFORMACIÓN");?></span>
                        </a>
                    </div>

                    <!--<div class="lyteShare twitter facebook social-inferior text-center bg-blanco padding15 margin-top-20"><?php echo __("¡Nos encanta compartir!");?><br><br><strong><?php echo __("¿Y a tí?");?></strong><br><br> </div-->

                </div>

            </aside>
        </article>
    </div><!--.container-->
</section>
<script>
!function(e){function t(e,l,a){return(t=d.getElementsByClassName?function(e,t,l){l=l||d;for(var a,n=l.getElementsByClassName(e),s=t?new RegExp("\\b"+t+"\\b","i"):null,r=[],o=0,i=n.length;i>o;o+=1)a=n[o],(!s||s.test(a.nodeName))&&r.push(a);return r}:d.evaluate?function(e,t,l){t=t||"*",l=l||d;for(var a,n,s=e.split(" "),r="",o="http://www.w3.org/1999/xhtml",i=d.documentElement.namespaceURI===o?o:null,h=[],u=0,c=s.length;c>u;u+=1)r+="[contains(concat(' ', @class, ' '), ' "+s[u]+" ')]";try{a=d.evaluate(".//"+t+r,l,i,0,null)}catch(m){a=d.evaluate(".//"+t+r,l,null,0,null)}for(;n=a.iterateNext();)h.push(n);return h}:function(e,t,l){t=t||"*",l=l||d;for(var a,n,s=e.split(" "),r=[],o="*"===t&&l.all?l.all:l.getElementsByTagName(t),i=[],h=0,u=s.length;u>h;h+=1)r.push(new RegExp("(^|\\s)"+s[h]+"(\\s|$)"));for(var c=0,m=o.length;m>c;c+=1){a=o[c],n=!1;for(var p=0,w=r.length;w>p&&(n=r[p].test(a.className),n);p+=1);n&&i.push(a)}return i})(e,l,a)}d=document;var l=new Object;l.twitter={url:"https://twitter.com/intent/tweet?text=%lT%&url=%lU%"},l.facebook={url:"https://www.facebook.com/sharer/sharer.php?u=%lU%&t=%lT%"},l.googleplus={url:"https://plus.google.com/share?url=%lU%&hl=en-US"},l.linkedin={url:"https://www.linkedin.com/shareArticle?mini=true&url=%lU%&title=%lT%"},l.yammer={url:"https://www.yammer.com/home/bookmarklet?bookmarklet_pop=1&v=1&u=%lU%"},l.tumblr={url:"http://www.tumblr.com/share/link?v=3&u=%lU%&t=%lT%"};var a=encodeURIComponent(window.location.href),n=encodeURIComponent(d.title),s="https://www.rvhotels.es/",r="35";sty=d.createElement("style"),sty.type="text/css",rulz=d.createTextNode(".lyteImg {border: 0px; margin: 2px; width:"+r+"; height:"+r+";}"),sty.styleSheet?sty.styleSheet.cssText=rulz.nodeValue:sty.appendChild(rulz),d.getElementsByTagName("head")[0].appendChild(sty),e.sh=function(){lDiv=t("lyteShare","div")[0],lDiv.className=lDiv.className.replace(/lyteShare/,"lyteShared")+" lP",classes=lDiv.className.split(" ");for(x in classes)if(thCl=classes[x],"lyteShared"!=thCl&&"undefined"!=typeof l[thCl]){var e=d.createElement("a");e.href=l[thCl].url.replace("%lU%",a).replace("%lT%",n),e.title=thCl,e.target="_blank",lDiv.appendChild(e);var o=d.createElement("img");o.src=s+thCl+"_"+r+".png",o.className="lyteImg",o.alt=e.title,e.appendChild(o)}}}(window.lyte=window.lyte||{}),function(){var e=window,t=document;e.addEventListener?(e.addEventListener("load",lyte.sh,!1),t.addEventListener("DomContentLoaded",function(){setTimeout("lyte.sh()",750)},!1)):(e.onload=lyte.sh,setTimeout("lyte.sh()",1e3))}();

// BOTON WHATSAPP
var offerTitle  = jQuery('.h1apt').text();
var offerLink   = window.location.href;

jQuery(function($){

    $('.lyteShare').append('<a style="display:none;transform:scale(1.3)translate(-1px,1px)" href="whatsapp://send?text='+offerTitle+' '+offerLink+'"  title="Enviar por Whatsapp" data-action="share/whatsapp/share" class="wa_btn wa_btn_l hidden-sm-up"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAjCAMAAADL21gSAAABoVBMVEUAAAD+/v7+/v7////+/v7////////+/v7+/v7+/v7+/v7+/v7+/v7+/v7////9/f3+/v79/f3////8/Pz+/v7////+/v7////+/v7+/v7////+/v7+/v79/f3////9/f3////////+/v79/f3+/v7+/v7+/v7+/v78/Pz9/f3+/v79/f3+/v7+/v7w8PD+/v78/Pz+/v7+/v4AqFn///8AqVoApFEAnEIAmDoAqlwAoUsAmz8Ap1YAplMAoEkAljYBok0AnkcAnkQAmTwAlzj2/PrY8eQAql0AlDOv48n5/Prt+PLA6NQRrmT0+/fn9u3b8ubG7Nuo38OM17KB0qh7zaFly5xbxpFHwYhUwIY5vH42unottnQasWoWq10AkzAAki7i9OrU8eTM7uC86NKf3b+Z3b+S27mR17KH06thxY9NwosxuXk7uHcxtHAVr2UAiyHg9OnN69jI6dW55MuO2rmh2beH1bB40ah90aZszJ1px5RWxZBewopPvH9MvH4tunxDuHgzt3dAt3YltHAtsmwirmQZrWIPqlwPqVsIpFJDF8oYAAAAM3RSTlMA/vLsDvrTvnHn29iskoh7WlE9MRQH4860saSAdnVjXzQhHBbh38nDt5+bZldLRUMrJgTmVxRAAAACk0lEQVQ4y4XT5XLbUBCGYa+5YU6atMEyt/rEZGaHmZmpzMy96kondiS3mfad8R/7GeuspPVUdDdcE/R6qzvqG1s9Z9fSVYVydK72/lmmx0ewSw6eMLrwt+mwydDOzKej4vzMRMpm7QOVxH8FwOrbX2Y8piixuKm8L9jXvFeBqgHsxMwIz7F4QZJeWyrQ4jI1BOxmFJmJEst8ToKCjmm0zNSyJguKxp8qfvkQoLqyaQ0QtpY0WdSLEQZKbHnSUn0ldJGwohhp9SCV3IvzjhISBVBnCV0G9jKysQjgYUx3kBxfAMjPTDNhSBA46QWsZlXOSVMLoAaGwsB2gufUURAhK7lPZU4C1xnqBN5YP0lbNnqqcq7EfSDIUBCYEzkuPg0gKwpuFOHzaGPIB1owOE43hoHxY9mNBH0YPoa8yBUVa5bErH3wDM+OXEYpeBm6BHyJ2l+ZmwB9XeL4tGKoBs/baAXnGAoB06o9ixEdAZIfM4o6X3i5KOkyZyzkUMVQHfDKtC+SjkXXAGzHxCFgcCKicdI0EGLoFmH9ZPC0GMkCGF4DwR6ZT4yBuhjyE1AUeaai8QlYEREwH4sIeVCvh3Ue2MiUHpeQOBwFayzGL03CPhKrl4B9SS69Rmr8YHwktT4l6uLPJKjbAuW/yi9Gyg+MEyVFN+KckhgBtTlrEAA+iJyTLmjpqLQJouZTNEBICTqvCHJZyZoZzQLu3QsTNhLRxBFnGppmES1iqu9WLVPrcboKzB1/G0+mdr8boiSp0R8zT8C2wOmBF4NTzwhWuUfZseejj/M28TV4XDUR5U5uIU6jwA3XYrJtIVvU3G4IkRWsT3u431PZNRD5atmw/U099fXdTX2eP2slqr7p9/y7O6HG/4jf2t25aw2zopMAAAAASUVORK5CYII=" alt="Whatsapp"></a>');

    $('.ca-es .sharetext1').text('Ens encanta compartir!');
    $('.ca-es .sharetext2').text('I a tu?');
    $('.en-gb .sharetext1').text('We love to share!');
    $('.en-gb .sharetext2').text('Do not you?');
    $('.fr-fr .sharetext1').text('Nous aimons partager!');
    $('.fr-fr .sharetext2').text('Et vous?');

    $('.desktop .affix-tools-apt, .tablet .affix-tools-apt').affix({
      offset: {
        top: 1200,
        bottom: 250
      }
    });

    $('.mobile .affix-tools-apt').affix({
      offset: {
        top: 400
      }
    });
});
</script>

<style>

.mobile .affix-tools-apt.affix {
    position:static!important;
}

@media (max-width:1023px) {.wa_btn{display:inline-block!important;}}

.masinfoaux {
    display:none;
}

.masinfoaux a {
    color:inherit;text-decoration:none;display:block;padding:10px;border:2px solid white;
    border-radius:20px;
}

.masinfoaux a:hover {
    background:rgba(0,0,0,0.15);
}

.mobile .affix-tools-apt.affix .masinfoaux {
    position:fixed;
    display:inline-block;
    right:5px;
    bottom:60px;
    z-index:10;
}

.mobile .affix-tools-apt.affix .masinfoaux a {
    width:30px;
    height:35px;
    padding:0;
    line-height:35px;
    border:0;
    font-size:35px;
    color:rgba(0,0,0,.6);
}



@media (min-width:768px) {


    .affix-tools-apt {
        min-width:280px;
    }

    .affix-tools-apt.affix {
        bottom:30px;
        box-sizing: border-box;
        position:fixed!important;
    }

    .affix-tools-apt.affix > * {
        box-sizing: border-box;
    }

    .affix .masinfoaux {
        display:block;
    }
}

@media (min-width:1200px) {
    .affix-tools-apt {
        min-width:336px;
    }
}
</style>

<script>
    jQuery(function($){

        // Marcar menu ofertas como actual, bug de page-promos y taxonomy-promo-categories
        $('#menu-menuprincipal li:nth-child(2)').addClass('current-menu-item');

        $('.menuseccionapart').clone().appendTo('.nav-clonada');

        function scrollNav() {
          $('.menuseccionapart a, .sticky-nav-mobile-inf a').bind('click', function(){  
            //Animate
            $('html, body').stop().animate({
                scrollTop: $( $(this).attr('href') ).offset().top - 140
            }, 400);
            return false;
          });

          /* sincronizar el cambio de color de los dos menus*/

          $('.nav-01').bind('click',function(){
            $('.menuseccionapart li, .sticky-nav-mobile-inf li').removeClass('activo');
            $('.nav-01').parent().addClass('activo');
          });

          $('.nav-02').bind('click',function(){
            $('.menuseccionapart li, .sticky-nav-mobile-inf li').removeClass('activo');
            $('.nav-02').parent().addClass('activo');
          });

          $('.nav-03').bind('click',function(){
            $('.menuseccionapart li, .sticky-nav-mobile-inf li').removeClass('activo');
            $('.nav-03').parent().addClass('activo');
          });

          $('.nav-04').bind('click',function(){
            $('.menuseccionapart li, .sticky-nav-mobile-inf li').removeClass('activo');
            $('.nav-04').parent().addClass('activo');
          });

          $('.nav-05').bind('click',function(){
            $('.menuseccionapart li, .sticky-nav-mobile-inf li').removeClass('activo');
            $('.nav-05').parent().addClass('activo');
          });



          $('.buscar-dispo-apart-scroll').click(function(){
            $('.buscadordispo-apart').removeClass('imhere').clone().prependTo('.saidbar');
            $('.buscadordispo-apart:first-of-type').remove();
            $('.buscadordispo-apart').addClass('imhere');
            $(window).resize();
            // no hay tu tia, solo lo hace una vez

            $('html, body').stop().animate({
                scrollTop: $( $(this).attr('href') ).offset().top - 130
            }, 400);
          });
        }
        scrollNav();

        $('.mobile .breadcrumbs').clone().appendTo('nav.main');

        // ofertas apartamento cambiar de 4 a 3 columnas
        $('.container-apart-ofertas .minioferta').removeClass('col-md-3 col-sm-3').addClass('col-md-4 col-sm-4');


    });
</script>
<style>
    /*!
     * baguetteBox.js
     * @author  feimosi
     * @version 1.8.2
     * @url https://github.com/feimosi/baguetteBox.js
     */#baguetteBox-overlay{display:none;opacity:0;position:fixed;overflow:hidden;top:0;left:0;width:100%;height:100%;z-index:1000000;background-color:#222;background-color:rgba(0,0,0,.8);-webkit-transition:opacity .5s ease;transition:opacity .5s ease}#baguetteBox-overlay.visible{opacity:1}#baguetteBox-overlay .full-image{display:inline-block;position:relative;width:100%;height:100%;text-align:center}#baguetteBox-overlay .full-image figure{display:inline;margin:0;height:100%}#baguetteBox-overlay .full-image img{display:inline-block;width:auto;height:auto;max-height:100%;max-width:100%;vertical-align:middle;-moz-box-shadow:0 0 8px rgba(0,0,0,.6);box-shadow:0 0 8px rgba(0,0,0,.6)}#baguetteBox-overlay .full-image figcaption{display:block;position:absolute;bottom:0;width:100%;text-align:center;line-height:1.8;white-space:normal;color:#ccc;background-color:#000;background-color:rgba(0,0,0,.6);font-family:sans-serif}#baguetteBox-overlay .full-image:before{content:"";display:inline-block;height:50%;width:1px;margin-right:-1px}#baguetteBox-slider{position:absolute;left:0;top:0;height:100%;width:100%;white-space:nowrap;-webkit-transition:left .4s ease,-webkit-transform .4s ease;transition:left .4s ease,-webkit-transform .4s ease;transition:left .4s ease,transform .4s ease;transition:left .4s ease,transform .4s ease,-webkit-transform .4s ease,-moz-transform .4s ease}#baguetteBox-slider.bounce-from-right{-webkit-animation:bounceFromRight .4s ease-out;animation:bounceFromRight .4s ease-out}#baguetteBox-slider.bounce-from-left{-webkit-animation:bounceFromLeft .4s ease-out;animation:bounceFromLeft .4s ease-out}@-webkit-keyframes bounceFromRight{0%,100%{margin-left:0}50%{margin-left:-30px}}@keyframes bounceFromRight{0%,100%{margin-left:0}50%{margin-left:-30px}}@-webkit-keyframes bounceFromLeft{0%,100%{margin-left:0}50%{margin-left:30px}}@keyframes bounceFromLeft{0%,100%{margin-left:0}50%{margin-left:30px}}.baguetteBox-button#next-button,.baguetteBox-button#previous-button{top:50%;top:calc(50% - 30px);width:44px;height:60px}.baguetteBox-button{position:absolute;cursor:pointer;outline:0;padding:0;margin:0;border:0;-moz-border-radius:15%;border-radius:15%;background-color:#323232;background-color:rgba(50,50,50,.5);color:#ddd;font:1.6em sans-serif;-webkit-transition:background-color .4s ease;transition:background-color .4s ease}.baguetteBox-button:focus,.baguetteBox-button:hover{background-color:rgba(50,50,50,.9)}.baguetteBox-button#next-button{right:2%}.baguetteBox-button#previous-button{left:2%}.baguetteBox-button#close-button{top:20px;right:2%;right:calc(2% + 6px);width:30px;height:30px}.baguetteBox-button svg{position:absolute;left:0;top:0}.baguetteBox-spinner{width:40px;height:40px;display:inline-block;position:absolute;top:50%;left:50%;margin-top:-20px;margin-left:-20px}.baguetteBox-double-bounce1,.baguetteBox-double-bounce2{width:100%;height:100%;-moz-border-radius:50%;border-radius:50%;background-color:#fff;opacity:.6;position:absolute;top:0;left:0;-webkit-animation:bounce 2s infinite ease-in-out;animation:bounce 2s infinite ease-in-out}.baguetteBox-double-bounce2{-webkit-animation-delay:-1s;animation-delay:-1s}@-webkit-keyframes bounce{0%,100%{-webkit-transform:scale(0);transform:scale(0)}50%{-webkit-transform:scale(1);transform:scale(1)}}@keyframes bounce{0%,100%{-webkit-transform:scale(0);-moz-transform:scale(0);transform:scale(0)}50%{-webkit-transform:scale(1);-moz-transform:scale(1);transform:scale(1)}}
@media (min-width:768px) {
    #baguetteBox-overlay .full-image figure img {
    min-width: 66%;
    width:auto;
    height: auto;
    max-width: 100%;
    max-height: 90%;
    }

}
.slides a {cursor:zoom-in;}

.wk-slideshow .slides>li>a:after {
    content:"\f00e";
    font-family:fontawesome;
    display:inline-block;
    position:absolute;
    left:0;
    top:0;
    right:0;
    bottom:0;
    margin:auto;
    font-size:100px;
    line-height:100px;
    width:100px;
    height:100px;
    color:rgba(255,255,255,0);
    text-align: center;
    transition:250ms;
    font-weight:normal;
}
.wk-slideshow .slides>li>a:hover:after {
    color:rgba(255,255,255,.8);
}
</style>
<script src="https://www.rvhotels.es/wp-content/themes/rvhotels/js/baguetteBox.min.js"></script>
<script>
    jQuery('.wk-gallery-showcase').ready(function($){
        baguetteBox.run('.slides');
        
        function ponertitle() {
            aptname = $('.h1apt').text();
            aptname = aptname.trim();
            $('.slides a').attr('title',aptname);
        }
        setTimeout(ponertitle(), 1000);
    });
    // cambiar en el bloque A TENER EN CUENTA, los bullet por otro icono
    jQuery('.tenerencuenta .fa-dot-circle-o').removeClass().addClass('fa fa-exclamation-circle').css('color','tomato');
    jQuery('#registre-turistic').css({
        'height':'50px',
        'overflow':'hidden',
        'text-overflow':'ellipsis',
        'cursor':'pointer',
        'white-space':'nowrap',
        'transition':'1s'});

    jQuery('#registre-turistic').click(function(){
        jQuery(this).css({
            'height':'auto',
            'white-space':'unset',
            'overflow':'visible'
        });
    });
</script>

<?php get_footer(); ?>