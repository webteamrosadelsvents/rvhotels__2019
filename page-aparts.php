<?php
/*
Template Name: Apartamentos
*/
?>
<?php get_header(); ?>
<section class="bg bgaparts">
    <?php// include  ('buscadordispo.php'); ?>
    <div class="container clearfix apartamentos">
        <div class="headerseccion">
            <h1 class="hotels"><?php echo __("Apartamentos en la Costa Brava");?></h1>
            <p class="subtitle"><?php echo __("La mejor selección de apartamentos en la Costa Brava con RV Hotels. ¡Disfruta de tus vacaciones y descubre los rincones más bonitos del Mediterráneo!");?></p>
        </div>
        <div class="clear"></div>
        <div class="bloque clearfix">
        <div class="row">
            <?php // Muestra el resto de apartamentos (el destacado no aparece)
            $numero=1;
            $apartNum=0;
         	$args=array(
           'post_type' => 'apartamento',
           'posts_per_page' => -1
            );
            query_posts($args);
            if (have_posts()) : while (have_posts()) : the_post();
                $apartNum++;
                $zona       =   $arr_ubicas[get_post_meta($post->ID,"custom_zona",1)];
                $novedad    =   (get_post_meta($post->ID,"custom_new",1))?"novedadaparts_".ICL_LANGUAGE_CODE:"";
                $src_img    =   wp_get_attachment_image_src( get_post_meta($post->ID,'_thumbnail_id',1),"thumbnail" );
            ?>
                <a href="<?php echo get_permalink();?>">
                    <article class="apart col-xs-12 col-sm-6 col-md-4 apartNum<?php echo $apartNum;?> <?php echo get_post_meta($post->ID, 'custom_zona', true); ?>">  
                        <div class="fichaaparts <?php echo $novedad;?>">
                            <div class="titleaparts">
                                <h2><span class="rvhotel">RV </span> <?php the_title();?></h2><h3><?php echo $zona;?></h3>    
                            </div>    
                            <div class="container-slide-aparts">
                                <img class="lazy-img" data-original="<?php echo $src_img[0];?>" alt="<?php echo $zona;?>" title="<?php echo $zona;?>">

                                <div class="extra-info">
                                    <div class="precio-desde">
                                        <?php
                                            // proceso el importe:
                                            ob_start();
                                            the_field('apt_preciodesde');
                                            $importe = ob_get_contents();
                                            ob_end_clean();                                        
                                            $importe = number_format($importe , 2 , ".", "");
                                        ?>
                                        <?php echo __("Desde");?> <span><?php echo $importe; ?></span> € / <?php echo __("noche");?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </article>
                </a>

                <!-- SEPARADOR -->
                <?php if ($apartNum==18) : ?>

        </div><!-- cerrar row -->
    </div> <!-- cerrar container -->
</section> <!-- cerrar section bgaparts -->
<div class="ornament-yellow"></div>
<section class="bgofertas">
    <div class="container clearfix">
        <div class="col-md-12 col-sm-12 col-xs-12 padding0-5">
                        <h2 class="tab-inverted"><?php echo __("Apartamentos en Costa Dorada y Costa Azahar");?></h2>
        </div>
        <div class="row">
            <!--<div class="col-xs-12 col-sm-6 col-md-4">&nbsp;</div>-->
                <?php endif; ?>

            <?php endwhile; ?>
            <?php endif; ?>

        </div><!-- end row -->
    </div><!--.container-->
    <br>
</section>

<?php get_footer(); ?>