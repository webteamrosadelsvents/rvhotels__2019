<?php
/* buscador dispo pagina general hoteles */
?>
<div class="buscador-hoteles-container bg-yellowgold buscador-dispo-aptos-h margin-top-40">
    <form id="form-buscador" name="form-buscador" class="" method="post" action="https://bookings.rvhotels.es/" target="_blank">
    <div class="buscador row">
        <!--TITLE-->
        <div class="col-md-12 ">
            <h3 class="bookingtitle darkblue margin-bottom-5 uppercase" style="font-size:120%;line-height: 120%;"><?php echo __("Disponibilidad y reservas");?></h3>
        </div>
        <!--/TITLE-->
        <!--SUBTITLE-->
        <div class="col-md-12 ">
            <div class="bookingError position-relative error-zona alert alert-warning text-center text-small hidden  margin-top-10 margin-bottom-0 animation-doing"><i class="fa fa-exclamation-circle"></i> <?php echo __("Escoge destino o apartamento");?></div>
        </div>
        <!--/SUBTITLE-->
        
        
        <!-- HOTELES -->
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="row mobile-margin-top-10">
                <div class="col-md-11 col-sm-11 col-xs-11">
                    
                    <select class="form-control hot">
                        <option value="-1" selected>- <?php _e('Hoteles', 'rvhotels'); ?> / <?php echo __("Destinos");?> -</option> 
                        <?php /* <!-- CUIDADO, el option con el valor por defecto no poner nunca el atributo DISABLED, que cascamos el funcionamiento en scriptosV2.js --> */?>

                        <?php /* <!-- display in desktop long names and info --> */?>
                        <optgroup label="Costa Brava | Girona" class="hidden-xs">
                            <!--option value="2">RV Hotel Ses Arrels **** | Vall Llobrega, Palamós, Baix Empordà</option-->
                            <option value="3">RV Hotel Palau Lo Mirador **** | Torroella de Montgrí - L'Estartit, Baix Empordà</option>
                            <option value="40">RV Hotel Nautic Park **** | Platja d'Aro, Baix Empordà</option>
                            <option value="41">RV Hotel Golf Costa Brava **** | Santa Cristina d'Aro, Baix Empordà</option>                                
                            <option value="30">RV Hotel GR 92 ** | Torroella de Montgrí - L'estartit, Baix Empordà</option>
                            <option value="38">RV Hotel Nieves Mar *** | L'Escala, Alt Empordà</option>
                        </optgroup>
                        <optgroup label="Costa Dorada | Tarragona" class="hidden-xs">
                            <option value="1">RV Hotel Ametlla Mar **** | L'Ametlla de Mar, Baix Ebre</option>
                        </optgroup>
                        <optgroup label="Pirineo | Lleida" class="hidden-xs">
                            <option value="4">RV Hotel Condes del Pallars *** - Rialp, Pallars Sobirà</option>
                            <option value="36">RV Hotels TUCA **** - Vielha, Vall Aran</option>
                            <option value="37">RV Hotels ORRI *** - Tredòs, Vall Aran</option>
                        </optgroup>
                        <optgroup label="Menorca" class="hidden-xs">
                                <option value="39">RV Hotel Sea Club Menorca **** - Ciutadella de Menorca, Menorca</option>
                        </optgroup>                                       


                        <?php /* <!-- display in mobile short names and info --> */?>
                        <optgroup label="Costa Brava | Girona:" class="text-bold visible-xs">
                            <!--option value="2">Hotel Ses Arrels ****</option-->
                            <option value="3">Palau Lo Mirador ****</option>
                            <option value="40">Hotel Nautic Park ****</option>
                            <option value="41">RV Hotel Golf Costa Brava ****</option>                                
                            <option value="35">Hotel GR 92 **</option>
                            <!--<option value="38">Hotel Nieves Mar ***</option>-->
                        </optgroup>
                        <optgroup label="Costa Dorada | Tarragona:" class="text-bold visible-xs">
                            <option value="1">Hotel Ametlla Mar ****</option>
                        </optgroup>
                        <optgroup label="Pirineo | Lleida:" class="text-bold visible-xs">
                            <option value="4">Condes del Pallars ***</option>
                            <option value="36">TUCA ****</option>
                            <option value="37">ORRI ***</option>
                        </optgroup>
                        <optgroup label="Menorca:" class="text-bold visible-xs">
                                <option value="39">Sea Club Menorca ****</option>
                        </optgroup>                               
                    </select>

                </div>
                <div class="selector-icon padding0 col-md-1 col-sm-1 col-xs-1">
                    <i class="fa fa-hospital-o darkblue"></i>
                </div>
            </div>
        </div>

        <!--/HOTELES-->

        <!--DESTINOS-->
        <input type="hidden" class="form-control dest hidden" value="-1">
        <!--/DESTINOS-->

        <!--APARTAMENTOS-->
        <input type="hidden" class="form-control apt hidden" value="-1">


        <!--/APARTAMENTOS-->

        <!--FECHAS-->
        <!-- date('Y-m-d H:i:s', strtotime($stop_date . ' +1 day')); -->
        <?php $manana = date('d/m/Y', strtotime('+1 day'));?>
        <?php $pasada = date('d/m/Y', strtotime('+2 day'));?>

        <div class="col-md-2 col-sm-6 col-xs-6 mobile-margin-top-10">
            <div class="row">
                <div class=" col-md-10 col-sm-10 col-xs-10">

                    <input type="text" class="form-control" id="arrival" name="arrival" placeholder="- <?php echo __("Check-in");?> -" readonly="readonly" autocomplete="off" value="">
                    <input type="hidden" id="checkin" name="checkin" value="" /> 
                </div>
                <label for="arrival" class="selector-icon padding0 col-md-1 col-sm-2 col-xs-2 hilite">
                    <i class="fa fa-calendar darkblue"></i>
                </label>
            </div>
        </div>
        <div class=" col-md-2 col-sm-6 col-xs-6 mobile-margin-top-10">
            <div class="row">
            <div class=" col-md-10 col-sm-10 col-xs-10">
                <input type="text" class="form-control" id="departure" name="departure" placeholder="- <?php echo __("Check-out");?> -" readonly="readonly" autocomplete="off" value="">
                <input type="hidden" id="checkout" name="checkout" value="" />
            </div>
            <label for="departure" class="selector-icon padding0 col-md-1 col-sm-2 col-xs-2 hilite">
                <i class="fa fa-calendar darkblue"></i>
            </label>
            </div>
        </div>
        <!--/FECHAS-->
        <!--BOTON-->
        <div class="form-group  col-md-2 col-sm-12 col-xs-12 mobile-margin-top-10">
            <div class="row">
            <div class=" col-md-12 col-sm-12">

                <input type="hidden" name="analytics" value="" /><!-- id="analytics" -->
                <input type="hidden" name="adwords" value="" /><!-- id="adwords" -->
                <input type="hidden" type="number" name="adults" value="2">
				<input type="hidden" name="gateway_condes" id="gateway_condes" value="Lz6YZwh2aUsWVB$nSAP:Lw">
				<input type="hidden" name="gateway_ametlla" id="gateway_ametlla" value="QVQPMyFcfVV90W9bve5IMw">
				<input type="hidden" name="gateway_nivesmar" id="gateway_nievesmar" value="9dJUYhOduTmRnuxypTJrTw">
				<input type="hidden" name="gateway_tuca" id="gateway_tuca" value="XNVTCpSQgfuZflMqdx4crQ">
				<input type="hidden" name="gateway_orri" id="gateway_orri" value="5hTbF9la6YChNpzlNXVhdg">
                <input type="hidden" name="gateway_palau" id="gateway_palau" value="PuBUcheCderU257gChVsCQ">
                <input type="hidden" name="gateway_gr92" id="gateway_gr92" value="yfRd3N7ZTegDF8GQDv3Acg">                
                <!--input type="hidden" name="gateway_sesarrels" id="gateway_sesarrels" value="uW9ZG0$rN9GYUeoyru6HCA"-->
                <input type="hidden" name="gateway_menorca" id="gateway_menorca" value="1635">
                <input type="hidden" name="gateway_nautic" id="gateway_nautic" value="dSa8BOwU4hSLoD%24LMjsmdA">
                <input type="hidden" name="gateway_hotelgolf" id="gateway_hotelgolf" value="PVS5%24aaAmBkCflVpGn456Q">                        
                <input type="hidden" name="gateway" id="gateway" value="">

                <input type="hidden" name="check_in_date" id="check_in_date" value="">
                <input type="hidden" name="check_out_date" id="check_out_date" value="">

                <button type="button" class="btn buscar bg-lightblue blanco hilite" style="width: 100%;font-size: 17px!important;"><?php echo __("RESERVAR");?></button>
            </div> 
            </div>   
        </div>
        
    </div>
    </form>
</div>