<nav class="text-center clearfix clear position-relative catswitcher">
    <p class="blanco"><?php echo __("Ver más ofertas de la categoría");?>:</p>
    <p>
            <select id="catSwitcher" class="catSwitcher">
                <option disabled value="<?php echo __("Escoger tipo");?>"><?php echo __("Escoger tipo");?></option>
            <?php 
                foreach ($taxonomies as $taxonomy) {

                    $term_meta = get_option( "taxonomy_$taxonomy->term_id" );
                    $esoculta = get_field('catpromo_visible', $taxonomy); //custom field categoria oculta en front
                    $url=get_bloginfo("url")."/promos/".$taxonomy->slug."/";

                    if (!$esoculta) {
                    ?>
                        <option value="<?php echo $url;?>"><?php echo $taxonomy->name;?></option>
                    <?php
                    }
                }
            ?>
                <option value="<?php echo __("https://www.rvhotels.es/ofertas/");?>#viewallpromos"><?php echo __("Ver todas");?></option>
            </select>
            <script>
                jQuery(function($){
                    $('#catSwitcher').change(function(){
                        goto = $(this).val();
                        window.location=goto;
                    })
                });
            </script>
    </p>
</nav>