<?php // OFERTAS DESTACADAS HOME ?>
<section class="destacadoshome">
    <div class="clear text-center margin-top-20 margin-bottom-40">
        <h3 class="subtitle-featured uppercase text-extrabold darkblue text-kern-extra text-15x"><?php echo __("Ofertas destacadas");?></h3>
    </div>

    <div class="row">
        <div class="col-md-12 container-ofertashome margin-top-10 padding0 clear">
            <?php
            $args = array(
                "posts_per_page"=>"4",
				"orderby"=> "time",
				"order"=>"ASC",
                "post_type"=>"promo",
                "post_status"=>array('publish'),
                "meta_query"=>array(
                    array(
                        "key"=>"custom_feat",
                        "value"=>1
                    ),
                    array(
                        "key"=>"custom_ofertaoculta",
                        "value"=>0
                    )
                )
            );

			/* $query = new WP_Query( array( 'post_type' => 'page' ) ); $posts = $query->posts; foreach($posts as $post) { // Do your stuff, e.g. // echo $post->post_name; } */
			
			
            query_posts( $args ); 
            while( have_posts()) : the_post();
                $metas          = get_post_meta($post->ID);
                $titulo         = get_the_title();
                $imagen         = wp_get_attachment_image_src( $metas['_thumbnail_id'][0],"thumbnail" );
                $precio         = $metas['custom_precio'][0];
                $dto            = $metas['custom_descuento'][0];
                $esHotel        = (isset($metas['custom_oferta_en_apart']) && $metas['custom_oferta_en_apart'][0]!="-")?"nohotel":"hotel";
                $hotel_apart    = descrimina_hotel_apartamento($metas, $esHotel);
                $link           = get_custom_link($metas, $post->ID);

                $precioSinDec   = explode(".",$precio);
                $precioSinDec   = $precioSinDec[0];
            ?>
			<?php include('minioferta.inc.php');?>
            <?php endwhile; ?>
            <?php wp_reset_query();?>
        </div>
    </div>
</section>