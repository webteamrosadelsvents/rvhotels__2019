 <?php
 /* buscador dispo home nuevo rvhotels */
 /* VER js/scriptosV2.js */
 ?>
    <div class="buscador-container col-lg-4  col-md-5 col-sm-5 col-xs-12 padding5 overflow-hidden">
        <div class="buscador clearfix">

            <div class="col-md-12 padding5">
                <h2 class="margin-bottom-10"><?php echo __("Disponibilidad y reservas");?></h2>
            </div>
            <div class="padding10">
                <div class="col-xs-6 col-md-6 padding0 border-bottom-blue">
                    <button class="tablabel tab-hoteles text-center activa" id="tab-hoteles"><?php echo __("Hotel");?>
                    </button>
                </div>
                <div class="col-xs-6 col-md-6 padding0 border-bottom-blue">
                    <button class="tablabel tab-aptos text-center" id="tab-aptos"><?php echo __("Apartamento");?>
                    </button>
                </div>
            </div>
            <form id="form-buscador" name="form-buscador" class="" method="post" action="https://bookings.rvhotels.es/" target="_blank">

                <div class="padding5 col-md-12 col-sm-12 col-xs-12 combo-container animation-channleft" id="combo-hoteles">
                    <div class="padding5 col-md-12 col-sm-12 col-xs-12 text-left ">
                        <p class="text-left clear"><?php echo __("Escoge entre nuestros hoteles en la zona que prefieras:");?></p>
                        <div class="bookingError position-relative error-zona alert alert-warning text-center text-small hidden padding5 margin-top-10 margin-bottom-0 animation-doing"><?php _e('Seleccione un hotel, apartamento o destino', 'rvhotels'); ?></div>
                    </div>
                    <div class="padding5 col-md-11 col-sm-11 col-xs-11">
                        
                        <select class="form-control hot">
                            <option value="-1" selected>- <?php _e('Hoteles', 'rvhotels'); ?> / <?php echo __("Destinos");?> -</option> 
                            <?php /* <!-- CUIDADO, el option con el valor por defecto no poner nunca el atributo DISABLED, que cascamos el funcionamiento en scriptosV2.js --> */?>

                            <?php /* <!-- display in DESKTOP long names and info --> */?>
                            <optgroup label="Costa Brava | Girona" class="hidden-xs">
                                <!--option value="2">RV Hotel Ses Arrels **** | Vall Llobrega, Palamós, Baix Empordà</option-->
                                <option value="3">RV Hotel Palau Lo Mirador **** | Torroella de Montgrí - L'Estartit, Baix Empordà</option>
                                <option value="40">RV Hotel Nautic Park **** | Platja d'Aro, Baix Empordà</option>
                                <option value="41">RV Hotel Golf Costa Brava **** | Santa Cristina d'Aro, Baix Empordà</option>                                
                                <option value="35">RV Hotel GR 92 ** | Torroella de Montgrí - L'Estartit, Baix Empordà</option>
                                <option value="38">RV Hotel Nieves Mar *** | L'Escala, Alt Empordà</option>
                            </optgroup>
                            <optgroup label="Costa Dorada | Tarragona" class="hidden-xs">
                                <option value="1">RV Hotel Ametlla Mar **** | L'Ametlla de Mar, Baix Ebre</option>
                            </optgroup>
                            <optgroup label="Pirineo | Lleida" class="hidden-xs">
                                <option value="4">RV Hotel Condes del Pallars *** - Rialp, Pallars Sobirà</option>
                                <option value="36">RV Hotels Tuca **** - Vielha, Vall Aran</option>
                                <option value="37">RV Hotels Orri *** - Tredòs, Vall Aran</option>                                
                            </optgroup>
                            <optgroup label="Menorca" class="hidden-xs">
                                <option value="39">RV Hotel Sea Club Menorca **** - Ciutadella de Menorca, Menorca</option>
                            </optgroup>                            


                            <?php /* <!-- display in MOBILE short names and info --> */?>
                            <optgroup label="Costa Brava | Girona:" class="text-bold visible-xs">
                                <!--option value="2">Hotel Ses Arrels ****</option-->
                                <option value="3">Palau Lo Mirador ****</option>
                                <option value="40">Hotel Nautic Park ****</option>
                                <option value="41">RV Hotel Golf Costa Brava ****</option>                                
                                <option value="35">GR 92 **</option>
                                <option value="38">Nieves Mar ***</option>
                            </optgroup>
                            <optgroup label="Costa Dorada | Tarragona:" class="text-bold visible-xs">
                                <option value="1">Ametlla Mar ****</option>
                            </optgroup>
                            <optgroup label="Pirineo | Lleida:" class="text-bold visible-xs">
                                <option value="4">Condes del Pallars ***</option>
                                <option value="36">Tuca ****</option>
                                <option value="37">Orri ***</option>
                            </optgroup>
                            <optgroup label="Menorca:" class="text-bold visible-xs">
                                <option value="39">Sea Club Menorca ****</option>
                            </optgroup>                                 
                        </select>

                    </div>
                    <div class="selector-icon padding0 col-md-1 col-sm-1 col-xs-1">
                        <i class="fa fa-hospital-o"></i>
                    </div>
                </div>

                <div class="padding5 col-md-12 col-sm-12 col-xs-12  combo-container animation-channright hidden" id="combo-aptos">
                    <div class="padding5 col-md-12 col-sm-12 col-xs-12 text-left ">
                        <p class="text-left clear"><?php echo __("Escoge entre nuestros apartamentos y villas, en la zona que prefieras.");?></p>
                        <div class="bookingError position-relative error-zona alert alert-warning text-center text-small hidden padding5 margin-top-10 margin-bottom-0 animation-doing"><?php _e('Seleccione un hotel, apartamento o destino', 'rvhotels'); ?></div>
                    </div>

                    <div class="padding5 col-md-11 col-sm-11 col-xs-11">
                        <select class="form-control apt">
                            <option value="-1">- <?php _e('Apartamentos', 'rvhotels'); ?> -</option>

                            <?php /* <!-- display in DESKTOP long names and info --> */?>
                            <optgroup label="Costa Brava - l'Escala" class="hidden-xs">
                                <option value="28">Apartamentos - RV Hotels Provenzal</option>
                            </optgroup>

                            <optgroup label="Costa Brava - l'Estartit - Illes Medes" class="hidden-xs">
                                <option value="6" >Apartamentos - RV Hotels Tropik</option>
                                <option value="16">Apartamentos - RV Hotels Estartit Confort</option>
                                <option value="7" >Apartamentos - RV Hotels Club Torrevella</option>

                                <option value="32" >Apartamentos - RV Hotels Club Torrevella - Heemsteede</option>
                                <option value="33" >Villas - RV Hotels Club Torrevella - Casa Heemsteede</option>
                                <!--option value="34" >Villas - RV Hotels Club Torrevella - Casa Breda</option-->

                                <option value="8" >Apartamentos - RV Hotels Els Salats</option>
                                <option value="9" >Apartamentos - RV Hotels La Pineda</option>
                                <option value="10">Apartamentos - RV Hotels Dúplex Bonsol</option>
                                <!--<option value="11">Apartamentos - RV Hotels Del Sol</option>-->
                                <option value="12">Apartamentos - RV Hotels Villas Piscis</option>
                                <option value="29">Apartamentos - RV Hotels Quijote</option>
                            </optgroup>

                            <optgroup label="Costa Brava - Platja d'Aro" class="hidden-xs">
                                <option value="13">Apartamentos - RV Hotels Treumal Park</option>
                                <option value="30">Apartamentos - RV Hotels Benelux</option>
                            </optgroup>

                            <optgroup label="Costa Brava - Blanes" class="hidden-xs">
                                <option value="17">Apartamentos - RV Hotels Lotus</option>
                                <option value="18">Apartamentos - RV Hotels Ses Illes</option>
                                <option value="19">Villas - RV Hotels Villa de Madrid</option>
                            </optgroup>

                            <?php /* <!-- display in MOBILE SHORT names and info --> */?>
                            <optgroup label="Costa Brava - l'Escala" class="text-bold visible-xs">
                                <option value="28">Provenzal</option>
                            </optgroup>

                            <optgroup label="Costa Brava - l'Estartit - Illes Medes" class="text-bold visible-xs">
                                <option value="6" >Tropik</option>
                                <option value="16">Estartit Confort</option>
                                <option value="7" >Club Torrevella</option>

                                <option value="32" >Club Torrevella - Heemsteede</option>
                                <option value="33" >Club Torrevella - Casa Heemsteede</option>
                                <option value="34" >Club Torrevella - Casa Breda</option>

                                <option value="8" >Els Salats</option>
                                <option value="9" >La Pineda</option>
                                <option value="10">Dúplex Bonsol</option>
                                <option value="11">Del Sol</option>
                                <option value="12">Villas Piscis</option>
                                <option value="29">Quijote</option>
                            </optgroup>

                            <optgroup label="Costa Brava - Platja d'Aro" class="text-bold visible-xs">
                                <option value="13">Treumal Park</option>
                                <option value="30">Benelux</option>
                            </optgroup>

                            <optgroup label="Costa Brava - Blanes" class="text-bold visible-xs">
                                <option value="17">Lotus</option>
                                <option value="18">Ses Illes</option>
                                <option value="19">Villa de Madrid</option>
                            </optgroup>

                        </select>
                    </div>
                    <div class="selector-icon padding0 col-md-1 col-sm-1 col-xs-1">
                        <i class="fa fa-home"></i>
                    </div>

                    <?php /* <!--DESTINOS *** DESTRUIR CUANDO SE ADCUE EL SCRIPT --> */ ?>
                    <input type="hidden" name="dest" class="dest" value="-1">

                    
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12 padding5">
                    <p><?php echo __("Selecciona la fecha de  entrada y salida");?>:</p>
                    <div class="bookingError position-relative error-fecha alert alert-warning text-center text-small padding5 margin-top-10 hidden margin-bottom-0 animation-doing"><?php _e('Selecione un afecha de entrada y de salida', 'rvhotels'); ?></div>
                </div>

                <?php /*
                    <!-- date('Y-m-d H:i:s', strtotime($stop_date . ' +1 day')); -->
                
                <?php $manana = date('d/m/Y', strtotime('+1 day'));?>
                <?php $pasada = date('d/m/Y', strtotime('+2 day'));?>
                <?php /*
                <!--
                <p class="text-left hidden">Tumarrou: <b><?php echo $manana; ?></b><br>
                    Pasaoutumarrou: <b><?php echo $pasada; ?></b>
                    </p>
                -->
                */ ?>
                <div class="padding5 col-md-4 col-sm-6 col-xs-6">
                    <div class="padding5 col-md-10 col-sm-10 col-xs-10">

                        <input type="text" class="form-control" id="arrival" name="arrival" placeholder="- <?php echo __("Check-in");?> -" readonly="readonly" autocomplete="off" value="">
                        <input type="hidden" id="checkin" name="checkin" value="" /> 
                    </div>
                    <label for="arrival" class="selector-icon padding0 col-md-1 col-sm-2 col-xs-2 hilite">
                        <i class="fa fa-calendar"></i>
                    </label>
                </div>
                <div class="padding5 col-md-4 col-sm-6 col-xs-6">

                    <div class="padding5 col-md-10 col-sm-10 col-xs-10">
                        <input type="text" class="form-control" id="departure" name="departure" placeholder="- <?php echo __("Check-out");?> -" readonly="readonly" autocomplete="off" value="">
                        <input type="hidden" id="checkout" name="checkout" value="" />
                    </div>
                    <label for="departure" class="selector-icon padding0 col-md-1 col-sm-2 col-xs-2 hilite">
                        <i class="fa fa-calendar"></i>
                    </label>
                </div>

                <div class="form-group padding5 col-md-4 col-sm-12 col-xs-12">
                    <div class="padding5 col-md-12 col-sm-12">
                        <input type="hidden" name="analytics" value="" /><!-- id="analytics" -->
                        <input type="hidden" name="adwords" value="" /><!-- id="adwords" -->
                        <input type="hidden" type="number" name="adults" value="2">
                        <input type="hidden" name="gateway_condes" id="gateway_condes" value="Lz6YZwh2aUsWVB$nSAP:Lw">
                        <input type="hidden" name="gateway_ametlla" id="gateway_ametlla" value="QVQPMyFcfVV90W9bve5IMw">
						<input type="hidden" name="gateway_nivesmar" id="gateway_nievesmar" value="9dJUYhOduTmRnuxypTJrTw">
						<input type="hidden" name="gateway_tuca" id="gateway_tuca" value="XNVTCpSQgfuZflMqdx4crQ">
						<input type="hidden" name="gateway_orri" id="gateway_orri" value="5hTbF9la6YChNpzlNXVhdg">
                        <input type="hidden" name="gateway_palau" id="gateway_palau" value="PuBUcheCderU257gChVsCQ">
                        <input type="hidden" name="gateway_gr92" id="gateway_gr92" value="yfRd3N7ZTegDF8GQDv3Acg">
                        <!--input type="hidden" name="gateway_sesarrels" id="gateway_sesarrels" value="uW9ZG0$rN9GYUeoyru6HCA"-->
                        <input type="hidden" name="gateway_menorca" id="gateway_menorca" value="1635">
                        <input type="hidden" name="gateway_nautic" id="gateway_nautic" value="dSa8BOwU4hSLoD%24LMjsmdA">
                        <input type="hidden" name="gateway_hotelgolf" id="gateway_hotelgolf" value="PVS5%24aaAmBkCflVpGn456Q">                        
                        <input type="hidden" name="gateway" id="gateway" value="">

                        <input type="hidden" name="check_in_date" id="check_in_date" value="">
                        <input type="hidden" name="check_out_date" id="check_out_date" value="">

                        <button type="button" class="btn btn-primary buscar"><?php echo __("RESERVAR");?></button>
                    </div>    
                </div>
            </form>
            <!--/BOTON-->
        </div>
    </div>