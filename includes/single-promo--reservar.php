<script type="text/javascript">
	jQuery("#custom_dtinicio").val("<?php echo $arrival; ?>");
    jQuery("#custom_dtfin").val("<?php echo $departure; ?>");
</script>

<?php if (!$promoExpired) {

$hotelname = resuelveHotel($idHotelWP);
$motor = resuelveMotor($hotelname);
echo "<script>console.log('Hotel :".$hotelname." Motor: ".$motor."');</script>";

// Booking engine - The Booking Button

//if ( $motor=="bookingbutton" ) {
    // ejemplo link :
    // https://app.thebookingbutton.com/properties/hotelcolldirect?locale=es&check_in_date=22-04-2017&check_out_date=23-04-2017&number_adults=2
    // siteminder bookingbutton funciona con un GET normal

    //switch ($hotelname) {
        // COMENTO PALAU Y GR92 QUE YA NO SON DE BOOKINGBUTTON, PASAN A SER DE BOOKINGS
        //case "gr92" : $hotelUrlName = "hotelcolldirect";break;
        //case "palau" : $hotelUrlName = "palaulomiradordirect";break;
        //case "sesarrels" : $hotelUrlName = "sesarrelsdirect";break;

    //}
    //$smdates ="";

    //if ($arrival!="") {
      //  $arrival = str_replace("/", "-", $arrival);
      //  $smdates.="&check_in_date=".$arrival;
    //}

    //if ($departure!="") {
      //  $departure = str_replace("/", "-", $departure);
      //  $smdates.="&check_out_date=".$departure;
    //}

    //$bookingLink = "http://app.thebookingbutton.com/properties/".$hotelUrlName."?locale=".$idiomalocal.$smdates."&number_adults=2";

    //echo '<a rel="nofollow" href="'.$bookingLink.'" class="btn-reservaroferta btn-bookingbutton display-inline-block uk-button uk-button-large blanco button-large text-uppercase text-bold" title="'.__("Reservar").'">'.__("Reservar").$reservarsolohotel.'</a>';

//}

if ( $motor=="bookings" ) {
    switch ($hotelname) {
        case "ametlla" :
            $bookingGateway = "QVQPMyFcfVV90W9bve5IMw";
            $hotelAnalytics = "UA-1651542-4";
            $hotelAdwords   = "1058507870";
            break;

        case "nievesmar" :
			$bookingGateway = "9dJUYhOduTmRnuxypTJrTw";
			$hotelAnalytics = "UA-112881849-1";
			$hotelAdwords   = "1058507870";
			break;
			
        case "tuca" :
			$bookingGateway = "XNVTCpSQgfuZflMqdx4crQ";
			$hotelAnalytics = "UA-1651542-79";
			$hotelAdwords   = "1058507870";
			break;

        case "orri" :
			$bookingGateway = "5hTbF9la6YChNpzlNXVhdg";
			$hotelAnalytics = "UA-107596064-1";
			$hotelAdwords   = "1058507870";
			break;

		case "condes" :
			$bookingGateway = "Lz6YZwh2aUsWVB\$nSAP:Lw";
			$hotelAnalytics = "UA-1651542-76";
			$hotelAdwords   = "1058507870";
            break;

        case "palau" :
			$bookingGateway = "PuBUcheCderU257gChVsCQ";
			$hotelAnalytics = "UA-1651542-45";
			$hotelAdwords   = "1058507870";
            break;            
            
        case "gr92" :
			$bookingGateway = "yfRd3N7ZTegDF8GQDv3Acg";
			$hotelAnalytics = "UA-1651542-78";
			$hotelAdwords   = "1058507870";
            break;    
            
        //case "sesarrels" :
		//	$bookingGateway = "uW9ZG0\$rN9GYUeoyru6HCA";
		//	$hotelAnalytics = "UA-1651542-46";
		//	$hotelAdwords   = "1058507870";
		//	break;   
			
		case "menorca" :
			$bookingGateway = "1635";
			$hotelAnalytics = "UA-1651542-80";
			$hotelAdwords   = "1058507870";
			break;   		
			
		case "nautic" :
			$bookingGateway = "dSa8BOwU4hSLoD%24LMjsmdA";
			$hotelAnalytics = "UA-1651542-80";
			$hotelAdwords   = "1058507870";
			break; 

		case "hotelgolf" :
			$bookingGateway = "PVS5%24aaAmBkCflVpGn456Q";
			$hotelAnalytics = "UA-1651542-80";
			$hotelAdwords   = "1058507870";
			break; 					
    }
	
	echo "<script type='text/javascript'>
				jQuery(document).ready(function($){
					if ($('#ocultar_submit').val() == \"\" || typeof $('#ocultar_submit').val() === \"undefined\") {
							var btn_html = \"<form id='reservaoferta' class='' action='https://bookings.rvhotels.es/".$idiomalocal."/step-1?id=".$bookingGateway."' method='post' name='reservaoferta'>".
												"<input type='hidden' name='gateway' value='".$bookingGateway."'>".
												"<input type='hidden' name='arrival' id='arrival' value='".$arrival."'>".
												"<input type='hidden' name='departure' id='departure' value='".$departure."'>".
												"<input type='hidden' name='adults' value='2'>".
												"<input type='hidden' name='nights' value='".$nights."'>".
												"<input type='hidden' name='lang' value='".$lang."'>".
												"<input name='analytics' value='".$hotelAnalytics."' type=hidden />".
												"<input name='adwords' value='".$hotelAdwords."' type=hidden />".
												"<input type='submit' class='btn-reservaroferta display-inline-block uk-button uk-button-large blanco button-large text-uppercase text-bold' value='".__("Reservar").$reservarsolohotel."'/></form>\";
						// aqui hariamos el prepend.
						$('#bookingblock div div.text-left')
							.first()
							.prepend(btn_html);
					}
				});
			</script>";
}
	
//if ( $motor=="acinet" ) {
//    switch ($hotelname) {
//        case "condes" :
//            $acinetGateway  = "Lz6YZwh2aUsWVB\$nSAP:Lw";
//            $hotelAnalytics = "UA-1651542-76";
//            $hotelAdwords   = "1058507870";
//            break;
//	}
//	
//    echo "<script>console.log('Gateway :".$acinetGateway."');</script>";
//    echo "<form id='reservaoferta' class='' action='https://bookings.rvhotels.es/".$idiomalocal."/step-1?id=".$acinetGateway."' method='post' name='reservaoferta'>".
//        "<input type='hidden' name='gateway' value='".$acinetGateway."'>".
//        "<input type='hidden' name='arrival' id='arrival' value='".$arrival."'>".
//		"<input type='hidden' name='departure' id='departure' value='".$departure."'>".
//        "<input type='hidden' name='adults' value=''>".
//        "<input type='hidden' name='nights' value='".$nights."'>".
//        "<input type='hidden' name='lang' value='".$lang."'>".
//        "<input name='analytics' value='".$hotelAnalytics."' type=hidden />".
//        "<input name='adwords' value='".$hotelAdwords."' type=hidden />".
//        "<input type='submit' class='btn-reservaroferta display-inline-block uk-button uk-button-large blanco button-large text-uppercase text-bold' value='".__("Reservar").$reservarsolohotel."'/>".
//     "</form>";
//
//}

// boton forfait

echo $botonforfait;

}
?>