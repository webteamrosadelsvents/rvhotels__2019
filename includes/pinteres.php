<section class="margin-top-30 pinterest-wall grid"></section>
<div id="adicionales" class="hidden"></div>
<div class="loading-results padding20 text-center" style="visibility:hidden"><i class="fa fa-spinner text-3x anim360 blanco"></i></div>

<script src="https://npmcdn.com/masonry-layout@4.0/dist/masonry.pkgd.min.js"></script>
<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>

<script>
	jQuery(function($){
  		var $grid;
  		var offset   = 0;
  		var bloquear = false;
  		var semaforo = true;
  		var tamanyo  = 150;
		if ($("body.mobile").length) { tamanyo = 350; }
  		console.log(tamanyo);
		function initmasonry() {
    		$grid = $('.grid').masonry({
      			"itemSelector": ".grid-item",
      			"gutter": 20,
      			"percentPosition": true,
      			"transitionDuration": '0.8s'
    		});
    		$('.lazy-img:not([src])' ).lazyload({
        		failure_limit : 6,
        		effect : "fadeIn"
    		});
    		$grid.imagesLoaded().progress( function() {
      			$grid.masonry();
    		});
  		}
		$(".grid").load("https://www.rvhotels.es/cargarposts/?postid=<?php echo $this_post;?>&offset="+offset, function() {
           	initmasonry();
  		});
		$(window).scroll(function() {
    		if( $(window).scrollTop() + $(window).height() >= ($(document).height() - tamanyo )) {
      			$('.loading-results').css('visibility','visible');
      			if (!bloquear) {
        			if (semaforo) {
        				offset += 5;      
        				semaforo = false;
        				$("#adicionales").load("https://www.rvhotels.es/cargarposts/?offset="+offset, function() {
           					var $items = $("#adicionales .grid-item");
           					if ($items.length<5) {
           						$('.loading-results').remove();
           						bloquear=true;
           					} else {
           						$('.loading-results').css('visibility','hidden');
           					}
							$grid.append( $items )
							.masonry( 'appended', $items );
							$('.lazy-img:not([src])').lazyload({
                   				failure_limit : 6,
                   				effect : "fadeIn"
           					});
							$grid.imagesLoaded().progress( function() {
           						$grid.masonry();
           					});
           					semaforo=true;
        				});
        			}
      			}
    		}
  		});
	});
</script>