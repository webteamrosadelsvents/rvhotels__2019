<div class="apart col-md-4 col-sm-4 col-xs-12 margin-bottom-20">
<a href="<?php echo get_permalink();?>">
<article class="fichaaparts <?php echo $novedad;?>">       
    <div class="titleaparts">
    <h2><span class="rvhotel">RV</span> <?php the_title();?></h2> <h3><?php echo $zona;?></h3>    
    </div>    
    <div class="container-slide-aparts">

        <img class="lazy-img" data-original="<?php echo $src_img[0];?>">

        <div class="extra-info">
            <div class="precio-desde">
                <?php
                // proceso el importe:
                ob_start();
                the_field('apt_preciodesde');
                $importe = ob_get_contents();
                ob_end_clean();                                        
                $importe = number_format($importe , 2 , ".", "");
                ?>
                <?php echo __("Desde");?> <span><?php echo $importe; ?></span> € / <?php echo __("noche");?>
            </div>
        </div>

    </div>
</article>
  </a>
</div>