<a href="<?php echo __("https://www.rvhotels.es/info-contacto/")."?edificio=".$apartamentoGet;?>" title="<?php echo __("REALIZAR UNA CONSULTA");?>" style="color:inherit;text-decoration:none">
<div class="info-widget padding15 bg-lightblue text-center margin-bottom-15">
	<div class="bg-white padding15">
	<h5 class="almostblack"><?php echo __("¿TIENES DUDAS?");?></h5>

	<hr style="min-width:100%;">

	<p><?php echo __("¡CONTACTA CON NOSOTROS Y TE AYUDAREMOS CON TU RESERVA!");?></p>

	<p class="big-icon lightblue"><i class="fa fa-envelope-square"></i></p>

	<p>info@rvhotels.es</p>

	<p class="big-icon lightblue"><i class="fa fa-phone-square"></i></p>

	<p class="lead"><b>+34 93 503 60 39</b></p>

	<p><b><?php echo __("Lunes - Viernes");?></b></p>

	<p>09:00 - 14:00 / 15:00 - 18:00</p>
	</div>
</div>
</a>