<div class="container idiomas">
	<?php 
//	do_action('icl_language_selector'); 
	$langos = icl_get_languages();
	/*
	echo "<pre>";
	print_r($langos);
	echo "</pre>";
	*/
	?>
	<div id="lang_sel_list" class="lang_sel_list_horizontal">
		<ul>
			<li class="icl-es">
				<?php $sel=($langos['es']['active'])?"lang_sel_sel":"lang_sel_other";?>
				<a href="<?php echo $langos['es']['url']; ?>" title="<?php echo $langos['es']['name']; ?>" class="<?php echo $sel;?>"></a>
			</li>
			<li class="icl-ca">
				<?php $sel=($langos['ca']['active'])?"lang_sel_sel":"lang_sel_other";?>
				<a href="<?php echo $langos['ca']['url']; ?>" title="<?php echo $langos['ca']['name']; ?>" class="<?php echo $sel;?>"></a>
			</li>
			<li class="icl-en">
				<?php $sel=($langos['en']['active'])?"lang_sel_sel":"lang_sel_other";?>
				<a href="<?php echo $langos['en']['url']; ?>" title="<?php echo $langos['en']['name']; ?>" class="<?php echo $sel;?>"></a>
			</li>
			<li class="icl-fr">
				<?php $sel=($langos['fr']['active'])?"lang_sel_sel":"lang_sel_other";?>
				<a href="<?php echo $langos['fr']['url']; ?>" title="<?php echo $langos['fr']['name']; ?>" class="<?php echo $sel;?>"></a>
			</li>
		</ul>
	</div>
</div>