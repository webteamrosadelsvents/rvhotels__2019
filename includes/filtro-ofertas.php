<div class="text-center" id="filtro-ofertas-container" data-spy="affix" data-offset-top="1200">
    <div class="container">
    <section class="filtro-ofertas">
        <div class="row row-small">
            <div class="sf-checkbox-wrapper col-md-4 col-xs-12 text-center">
                <i class="fa fa-search"></i> <?php echo __("Ofertas");?>:&nbsp;
                <label><input type="checkbox" value="hotel" id="tipo-hotel" name=""><span class="customcheck"></span> <span class="leyenda"><?php echo __("Hotel");?></span></label><!--
                --><label><input type="checkbox" value="nohotel" id="tipo-apartamento" name=""><span class="customcheck" ></span> <span class="leyenda"><?php echo __("Apartamento");?></span></label>
            </div>
            <div class="col-md-4 col-xs-6 text-center nowrap">
                <label for="destino" class="small-label hidden-xs"><?php echo __("¿Donde?");?></label>
                <select id="destino" name="destino" class="form-control unaUotra display-inline-block">
                    <?php
                    foreach ($arr_ubicas as $key => $value) {
                    ?>
                    <option value="<?php echo $key;?>"><?php echo $value;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
           <div class="col-md-4 col-xs-6 text-center nowrap">
                <label for="categoria" class="small-label hidden-xs"><?php echo __("Categoria");?></label>
                <select id="categoria" name="categoria" class="form-control unaUotra display-inline-block">
                    <option value=""><?php echo __("Cualquier categoría");?></option>
                    <?php
                    foreach ($taxonomies as $taxonomy) {
                        $catPromoOculta = get_field('catpromo_visible',$taxonomy);
                        if ($catPromoOculta!=1) { // se pueden ocultar categorias en el backend de wordpress, revisar campos
                    ?>
                    <option value="<?php echo "cat-".$taxonomy->slug;?>"><?php echo $taxonomy->name;?></option>
                    <?php }
                    }
                    ?>
                </select>
            </div>
            <input type="hidden" id="tipodto" name="tipodto" value=""/>
        </div>
    </section>
    </div>
</div>
<script>
// FILTRO

jQuery(document).ready(function($){
    

    var filtros ="";

    function filtra(filtros) {
        console.log("filtrando "+filtros);
        $('.grid').removeClass('nores');
        $('.grid').isotope({
              filter: filtros,
              "itemSelector": "article",
              "gutter": 0,
              "columnWidth": ".grid-sizer",
              "percentPosition": false,
              "transitionDuration": '0.8s'
            });
    }


    function checkRes() {
        results = $('.grid .minioferta:visible').length;
        console.log('Total '+results);
        if (results=="0") {
            console.log('son cero!');
            $('.grid').attr('nores','true');
            filtra(".noresults");
        } else {
            $('.grid').attr('nores','false');
        }

    }


    function preparar_variable_filtros(hotelval, apartval, zonaval, categoriaval) {
        filtros = "";
        if (hotelval != apartval) {
            if (hotelval) filtros = ".hotel";
            if (apartval) filtros = ".nohotel";
        }
        if (!zonaval) {
            zonaval="";
        }
        filtros +=zonaval;
        filtros +=categoriaval;
    }

    $("#tipo-hotel").change(function() {
        preparar_variable_filtros($(this).is(':checked'), $("#tipo-apartamento").is(':checked'), get_zona(),get_categoria());
        filtra(filtros);
        setTimeout(checkRes,1000);
    });

    $("#tipo-apartamento").change(function() {
        preparar_variable_filtros($("#tipo-hotel").is(':checked'), $(this).is(':checked'), get_zona(),get_categoria());
        filtra(filtros);
        setTimeout(checkRes,1000);
    });

    $("#destino").change(function() {
        preparar_variable_filtros($("#tipo-hotel").is(':checked'), $("#tipo-apartamento").is(':checked'), get_zona(),get_categoria());
        filtra(filtros);
        setTimeout(checkRes,1000);
    });

    $("#categoria").change(function() {
        preparar_variable_filtros($("#tipo-hotel").is(':checked'), $("#tipo-apartamento").is(':checked'), get_zona(),get_categoria());
        filtra(filtros);
        setTimeout(checkRes,1000);
    });

    function get_zona() {
        switch ($("#destino").val()) {
            case "-1" :
                return "";
            break;

            case "rialp" :
                return '.zona-rialp';
            break;

            case "valaran" :
                return '.zona-valaran';
            break;

            case "ametlla_mar" :
                return '.zona-ametlla_mar';
            break;

            case "estartit" :
                return '.zona-estartit';
            break;

            case "club_torrevella" :
                return '.zona-club_torrevella';
            break;

            case "platjadaro" :
                return '.zona-platjadaro';
            break;

            case "Blanes" :
                return '.zona-blanes';
            break;

            case "torroella_montgri" :
                return '.zona-torroella_montgri';
            break;

            case "vall_llobrega" :
                return '.zona-vall_llobrega';
            break;

            case "escala" :
                return '.zona-escala';
            break;

            case "menorca" :
                return '.zona-menorca';
            break;               

            default:
                return "";
            break;

        }
    }


    function get_categoria() {
        retorno = "";
        if ($('#categoria').val()!="") retorno = '.'+$('#categoria').val();
        return retorno; 
    }
    

});
</script>

