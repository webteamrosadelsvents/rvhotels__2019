<?php
/* buscador dispo pagina general apartamentos */
?>
<div class="buscador-aptos-container" style="margin-bottom:15px">
    <div class="buscador clearfix" style="padding:5px">
        <!--TITLE-->
        <div class="col-md-12 padding5">
            <h2><?php echo __("Disponibilidad y reservas");?></h2>
        </div>
        <!--/TITLE-->
        <!--SUBTITLE-->
        <div class="col-md-12 padding5">
            <p style="margin:0"><?php echo __("Escoge entre nuestros apartamentos y villas, en la zona que prefieras.");?>:</p>

            <div class="bookingError position-relative error-zona alert alert-warning text-center text-small hidden padding5 margin-top-10 margin-bottom-0 animation-doing"><i class="fa fa-exclamation-circle"></i> <?php echo __("Escoge destino o apartamento");?></div>
        </div>
        <!--/SUBTITLE-->
        
        <form id="form-buscador" name="form-buscador" class="" method="post" action="https://bookings.rvhotels.es/" target="_blank">
            <!-- HOTELES -->
            <input type="hidden" class="form-control hot hidden" value="-1">

            <!--/HOTELES-->

            <!--DESTINOS-->
            <div class="padding5 col-md-12 col-sm-12 col-xs-12">
                <div class="padding5 col-md-11 col-sm-11 col-xs-11">
                    <select class="form-control dest">
                        <option value="-1">- <?php _e('Destinos', 'rvhotels'); ?> -</option>
                        <!--<option value="20">Alcocéber - Costa Azahar</option>-->
                        <option value="31">L'Escala - Costa Brava</option>
                        <option value="24">L'Estartit - Costa Brava</option>
                        <option value="25">Platja d'Aro - Costa Brava</option>
                        <option value="23">Blanes - Costa Brava</option>
                    </select>
                </div>
                <div class="selector-icon padding0 col-md-1 col-sm-1 col-xs-1">
                    <i class="fa fa-location-arrow"></i>
                </div>
            </div>
            <!--/DESTINOS-->

            <!--APARTAMENTOS-->
            <div class="padding5 col-md-12 col-sm-12 col-xs-12">
                <div class="padding5 col-md-11 col-sm-11 col-xs-11">
                    <select class="form-control apt">
                        <option value="-1">- <?php _e('Apartamentos', 'rvhotels'); ?> -</option>
                        <optgroup label="Costa Brava - l'Escala">
                            <option value="28">Apartamentos - RV Hotels Provenzal</option>
                        </optgroup>

                        <optgroup label="Costa Brava - l'Estartit - Illes Medes">
                            <option value="6" >Apartamentos - RV Hotels Tropik</option>
                            <option value="16">Apartamentos - RV Hotels Estartit Confort</option>
                            <option value="7" >Apartamentos - RV Hotels Club Torrevella</option>

                                <option value="32" >Apartamentos - RV Hotels Club Torrevella - Heemsteede</option>
                                <option value="33" >Villas - RV Hotels Club Torrevella - Casa Heemsteede</option>
                                <!--option value="34" >Villas - RV Hotels Club Torrevella - Casa Breda</option-->

                            <option value="8" >Apartamentos - RV Hotels Els Salats</option>
                            <option value="9" >Apartamentos - RV Hotels La Pineda</option>
                            <option value="10">Apartamentos - RV Hotels Dúplex Bonsol</option>
                            <!--<option value="11">Apartamentos - RV Hotels Del Sol</option>-->
                            <option value="12">Apartamentos - RV Hotels Villas Piscis</option>
                            <option value="29">Apartamentos - RV Hotels Quijote</option>
                        </optgroup>

                        <optgroup label="Costa Brava - Platja d'Aro">
                            <option value="13">Apartamentos - RV Hotels Treumal Park</option>
                            <option value="30">Apartamentos - RV Hotels Benelux</option>
                        </optgroup>

                        <optgroup label="Costa Brava - Blanes">
                            <option value="17">Apartamentos - RV Hotels Lotus</option>
                            <option value="18">Apartamentos - RV Hotels Ses Illes</option>
                            <option value="19">Villas - RV Hotels Villa de Madrid</option>
                        </optgroup>
                    </select>
                </div>
                <div class="selector-icon padding0 col-md-1 col-sm-1 col-xs-1">
                    <i class="fa fa-home"></i>
                </div>
            </div>
            <!--/APARTAMENTOS-->

            <!--SUBTITLE 2-->
            <div class="col-md-12 col-sm-12 col-xs-12 padding5">
                <p style="margin:5px 0 0 0"><?php echo __("Selecciona la fecha de  entrada y salida");?>:</p>
            </div>
            <!--/SUBTITLE 2-->
            <!--FECHAS-->
            <!-- date('Y-m-d H:i:s', strtotime($stop_date . ' +1 day')); -->
            <?php $manana = date('d/m/Y', strtotime('+1 day'));?>
            <?php $pasada = date('d/m/Y', strtotime('+2 day'));?>

            <div class="padding5 col-md-4 col-sm-6 col-xs-6">
                <div class="padding5 col-md-10 col-sm-10 col-xs-10">

                    <input type="text" class="form-control" id="arrival" name="arrival" placeholder="- <?php echo __("Check-in");?> -" readonly="readonly" autocomplete="off" value="">
                    <input type="hidden" id="checkin" name="checkin" value="" /> 
                </div>
                <label for="arrival" class="selector-icon padding0 col-md-1 col-sm-2 col-xs-2 hilite">
                    <i class="fa fa-calendar"></i>
                </label>
            </div>
            <div class="padding5 col-md-4 col-sm-6 col-xs-6">

                <div class="padding5 col-md-10 col-sm-10 col-xs-10">
                    <input type="text" class="form-control" id="departure" name="departure" placeholder="- <?php echo __("Check-out");?> -" readonly="readonly" autocomplete="off" value="">
                    <input type="hidden" id="checkout" name="checkout" value="" />
                </div>
                <label for="departure" class="selector-icon padding0 col-md-1 col-sm-2 col-xs-2 hilite">
                    <i class="fa fa-calendar"></i>
                </label>
            </div>
            <!--/FECHAS-->
            <!--BOTON-->
            <div class="form-group padding5 col-md-4 col-sm-12 col-xs-12">
                <div class="padding5 col-md-12 col-sm-12">

                    <input type="hidden" name="analytics" value="" /><!-- id="analytics" -->
                    <input type="hidden" name="adwords" value="" /><!-- id="adwords" -->
                    <input type="hidden" type="number" name="adults" value="2">
                    <input type="hidden" name="gateway" id="gateway" value="">

                    <input type="hidden" name="check_in_date" id="check_in_date" value="">
                    <input type="hidden" name="check_out_date" id="check_out_date" value="">

                    <button type="button" class="btn btn-primary buscar" style="font-size:88%!important"><?php echo __("RESERVAR");?></button>
                </div>    
            </div>
        </form>
        <!--/BOTON-->
    </div>
</div>