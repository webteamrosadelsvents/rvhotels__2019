<?php include("modal-promo-apto.inc.php");?>
<section class="margin20 pinterest-promos grid">
  
</section>
<div id="adicionales" class="hidden"></div>

<?php
$langu = ICL_LANGUAGE_CODE;
$defaultLang="";
?>
<script>console.log('<?php echo$langu ;?>');</script>
<?php
switch ($langu) {
  case "ca" : 
    $defaultLang="ca/";
    break;
  case "en" : 
    $defaultLang="en/";
    break;
  case "fr" : 
    $defaultLang="fr/";
    break;
  default :
    $defaultLang="";
}
$loadAjaxPostsUrl = "https://www.rvhotels.es/".$defaultLang."cargarpromos/?offset=";
?>
<style>
.animate-slide-top {
animation-name: slidetop;
animation-duration: .5s;
animation-iteration-count: once;
}

@keyframes slidetop {
  from {opacity:.2;transform:translateY(150%)scale(.7);}
  to {opacity:.99;transform:translateY(0);}loadAjaxPostsUrl
}
</style>
<script>
jQuery(document).ready(function($){
  var $grid;
  var offset = 0;
  var bloquear = false;
  var semaforo = true;
  var tamanyo = 140;

  if ($("body.mobile").length) { tamanyo = 350; }
  console.log(tamanyo);

  var filtros="";

  function initmasonry() {
    $grid = $('.grid').isotope({
      filter: filtros,
      "itemSelector": ".minioferta",
      "gutter": 0,
      "columnWidth": ".grid-sizer",
      "percentPosition": false,
      "transitionDuration": '0.8s'
    });

    $('.lazy-img:not([src])').lazyload({
          failure_limit : 6,
          effect : "fadeIn"
    });

    $grid.imagesLoaded().progress( function() {
      $grid.isotope();
    });
  }

  $(".grid").load("<?php echo $loadAjaxPostsUrl;?>"+offset, function() {
    $(".grid").prepend('<div class="grid-sizer"></div><div class="gutter-sizer"></div><article class="sf-result-head text-center noresults hidden margin-bottom-30 animate-slide-top"><?php echo __("NO HAY RESULTADOS");?></article>');
    initmasonry();
    aptLinkPreventDefault();
    modalClickListener();
    
  });


  $(window).scroll(function() {
    if( $(window).scrollTop() + $(window).height() >= ($(document).height() - tamanyo )) {
      $('.loading-results').css('visibility','visible');
      if (!bloquear) {
        if (semaforo) {
          offset += 12;
          semaforo = false;
          $("#adicionales").load("<?php echo $loadAjaxPostsUrl;?>"+offset, function() {
            // create new item elements
            var $items = $("#adicionales .minioferta");
            if ($items.length<12) {
              $('.loading-results').remove();
              bloquear=true;
            } else {
              $('.loading-results').css('visibility','hidden');
            }
            // append items to grid
            $(".grid").append( $items )
              // add and lay out newly appended items
              .isotope( 'appended', $items );

            $('.lazy-img:not([src])').lazyload({
                  failure_limit : 6,
                  effect : "fadeIn"
            });

            $(".grid").imagesLoaded().progress( function() {
              $(".grid").isotope();
            });
            semaforo=true;

            aptLinkPreventDefault();
            modalClickListener();
          });
        }
      }
    }
  });

  function gotoHash(hash) {
       // animate
     $('html, body').animate({
         scrollTop: ($(hash).offset().top + 600)
       }, 800, function(){

         // when done, add hash to url
         // (default click behaviour)
         if (window.location.hash!="") {
         window.location.hash = hash;
         }
       });
  }

  function scrolltopromos() {
    $('html, body').animate({
      scrollTop: $( '#viewallpromos' ).offset().top + 600 //$(this).attr('href')
    }, 800);
  }

  $('#viewallpromos').click(function(event){
    event.preventDefault();
    scrolltopromos();
  });

  if (window.location.hash!="") {
    var urlHash = "#"+window.location.href.split("#")[1];
    if (urlHash.length > 0) {
        gotoHash(urlHash);
    }
  }

});
</script>


    