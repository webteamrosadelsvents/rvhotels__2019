<!-- ocultar destinos que no son de apartamentos 
        TA TO GUAPO EEEH
-->
<style>
    #destino option[value="rialp"],
    #destino option[value="valaran"],    
    #destino option[value="alcoceber"],
    #destino option[value="ametlla_mar"],
    #destino option[value="segur_de_calafell"],
    #destino option[value="club_torrevella"],
    #destino option[value="torroella_montgri"],
    #destino option[value="roses"],
    #destino option[value="vall_llobrega"] {
        display:none;
    }
</style>
<div class="text-center" id="filtro-ofertas-container" data-spy="affix" data-offset-top="1200">
    <div class="container">
    <section class="filtro-ofertas filtro-ofertas-apartamentos">
        <div class="row row-small">
            
            <div class="sf-checkbox-wrapper col-md-4 col-xs-12 text-center">
                <label><i class="fa fa-search"></i> <?php echo __("Ofertas apartamento");?>:</label>
                <?php /*&nbsp;
                <label><input type="checkbox" value="hotel" id="tipo-hotel" name=""><span class="customcheck"></span> <span class="leyenda"><?php echo __("Hotel");?></span></label><!--
                --><label><input type="checkbox" value="nohotel" id="tipo-apartamento" name=""><span class="customcheck" ></span> <span class="leyenda"><?php echo __("Apartamento");?></span></label>
                */?>
            </div>
            
            <div class="col-md-4 col-xs-6 text-center nowrap">
                <label for="destino" class="small-label hidden-xs"><?php echo __("Escoge tu destino");?></label>
                <select id="destino" name="destino" class="form-control unaUotra display-inline-block">
                    <?php
                    foreach ($arr_ubicas as $key => $value) {
                    ?>
                    <option value="<?php echo $key;?>"><?php echo $value;?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-4 col-xs-6 text-center nowrap">
                 <label for="tipodto" class="small-label hidden-xs"><?php echo __("Descuentos");?></label>
                 <select id="tipodto" name="tipodto" class="form-control unaUotra display-inline-block">
                     <option value=""><?php echo __("Todos los descuentos");?></option>
                     <option value="dto-puentes"><?php echo __("Especial puentes");?></option>
                     <option value="dto-largaestancia"><?php echo __("Larga estancia");?></option>
                     <option value="dto-ventanticipada"><?php echo __("Venta anticipada");?></option>
                     <!--<option value="dto-semanasanta"><?php echo __("Semana santa");?></option>-->
                     <option value="dto-dosnoches"><?php echo __("Dos noches");?></option>
                     <option value="dto-dtoweb"><?php echo __("Reservando en RVHotels.es");?></option>
                 </select>
             </div>
            <input type="hidden" id="categoria" name="categoria" value=""/>
        </div>
    </section>
    </div>
</div>
<script>
// FILTRO

jQuery(document).ready(function($){
    

    var filtros ="";

    function filtra(filtros) {
        console.log("filtrando "+filtros);
        $('.grid').removeClass('nores');
        $('.grid').isotope({
              filter: filtros,
              "itemSelector": "article",
              "gutter": 0,
              "columnWidth": ".grid-sizer",
              "percentPosition": false,
              "transitionDuration": '0.8s'
            });
    }


    function checkRes() {
        results = $('.grid .minioferta:visible').length;
        console.log('Total '+results);
        if (results=="0") {
            console.log('son cero!');
            $('.grid').attr('nores','true');
            filtra(".noresults");
        } else {
            $('.grid').attr('nores','false');
        }

    }


    function preparar_variable_filtros(hotelval, apartval, zonaval, categoriaval, dtoval) {
        filtros = "";
        if (hotelval != apartval) {
            if (hotelval) filtros = ".hotel";
            if (apartval) filtros = ".nohotel";
        }
        if (!zonaval) {
            zonaval="";
        }
        filtros += zonaval;
        filtros += categoriaval;
        filtros += dtoval;
    }

    $("#tipo-hotel").change(function() {
        preparar_variable_filtros($(this).is(':checked'), $("#tipo-apartamento").is(':checked'), get_zona(),get_categoria(),get_tipodto());
        filtra(filtros);
        setTimeout(checkRes,1000);
    });

    $("#tipo-apartamento").change(function() {
        preparar_variable_filtros($("#tipo-hotel").is(':checked'), $(this).is(':checked'), get_zona(),get_categoria(),get_tipodto());
        filtra(filtros);
        setTimeout(checkRes,1000);
    });

    $("#destino").change(function() {
        preparar_variable_filtros($("#tipo-hotel").is(':checked'), $("#tipo-apartamento").is(':checked'), get_zona(),get_categoria(),get_tipodto());
        filtra(filtros);
        setTimeout(checkRes,1000);
    });

    $("#categoria").change(function() {
        preparar_variable_filtros($("#tipo-hotel").is(':checked'), $("#tipo-apartamento").is(':checked'), get_zona(),get_categoria(),get_tipodto());
        filtra(filtros);
        setTimeout(checkRes,1000);
    });

    $("#tipodto").change(function() {
        preparar_variable_filtros($("#tipo-hotel").is(':checked'), $("#tipo-apartamento").is(':checked'), get_zona(),get_categoria(),get_tipodto());
        filtra(filtros);
        setTimeout(checkRes,1000);
    });

    function get_zona() {
        switch ($("#destino").val()) {
            case "-1" :
                return "";
            break;

            case "rialp" :
                return '.zona-rialp';
            break;

            case "ametlla_mar" :
                return '.zona-ametlla_mar';
            break;

            case "estartit" :
                return '.zona-estartit';
            break;

            case "club_torrevella" :
                return '.zona-club_torrevella';
            break;

            case "platjadaro" :
                return '.zona-platjadaro';
            break;

            case "Blanes" :
                return '.zona-blanes';
            break;

            case "torroella_montgri" :
                return '.zona-torroella_montgri';
            break;

            case "vall_llobrega" :
                return '.zona-vall_llobrega';
            break;

            case "escala" :
                return '.zona-escala';
            break;

            default:
                return "";
            break;
        }
    }


    function get_categoria() {
        retorno = "";
        if ($('#categoria').val()!="") retorno = '.'+$('#categoria').val();
        return retorno; 
    }

    function get_tipodto() {
        retorno = "";
        if ($('#tipodto').val()!="") retorno = '.'+$('#tipodto').val();
        return retorno; 
    }
    

});
</script>

