<?php
if (!isset($pornoche)) $pornoche="";
if (!isset($catPromos)) $catPromos="";
if (!isset($zonaClass)) $zonaClass="";
if (!isset($linkApartPromo)) $linkApartPromo="";
if (!isset($discountType)) $discountType="";
if (!isset($postID_class)) $postID_class="";
if (!isset($postID_urlencoded)) $postID_urlencoded="";
if (!isset($promo_oculta)) $promo_oculta="";
if (!isset($esHotel)) $esHotel="";

$promoExpired = false;
$caducidadPromo = (isset($metas['custom_promoexpires'][0]) ? date($metas['custom_promoexpires'][0]) : false );

if (($caducidadPromo) && ($caducidadPromo!="")) {
    $fechaActual = date("d/m/Y");

    if ( $fechaActual >= $caducidadPromo) {
      $promoExpired = true;
    }
}

// Imagen para descuentos

if (!isset($dto) or ($dto="NO") ) {
        
        $dto="";
        $dtoImg="";
    } else {
        $dto=str_replace("%", "", $dto);
        $dtoImg='<img class="dto" src="/wp-content/themes/rvhotels/images/dtos/dto-'.$dto.'.png" alt="-'.$dto.'%"/>';
}

/*echo "<pre style='display:none;'>";
print_r ($metas);
echo "</pre>";*/

if ((($metas['custom_ofertaoculta'][0]!="0") && ($metas['custom_ofertaoculta'][0]!="")) || ( ($metas['custom_show'][0]=="0") ||  ($metas['custom_show'][0]=="")) ) { //or ($promoExpired)
    // NO PINTAR OFERTAS OCULTAS ---> NI CADUCADAS
} else {
?>
    <article class="<?php echo $postID_class;?> minioferta padding5 col-md-3 col-sm-3 col-xs-6 <?php echo $esHotel." ".$zonaClass." ".$catPromos." ".$discountType;?>">

    <?php
        $langCode = ICL_LANGUAGE_CODE;
        $link_final = $link;

        $titulo_final = $titulo;
        $entradilla_final = get_the_excerpt();
        $hotelClassName = sanitize_title_with_dashes($hotel_apart[0]);

        if ($esHotel == "nohotel") {

            // Traducciones de apartamento

            if( get_field('titulo_oferta_'.$langCode) ) {
                $titulo_final = get_field('titulo_oferta_'.$langCode);
            }

            if( get_field('descripcion_corta_oferta_'.$langCode) ) {
                $entradilla_final = get_field('descripcion_corta_oferta_'.$langCode);
            }

            if( get_field('id_oferta_neo') ) {
                // LINK OFERTA APARTAMENTO
                // https://bookings.rvhotels.es/ca/step-1?id=:Qt15v7om0Iuv45Uat3elw
                $idneo=get_field('id_oferta_neo');
                $link_final="https://bookings.rvhotels.es/".$langCode."/step-1?id=".$idneo;
            }

            else {

                $link_final="https://bookings.rvhotels.es/".$langCode."/step-1?id=".$idAptoNeo."&arrival=".$arrival;

                if ($departure!="") $link_final.="&departure=".$departure;
                if ($nights!="") $link_final.="&nights=".$nights;

            }

        }
    ?>

        <a href="<?php echo $link_final;?>" title="<?php echo $titulo_final;?>" data-id="<?php echo $postID_urlencoded;?>" data-linkapart="<?php echo $linkApartPromo;?>" data-count="<?php echo $count;?>">

            <div class="oferta hot-<?php echo $hotelClassName;?>">
                <h3 class="titulo">
                    <div class="htype"></div>
                    <span><?php echo $titulo_final;?></span>
                </h3>
                <figure class="uk-vertical-align">
                    <img data-original="<?php echo $imagen[0];?>" width="100%" height="200" alt="<?php echo $titulo;?>" class="lazy-img overflow">
                    <figcaption class="padding5 text-center uk-vertical-align-middle"><span class="theintro"><?php echo $entradilla_final;?></span></figcaption>
                </figure>
				<?php 
					// En caso que tengamos precio 0, usaremos solo la celda azul de tamaño 12, 
					// en caso que el precio no sea 0, montaremos la celda azul de tamaño 8 + la celda amarilla de tamaño 4 con el precio
					$celda = "8";
					if ($precio == "0") $celda = "12"; 
				?>
                <div class="col-md-<?php echo $celda; ?> col-sm-12 col-xs-8 ubicacion">
                    <p class="hotel"><?php echo $hotel_apart[0];?></p>
                    <p class="destino"><?php echo str_replace(" - ", " <span>", $hotel_apart[1])."</span>"; ?></p>
                </div>
				<?php if ($celda == "8") { ?>
					<div class="desde padding0 col-md-4 col-sm-12 col-xs-4 position-relative bg-yellowgold">
						<div class="precio <?php echo $pornoche ?>">
							<?php echo $dtoImg;?>
							<?php echo __("desde");?>
							<div class="euros"> <?php echo $precio; ?>€</div>
							<b class="noche"><?php echo $pornoche ?></b>
						</div>
					</div>
				<?php } 
					// fin cambio tamaño celdas precio 0
				?>
            </div>  
        </a>
    </article>
<?php } ?>