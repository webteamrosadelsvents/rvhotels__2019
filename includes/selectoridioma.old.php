<div class="container idiomas">
	<?php 
//	do_action('icl_language_selector'); 
	$langos = icl_get_languages();
	/*
	echo "<pre>";
	print_r($langos);
	echo "</pre>";
	*/
	?>
	<div id="lang_sel_list" class="lang_sel_list_horizontal">
		<ul>
			<li class="icl-es">
				<?php $sel=($langos['es']['active'])?"lang_sel_sel":"lang_sel_other";?>
				<a href="<?php echo $langos['es']['url']; ?>" class="<?php echo $sel;?>"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAARCAMAAAABrcePAAAAk1BMVEUAAAAAAACyJjDRY23FRFDGWGWaEyOlEBqkDxnPP0/956OsDRjcY3LbYG/PWmX9zlbEQ07924KcAwn902X9ykf913P9xjj9xjf913TEQk25JzKpKSSzOiimIyOiGyD94I7YXmzmrGrUVWTmu2HmtFDmr0DEMUC9Uz7lqDK/USvlnybkmBvkkRGgBg3RgVj9ykbTfkULiU1xAAAACXRSTlMDFvTz8Wli8/OQBiVHAAAAnElEQVQY03XI2RaCIBRAUXIqbQBSEcF5Hqv//7ruqocIa6/zdNBftnnRmTZ8qzrpKgu+cLcE/OMv8Gtvq4bfUNpnWdZTRQO/5Ty9pV4avvGQ87CFP/i+pPIhfcUAf8I4wZImWDHBn4OgHJOxvAcfM/yFMZx7OWbRC4sYYwt8QkhMYogo4IvirCsEfKNzdZ0B39mv12/rwUFgt4XQE2LmFcgufVF6AAAAAElFTkSuQmCC" /></a>
			</li>
			<li class="icl-ca">
				<?php $sel=($langos['ca']['active'])?"lang_sel_sel":"lang_sel_other";?>
				<a href="<?php echo $langos['ca']['url']; ?>" class="<?php echo $sel;?>"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAARCAMAAAABrcePAAAAb1BMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAADhuHjhuHjWolCsDRjPP0/mu2Hmu2LmvGLxw1XyxFP8ykj9y0f8zE/9zU79z1X90Vzy0H/902P91Wr01Yb92HL92nn93ID93oj94I794pX945b85Jv95Jz95Z1WuWbdAAAACnRSTlMCBAYIDiYs5ebmrzJ0hwAAAGdJREFUeNqt0NEKwkAMRNE721RBwUf1/z9Td3fKQqEl7WMDB8IQEoiQRCrb0j0wLkMXGvj/iubXG/jufJ6zinQz0Hb6AwW2R5+MHMBJwFV5J9+9dv6YG8JUpR2t44lpy6xVqyLO/l8XwWpbdmCQiwgAAAAASUVORK5CYII=" /></a>
			</li>
			<li class="icl-en">
				<?php $sel=($langos['en']['active'])?"lang_sel_sel":"lang_sel_other";?>
				<a href="<?php echo $langos['en']['url']; ?>" class="<?php echo $sel;?>"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAARCAMAAAABrcePAAABDlBMVEUAAAAAAAAAAABjeKZccqIMJGgFH2QFGFBdbZhbbpdkeKcGHmT09fnj5/C7Dh3VHTflJkbTHDblJkff5O5/k74QNILdIT/KFy39/f75+vz29/rZ3+vY3OjS2Of03+XHz+GZqMkkO3roMVHeKUbwm6vsl6bqKUvaIDzYHzrPGjLOGTHHFSrFFCn49/np7fP64+fZz9zQx9bdw9GRoseKm8J9kbtziLVtg7Jjeq1gd6vQmahJX5QtS5BCWI88UosiQosZO4c1S4YuRYHjXHIaMnLtV3HpVW8ULG7SVGnKU2XlSWPaN1LkLUvTKEG5w9q4w9ncjJ/bjJ5XbJ5Xa51QZZlQZZjac4nac4jdSGPcSGLbm+WeAAAACnRSTlMEFgLu7u7uXVJRyaSPzgAAAQNJREFUGNNl0MV6g1AURWFKrA7BHYJb3N09qev7v0hPUpiQNbn7+wd3cBAURZFEZ0OvU7fJUjfg6WVZrFMUxdm2zcFbF8vLNPzxvtNN/rnV5hiG4dqtJ97Ud2/w2UfROqyUksaD81pJWR2s4id4v0Eqc90UBMdxBMHS5wrZ6IMP76Nc143nEHyEEwRO4DiOYRgR7RH4FPsvj+XPwcCm4LPHKM/z4jkDLzxE+b4fz8LJu2rt13glSZZlSfLF+Kmp3ZMvpC9jU1E7IrjYUSsb41tagK/3x57cHE+kIAikybgp9477Ndwus61qA5qm5TAMZXgHWnWbgbvlsnfJsjlw5OoyBP0DERsmjsPsjv8AAAAASUVORK5CYII=" /></a>
			</li>
			<li class="icl-fr">
				<?php $sel=($langos['fr']['active'])?"lang_sel_sel":"lang_sel_other";?>
				<a href="<?php echo $langos['fr']['url']; ?>" class="<?php echo $sel;?>"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAARCAMAAAABrcePAAABCFBMVEUAAAAAAAAAAAAHG27obGxleLRdcbDoZWXMERHKDAzcXl5YbqkFIHGvDQ38/Pz09PT39/fv7+/s7Oz5+fnx8fHe3t7CwsLp6elvhMDPAgJnfLxedLhVbbVNZrJfcrFFX648WKo1UaYsSaMkQp8bO5vzh4fxdnbqZWXvZGTsTk7rRkbqNzfpLi7oJiYTNJjvbm4ACFfsVlbpPj7VCwsHFmnmHx+Ak8p/kspBVZ44S5cvQ5EnO4wfMoQYK34SI3cNHXHtXl7tXV0BDF3oUlLlSEjjPj7hNTXfLi7dJSXbHh7YFxfYEhJUZ6tNYqdKXqT2lZUOKIf1gIDuenrzbGwFEWPoW1vxWFjfFxegFZ7lAAAADnRSTlMEFgLz9PPz8/PybGthYGzG2KYAAADTSURBVBjTZdBFgsJQFETR9KcNSwju7m7B3d1l/zuh3k9GcIZ3UIMSGGPCO2rM+BMkDlWKfBvQf93NRrOxE1X3BFz+sLF3k4PWp8ckTDHW8pKWaONmiRLM0Ns+0hYt3Pychjl6x086NrUvkqPxaLxA7wZI1yJxy2sFlui9EOlpfZXOwAq9HyZ9yc6tH1lYow8iZCDJ3CZThQ36MEqGWt9mc7BFr8dI3W7lark81KjHyU3Wer4A6Ex3KsJEdnGegqIoz3/8ZtJNyuBUeYjejC58fRLYCxxDK3F/I1bHAAAAAElFTkSuQmCC" /></a>
			</li>
		</ul>
	</div>
</div>