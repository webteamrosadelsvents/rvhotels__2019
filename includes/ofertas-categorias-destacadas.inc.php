<h3 class="subtitle-featured text-center uppercase text-extrabold darkblue text-kern-extra text-15x"><?php echo __("Especiales");?></h3>

<div class="row row-small row-home-catpromos margin-top-40">
    <?php // categorias de ofertas destacadas para home

// funciona con custom fields

// marcar mostrar categorias ofertas destacadas en la pagina home de wordpress rvhotels (page homepage rvhotels 4a pestanya)

// marcar destacar una categoria al editarla (categoria promocion)

$term_args = array(
    "posts_per_page"	=> "2",
    "nopaging"			=> false,
    "max_num_pages"		=> 1,
    "hide_empty"		=> 0,
    "taxonomy"			=> "promo_categories",
    "suppress_filters"	=> false
);

$terms = get_terms( $term_args );
$term_ids = array();

foreach( $terms as $term ) {
    if (get_field('categoria_destacada', $term)) {
    	$term_meta = get_option( "taxonomy_$term->term_id" );
		$catPromoOculta = get_field('catpromo_visible',$term);
    	$catPromoFeatured = get_field('mostrar_categorias_destacadas',$term);
		$url=get_bloginfo("url")."/promos/".$term->slug."/";
        $bgCss ="";
    
    if (get_field('categoria_img_peque', $term)) {
        
        $bgCatSmall = get_field('categoria_img_peque', $term);
        $bgCss = " style='background-image:url(".$bgCatSmall.")' ";
    }
    ?>
	<div class="col-xs-12 col-sm-12 col-md-6 cat-<?php echo $term_meta['custom_term_class'];?> margin-bottom-10 padding5">
		<a href="<?php echo $url;?>" class="catname blanco">
			<div class="categoria pic" <?php echo $bgCss;?> >
				<div class="categoria-title"><?php echo $term->name;?></div>
			</div>
		</a> 
	</div>
    <?php }} ?>
</div>