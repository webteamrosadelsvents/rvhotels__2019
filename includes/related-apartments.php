<div class="relacionados">
    <h3 class="text-center blanco margin-bottom-20"><?php echo __("TAMBIÉN TE PUEDE INTERESAR");?></h3>
    <?php 
    $arggs=array(
        'post_type' => 'apartamento',
        'posts_per_page' => 4,
        'post_status'=>array('publish'),
        'post__not_in' => array($apartamentoActual),
        'meta_query'=>array(
                            array(
                                "key"=>"custom_zona",
                                "value"=>$zonaRel)
                            ));
    query_posts($arggs);
    ?>

    <div style="margin:-5px">
    <?php
    if (!have_posts()) {
        $arggs=array(
            'post_type' => 'apartamento',
            'posts_per_page' => 4,
            'post_status'=>array('publish'),
            'post__not_in' => array($apartamentoActual),
            'meta_query'=>array(
                            array(
                                "key"=>"custom_zona",
                                "value"=>"Blanes")
                            ));
        query_posts($arggs);  
    }

    if (have_posts()) : while (have_posts()) : the_post();
            
                $zona       =   $arr_ubicas[get_post_meta($post->ID,"custom_zona",1)];
                $novedad    =   (get_post_meta($post->ID,"custom_new",1))?"novedadaparts_".ICL_LANGUAGE_CODE:"";
                $src_imgg    =   wp_get_attachment_image_src( get_post_meta($post->ID,'_thumbnail_id',1),"thumbnail" );
                ?>
                      
                    <a href="<?php echo get_permalink();?>" class="position-relative">
                        <article class="apart">
                            <div class="fichaaparts <?php echo $novedad;?>">
                                <div class="titleaparts">
                                    <h2><span class="rvhotel">RV</span> <?php the_title();?></h2> <h3><?php echo $zona;?></h3>    
                                </div>    
                                <div class="container-slide-aparts">
                                    <img class="lazy-img" data-original="<?php echo $src_imgg[0];?>">
                                    <div class="extra-info">
                                        <div class="precio-desde">
                                            <?php
                                                // proceso el importe:
                                                ob_start();
                                                the_field('apt_preciodesde');
                                                $importe = ob_get_contents();
                                                ob_end_clean();                                        
                                                $importe = number_format($importe , 2 , ".", "");
                                            ?>
                                            <?php echo __("Desde");?> <span><?php echo $importe; ?></span> € / <?php echo __("noche");?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </a>

                <?php 
        endwhile; 
    ?>
    </div>
    <?php endif; ?>

</div>