<?php 
	// categorias de ofertas destacadas para blog
	// funciona con custom fields
	// marcar mostrar categorias ofertas destacadas en la pagina Blog de wordpress rvhotels (page Blog)
	// marcar destacar una categoria al editarla (categoria entradas)

	$post_args = array(
			"posts_per_page"	=> "3",
			"nopaging"			=> false,
			"max_num_pages"		=> 1,
			"hide_empty"		=> 0,
		    "suppress_filters"	=> false ,
			"meta_key"			=> "categoria_destacada_blog",
			"meta_value"		=> 1
		);

    function post_excerpt(){
      $excerpt = get_the_excerpt();
      $excerpt = preg_replace(" ([.*?])",'',$excerpt);
      $excerpt = strip_shortcodes($excerpt);
      $excerpt = strip_tags($excerpt);
      $excerpt = substr($excerpt, 0, 100);
      $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
      $excerpt = $excerpt.'...';
      return $excerpt;
    }

	$posts = get_posts( $post_args );
	if (count($posts)>0) {
		?>
			<div class="fw-home-featureds">
				<div class="container">
					<center>
						<h3 class="subtitle-featured text-center uppercase text-extrabold darkblue text-kern-extra text-15x" style="margin-top: 40px; background: rgba(0, 79, 139, 0.8); padding: 15px; color: #FFFFFF; width: 40%;"><?php echo __("Specials Posts");?></h3>
					</center>
					<div class="row row-small row-home-catpromos margin-top-40" style="background: rgba(0, 79, 139, 0.8); padding-top:20px; width:100%; margin-bottom: 20px;">
						<?php
							foreach( $posts as $post ) {
								$metas = get_post_meta($post->ID);
								if ($metas['categoria_destacada_blog'][0]) {
									$bgCss ="";
									?>
										<div class="grid-item" style="float: left; margin-left: 10px;">
											<div id="container">
												<div class="column">
													<div class="box">
														<article class="bg-blanco margin-bottom-20" style="height: 380px;">
															<a href="<?php the_permalink($post->ID); ?>">
																<figure>
																	<img style="height: 275px;" data-original="<?php the_post_thumbnail_url($post->ID); ?>" alt="<?php the_title(); ?>" class="img-responsive lazy-img" width="100%" />
																	<h3 class="blanco padding20 padding-bottom-15 title-over-image" style="position: relative; margin-top:-89px; height: 110px; bottom:20px;"><?php echo $post->post_title; ?></h3>
																</figure>
																<div class="padding20 padding-top-15 echerp"><?php echo post_excerpt(); ?></div>
															</a>
														</article>
													</div>
												</div>
											</div>
										</div>
									<?php 
								}
							}
						?>
					</div>
				</div>
			</div>
		<?php
	} 
?>	