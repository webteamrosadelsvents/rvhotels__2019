<section class="offpage">
    <?php /* selector idioma para mobile */ ?>
        <nav id="phonelangs" class="visible-xs">
                <?php $sel=($langos['es']['active'])?"lang_sel_sel":"lang_sel_other";?>
                <a href="<?php echo $langos['es']['url']; ?>" title="<?php echo $langos['es']['name']; ?>" class="<?php echo $sel;?>">ES</a>

                <?php $sel=($langos['ca']['active'])?"lang_sel_sel":"lang_sel_other";?>
                <a href="<?php echo $langos['ca']['url']; ?>" title="<?php echo $langos['ca']['name']; ?>" class="<?php echo $sel;?>">CA</a>

                <?php $sel=($langos['en']['active'])?"lang_sel_sel":"lang_sel_other";?>
                <a href="<?php echo $langos['en']['url']; ?>" title="<?php echo $langos['en']['name']; ?>" class="<?php echo $sel;?>">EN</a>

                <?php $sel=($langos['fr']['active'])?"lang_sel_sel":"lang_sel_other";?>
                <a href="<?php echo $langos['fr']['url']; ?>" title="<?php echo $langos['fr']['name']; ?>" class="<?php echo $sel;?>">FR</a>
        </nav>

    <nav class="off-nav">
        <div class="container container-medium">

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
                <h4><?php echo  __("HOTELES");?></h4>
                <ul>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/hoteles-playa-mar-costa/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/hotels-platja-mar-costa/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/beach-hotels-sea-costa/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/hotels-plage-mer-costa/">
						<?php } ?>
						<?php echo  __("Hoteles de Playa");?></a></li>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/hoteles-montana-valle-aran-pirineo/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/hotels-muntanya-aran-pirineu/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/mountain-hotels-aran-pyrenee/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/hotels-montagne-valee-aran-pyrenees/">
						<?php } ?>
						<?php echo  __("Hoteles de Montaña");?></a></li>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/hoteles-costa-brava-costa-dorada-pirineo/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/hotels-costa-brava-costa-daurada-pirineu/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/hotels-costa-brava-costa-dorada-pyrenees/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/hotels-costa-brava-costa-dorada-pyrenees/">
						<?php } ?>
						<?php echo  __("Todos nuestros hoteles");?></a></li>
                </ul>

                <h4><?php echo  __("APARTAMENTOS");?></h4>
                <ul>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/apartamentos-en-estartit/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/apartaments-a-lestartit/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/apartments-in-estartit/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/appartements-a-lestartit/">
						<?php } ?>
						<?php echo  __("L'Estartit");?></a></li>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/villas-en-l-estartit/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/viles-a-l-estartit/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/villas-in-l-estartit/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/villas-louer-l-estartit/">
						<?php } ?>
						<?php echo  __("Villas en L'Estartit");?></a></li>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/apartamentos-en-blanes/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/apartaments-a-blanes/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/apartments-in-blanes/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/appartements-a-blanes/">
						<?php } ?>
						<?php echo  __("Blanes");?></a></li>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/apartamentos-playa-aro/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/apartaments-platja-d-aro/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/apartments-playa-aro/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/appartements-platja-daro/">
						<?php } ?>
						<?php echo  __("Platja d'Aro");?></a></li>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/apartamentos-en-costa-brava/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/apartaments-a-la-costa-brava/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/apartments-costa-brava/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/appartements-costa-brava/">
						<?php } ?>
						<?php echo  __("Búsqueda de apartamentos");?></a></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
            <h4><?php echo  __("OFERTAS");?></h4>
                <ul>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/promos/hoteles-pirineo/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/promos/hotels-pirineus/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/promos/pyrenee-hotels/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/promos/hotels-pyrenees-offres/">
						<?php } ?>
						<?php echo  __("Nieve / Esquí / Pirineo");?></a></li>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/promos/en-familia/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/promos/familia/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/promos/family/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/promos/famille/">
						<?php } ?>
						<?php echo  __("Findes en familia");?></a></li>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/promos/romantica/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/promos/romantiques/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/promos/romantic/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/promos/romantique/">
						<?php } ?>
						<?php echo  __("En pareja / Romántica");?></a></li>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/promos/especial-apartamento/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/promos/especial-apartament/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/promos/special-apartment/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/promos/appartement-special/">
						<?php } ?>
						<?php echo  __("Especial apartamentos");?></a></li>                    
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/ofertas/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/ofertes/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/offers/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/offres/">
						<?php } ?>
						<?php echo  __("Categorías / Ver todo");?></a></li>
                </ul>
                <h4><?php echo  __("¡NO TE PIERDAS NADA!");?></h4>
                <ul>
                    <li><a href="https://www.rvhotels.es/blog/"><?php echo  __("Blog");?></a></li>
                    <li><a href="//rvhotels.us2.list-manage.com/subscribe/post?u=23b5df66471f0ebe80f216080&id=5b7afe5083"><?php echo  __("Newsletter");?></a></li>                    
                </ul>
                <ul>
                    <li><?php if (ICL_LANGUAGE_CODE == "es"){ ?>
							<a href="https://www.rvhotels.es/info-contacto/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "ca"){ ?>
							<a href="https://www.rvhotels.es/ca/info-contacte/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "en"){ ?>
							<a href="https://www.rvhotels.es/en/info-contact/">
						<?php } ?>
						<?php if (ICL_LANGUAGE_CODE == "fr"){ ?>
							<a href="https://www.rvhotels.es/fr/info-contact/">
						<?php } ?>
						<?php echo  __("Acerca de RV Hotels / Contacto");?></a></li>
                </ul>
            </div>
        </div>
        </div>
    </nav>

    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="70px" height="70px" viewBox="0 0 70 70" enable-background="new 0 0 70 70" xml:space="preserve" class="aspa position-absolute hidden-xs hidden-sm">
    <g>
        <polygon fill-rule="evenodd" clip-rule="evenodd" points="35.002,35.005 35.002,4.145 27.647,28.027 	"></polygon>
        <polygon fill-rule="evenodd" clip-rule="evenodd" points="12.859,13.923 27.647,28.027 21.578,30.23 	"></polygon>
        <polygon fill-rule="evenodd" clip-rule="evenodd" points="35.002,35.005 65.857,35.005 41.976,27.646 	"></polygon>
        <polygon fill-rule="evenodd" clip-rule="evenodd" points="56.075,12.859 41.977,27.646 39.772,21.579 	"></polygon>
        <polygon fill-rule="evenodd" clip-rule="evenodd" points="34.998,35.005 34.998,65.855 42.353,41.977 	"></polygon>
        <polygon fill-rule="evenodd" clip-rule="evenodd" points="57.139,56.078 42.353,41.977 48.419,39.773 	"></polygon>
        <polygon fill-rule="evenodd" clip-rule="evenodd" points="34.998,35.005 4.143,35.005 28.023,42.355 	"></polygon>
        <polygon fill-rule="evenodd" clip-rule="evenodd" points="13.922,57.142 28.023,42.355 30.228,48.423 	"></polygon>
    </g>
    </svg>

    <button type="button" id="close-button" aria-label="Close" class="close-overlay position-absolute">
        <svg width="30" height="30"><g stroke="#fff" stroke-width="4"><line x1="5" y1="5" x2="25" y2="25"></line>
        <line x1="5" y1="25" x2="25" y2="5"></line></g></svg>
    </button>

</section>