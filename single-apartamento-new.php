<?php 
get_header();
$apartamentoActual = $post->ID;
$zona           = $arr_ubicas[get_post_meta($post->ID,"custom_zona",1)];
$slideshow      = print_slideshow($post->ID);
$contacto       = get_post_meta($post->ID,"custom_contacto",1);
$destacaPor     = get_post_meta($post->ID,"custom_destaca_por",1);
$descripcion    = get_post_meta($post->ID,"custom_descripcion",1);
$img_mapa       = wp_get_attachment_image_src(get_post_meta($post->ID,"custom_apart_img_mapa",1),"full");
$bookingCode    = get_post_meta($post->ID,"custom_bookings",1);
$tipologias     = get_tipologias($post->ID);
$nombreApto     = $post->post_title;
$slugTitleClass = strtolower($nombreApto);
$slugTitleClass = str_replace(" ", "-", $slugTitleClass);
$aidiom         = ICL_LANGUAGE_CODE;
?>
<section class="bg-lightblue sticky-nav hidden-xs" data-spy="affix" data-offset-top="300">

    <div class="container container-streched clearfix">

            <div class="row">

                <div class="col-md-8 col-sm-8 col-xs-12 nav-clonada">

                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <a href="#top" class="btn buscar-dispo-apart bg-yellowgold padding10 blanco hilite"><?php echo __("Buscar disponibilidad");?></a>
                </div>

            </div>

    </div>

</section>
<section class="bg bgaparts">
    <div class="container clearfix"> 
    <!-- end row --> 
        <article class="padding20">
            <div class="col-md-8 col-sm-8 col-xs-12">

                <nav class="menuseccion menuseccionapart">
                <ul class="">
                    <li class="activo"><a class="display-block fg-blanco text-center" href="#01_descripcion"><?php echo __("Descripción");?></a></li><!--

                    --><li class=""><a class="display-block fg-blanco text-center" href="#02_tipologia"><?php echo __("Alojamiento");?></a></li><!--

                    --><li class=""><a class="display-block fg-blanco text-center" href="#apart-servicios"><?php echo __("Servicios");?></a></li><!--

                    --><li class=""><a class="display-block fg-blanco text-center" href="#apart-mapa"><?php echo __("Mapa");?></a></li><!--

                    --><li class=""><a class="display-block fg-blanco text-center" href="#03_ofertas"><?php echo __("Ofertas");?></a></li><!--
                    --></ul>
                </nav>

                <div class="fichahotel ficha-apartamento novedad">

                    

                    <div class="titlehotel">
                        <h1 class="h1apt"><span class="rvhotel">RV </span> <?php the_title();?><br>
                        <span class="zonaapt"><?php echo __("Apartamentos en")." ".$zona;?></span></h1>
                    </div>

                    <div class="container-slide-ficha">
                        <?php echo $slideshow;?>
                    </div>

                    <!--/MENU APARTAMENTO-->  
                    <!--<div class="col-md-4 col-sm-3 col-xs-12 menu-apart padding0">
                        <div class="col-md-12 col-sm-12 col-xs-4 menu-apart-descripcion">
                            <a href="#01_descripcion"><h4><?php echo __("Descripción");?></h4></a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-4 menu-apart-tipo">
                            <a href="#02_tipologia"><h4><?php echo __("Tipología");?></h4></a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-4 menu-apart-ofertas">
                            <a href="#03_ofertas"><h4><?php echo __("Ofertas");?></h4></a>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-4 hidden-xs atencion">
                          <?php echo $contacto;?>
                        </div> 
                    </div>
                    -->
                    <!--/MENU APARTAMENTO-->

                    <!--DESCRIPCION APARTAMENTO-->
                    <div class="descripcion clear padding15 padding-bottom-0" id="01_descripcion">

                        <h3 class="uppercase darkblue margin-bottom-15"><?php echo __("Descripción general");?></h3>

                        <?php echo $descripcion; ?>

                        <hr class="fullwidth">
  
                    </div>
                    <!--/DESCRIPCION APARTAMENTO-->
                    <!--TIPOLOGIA APARTAMENTOS-->
                    <div class=" clear padding15" id="02_tipologia">

                        <h3 class="uppercase darkblue margin-bottom-15"><?php echo __("Alojamiento y tipologías");?></h3>
                        
                        <!--TIPOLOGIA 1 y 2 -->
                        <?php
                        foreach ($tipologias as $tipologia) {
                        ?>
                        <div class="instalaciones-servicios">
                            <h4 class="lightblue"><i class="fa fa-users"></i> <?php echo $tipologia['titulo'];?></h4>
                            <p class="padding20"><?php echo $tipologia['descripcion'];?></p>   
                        </div>
                        <!--/TIPOLOGIA 1 y 2 -->
                        <?php
                        }
                        ?>
                        <!-- TIPOLOGIA ADICIONAL -->
                        <?php if( get_field('tipologias_adicionales') ): ?>
                            <div class="instalaciones-servicios">
                                <?php the_field('tipologias_adicionales');?>
                            </div>
                        <?php endif;?>

                        <hr class="fullwidth">

                    </div>
                    <!--/TIPOLOGIA APARTAMENTOS-->

                    <div class=" clear padding15" id="apart-servicios">

                        <h3 class="uppercase darkblue margin-bottom-15"><?php echo __("Servicios");?></h3>

                        <?php if( get_field('servicios_obligatorios') ): ?>

                            <h4 class="lightblue"><?php echo __("Servicios obligatorios o incluidos");?></h4>
                            
                            <?php the_field('servicios_obligatorios'); ?>

                        <?php endif;?>


                        <?php if( get_field('servicios_opcionales') ): ?>

                            <h4 class="lightblue"><?php echo __("Servicios opcionales");?></h4>
                            
                            <?php the_field('servicios_opcionales'); ?>

                        <?php endif;?>

                        <div class="destacapor padding20 padding-top-0">
                            <?php if ($destacaPor !="") { echo $destacaPor; } ?>
                        </div>

                        <hr class="fullwidth">

                    </div>


                    <!-- BLOQUE UBICACION, 2 CASOS POSIBLES DE DESCRIPTIVO -->
                

                    <div class=" clear padding15" id="apart-mapa">
                        <h3 class="uppercase darkblue margin-bottom-15"><?php echo __("Mapa e información");?></h3>
                    

                    <?php if( get_field('diferente_direccion') ) {?>

                        <!-- CASO 1: DIFERENTES DIRECCIONES RECEPCION E INMUEBLE -->

                        <div class="alert padding10 alert-warning">
                            <?php echo __("ATENCIÓN: El inmueble y el lugar de recepción/recogida de llaves están en distintos lugares.");?>
                        </div>

                        <h4 class="lightblue margin-bottom-10"><?php echo __("Dirección de recepción y recogida de llaves");?></h4>

                        <div class="alert padding10 bg-lightblue fg-blanco caja-direccion-llaves">
                            <?php the_field('direccion_de_recepcion');?>
                        </div>

                        <h4 class="lightblue margin-bottom-10"><?php echo __("Dirección del inmueble");?></h4>

                        <div class="alert padding10 bg-lightblue-plus fg-blanco caja-direccion-inmueble">
                            <?php the_field('direccion_del_inmueble');?>
                        </div>

                    <?php } else { ?>

                        <!-- CASO 2: UNA SOLA DIRECCION RECEPCION E INMUEBLE -->

                        <h4 class="lightblue margin-bottom-10"><?php echo __("Recepción, recogida de llaves y ubicación del inmueble");?></h4>

                        <div class="alert padding10 bg-lightblue fg-blanco caja-direccion-llaves">
                            <?php the_field('direccion_de_recepcion');?>
                        </div>

                    <?php } ?>

                    </div>

                    


                    
                    <!--MAPA-->
                    <div class="container-apart-map padding0">
                        <iframe src="<?php echo get_post_meta($post->ID,"custom_url_mapa",1);?>" class="fullwidth apartMap" width="100%" height="350" frameborder="no"></iframe>
                    </div>
                    <!--/MAPA-->



                    <!--SECCION OFERTAS-->
                    <div class="clear padding15" id="03_ofertas"> <!-- col-md-12 col-sm-12 col-xs-12 ofertas clear padding0 contenedor-ofertas -->
                        <h3 class="uppercase darkblue margin-bottom-15"><?php echo __("OFERTAS DE ESTE APARTAMENTO");?></h3>

                        <!--CONTENEDOR OFERTAS-->
                            <div class="row row-small container-apart-ofertas">
                                <?php
                                $args = array(
                                                "posts_per_page"=>"4",
                                                "post_type"=>"promo",
                                                "post_status"=>array('publish'),
                                                "meta_query"=>array(
                                                                    array(
                                                                        "key"=>"custom_oferta_en_apart",
                                                                        "value"=>$post->ID)
                                                                    ));
                                query_posts( $args );
                                
                                while( have_posts()) : the_post();
                                    $metas          = get_post_meta($post->ID);
                                    $titulo         = get_the_title();
                                    $imagen         = wp_get_attachment_image_src( $metas['_thumbnail_id'][0],"thumbnail" );
                                    $precio         = ($metas['custom_precio'][0])?$metas['custom_precio'][0]:"0";
                                    $hotel_apart    = descrimina_hotel_apartamento($metas,"nohotel");
                                    $link           = get_custom_link($metas, $post->ID);
                    //                echo "<pre>".print_r($hotel_apart,true)."</pre>";
                                ?>
                            <!--OFERTA-->
                                    <div class="col-md-4 col-sm-4 col-xs-12 <?php echo $esHotel;?> padding5">
                                        <a href="<?php echo $link;?>" target="_blank">
                                            <article class="oferta <?php echo $esHotel;?>">
                                                <div class="col-md-12 titulo">
                                                    <?php echo $titulo;?>
                                                </div>
                                                <img data-original="<?php echo $imagen[0];?>" class="lazy-img overflow">
                                                <div class="col-md-12 padding5 descripcion">
                                                    <?php echo get_the_excerpt();?>
                                                </div>
                                                <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                                                    <p class="hotel">RV <?php echo $hotel_apart[0];?></p>
                                                    <p class="destino"><?php echo $hotel_apart[1];?></p>
                                                </div>
                                                <div class="desde padding0 col-md-3 col-sm-9 col-xs-3 bg-yellowgold">
                                                    <p class="precio"><?php echo __("desde");?><br/><span class="euros"> <?php echo $precio;?>€</span></p>
                                                </div>
                                                <div class="desde padding0 col-md-1 col-sm-3 col-xs-1 bg-yellowgold">
                                                    <i class="fa fa-angle-double-right"></i>
                                                </div>  
                                            </article>
                                        </a>
                                    </div>
                            <?php endwhile; ?>
                                <!--/OFERTA-->
                            </div>
                        <!--/CONTENEDOR OFERTAS-->   
                    </div>
                    <!--SECCION OFERTAS-->
                </div>   
            </div>

            <aside class="col-md-4 col-sm-4 col-xs-12">
                <?php require_once('buscadordispo-apart.php'); ?>

                <?php require_once('widget-info.php'); ?>
            </aside>
        </article>
    </div><!--.container-->
</section>
<!-- OPCION DISTINTA PARA RESERVAS TORREVELLA -->
<?php if ($zona=="L'Estartit - Torrevella") {?>
    <script>
        jQuery(function($){
            $('body').addClass('torrevella <?php echo $slugTitleClass;?>');

            $('.club-torrevella-casa-heemstede #form-buscador,'+
              '.club-torrevella-heemstede #form-buscador,'+
              '.club-torrevella-casa-breda #form-buscador').remove();
            
            $('.club-torrevella-casa-heemstede .buscadordispo-apart,'+
              '.club-torrevella-heemstede .buscadordispo-apart,'+
              '.club-torrevella-casa-breda .buscadordispo-apart').html('<div class="padding15"><div class="form-inline"><strong><?php echo __("Reservas en Booking.com:");?></strong> <a href="" target="_blank" class="btn btn-primary inlineblock nofloat btn-<?php echo $slugTitleClass;?> minWidth50"><?php echo __("RESERVAR AHORA");?></a></div></div>');
/*
            $('.club-torrevella-casa-heemstede .contenedor-ofertas,'+
              '.club-torrevella-heemstede .contenedor-ofertas,'+
              '.club-torrevella-casa-breda .contenedor-ofertas').before('<div class="padding15 bg-blanco">'+
              '<h3><?php echo __("Información y reservas");?></h3>'+
              '<p class="text-right"><strong><?php echo __("Reservas en Booking.com:");?></strong> &nbsp;<a href="" target="_blank" class="btn btn-primary inlineblock nofloat btn-<?php echo $slugTitleClass;?> minWidth50"><?php echo __("RESERVAR AHORA");?></a></p>'+
              '</div>');
*/

            $('.btn-club-torrevella-casa-heemstede').attr('href','https://www.booking.com/hotel/es/casa-heemstede.html?aid=330843;lang=<?php echo $aidiom; ?>');
            $('.btn-club-torrevella-heemstede').attr('href','https://www.booking.com/hotel/es/apartamentos-heemstede.html?aid=330843;lang=<?php echo $aidiom; ?>');
            $('.btn-club-torrevella-casa-breda').attr('href','https://www.booking.com/hotel/es/casa-breda.html?aid=330843;lang=<?php echo $aidiom; ?>');
        });

    </script>
    
<?php } ?>

<script>
    jQuery(function($){
        $('.menuseccionapart').clone().appendTo('.nav-clonada');

        function scrollNav() {
          $('.menuseccionapart a').click(function(){  
            //Toggle Class
            $(".activo").removeClass("activo");      
            $(this).closest('li').addClass("activo");
            var theClass = $(this).attr("class");
            $('.'+theClass).parent('li').addClass('activo');
            //Animate
            $('html, body').stop().animate({
                scrollTop: $( $(this).attr('href') ).offset().top - 140
            }, 400);
            return false;
          });
        }
        scrollNav();
    });
</script>

<?php get_footer(); ?>