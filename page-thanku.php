<?php
/*
Template Name: Thank U
*/
?>
<?php get_header(); ?>
<section class="bg bgcategorias">
	<div class="container"> 
		<div class="row">
			<div class="thanku col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="thankyou-wrapper">
					<h1 class="thankyou-title" style="letter-spacing:-0.04em">
					<?php echo __("¡Gracias!");?>
					</h1>
					
							
					<p class="hotels" style="font-size:36px;line-height:125%">
					  <?php echo __("En breve nos pondremos en contacto contigo.");?>
					</p>
					<p style="text-align:center!important">
					  <a class="btn btn-primary btn-large" style="max-width:300px;font-size:150%!important;float:none!important;font-weight:600!important" href="<?php bloginfo('url'); ?>"><?php echo __("Volver a la página principal");?>
					</p></a>
					</p>
				</div>
					
					
					
					
					
					


			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>