<?php
    /* Template Name: Hoteles MAR */
    
    ob_start();
    language_attributes(); 
    $textidioma = ob_get_contents();
ob_end_clean();    

$textidioma = explode("\"", $textidioma);
$textidioma = $textidioma[1];

?>

<?php get_header(); ?>
<section class="bg bghotels2016 bgofertas bg-beach-hotels">
	<?php //include  ('buscadordispo.php'); ?>
    <div class="container clearfix">
    	<div class="headerseccion">
			<h1><?php echo get_the_title(); ?></h1>
			<p class="subtitle"><?php echo get_the_excerpt(); ?></p>
		</div>
		
        <?php include ('includes/nav-hoteles.inc.php'); ?>    
        <?php include("includes/buscador-dispo-hotel-horizontal.php");?>

		<section class="categoria-intro text-center blanco subtitle padding30"><?php echo get_the_content();?></section>

        <?php 
        $args=array(
    	   'post_type'         => 'hotel',
           'posts_per_page'    => -1,
           'meta_query'        => array(
			array(
                "key"          => "custom_maromon",
                "value"        => "mar")
            ),
            'suppress_filters' => 0,
            'order'            => 'ASC',
            'meta_key'         => 'custom_ordenhotel',
            'orderby'          => 'meta_value',
            'orderby'          => 'custom_ordenhotel'
    	);
    	query_posts($args);		
        ?>
		
        <section class="row row-beach-hotels">
        	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            	<?php
            		$logo           =   wp_get_attachment_image_src(get_post_meta($post->ID,"custom_logo",1),"full");
            		$img_mapa       =   wp_get_attachment_image_src(get_post_meta($post->ID,"custom_img_mapa",1),"full");
            		$estrellas      =   get_post_meta($post->ID,"custom_star",1);
            		$zona           =   $arr_ubicas[get_post_meta($post->ID,"custom_zona",1)];
            		$novedad        =   (get_post_meta($post->ID,"custom_new",1))?"es-novedad novedad_".ICL_LANGUAGE_CODE:"";
            		$chapado        =   (get_post_meta($post->ID,"custom_chapado",1))?" hidden ":"";          
            		$capabilities   =   wp_get_attachment_image_src(get_post_meta($post->ID,"custom_capabilities",1),"full");
            		$hotelTitle     =   $post->post_title;
            		$slugTitle      =   strtolower($hotelTitle);
            		$slugTitle      =   str_replace(" ", "-", $slugTitle);
            		$encanto        =   (get_post_meta($post->ID,"custom_conencanto",1))?" con-encanto ":"";
				?>
            <div class="col-md-6 col-sm-6 col-xs-12 margin-bottom-20 <?php echo $chapado." ".$encanto;?>">
				<article class="fichahotel fichahotel-<?php echo $slugTitle; ?> <?php echo $novedad;?> padding0 col-md-12 col-sm-12 col-xs-12">
                	<div class="titlehotel col-md-8 col-sm-8 col-xs-12">
                        <h2 class="estrellas-<?php echo $estrellas;?>"><span class="rvhotel"><?php echo __("RV Hotels");?></span> <?php the_title(); ?></h2><h3><?php echo $zona;?></h3>    
                    </div>
                    <div class="logohotel col-md-4 col-sm-4 hidden-xs text-right"><img src="<?php echo $logo[0];?>" style="max-height:55px;width:auto" alt="<?php echo $hotelTitle;?>" title="<?php echo $hotelTitle;?>"></div>  
                    <?php echo print_slideshow($post->ID) ;?>
                    <div class="textohotel"><?php the_excerpt(); ?></div>
                    <?php 
                            if ($textidioma=="ca-ES") {
                                ?>
                                    <div class="container-icons">
                                        <img src="<?php echo $capabilities[0];?>" class="img-responsive" alt="Serveis" title="Serveis"> 
                                    </div>
                                <?php
                                } else if ($textidioma=="en-GB") {
                                ?>
                                    <div class="container-icons">
                                        <img src="<?php echo $capabilities[0];?>" class="img-responsive" alt="Services" title="Services"> 
                                    </div>
                                <?php
                                } else if ($textidioma=="fr-FR") {
                                ?>
                                    <div class="container-icons">
                                        <img src="<?php echo $capabilities[0];?>" class="img-responsive" alt="Les services" title="Les services"> 
                                    </div>
                                <?php
                                } else {
                                ?>
                                    <div class="container-icons">
                                        <img src="<?php echo $capabilities[0];?>" class="img-responsive" alt="Servicios" title="Servicios"> 
                                    </div>
                                <?php
                            }
                        ?>
                        <div class="container-map">
                        <a href="<?php echo get_post_meta($post->ID,"custom_url_mapa",1);?>" target="_blank"> 
                        <img class="lazy-img" width="100%" height="auto" data-original="<?php echo $img_mapa[0];?>" class="img-responsive" style="max-height:130px"></a>  
                    </div> 

                    <?php if ((get_post_meta($post->ID,"custom_URL_ofertas",1)!="") && (get_post_meta($post->ID,"custom_URL_ofertas",1)!="#")) {
                        // SI DISPONE DE OFERTAS
                    ?>
                    <a class="link-hotel-ofertas" href="<?php echo get_post_meta($post->ID,"custom_URL_ofertas",1);?>" target="_blank" title="<?php echo __("Ofertas hotel en ");?> <?php echo $zona;?> - <?php the_title(); ?>"><div class="ficha_ofertas col-md-6 col-xs-6"><p><?php echo __("OFERTAS");?></p></div></a>
                    <?php } else {
                        // SI NO DISPONE DE OFERTAS LINK DESACTIVADO
                    ?>
                    <span class="link-hotel-ofertas" title="<?php echo __("PROXIMAMENTE");?>" style="opacity:.5;cursor:default;"><div class="ficha_ofertas col-md-6 col-xs-6"><p><?php echo __("OFERTAS");?></p></div></span>
                    <?php } ?>

					<?php if ((get_post_meta($post->ID,"custom_URL",1)!="") && (get_post_meta($post->ID,"custom_URL",1)!="#")) {
                        // SI DISPONE DE WEB
                    ?>
                    <a class="link-hotel-web" href="<?php echo get_post_meta($post->ID,"custom_URL",1);?>" target="_blank" title="<?php echo __("Web oficial Hotel");?> <?php the_title(); ?> - RV Hotels"><div class="ficha_verweb col-md-6 col-xs-6"><p><?php echo __("WEB");?></p></div></a>
                    <?php } else {
                        // SI NO DISPONE DE WEB LINK DESACTIVADO
                    ?>
                    <span class="link-hotel-web" title="<?php echo __("PROXIMAMENTE");?>" style="opacity:.5;cursor:default;"><div class="ficha_verweb col-md-6 col-xs-6"><p><?php echo __("WEB");?></p></div></span>
                    <?php } ?>
				</article>   
			</div>
        	<?php endwhile; ?>
        	<?php endif; ?>
        </section>
	</div><!--.container-->
</section>
<section class="bg">
    <div class="container">
        <div class="padding10">
            <section class="categoria-intro text-center blanco subtitle padding30" id="bloque-encanto"><!-- ver js --></section>
            <section class="row row-encanto margin-bottom-30"></section>
        </div>
    </div>
</section>

<style type="text/css">
@media(min-width:768px) {
    .bg-beach-hotels {
        background-image:url(https://www.rvhotels.es/wp-content/themes/rvhotels/images/bg_apts_platja_daro.jpg),
        url(https://www.rvhotels.es/wp-content/uploads/2017/10/hoteles-playa-familias.png);
        background-position: center top, right bottom;
        background-repeat:no-repeat, no-repeat;
        background-size:100% auto, 60% auto;
        background-color:#2980b9;
    }
}
@media(min-width:1300px) {
    .bg-beach-hotels {background-position:center -3%, right bottom;}
}
	
@media(min-width:1600px) {
    .bg-beach-hotels {background-position:center -6%, right bottom;}
}
	
@media(min-width:1700px) {
    .bg-beach-hotels {background-position:center -7%, right bottom;}
}
	
@media(min-width:1800px) {
    .bg-beach-hotels {background-position:center -9%, right bottom;}
}
</style>

<script type="text/javascript">
	jQuery(document).ready(function($){
		$('.menuseccion .hots-beach').addClass('activo');
		$('.hoteles-con-encanto').appendTo('#bloque-encanto');
		$('.con-encanto').appendTo('.row-encanto')
	});
</script>

<?php get_footer(); ?>