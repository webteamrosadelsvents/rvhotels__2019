<?php
/*
Template Name: Apartamentos Filtro Mapa
*/
?>
<?php get_header(); ?>
<section class="bg bg-estartit">
    <div class="container clearfix apartamentos">
        <div class="headerseccion">
        	<h1><?php echo __("Apartamentos en la Costa Brava");?></h1>
        	<p class="subtitle"><?php echo __("La mejor selección de apartamentos en la Costa Brava con RV Hotels. ¡Disfruta de tus vacaciones y descubre los rincones más bonitos del Mediterráneo!");?></p>
        </div>
        <nav class="menuseccion">
        <ul class="">
        <li class="activo"><a class="display-block fg-blanco text-center" href="<?php echo __('https://www.rvhotels.es/en/apartments-costa-brava/');?>"><?php echo __("Todos los apartamentos");?></a></li><!--
        --><li class=""><a class="display-block fg-blanco text-center" href="<?php echo __('https://www.rvhotels.es/apartamentos-en-estartit/');?>"><?php echo __("Apartamentos en el Estartit");?></a></li><!--
        --><li class=""><a class="display-block fg-blanco text-center" href="<?php echo __('https://www.rvhotels.es/apartamentos-en-estartit-torrevella/');?>"><?php echo __("Apartamentos Torrevella");?></a></li><!--
        --><li class=""><a class="display-block fg-blanco text-center" href="<?php echo __('https://www.rvhotels.es/apartamentos-en-blanes/');?>"><?php echo __("Apartamentos en Blanes");?></a></li><!--
        --></ul>
        </nav>

        <div class="clear"></div>
        <?php echo do_shortcode( '[search-form id="myfilter" showall="1"]' );?>
        
    </div> <!-- cerrar container -->
</section> <!-- cerrar section bgaparts -->

<!-- generacion del mapa -->
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDWRq4pfhuas4Mbb_hP-JC58dmCq9-8Cjw&sensor=false&extension=.js'></script>
<script> 
    google.maps.event.addDomListener(window, 'load', init);

    var map;
    var mapOptions;
    var mapElement;

    function init() {
        mapOptions = {
            center: new google.maps.LatLng(41.250968,1.677245),
            zoom: 7,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.DEFAULT,
            },
            disableDoubleClickZoom: true,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
            },
            scaleControl: true,
            scrollwheel: true,
            panControl: true,
            streetViewControl: true,
            draggable : true,
            overviewMapControl: true,
            overviewMapControlOptions: {
                opened: false,
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [
              {
                "featureType": "landscape",
                "stylers": [
                  { "saturation": -100 },
                  { "lightness": 60 }
                ]
              },{
                "featureType": "road.local",
                "stylers": [
                  { "saturation": -100 },
                  { "lightness": 40 },
                  { "visibility": "on" }
                ]
              },{
                "featureType": "transit",
                "stylers": [
                  { "saturation": -100 },
                  { "visibility": "simplified" }
                ]
              },{
                "featureType": "administrative.province",
                "stylers": [
                  { "visibility": "off" }
                ]
              },{
                "featureType": "water",
                "stylers": [
                  { "visibility": "on" },
                  { "lightness": 30 }
                ]
              },{
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                  { "color": "#aaaaaa" }
                ]
              },{
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                  { "visibility": "off" }
                ]
              },{
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [
                  { "color": "#d7eb46" },
                  { "lightness": 40 },
                  { "saturation": -40 }
                ]
              }
               ,{  } 
            ] , 
        }

        /*
APT-ALANDALUS
APT-BONSOL
APT-CONFORT
APT-DELSOL
APT-HEEMSTEDE
APT-LOTUS
APT-PINEDA
APT-PORTCAN
APT-PROVENZAL
APT-QUIJOTE
APT-SALATS
APT-SESILLES
APT-TORREVELLA
APT-TREUMAL
APT-TROPIK
APT-VILLAMADRID
CAS-BREDA
CAS-CANTORRENTS
CAS-HEEMSTEDE
CAS-PISCIS
        */

        mapElement = document.getElementById('apartmentsmap');
        map = new google.maps.Map(mapElement, mapOptions);
        var locations = [
            ['RV Quijote', 'poibox poibox-apt boibox-apt-quijote', 'undefined', 'undefined', 'LINK', 42.0500968, 3.1883923000000323, 'undefined','Estartit - Costa Brava','APT-QUIJOTE'],
            ['Treumal Park', 'poibox poibox-apt boibox-apt-treumal-park', 'undefined', 'undefined', 'LINK', 41.8255748, 3.077218700000003, 'undefined','Platja d\'Aro','APT-TREUMAL'],
            ['Rv Al Ándalus', 'poibox poibox-apt boibox-apt-al-andalus', 'undefined', 'undefined', 'LINK', 40.24803989999999, 0.2783134000000018, 'undefined','Alcoceber - Costa Azahar','APT-ALANDALUS'],
            ['Europa Lotus', 'poibox poibox-apt boibox-apt-europa-lotus', 'undefined', 'undefined', 'LINK', 41.6660555, 2.7817681999999877, 'undefined','Blanes - Costa Brava','APT-LOTUS']
        ];
        for (i = 0; i < locations.length; i++) {
            if (locations[i][1] =='undefined'){ description ='';} else { description = locations[i][1];}
            if (locations[i][2] =='undefined'){ telephone ='';} else { telephone = locations[i][2];}
            if (locations[i][3] =='undefined'){ email ='';} else { email = locations[i][3];}
            if (locations[i][4] =='undefined'){ web ='';} else { web = locations[i][4];}
            if (locations[i][7] =='undefined'){ markericon ='https://www.rvhotels.es/wp-content/themes/rvhotels/images/map_marker.png';} else { markericon = locations[i][7];}
            if (locations[i][8] =='undefined'){ nombreubica ='';} else { nombreubica = locations[i][8];}
            if (locations[i][9] =='undefined'){ idapto ='';} else { idapto = locations[i][9];}

            marker = new google.maps.Marker({
                icon: markericon,
                position: new google.maps.LatLng(locations[i][5], locations[i][6]),
                map: map,
                title: locations[i][0],
                desc: description,
                tel: telephone,
                email: email,
                web: web,
                nombreubica: nombreubica,
                idapto: idapto
            });

            link = '';
            bindInfoWindow(marker, map, locations[i][0], description, telephone, email, web, link, nombreubica, idapto);
     }
 function bindInfoWindow(marker, map, title, desc, telephone, email, web, link, nombreubica, idapto) {
      var infoWindowVisible = (function () {
              var currentlyVisible = false;
              return function (visible) {
                  if (visible !== undefined) {
                      currentlyVisible = visible;
                  }
                  return currentlyVisible;
               };
           }());
           iw = new google.maps.InfoWindow();


           google.maps.event.addListener(marker, 'click', function() {
               if (infoWindowVisible()) {
                   iw.close();
                   infoWindowVisible(false);
               } else {

                iw.close();
                infoWindowVisible(false);

                   var html= "<div class='"+desc+"' style='color:#000;background-color:#fff;padding:5px;width:150px;'>"+
                       "<a  style='color:#444' href='"+web+"'>"+
                       "<h4 style='color:#444'>"+title+"</h4>"+
                       "<p><strong>"+nombreubica+"</strong></p>"+
                       "</a></div>";

                   iw = new google.maps.InfoWindow({content:html});
                   iw.open(map,marker);
                   infoWindowVisible(true);

                   jQuery('#sf-field-6').val(idapto).change();


               }
        });
        google.maps.event.addListener(iw, 'closeclick', function () {
            infoWindowVisible(false);
        });
 }
}

jQuery(document).ready(function($){
    $(window).bind('keydown', function(event) {
        if(event.keyCode === 39) {
            $('#apartmentsmap').toggleClass('hidden').appendTo('.map-panel');
            init();
            $('.filtro2016').toggleClass('map-showing');
        }
    });

    $(window).bind('keydown', function(event) {
        if(event.keyCode === 37) {
            $('.filtro2016').toggleClass('map-showing');
        }
    });

});



</script>

<style>
    #apartmentsmap {
        height:100%;
        width:100%;
    }
    .gm-style-iw * {
        display: block;
        /*width: 100%;*/
    }
    .gm-style-iw h4, .gm-style-iw p {
        margin: 0;
        padding: 0;
    }
    .gm-style-iw a {
        color: #4272db;
    }
</style>

<div id="apartmentsmap" class="hidden"></div>



<?php get_footer(); ?>