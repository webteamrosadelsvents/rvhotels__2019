<?php /* buscador de disponibilidad single apartamento */
global $precioDesde;
?>
<div class="buscadordispo-apart bg-yellowgold margin-bottom-15" id="dispo">

    <h3 class="text-center darkblue dispo-apart-title margin-bottom-15"><?php echo __("RESERVA ONLINE AL MEJOR PRECIO");?></h3>
    <div class="bg-darkblue blanco text-center padding10 precio-ficha-apart margin-bottom-15">
        <?php echo __("Desde");?> <span class="yellowgold"><?php echo $precioDesde; ?> €</span> / <?php echo __("noche");?>

    </div>

<?php if( get_field('reserva_online_no_disponible') ) { ?>

    <p class="text-center blanco"><?php echo __("Reserva online no disponible para este apartamento. Por favor, consúltenos disponibilidad.");?></p>

    <a href="<?php echo __("https://www.rvhotels.es/info-contacto/");?>" class="btn buscar buscar-dispo-apart bg-lightblue padding10 blanco hilite"><?php echo __("Contactar");?></a>

<?php } elseif( get_field('enlace_booking_com') ) {  
    $bookingComUrl = get_field('enlace_booking_com');
?>
    <p class="text-center blanco"><?php echo __("Reserva online disponible en BOOKING.COM");?></p>
    <a href="<?php echo $bookingComUrl;?>" target="_blank" class="btn buscar buscar-dispo-apart bg-lightblue padding10 blanco hilite"><?php echo __("Buscar disponibilidad");?></a>

<?php } else { ?>

        <form id="form-buscador" name="form-buscador" class="" method="post" action="https://bookings.rvhotels.es/" target="_blank">
        
            <!--FECHAS-->
            <div class="row margin-bottom-15">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <p class="darkblue "><?php echo __("Llegada");?></p>
                    <input id="arrival" name="arrival" type="text" readonly="readonly" class="deitpiquer form-control" placeholder="<?php echo __("Fecha");?>" autocomplete="off"><!--
                    --><label for="arrival" class="fa fa-calendar darkblue text-right hilite"></label>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <p class="darkblue "><?php echo __("Salida");?></p>
                    <input id="departure" name="departure" type="text" readonly="readonly" class="deitpiquer form-control" placeholder="<?php echo __("Fecha");?>" autocomplete="off"><!--
                        --><label for="departure" class="fa fa-calendar darkblue text-right hilite"></label>
                </div>
            </div>
            <!--FECHAS-->
            <!--BOTON-->
            
            <input type="hidden" name="analytics" id="analytics" value="" />
            <input type="hidden" name="adwords" id="adwords" value="" />

            <button type="button" class="btn buscar buscar-dispo-apart bg-lightblue padding10 blanco hilite"><?php echo __("Buscar disponibilidad");?></button>

            <!--BOTON-->   
        </form>

<?php } ?>


</div>
<script>
    var isMiniBuscador  = true;
    var bookingCode     = "<?php echo $bookingCode;?>";
</script>