<?php
/*
Template Name: Destinos
*/
?>
<?php get_header(); ?>

<section class="bg">
<div class="container clearfix">

        <h1><?php _e('Destinos y Actividades', 'rvhotels'); ?></h1>
        
    <div class="bloque clearfix">
    <div class="reservas col-sm-4 col-lg-3">
	<?php include  ('buscador.php'); ?>
    </div>
    
    <div class="slideshow col-sm-8 col-lg-9">
   <?php  if ( function_exists( 'get_wp_parallax_content_slider' ) ) { get_wp_parallax_content_slider(); } ?>
    </div>
    
      </div><!-- end row -->   
        
		<div id="destinos">
   
        
         <?php 
        $page_id = icl_object_id(34, 'page', false);
		$args=array(
		   'post_parent' => $page_id,
		   'post_type'   => 'page',
		   'posts_per_page' => 5,
		);
		query_posts($args);
		$temp=0;
		if (have_posts()) : while (have_posts()) : the_post(); $temp++; ?>

            <h3><?php the_title(); ?></h3>

            <div>
                <?php the_content(); ?>
                
            </div>

          
    <?php   endwhile; ?>
 	<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
 	<?php else : ?>
 
        <h2><?php _e('No encontrado', 'rvhotels'); ?></h2>
 
    <?php endif; ?>
       

       
       
        
        
        
</div>


</div><!--.container-->
</section>

<?php get_footer(); ?>