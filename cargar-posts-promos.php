<?php
/*
  Template Name: loadpromos
*/

$offset = htmlspecialchars(trim($_GET['offset']));

$argsPinteres = array(
                'suppress_filters'=>false,
                "posts_per_page"=>"12", // vigilar offset y intems<lenght en pinteres-promos.php
                "post_type"=>"promo",
                "post_status"=>'publish',
                'offset' => $offset,
                'caller_get_posts'=> 1
                );

query_posts( $argsPinteres );

while( have_posts()) : the_post();

    $metas          = get_post_meta($post->ID);
    $titulo         = get_the_title();
    $imagen         = wp_get_attachment_image_src( $metas['_thumbnail_id'][0],"thumbnail" );
    $precio         = $metas['custom_precio'][0];
    $dto            = $metas['custom_descuento'][0];
    $pornoche       = ( ($metas['custom_pornoche'][0]!="0") && ($metas['custom_pornoche'][0]!="") ) ? __("noche") : "";

    $esHotel        = (isset($metas['custom_oferta_en_apart']) && $metas['custom_oferta_en_apart'][0]!="-")?"nohotel":"hotel";
    $hotel_apart    = descrimina_hotel_apartamento($metas, $esHotel);

    $link           = get_custom_link($metas, $post->ID);

    $idApartWP      = (isset($metas['custom_oferta_en_apart'][0]) ? $metas['custom_oferta_en_apart'][0] : "" );
    $linkApartPromo = get_permalink($idApartWP);

    $idAptoNeo      = get_post_meta( $idApartWP, 'custom_bookings', true );
    $arrival        = (isset($metas['custom_dtinicio'][0]) ? $metas['custom_dtinicio'][0] : "" );
    $departure      = (isset($metas['custom_dtfin'][0])? $metas['custom_dtfin'][0] : "");
    $nights         = (isset($metas['custom_noches'][0])? $metas['custom_noches'][0] : "");

    $zonaClass      = (isset($metas['custom_oferta_en_zona'][0]) && $metas['custom_oferta_en_zona'][0]!="-") ? "zona-".$metas['custom_oferta_en_zona'][0] : "zona-";
    $zonaClass      = sanitize_title($zonaClass,"default");

    $postID         = "promoid=".$post->ID;
    $discountType   = "dto-".get_field("apto_tipodescuento");

    $taxonomies = get_terms( array(
        'taxonomy' => 'promo_categories',
        'hide_empty' => false,
        "suppress_filters"=>false
    ) );

    $terms = wp_get_post_terms($post->ID,'promo_categories');

    $catPromos ="";

    foreach ($terms as $term) {
        $catPromos .= " cat-".$term->slug;
    }

    include('includes/minioferta.inc.php');

endwhile;

wp_reset_query();
// Restore global post data stomped by the_post().
?>