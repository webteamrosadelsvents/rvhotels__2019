	<!-- modal thank you suscripcion news -->
	<div class="modal fade" id="finalModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><?php echo  __("Confirmación de suscripción");?></h4>
				</div>
				<div class="modal-body">
					<p><?php echo  __("Gracias por suscribirse a nuestra lista de mailing. A partir de ahora recibirá correos con ofertas y packs en su buzón de correo electrónico")?>.</p>
				</div>
			</div>
		</div>
	</div>

	<?php
	// detectar cuando la visita proviene de un email de confirmacion de suscripcion
	if($_GET['subscribe']=="ok"){
		?>
		<script>
		jQuery(document).ready(function ($){
			$('#finalModal').modal();
		});
		</script>
	<?php } ?>



		<footer class="rvhotelsFooter bg-darkblue">
			<div class="prefooter bg-blaurv3 uk-contrast blanco">
				<div class="uk-container uk-container-center container">
					<div class="uk-grid row">
						
						<div class="uk-width-medium-2-10 col-md-5 col-sm-5  mobile-center ">

							<div class="row">

								<div class="col-sm-4 col-md-4">
								<img src="https://www.rvhotels.es/logo-rvhotels-gruprv.png" alt="RV Hotels - Grup Rosa dels Vents" width="150" height="auto">
								</div>

								<div class="col-sm-8 col-md-8 mobile-margin-top-25">

									<div class="social_icons">
										<a href="https://www.facebook.com/RVHotels" class="almostwhite" target="_blank" rel="nofollow">
											<i class="fa fa-facebook"></i>
										</a>
										<a href="https://www.instagram.com/rvhotels/" rel="nofollow" class="almostwhite">
										<i class="fa fa-instagram pl-2 h4"></i>
										</a>
										<a href="https://twitter.com/Rv_hotels" class="almostwhite" target="_blank" rel="nofollow">
											<i class="fa fa-twitter"></i>
										</a>
										<a href="https://www.youtube.com/channel/UCaYFGdXPEmNpZadftzrgeMQ" class="almostwhite" target="_blank" rel="nofollow">
											<i class="fa fa-youtube"></i>
										</a>
										<a href="https://www.rvhotels.es/blog/" title="RVHOTELS BLOG">
													<svg id="blog-icon" xmlns="http://www.w3.org/2000/svg" width="30" height="40" style="transform:translateY(10px)">
										<path fill="#00619D" d="M.574 28V11.87h2.54c2.765 0 3.538 1.603 3.538 4.32 0 1.584-.305 3.124-1.93 3.573v.043c1.747.577 2.153 1.99 2.153 3.786C6.875 26.566 5.838 28 3.013 28H.573zM2.85 13.686h-.345v5.285c1.89.088 2.155-1.153 2.155-2.864 0-1.263-.407-2.44-1.81-2.418zM2.506 26.18c1.77.087 2.216-.876 2.216-2.65 0-1.777-.345-2.89-2.215-2.847v5.498zm7.206-14.31v14.268h3.415v1.86H7.68V11.87h2.03zm11.382 8.064c0 2.91-.04 8.28-3.983 8.28s-3.98-5.37-3.98-8.28c0-2.91.04-8.28 3.984-8.28s3.984 5.37 3.984 8.28zm-5.813 0c0 3.423.227 6.27 1.83 6.27 1.608 0 1.83-2.847 1.83-6.27 0-3.422-.222-6.268-1.83-6.268-1.603 0-1.83 2.845-1.83 6.268zm14.23-.45v.257c0 3.085.428 8.476-3.66 8.476-3.636 0-4.062-3.68-4.062-8.236 0-4.515.346-8.323 3.882-8.323 2.174 0 3.21 1.155 3.74 3.294l-1.93.835c-.206-1.07-.49-2.118-1.75-2.118-1.667 0-1.79 2.396-1.79 6.29 0 5.604.896 6.246 1.79 6.246.833 0 1.748-.127 1.748-3.977v-.877H25.77v-1.86h3.74z"></path>
										</svg>
										</a>
									</div>

								</div>

							</div>

							<div class="newsletter margin-top-30">
								<h5 class=" blanco font-weight-normal uk-text-contrast uk-text-small uk-margin-small-bottom ">
								<?php echo  __("NEWSLETTER");?> <i class="fa fa-arrow-right"></i> <?php echo  __("¡NO TE PIERDAS MÁS OFERTAS!");?>
								</h5>
								<p class="clear clearfix text-small margin-top-10 margin-bottom-0 almostwhite">
								<?php echo  __("* Tus datos estarán protegidos por nuestra");?> <?php icl_link_to_element(497,'page'); ?></p>

								<?php include("includes/mailchimper.php");?>
								<div class="clear clearfix"></div>
							</div>
							
						</div>

						<div class="uk-width-medium-3-10 uk-text-right text-right mobile-center col-md-3 col-sm-3">
							<h5 class=" blanco font-weight-normal uk-text-contrast uk-text-small uk-margin-small-bottom ">
								<?php echo  __("HOTELES EN COSTA DORADA");?>
							</h5>
							<ul class="uk-text-small uk-list uk-margin-top-remove  uk-margin-small-bottom">
								<li><a href="http://www.hotelametllamar.com/" rel="nofollow" target="_blank">Hotel Ametlla Mar ****</a></li>
							</ul>

							<h5 class="blanco font-weight-normal uk-text-contrast uk-text-small uk-margin-small-bottom uk-margin-top-remove">
								<?php echo  __("HOTELES EN COSTA BRAVA");?>
							</h5>
							<ul class="uk-text-small uk-list uk-margin-top-remove uk-margin-bottom-remove">
								<li><a href="http://www.hotelpalaulomirador.com/" rel="nofollow" target="_blank">Hotel Palau lo Mirador ****</a></li>
								<li><a href="https://www.hotelnauticpark.es/" target="_blank">Hotel Nautic Park ****</a></li>
								<li><a href="http://www.hotelgolfcostabrava.es/" rel="nofollow" target="_blank">Hotel Golf Costa Brava ****</a></li>
								<li><a href="http://www.hotelnievesmar.es/" rel="nofollow" target="_blank">Hotel Nieves Mar ***</a></li>
								<li><a href="http://www.hotelgr92.com/" rel="nofollow" target="_blank">Hotel GR 92 **</a></li>
							</ul>
						</div>
			
						<div class="uk-width-medium-3-10 uk-text-right text-right mobile-center col-md-4 col-sm-4">
						<h5 class="blanco font-weight-normal uk-text-contrast uk-text-small uk-margin-small-bottom">
							<?php echo  __("HOTELES EN LAS ISLAS BALEARES");?>
						</h5>
						<ul class="uk-text-small uk-list uk-margin-top-remove uk-margin-small-bottom">
							<li><a href="https://www.seaclubmenorca.com/" target="_blank">Hotel Sea Club menorca ****</a></li>
						</ul>						
						<h5 class="blanco font-weight-normal uk-text-contrast uk-text-small uk-margin-small-bottom">
							<?php echo  __("HOTELES EN EL PIRINEO");?>
						</h5>
						<ul class="uk-text-small uk-list uk-margin-top-remove uk-margin-small-bottom">
							<li><a href="http://www.hoteltuca.com/" rel="nofollow" target="_blank">Hotel Tuca ****</a></li>
							<li><a href="http://www.hotelorri.com/" rel="nofollow" target="_blank">Hotel Orri ***</a></li>
							<li><a href="http://www.hotelcondesdelpallars.com/" rel="nofollow" target="_blank"><?php echo  __("Hotel Condes del Pallars ***");?></a></li>
						</ul>
						<h5 class="blanco font-weight-normal uk-text-contrast uk-text-small uk-margin-small-bottom uk-margin-top-remove">
							<?php echo  __("APARTAMENTOS EN PLAYA");?>
						</h5>
						<ul class="uk-text-small uk-list uk-margin-top-remove uk-margin-bottom-remove">
							<li><a href="https://www.rvhotels.es/apartamentos-en-costa-brava/" rel="nofollow" target="_blank"><?php echo  __("Apartamentos en la Costa Brava");?></a></li>
						</ul>
						</div>
			
					</div>

				</div>
			</div>
			<div class="postfooter text-small uk-text-contrast bg-almostblack blanco padding15 margin-top-10">
				<div class="uk-container uk-container-center container">
					<div class="row">
						<div class="col-md-6 mobile-center">
							<span class="copyright">&copy; <?php echo date(Y);?></span> RV HOTELS | <?php icl_link_to_element(485,'page'); ?> | <?php icl_link_to_element(497,'page'); ?> | <a href="<?php echo  __("https://www.rvhotels.es/info-contacto/");?>"><?php echo  __("Contacto / Quienes somos");?></a> <br class="visible-xs"><br class="visible-xs">
						</div>
						<div class="col-md-6 text-right mobile-center">
							<address class="display-inline-block margin0 blanco uk-text-small uk-text-muted uk-margin-small-top uk-margin-small-bottom"><a href="https://www.rvhotels.es" class="uk-text-contrast">www.rvhotels.es</a> &nbsp; Diputació, 238 eº 3ª. 08007 (Barcelona) Spain.</address> 
							&nbsp;<br class="visible-xs"><br class="visible-xs">+34 935 036 039
						</div>
					</div>
				</div>
			</div>
		</footer>
	

	<?php wp_footer(); ?>
    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<?php if (isset($_GET['dev'])) {?>
		<script src="<?php bloginfo('template_url'); ?>/js/scriptosV2.js?v=<?php echo(rand(1,9999));?>" type="text/javascript"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/jsbundle.js?v=<?php echo(rand(1,9999));?>" type="text/javascript"></script>
	<?php } else { ?>
		<script src="<?php bloginfo('template_url'); ?>/js/scriptosV2.js" type="text/javascript"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/jsbundle.min.js?v=4" type="text/javascript"></script>
		<!-- This site is converting visitors into subscribers and customers with OptinMonster - https://optinmonster.com :: Campaign Title: RV HOTELS MOBILE -->
	<div id="om-w0ruxjyxrqjeecftnnjk-holder"></div><script>var w0ruxjyxrqjeecftnnjk,w0ruxjyxrqjeecftnnjk_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){w0ruxjyxrqjeecftnnjk_poll(function(){if(window['om_loaded']){if(!w0ruxjyxrqjeecftnnjk){w0ruxjyxrqjeecftnnjk=new OptinMonsterApp();return w0ruxjyxrqjeecftnnjk.init({"u":"25781.688758","staging":0,"dev":0,"beta":0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src="https://a.optnmstr.com/app/js/api.min.js",o.async=true,o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState==="loaded"||this.readyState==="complete"){try{d=om_loaded=true;w0ruxjyxrqjeecftnnjk=new OptinMonsterApp();w0ruxjyxrqjeecftnnjk.init({"u":"25781.688758","staging":0,"dev":0,"beta":0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName("head")[0]||document.documentElement).appendChild(o)}(document,"script","omapi-script");</script>
	<!-- / OptinMonster -->

	<?php } ?>

	<script type="text/javascript">
		jQuery(document).ready(function($){
			$("#modalOfertaApto").on('shown.bs.modal', function () {
			    $('#modalOfertaApto .lazy-img').lazyload();
			});
		});
	
	    /* INIT decorate linker */
	    window.onload = function()
	    {
	        decorateBookingEngineLinks();
	        decorateBookingEngineForms();
	    };
	    
		//(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		//i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		//m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		//})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		//ga('create', 'UA-1651542-16', 'auto');
		//ga('require', 'linker');
		//ga('send', 'pageview');

		/* crazyegg heatmap */

		/*setTimeout(function(){var a=document.createElement("script");
		var b=document.getElementsByTagName("script")[0];
		a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0058/0269.js?"+Math.floor(new Date().getTime()/3600000);
		a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);*/
	</script>

	<!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFnhxAzepRvq8Y1HyY4hggOTtX2jPKCnc&callback=initMap" async defer></script-->
</body>
</html>