<?php
/**
 * Template name: _feed ofertas
 *
 * @package WordPress
 */

header('Content-Type: ' . feed_content_type('rss-http') . '; charset=' . get_option('blog_charset'), true);
$more = 1;

echo '<?xml version="1.0" encoding="'.get_option('blog_charset').'"?'.'>'; ?>

<rss version="2.0"
	xmlns:content="https://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="https://wellformedweb.org/CommentAPI/"
	xmlns:dc="https://purl.org/dc/elements/1.1/"
	xmlns:atom="https://www.w3.org/2005/Atom"
	xmlns:sy="https://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="https://purl.org/rss/1.0/modules/slash/"
	<?php
	/**
	 * Fires at the end of the RSS root to add namespaces.
	 *
	 * @since 2.0.0
	 */
	do_action( 'rss2_ns' );
	?>
>

	<channel>
		<?php
		global $post;
		wp_reset_query();
		$args = array("posts_per_page"=>"-1","post_type"=>"promo","post_status"=>array('publish', 'draft', 'trash'));
		query_posts( $args ); 
		while( have_posts()) : the_post();
			$metas = get_post_meta($post->ID);
			/*
			echo "<pre>";
			print_r($post);
			echo "</pre>";
			*/
		?>
		<item>
			<wpPostId><?php echo $post->ID; ?></wpPostId>
			<estado><?php echo $post->post_status;?></estado>
			<titulo><![CDATA[<?php echo html_entity_decode(get_the_title($post->ID), ENT_QUOTES, 'UTF-8'); ?>]]></titulo>
			<extracto><![CDATA[<?php echo $post->post_excerpt; ?>]]></extracto>
			<alias><![CDATA[<?php echo $post->post_name; ?>]]></alias>
			<contenido><![CDATA[<?php echo $post->post_content; ?>]]></contenido>
			<pubDate><?php echo mysql2date('D, d M Y H:i:s +0000', get_post_time('Y-m-d H:i:s', true), false); ?></pubDate>
			<modifDate><?php echo $post->post_modified; ?></modifDate>
			<wpMetas>
				<?php
				foreach ($metas as $key => $value) {
					if(trim($key,"_")=="thumbnail_id"){
						$img=wp_get_attachment_image_src($value[0],"full");
						echo "<thumbnail>".$img[0]."</thumbnail>\n\t\t\t";
					}else{
						echo "<".trim($key,"_")."><![CDATA[".$value[0]."]]></".trim($key,"_").">\n\t\t\t";
					}
				}
				?>
			</wpMetas>
		</item>
		<?php endwhile; ?>
	</channel>
</rss>
