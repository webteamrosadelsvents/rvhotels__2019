<?php get_header(); ?>
<section class="bg">
	<div class="container clearfix pagina">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article class="entrada white-panel" style="margin:30px;padding:15px;background:white"> <!-- row -->
				<div class=""> <!-- col-xs-12 clearfix -->
					<h1><?php echo get_the_title(); ?></h1>
					<!--<h2 class="default-title"><?php //the_title(); ?></h2>-->
					<?php echo get_the_content(); ?>
				</div>
			</article>
 		<?php endwhile; ?>
		<?php else : ?>
 			<h2><?php _e('No encontrado', 'rvhotels'); ?></h2>
 		<?php endif; ?>
	</div><!--.container-->
</section>

<?php get_footer(); ?>