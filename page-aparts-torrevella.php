<?php
/*
Template Name: Club Torrevella
*/
?>
<?php get_header(); ?>
<section class="bg bg-torrevella" data-spy="affix" data-offset-top="280">

    <div class="container clearfix apartamentos">
        <div class="headerseccion">
        <h1><?php echo __("Villas en L'Estartit");?></h1>
        <p class="subtitle"><?php echo __("¿Estás planeado pasar unos días en el Estartit? RV Hotels te propone la mejor oferta de villas en el Estartit para que disfrutes de tus días libres con la mayor tranquilidad y con los mejores precios.");?></p>
        </div>
        <?php include ('includes/nav-apartamentos.inc.php'); ?>
        
        <?php include ('includes/buscador-neobookings-aptos-horizontal.php'); ?>

        <div class="contentseccion">
            <p class="subtitle"><?php echo $post->post_content;?></p>
        </div>
        
        <div class="bloque clearfix">
            <div class="row">
            <?php 
            $args=array(
                'post_type' => 'apartamento',
                'posts_per_page' => -1,
                "post_status"=>array('publish'),
                "meta_query"=>array(
                                    array(
                                        "key"=>"apt_tipo_edificio",
                                        "value"=>'"Casa"',
                                        'compare' => 'LIKE'),
                                    array(
                                        "key"=>"custom_zona",
                                        "value"=>"estartit",
                                        "orderby"       => "menu_order",
                                        "order"         => "ASC")
                                    ));
            

            query_posts($args);
            if (have_posts()) : while (have_posts()) : the_post();
                $zona       =   $arr_ubicas[get_post_meta($post->ID,"custom_zona",1)];
                $novedad    =   (get_post_meta($post->ID,"custom_new",1))?"novedadaparts_".ICL_LANGUAGE_CODE:"";
                $src_img    =   wp_get_attachment_image_src( get_post_meta($post->ID,'_thumbnail_id',1),"thumbnail" );
                //$lasmeta    =   get_post_meta($post->ID);
            ?>
            <!--<pre class="hidden col-md-4"><?php echo print_r($lasmeta, true);?></pre>-->
            <?php include('includes/tarjeta-apartamentos.inc.php');?>

            <?php endwhile; ?>
            </div><!-- end row -->
        <?php endif; ?>
        </div><!--.bloque-->
    </div><!--.container-->
</section>
<?php include ('includes/sticky-nav-landings-aptos.php'); ?>
<?php get_footer(); ?>