<?php
/*
Template Name: Nosotros
*/
?>
<?php get_header(); ?>

<section class="bg about">
<div class="container clearfix">

       <h1><?php  the_title(); ?></h1>

            <div class="row">

        <div class="col-sm-7 col-md-6">
                <article class="entrada">
                 <?php the_content(); ?>
                </article>
        </div>

        <div class="col-sm-5 col-md-6 foto">
            <img src="<?php bloginfo('template_url'); ?>/images/hotel.jpg" alt="Hotel"/>
        </div>
    </div><!-- end row -->


<!--
    <ul id="loguitos"> 
        <li><img src="<?php bloginfo('template_url'); ?>/images/logos/arrels.jpg" /></li>   
        <li><img src="<?php bloginfo('template_url'); ?>/images/logos/mirador.jpg" /></li>   
        <li><img src="<?php bloginfo('template_url'); ?>/images/logos/ametlla.jpg" /></li>   
        <li><img src="<?php bloginfo('template_url'); ?>/images/logos/canigo.jpg" /></li> 
        <li><img src="<?php bloginfo('template_url'); ?>/images/logos/tropik.jpg" /></li> 
        <li><img src="<?php bloginfo('template_url'); ?>/images/logos/treumal.jpg" /></li> 
        <li><img src="<?php bloginfo('template_url'); ?>/images/logos/torrevella.jpg" /></li>                                                                          
        <li><img src="<?php bloginfo('template_url'); ?>/images/logos/salats.jpg" /></li>                                                                          
        <li><img src="<?php bloginfo('template_url'); ?>/images/logos/piscis.jpg" /></li>                                                                          
        <li><img src="<?php bloginfo('template_url'); ?>/images/logos/delsol.jpg" /></li>                                                                          
        <li><img src="<?php bloginfo('template_url'); ?>/images/logos/bonsol.jpg" /></li>                                                                          
    </ul>
-->

</div><!--.container-->
</section>


<!-- Arrancaría el slide de logos: 
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#loguitos").flexisel({
            visibleItems: 6,
            animationSpeed: 2000,
            autoPlay: true,
            autoPlaySpeed: 5000,
            pauseOnHover: true,
            enableResponsiveBreakpoints: true,
            responsiveBreakpoints: { 
                portrait: { 
                    changePoint:480,
                    visibleItems: 2
                }, 
                landscape: { 
                    changePoint:640,
                    visibleItems: 3
                },
                tablet: { 
                    changePoint:768,
                    visibleItems: 4
                }
            }
        });
    });
</script>
-->

<?php get_footer(); ?>