<?php /* Template Name: BLOG */ ?>

<?php get_header(); ?>

<?php
    function get_excerpt(){
      $excerpt = get_the_excerpt();
      $excerpt = preg_replace(" ([.*?])",'',$excerpt);
      $excerpt = strip_shortcodes($excerpt);
      $excerpt = strip_tags($excerpt);
      $excerpt = substr($excerpt, 0, 100);
      $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
      $excerpt = $excerpt.'...';
      return $excerpt;
    }
?>

<style type="text/css">
#columns {-moz-column-count: 3; -moz-column-gap: 10px; -moz-column-fill: auto; -webkit-column-count: 3; -webkit-column-gap: 10px;
		  -webkit-column-fill: auto; column-count: 3; column-gap: 15px; column-fill: auto;}

.box {display: inline-block; margin: 0px 2px 0px; padding: 15px;}
	
article a, article a:hover {color:#000000; font-weight: bold; text-decoration: none;}
	
.pagination {clear:both; padding:20px 0; position:relative; font-size:11px; line-height:13px; display: block; float: right;}
.pagination span, .pagination a {display:block; float:left; margin: 2px 2px 2px 0px; padding:10px; text-decoration:none; width:auto;
								 color:#fff; background: #555; font-size: 15px;}
.pagination a:hover{color:#fff; background: #3279BB;}
.pagination .current{padding:10px; background: #3279BB; color:#fff; font-size: 15px;}
</style>

<div class="bg bg-estartit">
	<div class="container clearfix pagina">
	    <div class="headerseccion">
            <h1>Blog RVHotels</h1>
            <h2 style="color:white; margin:20px 0px;">Descubre nuevos rincones para tus escapadas en la Costa Brava, Costa Dorada y el Pirineo</h2>
            <p class="subtitle">Bienvenidos al blog de RVHotels, para viajeros inquietos, familias que buscan un respiro y gourmets que saben apreciar lo bueno. ¡Mucho más que hoteles y apartamentos!</p>
		</div>

		<?php
			// llamamos al include para visualizar los posts destacados
			include("includes/ofertas-categorias-destacadas_blog.inc.php");
		?>
		<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args=array(
				'cat' 			 	=> 1,
        		'post_type' 	 	=> 'post',
        		'post_status' 	 	=> 'publish',
        		'paged'				=> $paged,
        		'posts_per_page' 	=> 18,
        		'caller_get_posts' 	=> 1,
				'post__not_in' 		=> array($this_post)
    		);
			$my_query = null;
			$my_query = new WP_Query($args);
			if( $my_query->have_posts() ) {
				while ($my_query->have_posts()) : $my_query->the_post(); ?>
					<div class="grid-item" style="float: left; margin-left: 10px;">
						<div id="container">
							<div class="column">
								<div class="box">
									<article class="bg-blanco margin-bottom-20" style="height: 380px;">
										<a href="<?php the_permalink(); ?>">
											<figure>
												<img style="height: 275px;" data-original="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="img-responsive lazy-img" width="100%" />
												<h3 class="blanco padding20 padding-bottom-15 title-over-image" style="position: relative; margin-top:-89px; height: 110px; bottom:20px;"><?php the_title(); ?></h3>
											</figure>
											<div class="padding20 padding-top-15 echerp"><?php echo get_excerpt(); ?></div>
										</a>
									</article>
								</div>
							</div>
						</div>
					</div>
					<?php 
				endwhile; 
			}
			blog_paginacio($my_query->max_num_pages);
			wp_reset_query(); 
		?>

<!-- Mostra els posts del Blog - MASONRY -->

<div style="display:none;"><?php require_once("includes/pinteres.php");?></div>

<!-- /Mostra els posts del Blog - MASONRY -->
		
	</div><!--.container-->
</div>

<?php get_footer(); ?>