<?php 
/**
 * The template for displaying Hotel format single posts
 */
get_header(); 
?>
<script type="text/javascript">
    jQuery( document ).ready(function() {
        jQuery(".carga_mapa").click(function(){
            var iframe = document.getElementById("mapa");
            iframe.src = iframe.src;
        });
    });
</script>
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:5px 5px;overflow:hidden;word-break:normal;text-align:center;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:5px 50px;overflow:hidden;word-break:normal;}
.tg .tg-3kjg{font-weight:bold;background-color:#7f8c8d;color:#ffffff;text-align:center}
.tg .tg-s9gw{font-weight:bold;font-family:Arial, Helvetica, sans-serif !important;;background-color:#677C91;color:#ffffff;text-align:center}
.tg .tg-p9aw{font-weight:bold;background-color:#bdc3c7;text-align:center}
.tg .tg-s6fk{font-weight:bold;font-family:Arial, Helvetica, sans-serif !important;;background-color:#81E5FF;text-align:center}
.tg .tg-efv9{font-family:Arial, Helvetica, sans-serif !important;}
.tg .tg-91yh{font-weight:bold;font-family:Arial, Helvetica, sans-serif !important;;background-color:#3498db;color:#ffffff;text-align:center}
.tg .tg-1g9f{font-weight:bold;font-family:Arial, Helvetica, sans-serif !important;;background-color:#bdc3c7;text-align:center}
</style>

<section class="bg">
<div class="container clearfix oferta">
  
    
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article class="entrada row clearfix">

          <div>          
            <h1><strong><?php the_title(); ?></strong></h1>
            <?php include("buscador_peque.php"); ?>
            <div class="clearfix"></div>            
            <?php the_content(); ?>

          </div>

            

        </article>
 
    <?php endwhile; ?>

 
    <?php else : ?>
 
        <h2><?php _e('No encontrado', 'rvhotels'); ?></h2>
 
    <?php endif; ?>


</div><!--.container-->
</section>

<?php get_footer(); ?>