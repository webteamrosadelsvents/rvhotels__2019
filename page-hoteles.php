<?php
/*
Template Name: Hoteles
*/
?>
<?php get_header(); ?>
    <section class="bg bghotels2016">
    <?php //include  ('buscadordispo.php'); ?>
    <div class="container clearfix">
    <div class="headerseccion">
        <h1><?php echo __("Hoteles en la Costa Brava, Costa Dorada y Pirineo");?></h1>
        <p class="subtitle"><?php echo __("Descubre la selección de hoteles que RV Hotels tiene preparada para ti. Diviértete en familia en la Costa Dorada, relájate en los hoteles con más encanto de la Costa Brava o disfruta de las actividades en plena naturaleza en el Pirineo.");?></p>
    </div>

    <?php include ('includes/nav-hoteles.inc.php'); ?>    

    <?php include("includes/buscador-dispo-hotel-horizontal.php");?>
    <!-- end row -->         
        <?php 
        $args=array(
    	   'post_type' => 'hotel',
    	   'posts_per_page' => -1,
    	);
    	query_posts($args);		
        ?>
        <section class="row row-hoteles margin-bottom-60">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php
            $logo           =   wp_get_attachment_image_src(get_post_meta($post->ID,"custom_logo",1),"full");
            $img_mapa       =   wp_get_attachment_image_src(get_post_meta($post->ID,"custom_img_mapa",1),"full");
            $estrellas      =   get_post_meta($post->ID,"custom_star",1);
            $zona           =   $arr_ubicas[get_post_meta($post->ID,"custom_zona",1)];
            $novedad        =   (get_post_meta($post->ID,"custom_new",1))?"es-novedad novedad_".ICL_LANGUAGE_CODE:"";
            $chapado        =   (get_post_meta($post->ID,"custom_chapado",1))?" hidden ":"";            
            $capabilities   =   wp_get_attachment_image_src(get_post_meta($post->ID,"custom_capabilities",1),"full");
            $hotelTitle     =   $post->post_title;
            $slugTitle      =   strtolower($hotelTitle);
            $slugTitle      =   str_replace(" ", "-", $slugTitle);
            ?>
            <div class="col-md-6 col-sm-6 col-xs-12 margin-bottom-20 <?php echo $chapado;?>">
                <article class="fichahotel fichahotel-<?php echo $slugTitle; ?> <?php echo $novedad;?> padding0 col-md-12 col-sm-12 col-xs-12">
                    <div class="titlehotel col-md-8 col-sm-8 col-xs-12">
                        <h2 class="estrellas-<?php echo $estrellas;?>"><span class="rvhotel"><?php echo __("RV Hotels");?></span> <?php the_title(); ?></h2><h3><?php echo $zona;?></h3>    
                    </div>
                    <div class="logohotel col-md-4 col-sm-4 hidden-xs text-right">
                        <img src="<?php echo $logo[0];?>" style="max-height:55px;width:auto" alt="<?php echo $hotelTitle;?>" title="<?php echo $hotelTitle;?>">
                    </div>  
                    <?php echo print_slideshow($post->ID) ;?>
                    <div class="textohotel">
                        <?php the_excerpt(); ?>
                    </div>
                    <div class="container-icons">
                        <img src="<?php echo $capabilities[0];?>" class="img-responsive" alt="Servicios" title="Servicios"> 
                    </div>
                    <div class="container-map">
                        <a href="<?php echo get_post_meta($post->ID,"custom_url_mapa",1);?>" target="_blank"> 
                        <img class="lazy-img" width="100%" height="auto" data-original="<?php echo $img_mapa[0];?>" class="img-responsive" style="max-height:130px"></a>  
                    </div> 

                    <?php if ((get_post_meta($post->ID,"custom_URL_ofertas",1)!="") && (get_post_meta($post->ID,"custom_URL_ofertas",1)!="#")) {
                        // SI DISPONE DE OFERTAS
                    ?>
                    <a class="link-hotel-ofertas" href="<?php echo get_post_meta($post->ID,"custom_URL_ofertas",1);?>" target="_blank" title="<?php echo __("Ofertas hotel en ");?> <?php echo $zona;?> - <?php the_title(); ?>"><div class="ficha_ofertas col-md-6 col-xs-6"><p><?php echo __("OFERTAS");?></p></div></a>
                    <?php } else {
                        // SI NO DISPONE DE OFERTAS LINK DESACTIVADO
                    ?>
                    <span class="link-hotel-ofertas" title="<?php echo __("PROXIMAMENTE");?>" style="opacity:.5;cursor:default;"><div class="ficha_ofertas col-md-6 col-xs-6"><p><?php echo __("OFERTAS");?></p></div></span>
                    <?php } ?>



                    <?php if ((get_post_meta($post->ID,"custom_URL",1)!="") && (get_post_meta($post->ID,"custom_URL",1)!="#")) {
                        // SI DISPONE DE WEB
                    ?>
                    <a class="link-hotel-web" href="<?php echo get_post_meta($post->ID,"custom_URL",1);?>" target="_blank" title="<?php echo __("Web oficial Hotel");?> <?php the_title(); ?> - RV Hotels"><div class="ficha_verweb col-md-6 col-xs-6"><p><?php echo __("WEB");?></p></div></a>
                    <?php } else {
                        // SI NO DISPONE DE WEB LINK DESACTIVADO
                    ?>
                    <span class="link-hotel-web" title="<?php echo __("PROXIMAMENTE");?>" style="opacity:.5;cursor:default;"><div class="ficha_verweb col-md-6 col-xs-6"><p><?php echo __("WEB");?></p></div></span>
                    <?php } ?>
                
                </article>   
            </div>
        <?php endwhile; ?>
        <?php endif; ?>
        </section>
    </div><!--.container-->
    </section>
<script>jQuery('.menuseccion .hots-todos').addClass('activo');</script>
<?php get_footer(); ?>