<?php
/*
Template Name: Apartamentos Filtro 2016
*/
?>
<?php get_header(); ?>

<?php /* Clon de la navegacion para sticky */ ?>

<section class="bg bg-estartit" data-spy="affix" data-offset-top="280">
    <div class="container clearfix apartamentos">
        <div class="headerseccion">
        	<h1><?php echo __("Apartamentos en la Costa Brava");?></h1>
        	<p class="subtitle"><?php echo __("La mejor selección de apartamentos en la Costa Brava con RV Hotels. ¡Disfruta de tus vacaciones y descubre los rincones más bonitos del Mediterráneo!");?></p>
        </div>
        <?php include ('includes/nav-apartamentos.inc.php'); ?>

        <?php include ('includes/buscador-neobookings-aptos-horizontal.php'); ?>

        <div class="contentseccion">
            <p class="subtitle"><?php echo $post->post_content;?></p>
        </div>        
        
        <div class="clear"></div>

        <?php echo do_shortcode( '[search-form id="myfilter" showall="1"]' );?>
        
    </div> <!-- cerrar container -->
</section> <!-- cerrar section bgaparts -->
<?php include ('includes/sticky-nav-landings-aptos.php'); ?>
<?php get_footer(); ?>