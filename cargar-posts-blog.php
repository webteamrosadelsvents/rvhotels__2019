<?php
/*
  Template Name: loadposts
*/

      $this_post = $_GET["postid"];
      /*
      $offset = htmlspecialchars(trim($_GET['offset']));
      if ($offset == '') {
          $offset = 3;
      }
      */

      $offset = htmlspecialchars(trim($_GET['offset']));

      $args=array(
        'cat' => 1,
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => 5,  // vigilar offset y intems<lenght en pinteres.php
        'offset' => $offset,
        'caller_get_posts'=> 1,
        'post__not_in' => array($this_post)
      );
      $my_query = null;
      $my_query = new WP_Query($args);
      if( $my_query->have_posts() ) {
        //echo '<h2>Últimas entradas</h2>';
        while ($my_query->have_posts()) : $my_query->the_post(); ?>
        <div class="grid-item">
          <article class="bg-blanco margin-bottom-20">
            <a href="<?php the_permalink(); ?>">
            <figure>
              <img data-original="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="img-responsive lazy-img" width="100%" height="200"/>
              <h3 class="blanco padding20 padding-bottom-15 title-over-image"><?php the_title(); ?></h3>
            </figure>

            <div class="padding20 padding-top-15 echerp">
              <?php the_excerpt(); ?>
            </div>
            </a>
          </article>
          </div>
          <?php
        endwhile;
      }
      wp_reset_query();  // Restore global post data stomped by the_post().
      ?>