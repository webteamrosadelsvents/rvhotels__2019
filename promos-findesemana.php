<?php
/*
Template Name: HARCODE CATEGORIZADO, YA CAMBIARMEOS
*/
?>
<section class="bgcategorias">
	<?php get_header('home'); ?>
    <div class="container-categorias">
        <?php include ('filtro-ofertas.php'); ?>
        <div class="container fullscreen">
            <div class="col-md-12 container-chillon padding5">
              <div class="col-md-12 slide-chillon hidden-xs padding0">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                      <div class="item active">
                        <img src="<?php echo get_bloginfo('template_url');?>/images/promos_findesemana.jpg">
                        <div class="carousel-caption">
                          <h3>FIN DE SEMANA</h3>
                          <p>Nuestra mejor selección para que desconectes después de una dura semana de trabajo.</p>
                        </div>
                      </div>                
                    </div>
                    <!-- Left and right controls
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>-->
                </div>
              </div>
            </div>
        </div>
    </div>
</section>
<section class="bgofertas">       
    <div class="container padding0">
        <!--OFERTAS DESTACADAS-->
        <div class="col-md-12 container-ofertas padding0 clear">
            <div class="col-md-12 col-sm-12 col-xs-12 padding5">
                <h2>FIN DE SEMANA</h2>
            </div>
            <!--OFERTA-->
            <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                <div class="oferta">
                    <div class="col-md-12 titulo">
                        Los niños no pagan ¡Gratis siempre!
                    </div>
                    <img src="<?php echo get_bloginfo('template_url');?>/images/ninosgratis_400.jpg" class="img-responsive overflow">
                    <div class="col-md-12 padding5 descripcion">
                        <p>Ven al <strong>Hotel Ametlla Mar</strong> con tu familia, y aprovéchate de nuestras increíbles ofertas para niños.<p/><br/><p>¡<strong>LOS 2 NIÑOS GRATIS</strong>* toda la temporada!</p>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                        <p class="hotel">RV Hotel Ametlla Mar</p>
                        <p class="destino">Costa Dorada</p>
                    </div>
                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                        <p class="precio">desde<br/><span class="euros"> 0€</span></p>
                    </div>
                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                        <i class="fa fa-angle-double-right"></i>
                    </div>  
                </div>  
            </div>
            <!--/OFERTA-->
            <!--OFERTA-->
            <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                <div class="oferta">
                    <div class="col-md-12 titulo">
                        Escapada Love & Relax
                    </div>  
                    <img src="<?php echo get_bloginfo('template_url');?>/images/loverelax_400.jpg" class="img-responsive">
                    <div class="col-md-12 padding5 descripcion">
                        <p>Disfrute de una <strong>noche romántica en el Palau</strong> y déjese llevar por la magia de sus rincones medievales.<p/><br/><p>Le ofrecemos una <strong>escapada muy especial</strong> que le enamorará.</p>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                        <p class="hotel">RV Hotel Palau Lo Mirador</p>
                        <p class="destino">Costa Brava</p>
                    </div>
                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                        <p class="precio">desde<br/><span class="euros"> 129€</span></p>
                    </div>
                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                        <i class="fa fa-angle-double-right"></i>
                    </div>  
                </div>  
            </div>
            <!--/OFERTA-->
            <!--OFERTA-->
            <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                <div class="oferta">
                    <div class="col-md-12 titulo">
                        Paseo a caballo
                    </div>  
                    <img src="<?php echo get_bloginfo('template_url');?>/images/paseocaballo_400.jpg" class="img-responsive">
                    <div class="col-md-12 padding5 descripcion">
                        <p><strong>Completa tu estancia</strong> en Hotel Ses Arrels <strong>con 1 hora de Paseo a Caballo</strong> por la Costa Brava</strong>.</p><br/><p>Descubrirás los bonitos paisajes de la zona paseando por las montañas con vistas al mar.</p>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                        <p class="hotel">RV Hotel Ses Arrels</p>
                        <p class="destino">Costa Brava</p>
                    </div>
                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                        <p class="precio">desde<br/><span class="euros"> 99€</span></p>
                    </div>
                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                        <i class="fa fa-angle-double-right"></i>
                    </div>  
                </div>  
            </div>
            <!--/OFERTA-->
            <!--OFERTA-->
            <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                <div class="oferta">
                    <div class="col-md-12 titulo">
                        Visita a bodega y degustación de vino
                    </div>  
                    <img src="<?php echo get_bloginfo('template_url');?>/images/vino_400.jpg" class="img-responsive">
                    <div class="col-md-12 padding5 descripcion">
                        <p>Descubra el mundo del Vino de una manera pausada y divertida con nuestra <strong>Escapada Enológica</strong>.<p/><br/><p>Incluye visita y cata de 5 vinos de la región.</p>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                        <p class="hotel">RV Hotel Ses Arrels</p>
                        <p class="destino">Costa Brava</p>
                    </div>
                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                        <p class="precio">desde<br/><span class="euros"> 69€</span></p>
                    </div>
                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                        <i class="fa fa-angle-double-right"></i>
                    </div>  
                </div>  
            </div>
            <!--/OFERTA-->
            <!--OFERTA-->
            <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                <div class="oferta">
                    <div class="col-md-12 titulo">
                        Los niños no pagan ¡Gratis siempre!
                    </div>
                    <img src="<?php echo get_bloginfo('template_url');?>/images/ninosgratis_400.jpg" class="img-responsive overflow">
                    <div class="col-md-12 padding5 descripcion">
                        <p>Ven al <strong>Hotel Ametlla Mar</strong> con tu familia, y aprovéchate de nuestras increíbles ofertas para niños.<p/><br/><p>¡<strong>LOS 2 NIÑOS GRATIS</strong>* toda la temporada!</p>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                        <p class="hotel">RV Hotel Ametlla Mar</p>
                        <p class="destino">Costa Dorada</p>
                    </div>
                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                        <p class="precio">desde<br/><span class="euros"> 0€</span></p>
                    </div>
                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                        <i class="fa fa-angle-double-right"></i>
                    </div>  
                </div>  
            </div>
            <!--/OFERTA-->
            <!--OFERTA-->
            <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                <div class="oferta">
                    <div class="col-md-12 titulo">
                        Escapada Love & Relax
                    </div>  
                    <img src="<?php echo get_bloginfo('template_url');?>/images/loverelax_400.jpg" class="img-responsive">
                    <div class="col-md-12 padding5 descripcion">
                        <p>Disfrute de una <strong>noche romántica en el Palau</strong> y déjese llevar por la magia de sus rincones medievales.<p/><br/><p>Le ofrecemos una <strong>escapada muy especial</strong> que le enamorará.</p>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                        <p class="hotel">RV Hotel Palau Lo Mirador</p>
                        <p class="destino">Costa Brava</p>
                    </div>
                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                        <p class="precio">desde<br/><span class="euros"> 129€</span></p>
                    </div>
                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                        <i class="fa fa-angle-double-right"></i>
                    </div>  
                </div>  
            </div>
            <!--/OFERTA-->
            <!--OFERTA-->
            <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                <div class="oferta">
                    <div class="col-md-12 titulo">
                        Paseo a caballo
                    </div>  
                    <img src="<?php echo get_bloginfo('template_url');?>/images/paseocaballo_400.jpg" class="img-responsive">
                    <div class="col-md-12 padding5 descripcion">
                        <p><strong>Completa tu estancia</strong> en Hotel Ses Arrels <strong>con 1 hora de Paseo a Caballo</strong> por la Costa Brava</strong>.</p><br/><p>Descubrirás los bonitos paisajes de la zona paseando por las montañas con vistas al mar.</p>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                        <p class="hotel">RV Hotel Ses Arrels</p>
                        <p class="destino">Costa Brava</p>
                    </div>
                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                        <p class="precio">desde<br/><span class="euros"> 99€</span></p>
                    </div>
                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                        <i class="fa fa-angle-double-right"></i>
                    </div>  
                </div>  
            </div>
            <!--/OFERTA-->
            <!--OFERTA-->
            <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                <div class="oferta">
                    <div class="col-md-12 titulo">
                        Visita a bodega y degustación de vino
                    </div>  
                    <img src="<?php echo get_bloginfo('template_url');?>/images/vino_400.jpg" class="img-responsive">
                    <div class="col-md-12 padding5 descripcion">
                        <p>Descubra el mundo del Vino de una manera pausada y divertida con nuestra <strong>Escapada Enológica</strong>.<p/><br/><p>Incluye visita y cata de 5 vinos de la región.</p>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                        <p class="hotel">RV Hotel Ses Arrels</p>
                        <p class="destino">Costa Brava</p>
                    </div>
                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                        <p class="precio">desde<br/><span class="euros"> 69€</span></p>
                    </div>
                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                        <i class="fa fa-angle-double-right"></i>
                    </div>  
                </div>  
            </div>
            <!--/OFERTA-->
            <!--OFERTA-->
            <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                <div class="oferta">
                    <div class="col-md-12 titulo">
                        Los niños no pagan ¡Gratis siempre!
                    </div>
                    <img src="<?php echo get_bloginfo('template_url');?>/images/ninosgratis_400.jpg" class="img-responsive overflow">
                    <div class="col-md-12 padding5 descripcion">
                        <p>Ven al <strong>Hotel Ametlla Mar</strong> con tu familia, y aprovéchate de nuestras increíbles ofertas para niños.<p/><br/><p>¡<strong>LOS 2 NIÑOS GRATIS</strong>* toda la temporada!</p>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                        <p class="hotel">RV Hotel Ametlla Mar</p>
                        <p class="destino">Costa Dorada</p>
                    </div>
                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                        <p class="precio">desde<br/><span class="euros"> 0€</span></p>
                    </div>
                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                        <i class="fa fa-angle-double-right"></i>
                    </div>  
                </div>  
            </div>
            <!--/OFERTA-->
            <!--OFERTA-->
            <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                <div class="oferta">
                    <div class="col-md-12 titulo">
                        Escapada Love & Relax
                    </div>  
                    <img src="<?php echo get_bloginfo('template_url');?>/images/loverelax_400.jpg" class="img-responsive">
                    <div class="col-md-12 padding5 descripcion">
                        <p>Disfrute de una <strong>noche romántica en el Palau</strong> y déjese llevar por la magia de sus rincones medievales.<p/><br/><p>Le ofrecemos una <strong>escapada muy especial</strong> que le enamorará.</p>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                        <p class="hotel">RV Hotel Palau Lo Mirador</p>
                        <p class="destino">Costa Brava</p>
                    </div>
                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                        <p class="precio">desde<br/><span class="euros"> 129€</span></p>
                    </div>
                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                        <i class="fa fa-angle-double-right"></i>
                    </div>  
                </div>  
            </div>
            <!--/OFERTA-->
            <!--OFERTA-->
            <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                <div class="oferta">
                    <div class="col-md-12 titulo">
                        Paseo a caballo
                    </div>  
                    <img src="<?php echo get_bloginfo('template_url');?>/images/paseocaballo_400.jpg" class="img-responsive">
                    <div class="col-md-12 padding5 descripcion">
                        <p><strong>Completa tu estancia</strong> en Hotel Ses Arrels <strong>con 1 hora de Paseo a Caballo</strong> por la Costa Brava</strong>.</p><br/><p>Descubrirás los bonitos paisajes de la zona paseando por las montañas con vistas al mar.</p>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                        <p class="hotel">RV Hotel Ses Arrels</p>
                        <p class="destino">Costa Brava</p>
                    </div>
                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                        <p class="precio">desde<br/><span class="euros"> 99€</span></p>
                    </div>
                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                        <i class="fa fa-angle-double-right"></i>
                    </div>  
                </div>  
            </div>
            <!--/OFERTA-->
            <!--OFERTA-->
            <div class="padding5 col-md-3 col-sm-3 col-xs-12">
                <div class="oferta">
                    <div class="col-md-12 titulo">
                        Visita a bodega y degustación de vino
                    </div>  
                    <img src="<?php echo get_bloginfo('template_url');?>/images/vino_400.jpg" class="img-responsive">
                    <div class="col-md-12 padding5 descripcion">
                        <p>Descubra el mundo del Vino de una manera pausada y divertida con nuestra <strong>Escapada Enológica</strong>.<p/><br/><p>Incluye visita y cata de 5 vinos de la región.</p>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-8 ubicacion">
                        <p class="hotel">RV Hotel Ses Arrels</p>
                        <p class="destino">Costa Brava</p>
                    </div>
                    <div class="desde padding0 col-md-3 col-sm-9 col-xs-3">
                        <p class="precio">desde<br/><span class="euros"> 69€</span></p>
                    </div>
                    <div class="desde padding0 col-md-1 col-sm-3 col-xs-1">
                        <i class="fa fa-angle-double-right"></i>
                    </div>  
                </div>  
            </div>
            <!--/OFERTA-->
        </div>
        <!--/OFERTAS DESTACADAS-->
	</div><!--.container-->
<?php get_footer(); ?>
</section>