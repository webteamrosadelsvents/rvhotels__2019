<?php
/*
Template Name: Page URBANY
*/
?>
<?php get_header('covid'); ?>
<style>

.selector-icon {
    font-size: 25px;
    color: #588aab!important;
    min-height: 35px;
    position: relative;
}

h2 {
    text-transform: none!important;
}

/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {
    
    .posicion-imagen-covid {
        margin-top: -155px;
    }
    
    .display-4{
        font-size:40px;
    }
    
    .display-2{
        font-size: 5rem;
    }
    
    .mesures {
        background-image: url("https://www.rvhotels.es/images/covid/bg_urbany_london.jpg");
        background-repeat: no-repeat;
    }
}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {
    .posicion-imagen-covid {
        margin-top: -115px;
    }
}

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {
    .posicion-imagen-covid {
        margin-top: -160px;
    }
}

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {
    .posicion-imagen-covid {
        margin-top: -160px;
    }
}
</style>

<div id="covid">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 p-0 pt-5 pt-sm-5 pt-md-5 bg-category mesures" style="box-shadow: 0px 0px 15px rgba(0,0,0,0.5);">
                <div class="text-center col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-12 offset-md-0 pt-5">
                    <!--h1 class="display-2 font-weight-bold blue sombras-2"><?php echo __( "RVHotels","Category Mesures"); ?></h1-->
                    <h1 class="display-2 font-weight-bold white sombras pt-0 mt-0 pt-md-5 mt-md-5"><?php echo __( "Programa Health & Safe<br/> Hoteles y Apartamentos Covid-Free","Category Mesures"); ?></h1>
                </div>    
            </div>
            <div class="col-12">
                <div class="container p-0">
                    <div class="row">
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-4 posicion-imagen-covid text-center pt-5">
                            <img src="https://www.rvhotels.es/images/covid/urbany.svg" class="w-75" alt="">
                        </div>
                        <div class="col-sm-4">
                        </div>
                    </div>
                </div>    
            </div>
            <div class="col-12 pb-5 px-0 ">
                <div class="container text-left p-0">
                    <div class="row">
                        <div class="col-12 pb-5 my-5 px-5">
                            <p class="display-4 font-weight-bold text-center p-5" style="color:#616161;"><?php echo __( "<strong>After days of uncertainty, our priority is to offer happiness and health to our clients.</strong>","Category Mesures"); ?></p>
                            <p class="mt-5 h2" style="color:#616161;"><?php echo __( "We have been working on the implementation of processes and certificates that give us the security to enjoy the small moments that will <strong>reinvent</strong> this atypical summer.","Category Mesures"); ?></p>
                            <p class="mt-5 h2" style="color:#616161;"><?php echo __( "@ Urbany Hostels we have launched our Health&Safe program that guarantees that in fact, your only concern will be to enjoy. An extraordinary Health and Safety committee has been created to design a contingency plan to adapt all operations to the new health needs and thus be able to offer maximum safety guarantees to our employees and customers.","Category Mesures"); ?></p>
                            <p class="mt-5 h2" style="color:#616161;"><?php echo __( "Following the previously mentioned contingency plan, we detail the main lines of work that will serve as a thread in all our protocols:","Category Mesures"); ?></p>
                        </div> 
                    </div>                                            
                </div>  
                <div class="container-fluid p-5" style="background-color:#588aab;">
                    <div class="col-12">
                        <p class="display-4 font-weight-bold text-center p-1 white"><?php echo __( "Our main lines of action","Category Mesures"); ?></p>
                        <p class="display-3 font-weight-bold text-center p-1 white"><strong><?php echo __( "Health & Safe Program","Category Mesures"); ?></strong></p>
                    </div>
                    <div class="container py-5">
                        <div class="row">
                            <div class="col-lg-2 col-md-6 text-center">
                                <div class="mt-5">
                                    <svg version="1.1" id="Line_cutting_stroke_ex" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"  width="100px" class="mb-4">
                                    <style type="text/css">
                                        .st0{fill:#FFFFFF;}
                                    </style>
                                    <g>
                                        <path class="st0" d="M56,206c0-12.1,9.9-22,22-22h84c12.1,0,22,9.9,22,22v42h16v-42c0-21-17-38-38-38h-2v-46c0-9.9-8.1-18-18-18H98
                                            c-9.9,0-18,8.1-18,18v46h-2c-21,0-38,17-38,38v50h16V206z M96,122c0-1.1,0.9-2,2-2h44c1.1,0,2,0.9,2,2v46H96V122z"/>
                                        <path class="st0" d="M168,256v-30c0-9.9-8.1-18-18-18H90c-9.9,0-18,8.1-18,18v30h16v-30c0-1.1,0.9-2,2-2h60c1.1,0,2,0.9,2,2v30H168
                                            z"/>
                                        <path class="st0" d="M72,416c-13.3,0-24,10.7-24,24s10.7,24,24,24s24-10.7,24-24l0,0C96,426.8,85.2,416,72,416z M72,448
                                            c-4.4,0-8-3.6-8-8s3.6-8,8-8s8,3.6,8,8C80,444.4,76.4,448,72,448z"/>
                                        <path class="st0" d="M192.7,163.2c0.9,2.1,2.7,3.6,4.8,4.4l24,8c3.1,1,6.4,0.1,8.6-2.3l42-48v16.2l-54.6,77.9
                                            c-0.9,1.3-1.4,2.9-1.4,4.6v24h16v-21.5l52.2-74.5H322l22,73.2V256h16v-32c0-0.8-0.1-1.6-0.3-2.3L336,142.8V112v0c0-0.1,0-0.1,0-0.2
                                            l0,0c0-0.1,0-0.1,0-0.2l0-0.1l0-0.1c0-0.1,0-0.1,0-0.2l0,0c-0.1-1.2-0.5-2.3-1.1-3.3L302.9,52c-1.4-2.5-4.1-4-6.9-4h-96
                                            c-4.4,0-8,3.6-8,8v24c0,3.4,2.2,6.5,5.5,7.6l11.7,3.9l-0.6,1.5c-1.6,4.1,0.4,8.8,4.5,10.4c0.3,0.1,0.7,0.2,1,0.3l2,0.5l-23.3,52.5
                                            C191.8,158.8,191.8,161.2,192.7,163.2L192.7,163.2z M288,136v-16h32v16H288z M237.4,64h53.9l22.9,40H281l-54.2-13.5L237.4,64z
                                            M208,74.2V64h12.2l-5,12.6L208,74.2z M231.8,108.2l27.8,7l-38.1,43.6l-10.6-3.5L231.8,108.2z"/>
                                        <path class="st0" d="M494.7,411.6l-45-67.5c-3.3-5-9-8-15-8H424V88h-16v248h-16V34c0-1.1,0.9-2,2-2h12c1.1,0,2,0.9,2,2v38h16V34
                                            c0-9.9-8.1-18-18-18h-12c-9.9,0-18,8.1-18,18v238h-88c0-4.4-3.6-8-8-8H144c-4.4,0-8,3.6-8,8H24c-4.4,0-8,3.6-8,8v12
                                            c0,15.5,12.5,28,28,28h4v32h16v-32h72v96c0,4.4,3.6,8,8,8h96v-16h-88v-24h120v24h-16v16h24c4.4,0,8-3.6,8-8v-96h88v16h-10.6
                                            c-6,0-11.6,3-15,8l-45,67.5c-0.9,1.3-1.3,2.9-1.3,4.4v64H111.2c22.1-21.6,22.5-57.1,0.8-79.2c-12.5-12.8-30.3-18.8-48-16.2V368H48
                                            v21.4C20.1,402.7,8.1,436.1,21.4,464c9.3,19.5,29,32,50.6,32c1.3,0,2.7,0,4-0.1v0.1h412c4.4,0,8-3.6,8-8v-72
                                            C496,414.4,495.5,412.9,494.7,411.6z M272,280v56H152v-56H272z M44,304c-6.6,0-12-5.4-12-12v-4h104v16H44z M152,368v-16h120v16H152
                                            z M288,304v-16h88v16H288z M32,440c0-22.1,17.9-40,40-40s40,17.9,40,40s-17.9,40-40,40C49.9,480,32,462.1,32,440z M363.7,352.9
                                            c0.4-0.6,1-0.9,1.7-0.9h69.3c0.7,0,1.3,0.3,1.7,0.9l42.1,63.1H321.6L363.7,352.9z M448,480v-32h-16v32h-24v-32h-16v32h-24v-32h-16
                                            v32h-32v-48h160v48H448z"/>
                                        <path class="st0" d="M368,376h16v16h-16V376z"/>
                                        <path class="st0" d="M416,376h16v16h-16V376z"/>
                                    </g>
                                    </svg>
                                    <hr class="my-4" style="border-color:#ffffff!important;">
                                    <p class="white mt-5 h3"><?php echo __( "New cleaning, hygiene and disinfection protocols","Category Mesures"); ?></p>
                                </div>
                            </div>
                                <div class="col-lg-2 col-md-6 text-center">
                                    <div class="mt-5">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="100px" class="mb-4">
                                        <style type="text/css">
                                            .st0{fill:#FFFFFF;}
                                        </style>
                                        <g>
                                            <path class="st0" d="M165,364.7L132.3,342c-1.4-1-2.2-2.5-2.2-4.2v-24.2c12.5-9.8,20.6-25.1,20.6-42.2v-15.7
                                                c0-29.6-24.1-53.6-53.6-53.6c0,0,0,0,0,0c-14.3,0-27.8,5.6-37.9,15.7c-10.1,10.1-15.7,23.6-15.7,37.9v15.7
                                                c0,17.1,8.1,32.4,20.6,42.2v24.2c0,1.7-0.8,3.2-2.2,4.2l-15,10.4c-3.4,2.4-4.2,7-1.9,10.4c1.5,2.1,3.8,3.2,6.1,3.2
                                                c1.5,0,3-0.4,4.3-1.3l15-10.4c5.4-3.8,8.6-9.9,8.6-16.5v-15.9c5.7,2,11.7,3.1,18.1,3.1c6.3,0,12.4-1.1,18.1-3.1v15.9
                                                c0,6.6,3.2,12.7,8.6,16.5l32.6,22.7c14.3,10,22.8,26.3,22.8,43.7v18.4c0,1.8-1.3,3.4-3.1,3.8c-52.4,9.9-105.6,9.9-158.1,0
                                                c-1.8-0.3-3.1-1.9-3.1-3.8v-18.4c0-14.1,5.8-27.9,15.8-37.8c2.9-2.9,3-7.6,0.1-10.6c-2.9-2.9-7.6-3-10.6-0.1
                                                C7.4,385,0,402.6,0,420.7v18.4c0,9,6.4,16.8,15.3,18.5c27.1,5.1,54.5,7.7,81.8,7.7c27.3,0,54.7-2.6,81.8-7.7
                                                c8.9-1.7,15.3-9.4,15.3-18.5v-18.4C194.2,398.4,183.3,377.5,165,364.7L165,364.7z M58.4,271.4v-15.7c0-10.3,4-20,11.3-27.4
                                                c7.3-7.3,17-11.3,27.4-11.3c21.3,0,38.7,17.4,38.7,38.7v15.7c0,21.3-17.4,38.7-38.7,38.7S58.4,292.8,58.4,271.4z"/>
                                            <path class="st0" d="M493.2,373.6c-2.9-3-7.6-3.1-10.6-0.3c-3,2.9-3.1,7.6-0.3,10.6c9.5,10,14.7,23,14.7,36.7v18.4
                                                c0,1.8-1.3,3.4-3.1,3.8c-52.4,9.9-105.6,9.9-158.1,0c-1.8-0.3-3.1-1.9-3.1-3.8v-18.4c0-17.4,8.5-33.8,22.8-43.7l32.6-22.7
                                                c5.4-3.8,8.6-9.9,8.6-16.5V322c5.7,2,11.8,3.1,18.1,3.1c6.3,0,12.4-1.1,18.1-3.1v15.9c0,6.6,3.2,12.7,8.6,16.5l16.4,11.4
                                                c1.3,0.9,2.8,1.3,4.3,1.3c2.4,0,4.7-1.1,6.1-3.2c2.4-3.4,1.5-8.1-1.9-10.4L450.2,342c-1.4-1-2.2-2.5-2.2-4.2v-23.6
                                                c0-0.2,0-0.4,0-0.6c12.5-9.8,20.6-25.1,20.6-42.2v-15.7c0-29.6-24.1-53.6-53.6-53.6c-27,0-49.8,20.2-53.2,46.9
                                                c-0.5,4.1,2.4,7.8,6.5,8.4c4.1,0.5,7.8-2.4,8.4-6.5c2.4-19.3,18.9-33.8,38.4-33.8c21.3,0,38.7,17.4,38.7,38.7v15.7
                                                c0,21.3-17.4,38.7-38.7,38.7c-18.1,0-33.6-12.3-37.7-30c-0.9-4-4.9-6.5-9-5.6c-4,0.9-6.5,4.9-5.6,9c2.9,12.4,9.8,23,19.2,30.3
                                                c0,0.2,0,0.3,0,0.5v23.6c0,1.7-0.8,3.2-2.2,4.2L347,364.7c-18.3,12.7-29.2,33.7-29.2,56v18.4c0,9,6.4,16.8,15.3,18.5
                                                c27.1,5.1,54.5,7.7,81.8,7.7c27.3,0,54.7-2.6,81.8-7.7c8.9-1.7,15.3-9.4,15.3-18.5v-18.4C512,403.1,505.3,386.4,493.2,373.6
                                                L493.2,373.6z"/>
                                            <path class="st0" d="M284.5,403.8c-2.3-3.4-7-4.3-10.4-1.9c-3.4,2.3-4.3,7-1.9,10.4l2.9,4.3h-38.1l2.9-4.3
                                                c2.3-3.4,1.5-8.1-1.9-10.4c-3.4-2.3-8.1-1.5-10.4,1.9l-11,16c-1.7,2.5-1.7,5.9,0,8.5l11,16c1.4,2.1,3.8,3.3,6.2,3.3
                                                c1.5,0,2.9-0.4,4.2-1.3c3.4-2.3,4.3-7,1.9-10.4l-2.9-4.3h38.1l-2.9,4.3c-2.3,3.4-1.5,8.1,1.9,10.4c1.3,0.9,2.8,1.3,4.2,1.3
                                                c2.4,0,4.7-1.1,6.2-3.3l11-16c1.7-2.5,1.7-5.9,0-8.5L284.5,403.8z"/>
                                            <path class="st0" d="M32.7,167.4c1.4,2.1,3.8,3.3,6.2,3.3c1.5,0,2.9-0.4,4.2-1.3c3.4-2.3,4.3-7,1.9-10.4l-2.9-4.3h427.7l-2.9,4.3
                                                c-2.3,3.4-1.5,8.1,1.9,10.4c1.3,0.9,2.8,1.3,4.2,1.3c2.4,0,4.7-1.1,6.2-3.3l11-16c1.7-2.5,1.7-5.9,0-8.5l-11-16
                                                c-2.3-3.4-7-4.3-10.4-1.9c-3.4,2.3-4.3,7-1.9,10.4l2.9,4.3H42.1l2.9-4.3c2.3-3.4,1.5-8.1-1.9-10.4c-3.4-2.3-8.1-1.5-10.4,1.9
                                                l-11,16c-1.7,2.5-1.7,5.9,0,8.5L32.7,167.4z"/>
                                            <path class="st0" d="M192.1,79.8c4.1,0,7.5-3.4,7.5-7.5v-4.5c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1v5.1c0,1.5-0.6,3-1.6,4.1
                                                L188,101.4c-2,2.2-2.5,5.4-1.3,8.1c1.2,2.7,3.9,4.5,6.8,4.5h25.8c4.1,0,7.5-3.4,7.5-7.5s-3.3-7.5-7.5-7.5h-8.8l10.8-11.9
                                                c3.5-3.9,5.5-8.9,5.5-14.2v-5.1c0-11.6-9.5-21.1-21.1-21.1c-11.6,0-21.1,9.5-21.1,21.1v4.5C184.6,76.5,188,79.8,192.1,79.8z"/>
                                            <path class="st0" d="M265.4,113.9c4.1,0,7.5-3.4,7.5-7.5V73.3l6.3,6.7c1.5,1.6,3.6,2.5,5.7,2.4c2.2-0.1,4.2-1.1,5.6-2.8l3.4-4.2v31
                                                c0,4.1,3.3,7.5,7.5,7.5s7.5-3.4,7.5-7.5V54.2c0-3.2-2-6-5-7.1c-3-1.1-6.3-0.1-8.3,2.4l-11.3,14l-13.3-14.4c-2.1-2.3-5.4-3-8.2-1.9
                                                c-2.9,1.1-4.7,3.9-4.7,7v52.2C257.9,110.6,261.2,113.9,265.4,113.9z"/>
                                            <path class="st0" d="M328.5,113.9h3.4c4.1,0,7.5-3.4,7.5-7.5s-3.3-7.5-7.5-7.5h-3.4c-4.1,0-7.5,3.4-7.5,7.5
                                                S324.4,113.9,328.5,113.9z"/>
                                        </g>
                                        </svg>
                                        <hr class="my-4" style="border-color:#ffffff!important;">
                                        <p class="white mt-5 h3"><?php echo __( "Social distancing","Category Mesures"); ?></p>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-6 text-center">
                                    <div class="mt-5">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="100px" class="mb-4">
                                        <style type="text/css">
                                            .st0{fill:#FFFFFF;}
                                        </style>
                                        <g>
                                            <path class="st0" d="M263.5,324v15H497v45H15v-15H0v30h512v-75H263.5z"/>
                                            <path class="st0" d="M15,339h233.5v-15H0v30h15V339z"/>
                                            <path class="st0" d="M101.8,187.5l-11-10.2c-34,36.4-54.6,81.6-59.6,131l14.9,1.5C50.8,263.7,70.1,221.5,101.8,187.5z"/>
                                            <path class="st0" d="M112,177.3c39.2-36.6,90.3-56.8,144-56.8c108.6,0,198.9,81.4,209.9,189.3l14.9-1.5
                                                c-5.7-55.3-31.6-106.5-72.9-144.1c-33.4-30.3-74.6-49.7-118.7-56.2c2.8-5.3,4.3-11.3,4.3-17.4c0-20.7-16.8-37.5-37.5-37.5
                                                s-37.5,16.8-37.5,37.5c0,6.2,1.5,12.1,4.3,17.4c-45.1,6.7-87.2,26.8-121,58.4L112,177.3z M233.5,90.5c0-12.4,10.1-22.5,22.5-22.5
                                                c12.4,0,22.5,10.1,22.5,22.5c0,5.9-2.3,11.4-6.3,15.6c-5.4-0.4-10.8-0.6-16.2-0.6c-5.4,0-10.8,0.2-16.2,0.6
                                                C235.8,101.9,233.5,96.4,233.5,90.5z"/>
                                            <path class="st0" d="M226,143h15v15h-15V143z"/>
                                            <path class="st0" d="M416.8,272.1l13.9-5.5c-14.5-36.6-37.7-67.3-66.9-88.8C332.8,155,295.6,143,256,143v15
                                                C328.2,158,388.3,200.7,416.8,272.1z"/>
                                            <path class="st0" d="M63.4,294h15v15h-15V294z"/>
                                            <path class="st0" d="M93.4,294h15v15h-15V294z"/>
                                            <path class="st0" d="M120,444h-15v15h30v-45h-15V444z"/>
                                            <path class="st0" d="M75,414H60v45h30v-15H75V414z"/>
                                            <path class="st0" d="M437,444h-15v15h30v-45h-15V444z"/>
                                            <path class="st0" d="M392,414h-15v45h30v-15h-15V414z"/>
                                            <path class="st0" d="M434,354h45v15h-45V354z"/>
                                            <path class="st0" d="M404,354h15v15h-15V354z"/>
                                            <path class="st0" d="M263.5,214.8h15v15h-15V214.8z"/>
                                            <path class="st0" d="M233.5,214.8h15v15h-15V214.8z"/>
                                        </g>
                                        </svg>
                                        <hr class="my-4" style="border-color:#ffffff!important;">
                                        <p class="white mt-5 h3"><?php echo __( "New buffet and restoration protocols","Category Mesures"); ?></p>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-6 text-center">
                                    <div class="mt-5">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="100px" class="mb-4">
                                        <style type="text/css">
                                            .st0{fill:#FFFFFF;}
                                        </style>
                                        <g>
                                            <circle class="st0" cx="189.6" cy="58.5" r="5.2"/>
                                            <path class="st0" d="M215.8,407.2h-52.4c-10.8,0-19.6,8.8-19.6,19.6s8.8,19.6,19.6,19.6h52.4c10.8,0,19.6-8.8,19.6-19.6
                                                S226.6,407.2,215.8,407.2z M215.8,433.4h-52.4c-3.6,0-6.5-2.9-6.5-6.5s2.9-6.5,6.5-6.5h52.4c3.6,0,6.5,2.9,6.5,6.5
                                                S219.4,433.4,215.8,433.4z"/>
                                            <path class="st0" d="M235.4,209.9c0,3.6,2.9,6.5,6.5,6.5h185.1c3.6,0,6.5-2.9,6.5-6.5s-2.9-6.5-6.5-6.5H242
                                                C238.4,203.4,235.4,206.3,235.4,209.9z"/>
                                            <path class="st0" d="M427.1,229.6H242c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5h185.1c3.6,0,6.5-2.9,6.5-6.5
                                                S430.7,229.6,427.1,229.6z"/>
                                            <path class="st0" d="M427.1,255.8H242c-3.6,0-6.5,2.9-6.5,6.5s2.9,6.5,6.5,6.5h185.1c3.6,0,6.5-2.9,6.5-6.5
                                                S430.7,255.8,427.1,255.8z"/>
                                            <path class="st0" d="M479.5,153.2H340.2V58.5c0-18.1-14.7-32.7-32.7-32.7H71.7C53.7,25.7,39,40.4,39,58.5V440
                                                c0,18.1,14.7,32.7,32.7,32.7h235.7c18.1,0,32.7-14.7,32.7-32.7V319.1h139.3c3.6,0,6.5-2.9,6.5-6.5V159.7
                                                C486,156.1,483.1,153.2,479.5,153.2z M327.1,440c0,10.8-8.8,19.6-19.6,19.6H71.7c-10.8,0-19.6-8.8-19.6-19.6V58.5
                                                c0-10.8,8.8-19.6,19.6-19.6h235.7c10.8,0,19.6,8.8,19.6,19.6v94.7H314V58.5c0-3.6-2.9-6.5-6.5-6.5h-80.8c-1.7,0-3.4,0.7-4.6,1.9
                                                l-18.5,18.5c-3.7,3.7-8.7,5.8-13.9,5.8c-5.2,0-10.2-2-13.9-5.8c0,0-11.5-11.5-18.5-18.5c-1.2-1.2-2.9-1.9-4.6-1.9H71.7
                                                c-3.6,0-6.5,2.9-6.5,6.5v151.5c0,3.6,2.9,6.5,6.5,6.5s6.5-2.9,6.5-6.5V65h71.6c0,0,15.9,15.9,16.7,16.6l0,0
                                                c6.2,6.2,14.4,9.6,23.1,9.6c8.7,0,16.9-3.4,23.2-9.6L229.4,65h71.6v88.2H189.6c-3.6,0-6.5,2.9-6.5,6.5v52.7l-35.7,17.8
                                                c-2.2,1.1-3.6,3.4-3.6,5.9s1.4,4.7,3.6,5.9l35.7,17.8v52.7c0,3.6,2.9,6.5,6.5,6.5h111.3v62H78.3V236.1c0-3.6-2.9-6.5-6.5-6.5
                                                s-6.5,2.9-6.5,6.5v151.5c0,3.6,2.9,6.5,6.5,6.5h235.7c3.6,0,6.5-2.9,6.5-6.5v-68.5h13.1V440z M472.9,306H196.1v-50.2
                                                c0-2.5-1.4-4.7-3.6-5.9L165,236.1l27.6-13.8c2.2-1.1,3.6-3.4,3.6-5.9v-50.2h276.8L472.9,306L472.9,306z"/>
                                        </g>
                                        </svg>

                                        <hr class="my-4" style="border-color:#ffffff!important;">
                                        <p class="white mt-5 h3"><?php echo __( "Digitalization of procedures","Category Mesures"); ?></p>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-6 text-center">
                                    <div class="mt-5">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 496 496" style="enable-background:new 0 0 496 496;" xml:space="preserve" width="100px" class="mb-4">
                                        <style type="text/css">
                                            .st0{fill:#FFFFFF;}
                                        </style>
                                        <path class="st0" d="M479.4,400.8v-55.5l-21.3-5.4c-0.1-0.4-0.3-0.7-0.4-1.1l11.2-18.9l-39.3-39.3l-18.9,11.2
                                            c-0.4-0.2-0.7-0.3-1.1-0.4l-5.4-21.3h-55.5l-5.4,21.3c-0.4,0.1-0.7,0.3-1.1,0.4l-18.9-11.2L284,319.9l11.2,18.9
                                            c-0.2,0.4-0.3,0.7-0.4,1.1l-21.3,5.4v55.5l21.3,5.4c0.1,0.4,0.3,0.7,0.4,1.1L284,426.2l39.3,39.3l18.9-11.2c0.4,0.2,0.7,0.3,1.1,0.4
                                            l5.4,21.3h55.5l5.4-21.3c0.4-0.1,0.7-0.3,1.1-0.4l18.9,11.2l39.3-39.3l-11.2-18.9c0.2-0.4,0.3-0.7,0.4-1.1L479.4,400.8z
                                            M450.4,423.9L427.3,447l-15.7-9.3l-3.5,1.7c-2.3,1.1-4.6,2.1-7,2.9l-3.7,1.3l-4.5,17.7h-32.7l-4.5-17.7l-3.7-1.3
                                            c-2.4-0.8-4.7-1.8-7-2.9l-3.5-1.7l-15.7,9.3l-23.1-23.1l9.3-15.7l-1.7-3.5c-1.1-2.3-2.1-4.6-2.9-7l-1.3-3.7l-17.7-4.5v-32.7
                                            l17.7-4.5l1.3-3.7c0.8-2.4,1.8-4.7,2.9-7l1.7-3.5l-9.3-15.7l23.1-23.1l15.7,9.3l3.5-1.7c2.3-1.1,4.6-2.1,7-2.9l3.7-1.3l4.5-17.7
                                            h32.7l4.5,17.7l3.7,1.3c2.4,0.8,4.7,1.8,7,2.9l3.5,1.7l15.7-9.3l23.1,23.1l-9.3,15.7l1.7,3.5c1.1,2.3,2.1,4.6,2.9,7l1.3,3.7
                                            l17.7,4.5v32.7l-17.7,4.5l-1.3,3.7c-0.8,2.4-1.8,4.7-2.9,7l-1.7,3.5L450.4,423.9z"/>
                                        <path class="st0" d="M376.5,314.2c-32.4,0-58.8,26.4-58.8,58.8s26.4,58.8,58.8,58.8s58.8-26.4,58.8-58.8S408.9,314.2,376.5,314.2z
                                            M376.5,417.2c-24.3,0-44.1-19.8-44.1-44.1s19.8-44.1,44.1-44.1c24.3,0,44.1,19.8,44.1,44.1S400.8,417.2,376.5,417.2z"/>
                                        <path class="st0" d="M376.5,343.6c-16.2,0-29.4,13.2-29.4,29.4s13.2,29.4,29.4,29.4c16.2,0,29.4-13.2,29.4-29.4
                                            S392.7,343.6,376.5,343.6z M376.5,387.7c-8.1,0-14.7-6.6-14.7-14.7s6.6-14.7,14.7-14.7c8.1,0,14.7,6.6,14.7,14.7
                                            S384.6,387.7,376.5,387.7z"/>
                                        <path class="st0" d="M209.7,144.5c2.5,7.2,6.8,13.6,12.3,18.6v6.2l-35,13.1c-14.3,5.3-23.9,19.2-23.9,34.4v38.5h176.5v-38.5
                                            c0-15.2-9.6-29.1-23.9-34.4l-35-13.1v-6.2c5.6-5,9.9-11.4,12.3-18.6c13.9-2.4,24.5-14.4,24.5-28.9V86.2c0-36.5-29.7-66.2-66.2-66.2
                                            s-66.2,29.7-66.2,66.2v29.4C185.2,130.1,195.8,142.2,209.7,144.5z M266.1,172.4l-14.7,24.5l-14.7-24.5v-0.5
                                            c4.6,1.6,9.5,2.6,14.7,2.6c5.2,0,10.1-0.9,14.7-2.6V172.4z M226.2,183.5l14.8,24.7l-14.7,7.4l-9.5-28.5L226.2,183.5z M325,216.9
                                            v23.7H177.9v-23.7c0-9.2,5.8-17.4,14.3-20.7l10.8-4.1l14.7,44.1l33.7-16.9l33.7,16.9l14.7-44.1l10.8,4.1
                                            C319.2,199.5,325,207.8,325,216.9z M286.1,187l-9.5,28.5l-14.7-7.4l14.8-24.7L286.1,187z M251.4,159.7c-16.2,0-29.4-13.2-29.4-29.4
                                            v-29.9c10.8-1.1,20.9-5.1,29.4-11.7c8.5,6.7,18.7,10.6,29.4,11.7v29.9C280.8,146.5,267.6,159.7,251.4,159.7z M295.5,128.3V103
                                            c4.4,2.6,7.4,7.2,7.4,12.7S299.9,125.7,295.5,128.3z M251.4,34.7c28.4,0,51.5,23.1,51.5,51.5v4.1c-4.3-2.5-9.3-4.1-14.7-4.1h-1.3
                                            c-11.5,0-22.2-4.5-30.3-12.6l-5.2-5.2l-5.2,5.2c-8.1,8.1-18.9,12.6-30.3,12.6h-1.3c-5.4,0-10.4,1.6-14.7,4.1v-4.1
                                            C199.9,57.8,223,34.7,251.4,34.7z M207.3,102.9v25.3c-4.4-2.6-7.4-7.2-7.4-12.7S202.9,105.5,207.3,102.9z"/>
                                        <path class="st0" d="M23.4,476h147.1V284.8H23.4V476z M38.1,299.5h117.7v161.8H38.1V299.5z"/>
                                        <path class="st0" d="M52.8,314.2h88.3v14.7H52.8V314.2z"/>
                                        <path class="st0" d="M52.8,343.6h14.7v14.7H52.8V343.6z"/>
                                        <path class="st0" d="M82.3,343.6h58.8v14.7H82.3V343.6z"/>
                                        <path class="st0" d="M52.8,373h88.3v14.7H52.8V373z"/>
                                        <path class="st0" d="M52.8,402.5h88.3v14.7H52.8V402.5z"/>
                                        <path class="st0" d="M126.4,431.9h14.7v14.7h-14.7V431.9z"/>
                                        <path class="st0" d="M52.8,431.9h58.8v14.7H52.8V431.9z"/>
                                        <path class="st0" d="M219.8,348.8l-10.4-10.4L174.8,373l34.6,34.6l10.4-10.4L203,380.4h55.8v-14.7H203L219.8,348.8z"/>
                                        <path class="st0" d="M104.3,137.7c0-4.1,3.3-7.4,7.4-7.4h33.7l-16.9,16.9l10.4,10.4l34.6-34.6l-34.6-34.6l-10.4,10.4l16.9,16.9
                                            h-33.7c-12.2,0-22.1,9.9-22.1,22.1v132.4h14.7V137.7z"/>
                                        <path class="st0" d="M376.5,137.7v92.6l-16.9-16.9l-10.4,10.4l34.6,34.6l34.6-34.6L408,213.4l-16.9,16.9v-92.6
                                            c0-12.2-9.9-22.1-22.1-22.1h-36.8v14.7h36.8C373.2,130.3,376.5,133.6,376.5,137.7z"/>
                                        </svg>
                                    <hr class="my-4" style="border-color:#ffffff!important;">
                                    <p class="white mt-5 h3"><?php echo __( "Employee training","Category Mesures"); ?></p>
                                    </div>          
                                </div>  
                                <div class="col-lg-2 col-md-6 text-center">
                                    <div class="mt-5">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="100px" class="mb-4">
                                        <style type="text/css">
                                            .st0{fill:#FFFFFF;}
                                        </style>
                                        <g>
                                            <g>
                                                <path class="st0" d="M421.3,107.8H178.5c-3.9,0-7.1-3.2-7.1-7.1c0-3.9,3.2-7.1,7.1-7.1h242.8c3.9,0,7.1,3.2,7.1,7.1
                                                    C428.4,104.6,425.2,107.8,421.3,107.8z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M421.3,193.4H407c-3.9,0-7.1-3.2-7.1-7.1s3.2-7.1,7.1-7.1h14.3c3.9,0,7.1,3.2,7.1,7.1
                                                    S425.2,193.4,421.3,193.4z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M421.3,236.3c-1.9,0-3.7-0.8-5.1-2.1c-1.3-1.3-2.1-3.2-2.1-5c0-1.9,0.8-3.7,2.1-5.1c1.3-1.3,3.2-2.1,5.1-2.1
                                                    s3.7,0.8,5.1,2.1c1.3,1.3,2.1,3.2,2.1,5.1c0,1.9-0.8,3.7-2.1,5C425,235.5,423.2,236.3,421.3,236.3z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M421.3,150.6H292.8c-3.9,0-7.1-3.2-7.1-7.1c0-3.9,3.2-7.1,7.1-7.1h128.5c3.9,0,7.1,3.2,7.1,7.1
                                                    C428.4,147.4,425.2,150.6,421.3,150.6z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M264.2,150.6h-85.7c-3.9,0-7.1-3.2-7.1-7.1c0-3.9,3.2-7.1,7.1-7.1h85.7c3.9,0,7.1,3.2,7.1,7.1
                                                    C271.3,147.4,268.1,150.6,264.2,150.6z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M435.6,347.6c-3.7,0-7.3-1.5-10.1-4.2l-9.2-9.2c-2.8-2.8-2.8-7.3,0-10.1c2.8-2.8,7.3-2.8,10.1,0l9.2,9.2
                                                    l0-47.7c0-3.5,2.5-6.5,5.9-7c17-2.9,29.8-18,29.8-35.2V86.3c0-19.7-16-35.7-35.7-35.7H164.2c-19.7,0-35.7,16-35.7,35.7v71.4
                                                    c0,3.9-3.2,7.1-7.1,7.1c-3.9,0-7.1-3.2-7.1-7.1V86.3c0-27.6,22.4-50,50-50h271.3c27.6,0,50,22.4,50,50v157.1
                                                    c0,22.4-14.7,41.6-35.7,47.9v42c0,5.8-3.5,11-8.8,13.2C439.3,347.2,437.4,347.6,435.6,347.6z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M335.6,250.6H92.8c-3.9,0-7.1-3.2-7.1-7.1s3.2-7.1,7.1-7.1h242.8c3.9,0,7.1,3.2,7.1,7.1
                                                    S339.6,250.6,335.6,250.6z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M335.6,336.3h-92.8c-3.9,0-7.1-3.2-7.1-7.1c0-3.9,3.2-7.1,7.1-7.1h92.8c3.9,0,7.1,3.2,7.1,7.1
                                                    C342.7,333.1,339.6,336.3,335.6,336.3z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M214.2,336.3H92.8c-3.9,0-7.1-3.2-7.1-7.1c0-3.9,3.2-7.1,7.1-7.1h121.4c3.9,0,7.1,3.2,7.1,7.1
                                                    C221.4,333.1,218.2,336.3,214.2,336.3z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M335.6,379.1H92.8c-3.9,0-7.1-3.2-7.1-7.1s3.2-7.1,7.1-7.1h242.8c3.9,0,7.1,3.2,7.1,7.1
                                                    S339.6,379.1,335.6,379.1z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M135.7,293.4H92.8c-3.9,0-7.1-3.2-7.1-7.1c0-3.9,3.2-7.1,7.1-7.1h42.8c3.9,0,7.1,3.2,7.1,7.1
                                                    C142.8,290.2,139.6,293.4,135.7,293.4z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M335.6,293.4H164.2c-3.9,0-7.1-3.2-7.1-7.1c0-3.9,3.2-7.1,7.1-7.1h171.4c3.9,0,7.1,3.2,7.1,7.1
                                                    C342.7,290.2,339.6,293.4,335.6,293.4z"/>
                                            </g>
                                            <g>
                                                <path class="st0" d="M78.6,490.4c-1.8,0-3.7-0.4-5.5-1.1c-5.4-2.2-8.8-7.4-8.8-13.2v-42c-21-6.2-35.7-25.5-35.7-47.9V229.1
                                                    c0-27.6,22.4-50,50-50h271.3c27.6,0,50,22.4,50,50v157.1c0,27.6-22.4,50-50,50H138.6l-50,50C85.9,488.9,82.3,490.4,78.6,490.4z
                                                    M78.5,193.4c-19.7,0-35.7,16-35.7,35.7v157.1c0,17.2,12.8,32.3,29.8,35.2c3.4,0.6,5.9,3.6,5.9,7v47.7l52.1-52.1
                                                    c1.3-1.3,3.2-2.1,5-2.1h214.2c19.7,0,35.7-16,35.7-35.7V229.1c0-19.7-16-35.7-35.7-35.7L78.5,193.4L78.5,193.4z"/>
                                            </g>
                                        </g>
                                        </svg>
                                    <hr class="my-4" style="border-color:#ffffff!important;">
                                    <p class="white mt-5 h3"><?php echo __( "Development of new communication channels","Category Mesures"); ?></p>
                                    </div>          
                                </div> 
                            </div>
                        </div>
                    </div>
                    <div class="text-center col-12">
                            <p class="mt-3 px-5 pt-5 h2"><?php echo __( "Below you will find the <strong>main measures adopted</strong> based on the above criteria by hostel space.","Category Mesures"); ?></p>             
                    </div>                          
                <div class="container">
                <div class="row px-5">
                    <div class="col-12 col-sm-6 text-left pt-5">
                        <div class="text-center">
                            <img src="https://www.rvhotels.es/images/covid/hotel.svg" class="w-25" alt="">
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <p class="display-4 font-weight-bold text-center p-5" style="color:#616161;"><strong><?php echo __( "Common areas","Category Mesures"); ?></strong></p>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Increase the frequency of cleaning and disinfection of common areas in hostels, reception desks, lifts, doors, bathrooms...","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Provision of disinfectant gel in the most recurrent points of possible transmission such as access to lifts, toilets, reception...","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Capacity control of all common areas (bar, dining area, guest kitchen, luggage room, rooftops, etc.) of all hostels.","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "ELEVATORS: Use of masks compulsory if they are not members of the same family unit","Category Mesures"); ?></p>
                                    </div>
                                </div>                                                        
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 text-left pt-5">
                        <div class="text-center">
                            <img src="https://www.rvhotels.es/images/covid/pool.svg" class="w-25" alt="">
                        </div>
                        <div class="row">
                            <div class="col-12 text-center">
                                <p class="display-4 font-weight-bold text-center p-5" style="color:#616161;"><strong><?php echo __( "Recreational Spaces","Category Mesures"); ?></strong></p>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "New Maximum capacity established, ensuring safety distance between clients","Category Mesures"); ?></p>
                                    </div>
                                </div>                                                        
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Loungers shall be at least 1.5 metres apart","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>                                                    
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Increased disinfection and cleaning of all facilities in the pool, the spa and the gym.","Category Mesures"); ?> </p>
                                    </div>
                                </div>                                                        
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row px-5">
                    <div class="col-12 col-sm-6 text-left pt-5">
                            <div class="text-center">
                                <img src="https://www.rvhotels.es/images/covid/room.svg" class="w-25" alt="">
                            </div>                                        
                            <div class="row">
                                <div class="col-12 text-center">
                                    <p class="display-4 font-weight-bold text-center p-5" style="color:#616161;"><strong><?php echo __( "Rooms / Accommodation","Category Mesures"); ?></strong></p>
                                </div>
                                <div class="col-12 col-sm-12 py-4">
                                    <div class="row mx-md-5 align-items-center">
                                        <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                            <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-10 col-9 p-0 m-0">
                                            <p class="mt-0 h2"><?php echo __( "Reduction of textiles (including carpets) in the room, decorative objects, to act according to the defined contingency plan","Category Mesures"); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 py-4">
                                    <div class="row mx-md-5 align-items-center">        
                                        <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                            <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-10 col-9 p-0 m-0">
                                            <p class="mt-0 h2"><?php echo __( "Our laundry assures and certifies the treatment of bed linen and towels at more than 60º and with disinfectant products","Category Mesures"); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 py-4">
                                    <div class="row mx-md-5 align-items-center">
                                        <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                            <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-10 col-9 p-0 m-0">
                                            <p class="mt-0 h2"><?php echo __( "Rooms not only cleaned, but thoroughly disinfected after every departure ","Category Mesures"); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 py-4">
                                    <div class="row mx-md-5 align-items-center">
                                        <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                            <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-10 col-9 p-0 m-0">
                                            <p class="mt-0 h2"><?php echo __( "After departure, no check-in to the same room for a minimum of 24h to ensure maximum cleanliness and safety","Category Mesures"); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 py-4">
                                    <div class="row mx-md-5 align-items-center">
                                        <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                            <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-10 col-9 p-0 m-0">
                                            <p class="mt-0 h2"><?php echo __( "Spaced out bunking, rooms filled to max 50% capacity with exceptions for families or small groups travelling together (in accordance with local regulations)","Category Mesures"); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 py-4">
                                    <div class="row mx-md-5 align-items-center">
                                        <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                            <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-10 col-9 p-0 m-0">
                                            <p class="mt-0 h2"><?php echo __( "Special attention to items of frequent contact such as doorknobs, lockers, coat hangers...","Category Mesures"); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="col-12 col-sm-6 text-left pt-5">
                        <div class="text-center">
                            <img src="https://www.rvhotels.es/images/covid/recepcion.svg" class="w-25" alt="">
                        </div>                                        
                        <div class="row">
                            <div class="col-12 text-center">
                                <p class="display-4 font-weight-bold text-center p-5" style="color:#616161;"><strong><?php echo __( "Reception","Category Mesures"); ?></strong></p>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "New maximum capacity established, ensuring the safety distance between clients","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Implementation of protective screens between client and receptionist","Category Mesures"); ?></p>
                                    </div>
                                </div>                                            
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Our employees will be provided with personal protective equipment for their own and the customer's safety","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Encouraging payment by credit card, preferably by contactless. Applicable to all the services of the establishment","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "New implementation of the PRE-CHECK IN, anticipating the customer's reservation data to minimize contact and avoid paper deliveries, as well as agglomerations on arrival (Starting date pending).","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="row px-5">
                    <div class="col-12 col-sm-6 text-left pt-5">
                        <div class="text-center">
                            <img src="https://www.rvhotels.es/images/covid/buffet.svg" class="w-25" alt="">
                        </div>                                        
                        <div class="row">
                            <div class="col-12 text-center">
                                <p class="display-4 font-weight-bold text-center p-5" style="color:#616161;"><strong><?php echo __( "Restaurant / Buffet","Category Mesures"); ?></strong></p>
                            </div>                                                
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Capacity control, ensuring the safety distance between clients","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "We have reduced the number of tables in our Buffets (minimum of 1.5 meters between tables).","Category Mesures"); ?></p>
                                    </div>
                                </div>                                            
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "We have reformulated the service to offer, as far as possible, individual plates and/or covered single-dose.","Category Mesures"); ?> </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Depending on the capacity, we will propose two shifts of food or, failing that, we will look for alternative spaces in the hostels to guarantee the buffet service","Category Mesures"); ?> </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "A predefined route is suggested to avoid crowding in passing areas","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Room service personnel will also wear a mask whenever it is not possible to maintain a safe distance from the customer","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Room service personnel will also wear a mask whenever it is not possible to maintain a safe distance from the customer","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 text-left pt-5">
                        <div class="text-center">
                            <img src="https://www.rvhotels.es/images/covid/animacion.svg" class="w-25" alt="">
                        </div>                                        
                        <div class="row">
                            <div class="col-12 text-center">
                                <p class="display-4 font-weight-bold text-center p-5" style="color:#616161;"><strong><?php echo __( "Animation / Hostel Events","Category Mesures"); ?></strong></p>
                            </div>                                                  
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center">      
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Our animation/events teams are trained in prevention, hygiene and safety protocols in the development of the activities.","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center"> 
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "The use of masks is obligatory for those activities that take place outside the main area of the hostel and for those that do not allow the minimum safety distance to be maintained","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center"> 
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Disinfection of the material used in each activity after use","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center"> 
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Limited seating and shift arrangements for access to common areas.","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 py-4">
                                <div class="row mx-md-5 align-items-center"> 
                                    <div class="col-md-2 col-3 p-0 m-0 pl-5 pl-md-0">
                                        <i class="fa fa-check selector-icon" aria-hidden="true"></i>
                                    </div>
                                    <div class="col-md-10 col-9 p-0 m-0">
                                        <p class="mt-0 h2"><?php echo __( "Ventilation and disinfection of the common areas.","Category Mesures"); ?></p>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
                <p class="display-4 font-weight-bold text-center p-5" style="color:#616161;"><strong><?php echo __( "Preguntas frecuentes","Category Mesures"); ?></strong></p>
                <div class="row">
                    <script>
                            jQuery(document).ready(function(){
                                // Add minus icon for collapse element which is open by default
                                jQuery(".collapse.show").each(function(){
                                    jQuery(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
                                    jQuery(this).prev(".card-header").find(".blue-covid").addClass("white-covid").removeClass("blue-covid");
                                    jQuery(this).prev(".card-header").removeClass("bg-blue-covid").addClass("bg-blue-covid");
                                });
                                
                                // Toggle plus minus icon on show hide of collapse element
                                jQuery(".collapse").on('show.bs.collapse', function(){
                                    jQuery(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus"); 
                                    jQuery(this).prev(".card-header").find(".blue-covid").addClass("white-covid").removeClass("blue-covid");
                                    jQuery(this).prev(".card-header").removeClass("bg-blue-covid").addClass("bg-blue-covid");
                                    
                                }).on('hide.bs.collapse', function(){
                                    jQuery(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
                                    jQuery(this).prev(".card-header").find(".white-covid").addClass("blue-covid").removeClass("white-covid");
                                    jQuery(this).prev(".card-header").removeClass("bg-blue-covid");
                                });
                            });
                        </script>

                        <div class="accordion w-100 py-5" id="accordionExample">
                            <div class="card rounded-0">
                                <div class="card-header rounded-0" id="headingOne">
                                    <button class="btn-link w-100 text-left text-decoration-none" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <p class="m-3 font-weight-bold text-left h1 white-covid"><?php echo __( "Do I have to wear a mask on the premises?","Category Mesures"); ?> <i class="fa fa-plus float-right"></i>
                                        </p>
                                    </button>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="h2 m-3"><?php echo __( "The use of a mask will be necessary only and exclusively when the necessary safety distances cannot be respected, such as in the elevator and if they are not from the same family unit. Our protocol is aimed at reducing the use of masks (through social distance) in our facilities, however, there will be times when they must be used.","Category Mesures"); ?> 
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card rounded-0">
                                <div class="card-header rounded-0" id="headingTwo">
                                    <button class="btn-link w-100 text-left text-decoration-none" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <p class="m-3 font-weight-bold text-left h1 blue-covid"><?php echo __( "If I get sick from Covid-19 the day before I arrive, I will be charged the penalty?","Category Mesures"); ?> <i class="fa fa-plus float-right"></i>
                                        </p>
                                    </button>
                                </div>
                                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="h2 m-3"><?php echo __( "In this case, as before the exceptional situation of the COVID-19, our cancellation policy included the total cancellation without fees in case of a justified cause as could be a disease that prevents you from traveling, as long as you send us a proof statement, that is the same policy that will be applied in cases of illness by Covid-19, being the cancellation free if the medical report is attached.","Category Mesures"); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card rounded-0">
                                <div class="card-header rounded-0" id="headingThree">
                                    <button class="btn-link w-100 text-left text-decoration-none" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <p class="m-3 font-weight-bold text-left h1 blue-covid"><?php echo __( "Are there any restrictions on facilities or any facilities that are not available?","Category Mesures"); ?> <i class="fa fa-plus float-right"></i>
                                        </p>
                                    </button>
                                </div>
                                <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="h2 m-3"><?php echo __( "All of our facilities in all of our hostels will be available, however, reformulating their use based on the criteria set by the new regulations, reducing capacity, applying safety and distance measures, etc. So yes they will be available but we will have to get used to the new \"normality\" including the use of our facilities during your holidays.","Category Mesures"); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card rounded-0">
                                <div class="card-header rounded-0" id="headingFour">
                                    <button class="btn-link w-100 text-left text-decoration-none" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <p class="m-3 font-weight-bold text-left h1 blue-covid"><?php echo __( "The breakfast & dinner will remain buffet service?","Category Mesures"); ?> <i class="fa fa-plus float-right"></i>
                                        </p>                                            </button>
                                </div>
                                <div id="collapseFour" class="collapse show" aria-labelledby="headingFour" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="h2 m-3"><?php echo __( "Yes, all our hostels will operate with buffets as usual, there will obviously be adaptations to the new regulations such as single-dose plates to avoid maximum contact between guests. Seats will be reformulated according to the space available, taking into account the safety distances between tables. This may mean, depending on the hostel, that new spaces will be set up for dining room use and thus avoid having to take turns using the buffet during times of high occupation. However, if necessary, shifts will be established for the buffet pass, which will be managed in an orderly manner from the reception or from the dining room itself.","Category Mesures"); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card rounded-0">
                                <div class="card-header rounded-0" id="headingFive">
                                    <button class="btn-link w-100 text-left text-decoration-none" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        <p class="m-3 font-weight-bold text-left h1 blue-covid"><?php echo __( "Will the groups eat in shifts at the restaurant?","Category Mesures"); ?> <i class="fa fa-plus float-right"></i>
                                        </p>
                                    </button>
                                </div>
                                <div id="collapseFive" class="collapse show" aria-labelledby="headingFive" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="h2 m-3"><?php echo __( "If necessary, shifts will be established for the buffet pass, which will be managed in an orderly manner from reception or from the dining room itself.","Category Mesures"); ?> 
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card rounded-0">
                                <div class="card-header rounded-0" id="headingSix">
                                    <button class="btn-link w-100 text-left text-decoration-none" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                        <p class="m-3 font-weight-bold text-left h1 blue-covid"><?php echo __( "The activities will work as usual?","Category Mesures"); ?> <i class="fa fa-plus float-right"></i>
                                        </p>
                                    </button>
                                </div>
                                <div id="collapseSix" class="collapse show" aria-labelledby="headingSix" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <p class="h2 m-3"><?php echo __( "We know that for many people, the animation service is something essential in our type of accommodation, however, we have to understand that the activities as we have understood them until now, by regulation, will not be able to be exactly the same, nevertheless our great team of animation, is working hard to adapt and to reformulate the activities that so much you like so that all we can enjoy in a sure way so much of the facilities as of the activities.","Category Mesures"); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>    

<?php get_footer('covid'); ?>