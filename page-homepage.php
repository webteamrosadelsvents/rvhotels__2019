<?php
/*
Template Name: Homepage RVHotels
*/
?>
<?php get_header(); ?>
<div class="home-subheading position-absolute" style="width:95%;">
  <div class="row">
    <div class="col-xs-12 col-sm-2 col-md-2">
    <a class="logo-big display-inline-block margin20" href="<?php bloginfo('url'); ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>">
      <svg version="1.1" id="logorvhotels-svg" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 240 160" style="enable-background:new 0 0 240 160;" xml:space="preserve"><rect id="rvh-rect-blue-star" x="7.5" y="7.3" class="st0" width="112.5" height="112.5"/><g id="rvh-star" > <polygon class="st1" points="63.8,63.5 63.9,20.2 53.5,53.7"/><polygon class="st1" points="32.8,33.8 53.5,53.7 44.9,56.8"/><polygon class="st1" points="73.6,53.2 107.1,63.7 63.8,63.5"/><polygon class="st1" points="93.5,32.5 73.6,53.2 70.5,44.7"/><polygon class="st1" points="74,73.4 63.6,106.9 63.7,63.5"/><polygon class="st1" points="94.7,93.2 74,73.4 82.6,70.3"/><polygon class="st1" points="53.9,73.8 20.4,63.4 63.7,63.5"/><polygon class="st1" points="34,94.5 53.9,73.8 57,82.4"/></g><rect id="rvh-rect-blue-name" x="120" y="7.3" class="st2" width="112.5" height="112.5"/><path id="rvh-letter-r" class="st1" d="M151,53.6h2.9c2.4,0,4,4.1,5.3,7.4c0.2,0.5,0.4,0.9,0.5,1.3c2.8,6.4,5.5,8.6,10.7,8.6c1.5,0,3.3-0.4,3.9-0.5l0.7-6.7c-0.3,0.1-0.8,0.2-1.3,0.2c-3.5,0-5-3.2-6.4-6.3c-0.2-0.5-0.5-1.1-0.8-1.6c-0.9-1.8-1.8-3.1-2.9-4l-0.6-0.5l0.7-0.2c3.9-1.2,8.4-4.9,8.4-11.4c0-7.4-5-11.7-13.8-11.7h-17.4v42.5h9.9V53.6z M155.3,46.6H151V35.1h5.6c3.8,0,5.8,2,5.8,5.5C162.4,44.5,159.9,46.6,155.3,46.6"/><polygon id="rvh-letter-v" class="st1" points="205.8,28.2 195.9,58.6 185.9,28.2 175.6,28.2 190.2,70.7 200.1,70.7 214.6,28.2 "/><g id="rvh-letters-hotels" ><polygon class="st1" points="155.9,98.9 153.8,98.9 153.8,90.3 143.2,90.3 143.2,98.9 141.1,98.9 141.1,79.7 143.2,79.7 143.2,88.3 153.8,88.3 153.8,79.7 155.9,79.7 "/><path class="st1" d="M165.1,99.2c-4,0-7.2-3.2-7.2-7.4c0-4,3.2-7.2,7.2-7.2c4,0,7.2,3.2,7.2,7.2C172.3,96,169.1,99.2,165.1,99.2 M165.1,86.6c-2.9,0-5.1,2.3-5.1,5.2c0,3,2.3,5.4,5.1,5.4c2.9,0,5.1-2.4,5.1-5.4C170.2,88.9,168,86.6,165.1,86.6"/><path class="st1" d="M180,96.9l-0.6,0.1c0,0-0.2,0-0.5,0c-1.2,0-2.6-0.6-2.6-3.4v-6.7h3.4v-2h-3.4v-3.6h-2v3.6v2v6.8c0,4.8,3.2,5.3,4.6,5.3c0.4,0,0.7-0.1,0.7-0.1l0.4-0.1L180,96.9z"/><path class="st1" d="M188.1,99.2c-4.2,0-7.2-3.1-7.2-7.3c0-2.2,0.7-4.1,2.1-5.4c1.2-1.2,2.9-1.9,4.7-1.9c4,0,5.8,3.2,5.8,6.4c0,0.3-0.1,0.7-0.1,0.8l-0.1,0.4H183c0.2,3,2.2,5,5.1,5c2.3,0,3.7-1.5,3.7-1.5l0.4-0.4l1.1,1.7l-0.3,0.3C192.9,97.4,191,99.2,188.1,99.2 M183.1,90.3h8.2c-0.4-3.4-2.9-3.7-3.7-3.7C185.4,86.6,183.7,88,183.1,90.3"/><path class="st1" d="M198.8,99c-1.1,0-2.9-0.4-2.9-3.4V81.7v-2h2v15.7c0,1.4,0.5,1.5,1.1,1.5c0.4,0,0.7,0,0.7,0l0.6-0.1l0,2l-0.4,0C199.7,98.9,199.3,99,198.8,99"/><path class="st1" d="M206.7,99.2c-3.4,0-5.2-1.8-5.2-3.5v-0.5h2l0,0.4c0.1,1.1,2,1.6,3.3,1.6c1.4,0,2.9-0.6,2.9-1.9c0-1.2-1.2-1.8-3.2-2.5l-0.3-0.1c-1.8-0.7-4.3-1.6-4.3-4.2c0-2.5,2.4-3.8,4.7-3.8c1,0,2.1,0.2,2.9,0.6c1.1,0.5,1.7,1.4,1.7,2.3v0.8h-2l0-0.5c0-0.7-1.2-1.3-2.6-1.3c-1.3,0-2.7,0.5-2.7,1.7c0,1.2,1.2,1.7,3.2,2.5l0.1,0c2,0.8,4.5,1.7,4.5,4.3C211.9,97.6,209.8,99.2,206.7,99.2"/><polygon class="st1" points="155.9,98.9 153.8,98.9 153.8,90.3 143.2,90.3 143.2,98.9 141.1,98.9 141.1,79.7 143.2,79.7 143.2,88.3 153.8,88.3 153.8,79.7 155.9,79.7 "/><path class="st1" d="M165.1,99.2c-4,0-7.2-3.2-7.2-7.4c0-4,3.2-7.2,7.2-7.2c4,0,7.2,3.2,7.2,7.2C172.3,96,169.1,99.2,165.1,99.2 M165.1,86.6c-2.9,0-5.1,2.3-5.1,5.2c0,3,2.3,5.4,5.1,5.4c2.9,0,5.1-2.4,5.1-5.4C170.2,88.9,168,86.6,165.1,86.6"/><path class="st1" d="M180,96.9l-0.6,0.1c0,0-0.2,0-0.5,0c-1.2,0-2.6-0.6-2.6-3.4v-6.7h3.4v-2h-3.4v-3.6h-2v3.6v2v6.8c0,4.8,3.2,5.3,4.6,5.3c0.4,0,0.7-0.1,0.7-0.1l0.4-0.1L180,96.9z"/><path class="st1" d="M188.1,99.2c-4.2,0-7.2-3.1-7.2-7.3c0-2.2,0.7-4.1,2.1-5.4c1.2-1.2,2.9-1.9,4.7-1.9c4,0,5.8,3.2,5.8,6.4c0,0.3-0.1,0.7-0.1,0.8l-0.1,0.4H183c0.2,3,2.2,5,5.1,5c2.3,0,3.7-1.5,3.7-1.5l0.4-0.4l1.1,1.7l-0.3,0.3C192.9,97.4,191,99.2,188.1,99.2 M183.1,90.3h8.2c-0.4-3.4-2.9-3.7-3.7-3.7C185.4,86.6,183.7,88,183.1,90.3"/><path class="st1" d="M198.8,99c-1.1,0-2.9-0.4-2.9-3.4V81.7v-2h2v15.7c0,1.4,0.5,1.5,1.1,1.5c0.4,0,0.7,0,0.7,0l0.6-0.1l0,2l-0.4,0C199.7,98.9,199.3,99,198.8,99"/><path class="st1" d="M206.7,99.2c-3.4,0-5.2-1.8-5.2-3.5v-0.5h2l0,0.4c0.1,1.1,2,1.6,3.3,1.6c1.4,0,2.9-0.6,2.9-1.9c0-1.2-1.2-1.8-3.2-2.5l-0.3-0.1c-1.8-0.7-4.3-1.6-4.3-4.2c0-2.5,2.4-3.8,4.7-3.8c1,0,2.1,0.2,2.9,0.6c1.1,0.5,1.7,1.4,1.7,2.3v0.8h-2l0-0.5c0-0.7-1.2-1.3-2.6-1.3c-1.3,0-2.7,0.5-2.7,1.7c0,1.2,1.2,1.7,3.2,2.5l0.1,0c2,0.8,4.5,1.7,4.5,4.3C211.9,97.6,209.8,99.2,206.7,99.2"/></g><rect id="rvh-rect-blue-gray" x="7.5" y="124.4" class="st3" width="225" height="28.3"/><g id="rvh-letters-gruprv"><path class="st0" d="M124.9,132.2h2.8c3.9,0,5.6,2.5,5,6.3c-0.9,5.9-4.7,6.4-6.8,6.4h-3L124.9,132.2z M126.8,141.9c1.6,0,2.1-2,2.3-3.2c0.2-1.4,0.3-3.3-1.3-3.3L126.8,141.9z"/><polygon class="st0" points="135,132.2 141.3,132.2 140.9,135.2 137.9,135.2 137.7,137 140.3,137 139.8,140 137.2,140 136.9,141.9 139.8,141.9 139.3,144.9 133,144.9 "/><polygon class="st0" points="141,144.9 143,132.2 146.5,132.2 144.9,141.9 147.5,141.9 147,144.9 "/><path class="st0" d="M148.5,140.7c0.4,0.7,1.1,1.4,2,1.4c0.5,0,1.1-0.3,1.1-0.8c0.1-0.3,0-0.5-0.2-0.7c-0.1-0.2-0.3-0.3-0.5-0.5c-0.6-0.5-1.1-1-1.4-1.6c-0.3-0.6-0.5-1.3-0.3-2.2c0.2-1.5,1.6-4.2,4.5-4.2c0.8,0,1.7,0.3,2.3,0.6l-0.6,3.9c-0.3-0.6-1-1.4-1.8-1.4c-0.4,0-0.9,0.3-1,0.8c-0.1,0.3,0.1,0.5,0.2,0.7c0.2,0.2,0.4,0.4,0.5,0.5c0.6,0.5,1.1,1,1.4,1.5c0.3,0.6,0.4,1.2,0.3,2.2c-0.4,2.4-2.3,4.2-4.7,4.2c-0.9,0-1.7-0.2-2.5-0.5L148.5,140.7z"/><path class="st0" d="M162.4,144.9l-0.9-12.7h3.6l0,5.2c0,1-0.1,2-0.1,3.1h0c0.3-1,0.5-2,0.9-3.1l1.8-5.2h3.7l-5.5,12.7H162.4z"/><polygon class="st0" points="172,132.2 178.4,132.2 177.9,135.2 175,135.2 174.7,137 177.3,137 176.9,140 174.2,140 173.9,141.9 176.9,141.9 176.4,144.9 170,144.9 "/><path class="st0" d="M180.1,132.2h3.3l1.6,7.6l0,0c0.1-1.5,0.1-2.9,0.3-4.4l0.5-3.2h3.3l-2,12.7h-3.3l-1.4-7.3l0,0c-0.1,1.2-0.2,2.4-0.3,3.6l-0.6,3.7h-3.3L180.1,132.2z"/><polygon class="st0" points="190,144.9 191.5,135.3 189.6,135.3 190.1,132.2 197.4,132.2 196.9,135.3 194.9,135.3 193.4,144.9 "/><path class="st0" d="M196.8,140.7c0.4,0.7,1.1,1.4,2,1.4c0.5,0,1.1-0.3,1.1-0.8c0.1-0.3,0-0.5-0.2-0.7c-0.1-0.2-0.3-0.3-0.5-0.5c-0.6-0.5-1.1-1-1.4-1.6c-0.3-0.6-0.5-1.3-0.3-2.2c0.2-1.5,1.6-4.2,4.5-4.2c0.8,0,1.7,0.3,2.3,0.6l-0.6,3.9c-0.3-0.6-1-1.4-1.8-1.4c-0.4,0-0.9,0.3-1,0.8c-0.1,0.3,0.1,0.5,0.2,0.7c0.2,0.2,0.4,0.4,0.5,0.5c0.6,0.5,1.1,1,1.4,1.5c0.3,0.6,0.4,1.2,0.3,2.2c-0.4,2.4-2.3,4.2-4.7,4.2c-0.9,0-1.7-0.2-2.5-0.5L196.8,140.7z"/><path class="st0" d="M85.5,144.9l-1.1-5.3l-0.1,0c-0.1,0.6-0.1,1.2-0.2,1.9l-0.5,3.4h-3.4l2-12.7h3.3c3,0,5,0.9,4.5,4.2c-0.2,1.4-1,2.6-2.5,3.2l1.7,5.3H85.5z M84.9,138.1c0.9,0,1.5-0.8,1.7-1.6c0.2-1.1-0.4-1.6-1.4-1.5l-0.5,3.1L84.9,138.1z"/><path class="st0" d="M96.5,132c3.5,0,4.3,3.5,3.8,6.6c-0.5,3.1-2.4,6.6-5.9,6.6c-3.5,0-4.3-3.5-3.8-6.6C91.1,135.5,93,132,96.5,132 M94.9,141.9c1.3,0,1.8-2.6,1.9-3.4c0.1-0.8,0.5-3.4-0.8-3.4c-1.3,0-1.8,2.6-1.9,3.4C94,139.4,93.6,141.9,94.9,141.9"/><path class="st0" d="M101,140.7c0.4,0.7,1.1,1.4,2,1.4c0.5,0,1.1-0.3,1.1-0.8c0.1-0.3,0-0.5-0.2-0.7c-0.1-0.2-0.3-0.3-0.5-0.5c-0.6-0.5-1.1-1-1.4-1.6c-0.3-0.6-0.5-1.3-0.3-2.2c0.2-1.5,1.6-4.2,4.5-4.2c0.8,0,1.7,0.3,2.3,0.6l-0.6,3.9c-0.3-0.6-1-1.4-1.8-1.4c-0.4,0-0.9,0.3-1,0.8c-0.1,0.3,0.1,0.5,0.2,0.7c0.2,0.2,0.4,0.4,0.5,0.5c0.6,0.5,1.1,1,1.4,1.5c0.3,0.6,0.4,1.2,0.3,2.2c-0.4,2.4-2.3,4.2-4.7,4.2c-0.9,0-1.7-0.2-2.5-0.5L101,140.7z"/><path class="st0" d="M113.8,144.9v-1.6h-2.3l-0.5,1.6h-3.5l5.1-12.7h4.2l0.7,12.7H113.8z M113.8,140.6l0.1-2.9c0-0.8,0.1-1.6,0.1-2.4h0c-0.2,0.8-0.4,1.6-0.7,2.4l-0.9,2.9H113.8z"/><path class="st0" d="M45.9,137.6l-0.1,0.5c-0.5,3.1-1.7,7-5.7,7c-3.7,0-4.4-3.3-3.9-6.5c0.5-3.2,2.2-6.6,6-6.6c2.1,0,3.4,1.3,3.8,3.1l-3.5,1.2c0.1-0.5-0.2-1.3-0.8-1.3c-1.6,0-1.9,2.9-2.1,3.9c-0.1,0.9-0.5,3.1,0.8,3.1c0.9,0,1.4-1.1,1.5-1.8h-1.3l0.4-2.7H45.9z"/><path class="st0" d="M51.3,144.9l-1.1-5.3l-0.1,0c-0.1,0.6-0.1,1.2-0.2,1.9l-0.5,3.4h-3.4l2-12.7h3.3c3,0,5,0.9,4.5,4.2c-0.2,1.4-1,2.6-2.5,3.2l1.7,5.3H51.3z M50.7,138.1c0.9,0,1.5-0.8,1.7-1.6c0.2-1.1-0.4-1.6-1.4-1.5l-0.5,3.1L50.7,138.1z"/><path class="st0" d="M66.3,132.2l-1.3,8.3c-0.2,1.5-1.1,4.6-5,4.6c-4,0-3.8-3.1-3.6-4.6l1.3-8.3h3.4l-1.3,8c-0.1,0.3-0.1,0.8-0.1,1.1c0.1,0.4,0.2,0.6,0.7,0.6c0.5,0,0.8-0.3,0.9-0.6c0.2-0.4,0.2-0.8,0.3-1.1l1.3-8H66.3z"/><path class="st0" d="M65.9,144.9l2-12.7h3.1c3,0,5.2,1,4.6,4.3c-0.5,3.3-2.7,4.3-5.7,4.2l-0.7,4.2H65.9z M71.2,135l-0.3,0l-0.5,2.9c0.9,0.1,1.6-0.5,1.8-1.4C72.4,135.7,72,135,71.2,135"/></g></svg>
    </a>
    </div>
    <div class="col-xs-12 col-sm-8 col-md-8 col-h1">
      <h1 class="blanco"><?php echo __("Hoteles y Apartamentos")." <br>".__("en la Costa Brava, Costa Dorada y Pirineo");?></h1>
	</div>
	
	<div class="col-xs-12 col-sm-2 col-md-2 aviso-header" style="margin-top: 10rem; padding-left: 5rem; padding-right:0;">
		<!--a href="<?php echo __("https://www.rvhotels.es/comunicado-covid19/");?>"><div class="aviso-header-fecha text-center white" style="width: 100%;"><?php echo __("¡Volvemos el <strong>23 de Junio</strong>!");?></div-->
		<a href="<?php echo __("https://www.rvhotels.es/comunicado-covid19/");?>">
			<button class="w-100 bg-yellowgold" style="width: 100%; padding:5px; color: black;"><?php echo __("Info Covid-19");?></button>
		</a>
    </div>
  </div>
</div>
<section class="bg bghome ">
<?php
  /*
  * se cargan 2 slides, el grande, mediante media querie y js, para desktop, solo a partir de 768px
  * ver archivo js/jsbundle.js
  * el pequeno, que contiene los textos flotantes en cada slide, se queda solo en mobile
  * ambos slide estan sincronizados por las mismas flechas y parecen uno solo
  */
?>
<div class="slideshow slideshow-home-xl hidden-xs bg-lightblue"></div>
<div class="container-buscador-sub-slides">
	<div class="container" style="overflow-x:hidden;">
    <div class="row">
		<?php include ('includes/buscador_new.php');?>
        <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12 padding20 slideshows-container">

        <?php
			if( get_field('slide_01_contenido') ) {

			  $slide01_img_desk = get_field('slide_01_imagen_desktop');
			  $slide01_img_mob  = get_field('slide_01_imagen_mobile');
			  $slide01_content  = get_field('slide_01_contenido');

			}

			if( get_field('slide_02_contenido') ) {

			  $slide02_img_desk = get_field('slide_02_imagen_desktop');
			  $slide02_img_mob  = get_field('slide_02_imagen_mobile');
			  $slide02_content  = get_field('slide_02_contenido');

			}

			if( get_field('slide_03_contenido') ) {

			  $slide03_img_desk = get_field('slide_03_imagen_desktop');
			  $slide03_img_mob  = get_field('slide_03_imagen_mobile');
			  $slide03_content  = get_field('slide_03_contenido');

			}

			if( get_field('slide_04_contenido') ) {

			  $slide04_img_desk = get_field('slide_04_imagen_desktop');
			  $slide04_img_mob  = get_field('slide_04_imagen_mobile');
			  $slide04_content  = get_field('slide_04_contenido');

			}

			if( get_field('slide_05_contenido') ) {

			  $slide05_img_desk = get_field('slide_05_imagen_desktop');
			  $slide05_img_mob  = get_field('slide_05_imagen_mobile');
			  $slide05_content  = get_field('slide_05_contenido');

			}

		?>

		<style type="text/css">
        	.home-slide-1 {background-image: url('<?php echo $slide01_img_desk;?>');}
          	.home-slide-2 {background-image: url('<?php echo $slide02_img_desk;?>');}
          	.home-slide-3 {background-image: url('<?php echo $slide03_img_desk;?>');}
          	.home-slide-4 {background-image: url('<?php echo $slide04_img_desk;?>');}
			.home-slide-5 {background-image: url('<?php echo $slide05_img_desk;?>');}			

          @media (max-width:767px) {

            #carousel-home-offers .oferta1 {background-image: url('<?php echo $slide01_img_mob;?>');}
            #carousel-home-offers .oferta2 {background-image: url('<?php echo $slide02_img_mob;?>');}
            #carousel-home-offers .oferta3 {background-image: url('<?php echo $slide03_img_mob;?>');}
            #carousel-home-offers .oferta4 {background-image: url('<?php echo $slide04_img_mob;?>');}
			#carousel-home-offers .oferta5 {background-image: url('<?php echo $slide05_img_mob;?>');}			  
          }
        </style>

		<div id="carousel-home-offers" class="carousel carousel-sync slide" data-ride="carousel" data-interval="10000">
            <div class="carousel-inner" role="listbox">
				<?php if( get_field('slide_01_contenido') ) {?>
                	<div class="item active">
                    	<article class="oferta_home_xl text-large blanco text-bold oferta1">
                        	<div><?php echo $slide01_content;?></div>  
                      	</article>
					</div>
				<?php } ?>

				<?php if( get_field('slide_02_contenido') ) {?>
                	<div class="item">
                    	<article class="oferta_home_xl oferta2">
                        	<div><?php echo $slide02_content;?></div>  
						</article>
					</div>
				<?php } ?>

				<?php if( get_field('slide_03_contenido') ) {?>
                	<div class="item">
                    	<article class="oferta_home_xl oferta3">
                        	<div><?php echo $slide03_content;?></div>  
						</article>
					</div>
				<?php } ?>

				<?php if( get_field('slide_04_contenido') ) {?>
                	<div class="item">
                    	<article class="oferta_home_xl oferta4">
                        	<div><?php echo $slide04_content;?></div>  
						</article>
					</div>
				<?php } ?>
				
				<?php if( get_field('slide_05_contenido') ) {?>
                	<div class="item">
                    	<article class="oferta_home_xl oferta5">
                        	<div><?php echo $slide05_content;?></div>  
						</article>
					</div>
				<?php } ?>
			</div>

            <a class="left carousel-control" href="#carousel-home-offers" role="button" data-slide="prev">
            	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              	<span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-home-offers" role="button" data-slide="next">
              	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              	<span class="sr-only">Next</span>
            </a>
        </div>
	</div>
</div><!-- row -->
</div><!--.container - -->
</div><!--.container - buscador and sub slides with text absolute -->
<div class="fw-home-featureds">
	<div class="container">
    	<?php if ( get_field("mostrar_ofertas_destacadas") ) :
			include("includes/ofertas-destacadas.inc.php");
        ?>
        <script type="text/javascript">console.log('mostrar ofertas en home ON');</script>
		<?php endif; ?>
		<?php if ( get_field("mostrar_categorias_destacadas") ) :
			include("includes/ofertas-categorias-destacadas.inc.php");
		?>
        <script type="text/javascript">console.log('mostrar destacadas en home ON');</script>
		<?php 
			else:
      	?>
      	<style type="text/css">.getdown {display:none;}</style>
      	<?php
      		endif;
      	?>
		<?php // boton ver todas las ofertas
			if ( get_field("mostrar_ofertas_destacadas") ) :?>
				<div class="clear text-center margin-top-30 margin-bottom-30">
      				<a href="<?php echo __("https://www.rvhotels.es/ofertas/");?>" title="<?php echo __("Todas las ofertas de hoteles y apartamentos");?>" class="btn btn-lg margin-bottom-20 bg-lightblue padding10 blanco hilite phone-block text-uppercase text-bold">
      				<?php echo __("Ver todas las ofertas");?> &raquo;</a>
      			</div>
		<?php endif;?>
	</div>
</div>
</section>
<?php get_footer(); ?>
<script type="text/javascript">
	jQuery(document).ready(function($){
    	$('.smartphone .buscador-container').insertAfter('.slideshows-container');
	});
</script>