<?php get_header(); ?>
<section class="bg">
	<div class="container"> 
		<div class="row">
			<article class="alerta404 col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
				<div class="error-panel padding30">
					<div class="center-vertical text-center blanco">
							<i class="fa fa-frown-o text-large big-icon display-inline-block"></i><br>
							<h1 class="text-center blanco margin-top-0"><?php echo __("¡Lo sentimos!");?></h1>
							<p class="text-center blanco lead margin-bottom-0"><?php echo __("La página solicitada ya no se encuentra en RV Hotels.");?></p>
							<p class="text-center blanco"><?php echo __("Sin embargo, aquí tienes unas cuantas sugerencias:");?></p>


						<?php get_template_part('searchform'); ?>
						    
						    <?php 
						        global $wp_query; 
						        $wp_query->query_vars['is_search'] = true;

						                        
						        $s = str_replace("-", " ", $wp_query->query_vars['category_name']);//name
						        
						        //$loop = new WP_Query('post_type=any&s=' .$s.'&posts_per_page=12');

						        $loop = new WP_Query(array(
						        	'post_type' => array('post', 'page','apartamento','promo'),
						        	'posts_per_page' => '12',
						        	's' => $s,
						        	'post__not_in' => array('1563','1560','1552','1558','3674','3619','3675','3676','497','485','501','491','489','499','500','490')
						        ));

						    ?>

						    <div class="margin-top-30">
						    	<div class="row blanco text-center">  

							    <?php if ($loop->have_posts()): ?>

							    			
							               
							                <?php while ($loop->have_posts()): $loop->the_post(); ?> 
							                    <div class="col-md-6 col-xs-12 text-center margin-bottom-10"> 
							                        <a href="<?php the_permalink(); ?>" class="blanco text-bold" style="text-decoration:underline"><?php the_title(); ?></a>
							                    </div>
							                <?php endwhile; ?> 
							            
							    <?php endif; ?>
							    </div> 

						    </div>

						    <hr>
							
							<div class="margin-top-30">
						    	<p class="text-center blanco text-large">
						    		<a href="<?php echo __("https://www.rvhotels.es/hoteles-costa-brava-costa-dorada-pirineo/");?>" class="blanco display-inline-block margin-bottom-10"  style="text-decoration:underline"><?php echo __("Hoteles Costa Brava, Costa Dorada y Pirineo");?></a><span class="hidden-xs">&nbsp;&nbsp;|&nbsp;&nbsp;</span><br class="visible-xs">
						    		
						    		<a href="<?php echo __("https://www.rvhotels.es/apartamentos-en-costa-brava/");?>" class="blanco display-inline-block margin-bottom-10"  style="text-decoration:underline"><?php echo __("Apartamentos en la Costa Brava");?></a><br>
						    		<a href="<?php echo __("https://www.rvhotels.es/ofertas/");?>" class="blanco display-inline-block margin-bottom-10"  style="text-decoration:underline"><?php echo __("Ofertas y escapadas de hotel y apartamento");?></a><span class="hidden-xs">&nbsp;&nbsp;|&nbsp;&nbsp;</span><br class="visible-xs">
						    		<a href="<?php echo __("https://www.rvhotels.es/blog/");?>" class="blanco display-inline-block margin-bottom-10"  style="text-decoration:underline"><?php echo __("Blog RV Hotels");?></a>
						    	</p>
						    </div>
						    

					</div>

					
				</div>
			</article>
		</div>
	</div>
</div>
<?php get_footer(); ?>