<?php
$expira = trim(date('r', strtotime('+5 month')),"+0000")."GMT";
$phonetest ="";
if (isset($_GET['test'])) {
    $phonetest ="phonetest";
}
?>
<!DOCTYPE html>
<html lang="<?php echo ICL_LANGUAGE_CODE; ?>" class="<?php echo $phonetest;?>">
<head> 
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php wp_title('|','true','right'); ?><?php bloginfo('name'); ?></title>
<meta http-equiv="expires" content="<?php echo $expira;?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="google-site-verification" content="gFEGRxq36Pvy75L_zVdV-lrYoJRlwMpR_rvGeFth5SE" />
<link rel="icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" type="image/x-icon"/>
<?php wp_head(); 
$actuaLang = ICL_LANGUAGE_CODE;
?>
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/icons/favicon.ico" type="image/x-icon"/>
<link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/icons/touch-icon-iphone.png" />
<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('template_url'); ?>/icons/touch-icon-ipad.png" />
<link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo('template_url'); ?>/icons/touch-icon-iphone-retina.png" />
<link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo('template_url'); ?>/icons/touch-icon-ipad-retina.png" />
<link href="<?php bloginfo('template_url'); ?>/css/bootstrawesome.min.css" rel="stylesheet" media="all" type="text/css" />
<link href='https://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>

<?php if (isset($_GET['dev'])) {?>
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/rvhotels.dev.css?v=<?php echo(rand(1,9999));?>"/>
<?php } else {?>
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/rvhotels.min.css?v=81117"/>
<?php } ?>

<!-- DATE PICKER -->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/jqueryui.custom.css">
<script type="text/javascript">
	var msg1 		= "<?php _e('Seleccione un hotel, apartamento o destino', 'rvhotels'); ?>";
	var msg2 		= "<?php _e('Selecione un afecha de entrada y de salida', 'rvhotels'); ?>";
	var lang_code 	= "<?php echo ICL_LANGUAGE_CODE; ?>";
</script>

<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<link href="<?php bloginfo('template_url'); ?>/css/ie.css" rel="stylesheet" type="text/css" />
<script src="<?php bloginfo('template_url'); ?>/js/respond.js"></script>
<![endif]-->
</head>
<body <?php body_class($class); ?> id="<?php echo ICL_LANGUAGE_CODE; ?>">
<header class="header" id="top">
<?php require_once('includes/selectoridioma.inc.php');?>
<div class="container clearfix">
	<div class="social_icons padding5  hidden-xs">
        <a href="https://www.facebook.com/RVHotels" target="_blank" rel="noindex, nofollow">
        	<i class="fa fa-facebook"></i>
        </a>
        <a href="https://twitter.com/Rv_hotels" target="_blank" rel="noindex, nofollow">
    	    <i class="fa fa-twitter"></i>
    	</a>
        <a href="https://www.youtube.com/channel/UCaYFGdXPEmNpZadftzrgeMQ" target="_blank" rel="noindex, nofollow">
    		<i class="fa fa-youtube"></i>
    	</a>
    	<a href="https://www.rvhotels.es/blog" title="RVHOTELS BLOG">
			<svg id="blog-icon" xmlns="http://www.w3.org/2000/svg" width="30" height="40" style="transform:translateY(10px)">
  <path fill="#00619D" d="M.574 28V11.87h2.54c2.765 0 3.538 1.603 3.538 4.32 0 1.584-.305 3.124-1.93 3.573v.043c1.747.577 2.153 1.99 2.153 3.786C6.875 26.566 5.838 28 3.013 28H.573zM2.85 13.686h-.345v5.285c1.89.088 2.155-1.153 2.155-2.864 0-1.263-.407-2.44-1.81-2.418zM2.506 26.18c1.77.087 2.216-.876 2.216-2.65 0-1.777-.345-2.89-2.215-2.847v5.498zm7.206-14.31v14.268h3.415v1.86H7.68V11.87h2.03zm11.382 8.064c0 2.91-.04 8.28-3.983 8.28s-3.98-5.37-3.98-8.28c0-2.91.04-8.28 3.984-8.28s3.984 5.37 3.984 8.28zm-5.813 0c0 3.423.227 6.27 1.83 6.27 1.608 0 1.83-2.847 1.83-6.27 0-3.422-.222-6.268-1.83-6.268-1.603 0-1.83 2.845-1.83 6.268zm14.23-.45v.257c0 3.085.428 8.476-3.66 8.476-3.636 0-4.062-3.68-4.062-8.236 0-4.515.346-8.323 3.882-8.323 2.174 0 3.21 1.155 3.74 3.294l-1.93.835c-.206-1.07-.49-2.118-1.75-2.118-1.667 0-1.79 2.396-1.79 6.29 0 5.604.896 6.246 1.79 6.246.833 0 1.748-.127 1.748-3.977v-.877H25.77v-1.86h3.74z"/>
</svg>
		</a>
	</div>
	<div class="telefono padding5 hidden-xs">
	   	<i class="fa fa-mobile"></i> +34 93 503 60 39
	</div>

	<div class="logoheader text-center">
		<a href="<?php bloginfo('url'); ?>" alt="<?php bloginfo('name'); ?>">
			<img src="https://www.rvhotels.es/logo-rvhotels-gruprv.png" alt="RV Hotels - Grup Rosa dels Vents" id="logorvhotels"/>
		</a>
	</div>

</div>

<nav class="main">
<div class="container">
<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
</div>
</nav>
</header>