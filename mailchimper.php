<!-- Begin MailChimp Signup Form -->
<style>
    ::-webkit-input-placeholder { /* Safari, Chrome and Opera */
      color: white;
    }

    :-moz-placeholder { /* Firefox 18- */
      color: white;
    }

    ::-moz-placeholder { /* Firefox 19+ */
      color: white;
    }

    :-ms-input-placeholder { /* IE 10+ */
      color: white
    }

    ::-ms-input-placeholder { /* Edge */
      color: white
    }

    :placeholder-shown { /* Standard one last! */
      color: white;
    }

    #mc_embed_signup input[type="email"] {
        color: white;
    }

    #mc_embed_signup input {
        border: none!important;
        background: rgba(28, 105, 173, 0.91);
        -webkit-appearance: none;
    }
    #mc_embed_signup div.mce_inline_error{
        background-color: transparent!important;
        color:red!important;
    }
        .my-text-left{
            margin-left: -20px;
            margin-right: 18px
        }
    #mc_embed_signup div.response{
        padding: 0!important;
    }
    
    .modal{
        overflow:hidden;
        line-height: 20px;
        height: auto;
    }
    .modal-dialog,
    .modal-body{
        height: auto;   
    }
    .modal-header{
        margin: 5px 5px -10px 5px;
    }
    .modal-title{
        color:#1966AC;
    }
</style>
<link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
<div id="mc_embed_signup">
    <form action="https://rvhotels.us2.list-manage.com/subscribe/post?u=23b5df66471f0ebe80f216080&amp;id=5b7afe5083" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
        <div class="mc-field-group row">
            <div class="col-lg-10 col-md-10 my-text-left">
                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="E-mail" style="float:right; width:85%;">
            </div>
            <div class="col-lg-2 col-md-2">
                <input type="submit" value="<?php _e('Enviar', 'rvhotels'); ?>" name="subscribe" id="mc-embedded-subscribe" class="button">
            </div>
        </div>
        <div id="mce-responses" class="clear">
            <div for="mce-EMAIL" class="mce_inline_error"></div>
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
        </div>    
    </form>
</div>

<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>
    
    (function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text'; 
    $.extend($.validator.messages, {
      required: "<?php echo __('Campo obligatorio');?>.",
      email: "<?php echo __('Por favor, escribe una dirección de correo válida');?>"
    });}(jQuery));var $mcj = jQuery.noConflict(true);
    
</script>

<!--End mc_embed_signup-->