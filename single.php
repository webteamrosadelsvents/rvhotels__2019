<?php
get_header();
$this_post = $post->ID;
?>

<style>#clousmi {display:none;}</style>
<section class="bg">
<div class="container clearfix">
  
    
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article class="entrada margin-extra bg-blanco">

          <div class="row">

            <div class="col-xs-12 col-md-4">
              <figure>         
                  <img data-original="<?php the_post_thumbnail_url('medium'); ?>" alt="<?php the_title(); ?>" class="img-responsive lazy-img" style="width:100%"/>
               </figure>

               <div class="lyteShare twitter facebook social-inferior text-center margin-extra">¡Nos encanta compartir!<br><br><strong>¿Y a tí?</strong><br><br> </div>
               <script>
                 jQuery('.lyteShare').append('<a style="display:none;transform:scale(1.3)translate(-1px,1px)" href="whatsapp://send?text=<?php the_title(); ?> <?php the_permalink(); ?>"  title="Enviar por Whatsapp" data-action="share/whatsapp/share" class="wa_btn wa_btn_l hidden-sm-up"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAjCAMAAADL21gSAAABoVBMVEUAAAD+/v7+/v7////+/v7////////+/v7+/v7+/v7+/v7+/v7+/v7+/v7////9/f3+/v79/f3////8/Pz+/v7////+/v7////+/v7+/v7////+/v7+/v79/f3////9/f3////////+/v79/f3+/v7+/v7+/v7+/v78/Pz9/f3+/v79/f3+/v7+/v7w8PD+/v78/Pz+/v7+/v4AqFn///8AqVoApFEAnEIAmDoAqlwAoUsAmz8Ap1YAplMAoEkAljYBok0AnkcAnkQAmTwAlzj2/PrY8eQAql0AlDOv48n5/Prt+PLA6NQRrmT0+/fn9u3b8ubG7Nuo38OM17KB0qh7zaFly5xbxpFHwYhUwIY5vH42unottnQasWoWq10AkzAAki7i9OrU8eTM7uC86NKf3b+Z3b+S27mR17KH06thxY9NwosxuXk7uHcxtHAVr2UAiyHg9OnN69jI6dW55MuO2rmh2beH1bB40ah90aZszJ1px5RWxZBewopPvH9MvH4tunxDuHgzt3dAt3YltHAtsmwirmQZrWIPqlwPqVsIpFJDF8oYAAAAM3RSTlMA/vLsDvrTvnHn29iskoh7WlE9MRQH4860saSAdnVjXzQhHBbh38nDt5+bZldLRUMrJgTmVxRAAAACk0lEQVQ4y4XT5XLbUBCGYa+5YU6atMEyt/rEZGaHmZmpzMy96kondiS3mfad8R/7GeuspPVUdDdcE/R6qzvqG1s9Z9fSVYVydK72/lmmx0ewSw6eMLrwt+mwydDOzKej4vzMRMpm7QOVxH8FwOrbX2Y8piixuKm8L9jXvFeBqgHsxMwIz7F4QZJeWyrQ4jI1BOxmFJmJEst8ToKCjmm0zNSyJguKxp8qfvkQoLqyaQ0QtpY0WdSLEQZKbHnSUn0ldJGwohhp9SCV3IvzjhISBVBnCV0G9jKysQjgYUx3kBxfAMjPTDNhSBA46QWsZlXOSVMLoAaGwsB2gufUURAhK7lPZU4C1xnqBN5YP0lbNnqqcq7EfSDIUBCYEzkuPg0gKwpuFOHzaGPIB1owOE43hoHxY9mNBH0YPoa8yBUVa5bErH3wDM+OXEYpeBm6BHyJ2l+ZmwB9XeL4tGKoBs/baAXnGAoB06o9ixEdAZIfM4o6X3i5KOkyZyzkUMVQHfDKtC+SjkXXAGzHxCFgcCKicdI0EGLoFmH9ZPC0GMkCGF4DwR6ZT4yBuhjyE1AUeaai8QlYEREwH4sIeVCvh3Ue2MiUHpeQOBwFayzGL03CPhKrl4B9SS69Rmr8YHwktT4l6uLPJKjbAuW/yi9Gyg+MEyVFN+KckhgBtTlrEAA+iJyTLmjpqLQJouZTNEBICTqvCHJZyZoZzQLu3QsTNhLRxBFnGppmES1iqu9WLVPrcboKzB1/G0+mdr8boiSp0R8zT8C2wOmBF4NTzwhWuUfZseejj/M28TV4XDUR5U5uIU6jwA3XYrJtIVvU3G4IkRWsT3u431PZNRD5atmw/U099fXdTX2eP2slqr7p9/y7O6HG/4jf2t25aw2zopMAAAAASUVORK5CYII=" alt="Whatsapp"></a>');
                 // Move social media after the content in mobile
                 jQuery(document).ready(function($) {
                  $('.mobile .lyteShare').appendTo('.post-contenido');
                 });
               </script>
               <style>@media (max-width:1023px) {.wa_btn{display:inline-block!important;}}</style>
            </div>

            <div class="col-xs-12 col-md-8 position-relative">
              <nav class="navigation text-right text-15x position-absolute top-right">
                <?php next_post_link('%link', '<i class="fa fa-chevron-left"></i>', TRUE); ?>
                <?php previous_post_link('%link', '<i class="fa fa-chevron-right"></i>', TRUE); ?>
                <a href="#" id="clousmi"><i class="fa fa-times"></i></a>
              </nav>
              <div class="padding15">
                
                <p class="text-small almostblack"><strong><a href="https://www.rvhotels.es/blog/">BLOG RVHOTELS</a></strong> > <?php the_title(); ?></p>
               
                <h1><strong><?php the_title(); ?></strong></h1>

                <div class="text-small almostblack margin-bottom-15">
                  Fecha de publicación: <?php the_date(); ?>
                </div>
                
                <div class="text-bold">
                  <?php the_excerpt(); ?>
                </div>

                <div class="post-contenido margin-bottom-15">
                <?php the_content(); ?>
                </div>

                

              </div>
            </div>

          </div>

        </article>
 
    <?php endwhile; ?>
 
    <?php else : ?>
 
        <h2><?php _e('No encontrado', 'rvhotels'); ?></h2>
 
    <?php endif; ?>

    <?php include("includes/pinteres.php");?>

</div><!--.container-->
</section>
<script src="https://www.rvhotels.es/wp-content/themes/rvhotels/js/lyteShare.min.js"></script>
<script>
  jQuery(function($){
    $('.post-contenido a').attr('target','_blank');
    $('.banner-ofertas-rvhotels').insertAfter('.pinterest-wall > article:nth-of-type(4)');
  });
</script>
<?php get_footer(); ?>