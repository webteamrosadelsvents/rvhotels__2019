<?php
/*
Template Name: OFERTAS
*/
$taxonomies =   get_terms("promo_categories", array(
    "hide_empty"=>0,
    "taxonomy"=>"promo_categories",
    "suppress_filters"=>false)
); 

$aparts     =   get_posts( array("post_type"=>"apartamento", "posts_per_page"=>-1, "suppress_filters"=>false) ); 
$hotels     =   get_posts( array("post_type"=>"hotel", "posts_per_page"=>-1, "suppress_filters"=>false) );

//  echo "<pre>".print_r($aparts,true)."</pre>";
$mostrarLog=false;
$log = "<pre style='display:none;' id=jordi>";
?>
<?php get_header(); ?>
<script src="https://www.rvhotels.es/wp-content/themes/rvhotels/js/isotope.pkgd.min.js"></script>
<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js" ></script>
<?php include("includes/modal-promo-apto-v2.inc.php");?>
<style>#filtro-ofertas-container{transition:300ms;}</style>
<section class="bg bgcategorias">
    <div class="container-categorias">
        <div class="container parrilla-categorias">
            <article>
                <div class="headerseccion headerseccion-ofertas">
                    <h1 class="text-center"><?php echo __("Ofertas de Hoteles y Apartamentos RV Hotels");?></h1>
                    <h2 class="text-center subtitle"><?php echo __("Promociones especiales en la Costa Brava, Costa Dorada y en el Pirineo");?></h2>
                </div>
                <?php echo get_the_content(); ?>
            </article>
        </div><!-- cerrar container para pillar fullwidth -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/js/slick/slick-rvhotels.css">
        <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slick/slick.min.js"></script>
        <div class="slick position-relative" id="carrusel">
            <?php 
            foreach ($taxonomies as $taxonomy) {
                $term_meta = get_option( "taxonomy_$taxonomy->term_id" );
                $url=get_bloginfo("url")."/promos/".$taxonomy->slug."/";

                // IMAGEN PEQUENYA COMO ADVANCED CUSTOM FIELD
                // not working, esto no me sale hoy na de na! 13/10/17
                
                $bgCss ="";
                
                if (get_field('categoria_img_peque', $taxonomy)) {
                    
                    $bgCatSmall = get_field('categoria_img_peque', $taxonomy);
                    $bgCss = " style='background-image:url(".$bgCatSmall.")' ";
                }

                // Categoria visible en el front, 1 visible, 0 no visible
                // se pueden ocultar categorias en el backend de wordpress, revisar campos
                // esto se usa aqui y en el filtro de ofertas, que recorre las categorias visibles en un desplegable
                // EJEMPLO: Navidad, semana santa, fin de anyo... categorias publicadas, pero ocultas cuando pasan
                $catPromoOculta = get_field('catpromo_visible',$taxonomy);
                if ($catPromoOculta!=1) {
                ?>
                    <div class="cat-<?php echo $term_meta['custom_term_class'];?>">
                        <div class="categoria pic" <?php echo $bgCss;?> >
                            <div class="categoria-title"><a href="<?php echo $url;?>" class="catname blanco"><?php echo $taxonomy->name;?></a></div>
                        </div> 
                    </div>
                <?php
                }
            }
            ?>
            
        </div>

        <span class="slicknav slicknav-prev fa fa-chevron-left blanco text-large hidden" ></span>
        <span class="slicknav slicknav-next fa fa-chevron-right blanco text-large hidden"></span>

        <script>

        jQuery(document).ready(function($){

              $('.slick').slick({
                dots: true,
                //rows: 2,
                arrows: true,
                infinite: true,
                speed: 300,
                slidesToShow: 6,
                slidesToScroll: 2,
                prevArrow: '.slicknav-prev',
                nextArrow: '.slicknav-next',
                //swipeToSlide: true,
                //variableWidth: true,
                responsive: [
                    {
                      breakpoint: 1600,
                      settings: {
                        slidesToShow: 6,
                        slidesToScroll: 2,
                        infinite: true,
                        dots: true
                      }
                    },
                    {
                      breakpoint: 1280,
                      settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        infinite: true,
                        dots: true
                      }
                    },
                  {
                    breakpoint: 1023,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 3
                    }
                  },
                  {
                    breakpoint: 480,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 2
                    }
                  },

                  // You can unslick at a given breakpoint now by adding:
                  // settings: "unslick"
                  // instead of a settings object
                ],
                //centerMode: true,
              });

              $('.slicknav-prev').insertBefore ('.slick-dots').removeClass('hidden').addClass('display-inline-block');
              $('.slicknav-next').insertAfter('.slick-dots').removeClass('hidden').addClass('display-inline-block');
        });

        </script>

</section>
<section class="bgofertas" id="viewallpromos">
    <div class="container">

        <h3 class="text-center blanco margin-top-30 margin-top-40 margin-bottom-20 subtitle">
            <?php echo __("Todas las ofertas de hoteles y apartamentos ¡Escoge tu destino!");?>
        </h3>

    </div>

    <?php include('includes/filtro-ofertas.php');?>

    <div class="container">

        <!--OFERTAS -->
        <div class="container-ofertas padding0 clear">

        <div class="row-ofertas grid">

            <div class="grid-sizer"></div>
            <div class="gutter-sizer"></div>
            <article class="sf-result-head text-center noresults hidden margin-bottom-30 animate-slide-top"><?php echo __("NO HAY RESULTADOS");?></article>

            <?php // begin loop

            $argsPromos = array(
                            'suppress_filters'=>false,
                            "posts_per_page"=>"999",
                            "post_type"=>"promo",
                            "post_status"=>'publish',
                            'caller_get_posts'=> 1
                            );

            query_posts( $argsPromos );

            $count=0;

            while( have_posts()) : the_post();

                $metas          = get_post_meta($post->ID);
                $titulo         = get_the_title();
                $imagen         = wp_get_attachment_image_src( $metas['_thumbnail_id'][0],"thumbnail" );
                $precio         = $metas['custom_precio'][0];
                $dto            = $metas['custom_descuento'][0];
                $pornoche       = ( ($metas['custom_pornoche'][0]!="0") && ($metas['custom_pornoche'][0]!="") ) ? __("noche") : "";

                $esHotel        = (isset($metas['custom_oferta_en_apart']) && $metas['custom_oferta_en_apart'][0]!="-")?"nohotel":"hotel";
                $hotel_apart    = descrimina_hotel_apartamento($metas, $esHotel);

                $link           = get_custom_link($metas, $post->ID);

                $idApartWP      = (isset($metas['custom_oferta_en_apart'][0]) ? $metas['custom_oferta_en_apart'][0] : "" );
                $linkApartPromo = get_permalink($idApartWP);

                $idAptoNeo      = get_post_meta( $idApartWP, 'custom_bookings', true );
                $arrival        = (isset($metas['custom_dtinicio'][0]) ? $metas['custom_dtinicio'][0] : "" );
                $departure      = (isset($metas['custom_dtfin'][0])? $metas['custom_dtfin'][0] : "");
                $nights         = (isset($metas['custom_noches'][0])? $metas['custom_noches'][0] : "");

                $zonaClass      = (isset($metas['custom_oferta_en_zona'][0]) && $metas['custom_oferta_en_zona'][0]!="-") ? "zona-".$metas['custom_oferta_en_zona'][0] : "zona-";
                $zonaClass      = sanitize_title($zonaClass,"default");

                $postID_class    = "promoid-".$post->ID;
                $postID_urlencoded = urlencode("?promoid=".$post->ID);

                $discountType   = "dto-".get_field("apto_tipodescuento");

                // clases css para categorias, solo se usan en la pagina general de todas las categorias de oferta
                // en taxonomy-promo_categories.php no hace falta porque son vistas de dentro de una categoria

                $taxonomies = get_terms( array(
                    'taxonomy' => 'promo_categories',
                    'hide_empty' => false,
                    "suppress_filters"=>false
                ) );

                $terms = wp_get_post_terms($post->ID,'promo_categories');

                $catPromos ="";

                foreach ($terms as $term) {
                    $catPromos .= " cat-".$term->slug;
                }

                include('includes/minioferta.inc.php');

                $count++;

            endwhile;

            wp_reset_query();

            // end loop ?> 
            
        </div> <!-- row-ofertas grid -->
	</div> <!--.container-ofertas -->
</div> <!--.container-->
<?php include("includes/compartir-share.inc.php");?>

    <div class="catswitcher-container text-center">
        <?php include("includes/catswitcher.inc.php");?>
    </div>

    <script src="<?php bloginfo('template_url'); ?>/js/isotope.behavior.js"></script>
</section>
<?php get_footer(); ?>