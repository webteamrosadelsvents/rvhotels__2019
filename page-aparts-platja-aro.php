<?php
/*
Template Name: Apartamentos Platjadaro
*/
?>
<?php get_header(); ?>
<section class="bg bg-platja-aro" data-spy="affix" data-offset-top="280">

    <div class="container clearfix apartamentos">
        <div class="headerseccion">
            <h1><?php echo __("Apartamentos en Playa de Aro");?></h1>
            <p class="subtitle"><?php echo __("Situada en el Baix Empordà, es una antigua localidad pesquera y actualmente una de las más concurridas y con mejor calidad de servicios y ambiente turístico de la Costa Brava.");?></p>
        </div>

        <?php include ('includes/nav-apartamentos.inc.php'); ?>

        <?php include ('includes/buscador-neobookings-aptos-horizontal.php'); ?>
        <div class="contentseccion">
            <p class="subtitle"><?php echo $post->post_content;?></p>
        </div>
          <div class="bloque clearfix">
                <div class="row">
                <?php 
                $args=array(
                    'post_type' => 'apartamento',
                    'posts_per_page' => -1,
                    "post_status"=>array('publish'),
                    "meta_query"=>array(
                                        array(
                                            "key"=>"custom_zona",
                                            "value"=>"platjadaro",
                                            "orderby"       => "menu_order",
                                            "order"         => "ASC")
                                        ));
                query_posts($args);
                if (have_posts()) : while (have_posts()) : the_post();
                    $zona       =   $arr_ubicas[get_post_meta($post->ID,"custom_zona",1)];
                    $novedad    =   (get_post_meta($post->ID,"custom_new",1))?"novedadaparts_".ICL_LANGUAGE_CODE:"";
                    $src_img    =   wp_get_attachment_image_src( get_post_meta($post->ID,'_thumbnail_id',1),"thumbnail" );
                ?>
                <?php include('includes/tarjeta-apartamentos.inc.php');?>
                <?php endwhile; ?>
                </div><!-- end row -->
            <?php endif; ?>


            </div><!--.bloque-->
    </div><!--.container-->
</section>
<?php include ('includes/sticky-nav-landings-aptos.php'); ?>
<?php get_footer(); ?>