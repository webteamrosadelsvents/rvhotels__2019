<?php 
//register menus
function rm_register_menus() {
  register_nav_menus(
	array(
		'header-menu' => __( 'Header Menu' ),
		'offpage-menu' => __( 'Offpage Menu' )
	)
  );
}
add_action( 'init', 'rm_register_menus' );
wp_enqueue_script( 'jquery' );

// Allow html in category description
remove_filter('pre_term_description', 'wp_filter_kses');

// register widgets
function rm_widgets_init() {
	register_sidebar (array(
		'name'          => __('1er peu','rm'),
		'id'            => "footer-first-widget-area",
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>' )
		);
	register_sidebar (array(
		'name'          => __('2on peu','rm'),
		'id'            => "footer-second-widget-area",
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>' )
		);
	register_sidebar (array(
		'name'          => __('3er peu','rm'),
		'id'            => "footer-third-widget-area",
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>' )
		);
	register_sidebar (array(
		'name'          => __('4rt peu','rm'),
		'id'            => "footer-fourth-widget-area",
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle">',
		'after_title'   => '</h3>' )
		);
}
add_action('init', 'rm_widgets_init');


// Excerpts en paginas:
add_action('init', 'my_custom_init');
function my_custom_init() {
	add_post_type_support( 'page', 'excerpt' );
}


// *************************************************************************************************
// ********************* IMAGE SIZES  **************************************************************
// ************************************************************************************************


if ( function_exists( 'add_image_size' ) ) { // Nova mida d'imatge, per les imatges destacades
add_image_size( 'Thumbnail-Post', 300, 225, true );
add_image_size( 'Promo-Feat', 600, 400, true );
}

add_theme_support( 'post-thumbnails' ); // Permet "imagen destacada"





// *************************************************************************************************
// ********************* LANGUAGES  **************************************************************
// ************************************************************************************************

function lang_category_id($id){
  if(function_exists('icl_object_id')) {
    return icl_object_id($id,'page',true);
  } else {
    return $id;
  }
}


function datepickerLanguage() {
//	echo ICL_LANGUAGE_CODE;
	if (ICL_LANGUAGE_CODE == 'es')
//	echo "es";
	wp_enqueue_script( 'datepicker-es', get_template_directory_uri() . '/js/jquery.ui.datepicker-es.js', array(), '1.0.0', true );

	if (ICL_LANGUAGE_CODE == 'fr')
//	echo "fr";
	wp_enqueue_script( 'datepicker-fr', get_template_directory_uri() . '/js/jquery.ui.datepicker-fr.js', array(), '1.0.0', true );

	if (ICL_LANGUAGE_CODE == 'ca')
//	echo "ca";
	wp_enqueue_script( 'datepicker-ca', get_template_directory_uri() . '/js/jquery.ui.datepicker-ca.js', array(), '1.0.0', true );
	
	if (ICL_LANGUAGE_CODE == 'en')
//	echo "en";
	wp_enqueue_script( 'datepicker-en', get_template_directory_uri() . '/js/jquery.ui.datepicker-en.js', array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'datepickerLanguage' );

// *************************************************************************************************
// ********************* EXTRAS  *******************************************************************
// *************************************************************************************************


function remove_menu_items() {
  global $menu;
  $restricted = array(__('Links'), __('Comments') ); //, __('Posts')
  end ($menu);
  while (prev($menu)){
    $value = explode(' ',$menu[key($menu)][0]);
    if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
      unset($menu[key($menu)]);}
    }
  }

add_action('admin_menu', 'remove_menu_items');

	
// Muestra logo en inicio
function custom_login_logo() {
	echo '<style type="text/css">
	.login h1 a { background: url("https://www.rvhotels.es/logo-rvhotels-gruprv.png") center / 100% auto no-repeat!important;height:147px!important;width:225px!important;}
	</style>';
}
add_action('login_head', 'custom_login_logo');


function custom_admin_css() {
	echo '<style type="text/css">
	input[type="text"][id*="custom_"],
	select[id*="custom_"],
	input[type="checkbox"][id*="custom_"]{
		background:azure!important;
		border-radius:0!important;
		border:2px solid #00a5df!important;
		box-shadow:none!important;
	}

	label[id*="custom_"] {
		color:#00a5df!important;
		text-transform:uppercase;
		font-weight:bold;
		font-size:12px;
		line-height:30px;
	}
	input[type=checkbox][id*="custom_"]:checked {
		background:#00a5df!important;
	}
	input[type=checkbox][id*="custom_"]:disabled {
		opacity:.3!important;
	}
	input[type=checkbox][id*="custom_"]:checked:before {
		color:#fff!important;
	}
	</style>';
}

add_action('admin_head', 'custom_admin_css');



/*
function do_feed_ofertas() {
	load_template( get_template_directory() . '/feeds/feed-ofertas-rss2.php' );
}
add_action( 'do_feed_ofertas','do_feed_ofertas',10, 1 );
*/

$arr_ubicas = array (  
	//					'alcoceber' => __('Alcoceber - Costa Azahar'),
	//					'segur_de_calafell' => __('Segur de Calafell/cunit - Costa Daurada'),
	//					'roses' => __('Roses - Costa Brava'),
	'-' => __('Seleccionar'),
	'rialp' => __('Rialp - Pirineo de Lleida'),
	'ametlla_mar' => __('L\'Ametlla de Mar - Costa Daurada'),
	'estartit' => __('L\'Estartit - Costa Brava'),
	'club_torrevella' => __('L\'Estartit - Torrevella'),
	'santacristina' => __('Santa Cristina d\'Aro - Costa Brava'),
	'platjadaro' => __('Platja D\'Aro - Costa Brava'),
	'Blanes' => __('Blanes - Costa Brava'),
	'torroella_montgri' => __('Torroella de Montgrí - Costa Brava'),
	'vall_llobrega' => __('Vall-Llobrega/Palamós - Costa Brava'),
	'escala' => __('L\'Escala - Costa Brava'),
	'valaran' => __('Vall d\'Aran - Pirineo de Lleida'),
	'menorca' => __('Ciutadella - Menorca')
	);


// *** CUSTOM POST & CUSTOM META  ***
// **********************************
include  ('functions-meta.php');

function campo_ordenacion_por_apartamentos(){
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }


    //only add filter to post type you want
    if ('promo' == $type){

		$aparts 	= 	get_posts( array("post_type"=>"apartamento","posts_per_page"=>"-1","suppress_filters"=>"0") );
		$def_aparts = 	array();
		foreach ($aparts as $apart) {
			$def_aparts[$apart->post_title]=$apart->ID;
		}		

		$hotels 	= 	get_posts( array("post_type"=>"hotel","posts_per_page"=>"-1","suppress_filters"=>"0") );
		$def_hotels = 	array();
		foreach ($hotels as $key => $value) {
			$def_hotels[$value->post_title]=$value->ID;
		}
        
        ?>
        <select name="filtrar_aparts">
        <option value=""><?php _e('Apartamentos ', 'Apartamento'); ?></option>
        <?php
            $current_v = isset($_GET['filtrar_aparts'])? $_GET['filtrar_aparts']:'';
            foreach ($def_aparts as $label => $value) {
                printf
                    (
                        '<option value="%s"%s>%s</option>',
                        $value,
                        $value == $current_v? ' selected="selected"':'',
                        $label
                    );
                }
        ?>
        </select>

        <select name="filtrar_hoteles">
        <option value=""><?php _e('Hoteles ', 'Hotel'); ?></option>
        <?php
            $current_v = isset($_GET['filtrar_hoteles'])? $_GET['filtrar_hoteles']:'';
            foreach ($def_hotels as $label => $value) {
                printf
                    (
                        '<option value="%s"%s>%s</option>',
                        $value,
                        $value == $current_v? ' selected="selected"':'',
                        $label
                    );
                }
        ?>
        </select>
        <?php
    }
}



function campo_ordenacion_por_apartamentos_filtrado( $query ){
    global $pagenow;
    $type = 'post';
    if (isset($_GET['post_type'])) {
        $type = $_GET['post_type'];
    }
    if ( 'promo' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['filtrar_aparts']) && $_GET['filtrar_aparts'] != '') {
        $query->query_vars['meta_key'] = 'custom_oferta_en_apart';
        $query->query_vars['meta_value'] = $_GET['filtrar_aparts'];
    }
    if ( 'promo' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['filtrar_hoteles']) && $_GET['filtrar_hoteles'] != '') {
        $query->query_vars['meta_key'] = 'custom_oferta_en_hotel';
        $query->query_vars['meta_value'] = $_GET['filtrar_hoteles'];
    }
}


add_action( 'restrict_manage_posts', 'campo_ordenacion_por_apartamentos' );
add_filter( 'parse_query', 'campo_ordenacion_por_apartamentos_filtrado' );




/*	FUNCIONES VARIAS
----------------------- */
function descrimina_hotel_apartamento($metas,$type="hotel"){
//	echo "TIPO = ".$type."<br />";
	global $arr_ubicas;
	if($type=="hotel"){
		$destino_id=$metas['custom_oferta_en_hotel'][0];
	}else{
		$destino_id=$metas['custom_oferta_en_apart'][0];		
	}
	$destino 	= get_the_title($destino_id);
	if($destino=="-"){
		$destino_id=$metas['custom_oferta_en_apart'][0];
	}
	$zona 		= get_post_meta($destino_id,"custom_zona",1);
	return array($destino, $arr_ubicas[$zona]);
}


function get_custom_link($metas, $post_id){
	if($metas['custom_oferta_en_hotel_url'][0] && $metas['custom_oferta_en_hotel_url'][0]!=""){
		$link = $metas['custom_oferta_en_hotel_url'][0];
	}else{
		$link = get_permalink($post_id);
	}
	return $link;
}

function print_slideshow($id=false,$contenido=false){
	$shorted_path = array(
						"img_path_en_plantilla"=>get_bloginfo('template_url'),
						"img_path_en_root"=>get_bloginfo('wpurl')
						);
	$slide_show=(isset($id) && $id!="")?get_post_meta($id,"custom_slide",1):$contenido;
	foreach ($shorted_path as $key => $value) {
		$slide_show = str_replace("[".$key."]",$value,$slide_show);
	}
	return $slide_show;
}


function get_tipologias($postId){
	$tipologias = array();
	$metas = get_post_meta($postId);
	if(isset($metas['custom_titulo_tipo_1']) && $metas['custom_titulo_tipo_1']!=""){
		$tipologias[0]['titulo']=$metas['custom_titulo_tipo_1'][0];
		$tipologias[0]['descripcion']=$metas['custom_descripcion_tipo_1'][0];
		$tipologias[0]['slide']=print_slideshow(false, $metas['custom_slide_tipo_1'][0]);
	}
	if(isset($metas['custom_titulo_tipo_2']) && $metas['custom_titulo_tipo_2']!=""){
		$tipologias[1]['titulo']=$metas['custom_titulo_tipo_2'][0];
		$tipologias[1]['descripcion']=$metas['custom_descripcion_tipo_2'][0];
		$tipologias[1]['slide']=print_slideshow(false, $metas['custom_slide_tipo_2'][0]);
	}
	return $tipologias;
}


/* FUNCIONES DE PULIDO DE HEADER Y CARGA DE SCRIPTS:
---------------------------------------------------- */

/* QUITA EL JQUERY MIGRATE */
function dequeue_jquery_migrate( &$scripts){
	if(!is_admin()){
		$scripts->remove( 'jquery');
		$scripts->add( 'jquery', false, array( 'jquery-core' ) );
	}
}
add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );


/* AÑADE ASYNC A TODOS LOS TAGS SCRIPT qUE NO SEAN JQUERY */
/*
function script_tag_defer($tag, $handle) {
	if(strpos($tag, "/jquery.js") === false)
    	return str_replace(' src',' async src', $tag);
    return str_replace(' src',' src', $tag);
}
add_filter('script_loader_tag', 'script_tag_defer',10,2);
*/

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


/* Elimina el meta lang
--------------------------------- */
//global $sitepress;
//remove_action('wp_head', array($sitepress, 'head_langs'));
/* Elimina el meta generator
--------------------------------- */
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', array($sitepress, 'meta_generator_tag' ) );

/* FUNCION DE NORMALIZACION DE ACENTOS ETC DESCARGADA - COMENTAR CON JORDI */
/* ALGUNAS INICIALES COMO ANGELS EN CATALAN LLEVA ACENTO, HAY QUE NORMALIZAR PARA QUE SUBSTITUYA POR UNA A*/

function normaliza($cadena){
    $originales  = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $cadena = utf8_decode($cadena);
    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
    $cadena = strtolower($cadena);
    return utf8_encode($cadena);
}

/* funcion para la inversion de formatos de fechas */
function cambia_fecha($fecha, $separador_original, $separador_definitivo) {
	$retorno = "";
	if ($fecha != "") {
		$valor = explode($separador_original, $fecha);
		$limpiamos_resto = explode(" ", $valor[2]);
		$valor[2] = $limpiamos_resto[0];
		if ($valor[0] + $valor[1] + $valor[2]) {
			$retorno = $valor[2].$separador_definitivo.$valor[1].$separador_definitivo.$valor[0];
		} 
	} 
	return $retorno;
}

/* para single-oferta de hotel, averiguar de que hotel se trata segun el post id de cada hotel, que es diferente en cada idioma */
/* resolver el hotel es el primer paso para resolver el motor de reservas usado en ese hotel */

function resuelveHotel($hotelID) {
	switch ($hotelID) {
		case 51: // es
		case 224: // ca
		case 329: // en
		case 328: // fr
			return "ametlla";
			break;

		case 519: // es
		case 660: // ca
		case 675: // en
		case 678: // fr
			return "condes";
			break;

		case 2741: // es
		case 2749: // ca
		case 2747: // en
		case 2748: // fr
			return "gr92";
			break;

		//case 49: // es
		//case 223: // ca
		//case 331: // en
		//case 330: // fr
		//	return "sesarrels";
		//	break;

		case 23: // es
		case 226: // ca
		case 333: // en
		case 332: // fr
			return "palau";
			break;

		case 4277: // es
		case 4281: // ca
		case 4282: // en
		case 4283: // fr
			return "tuca";
			break;

		case 4681: // es
		case 4688: // ca
		case 4689: // en
		case 4690: // fr
			return "orri";
			break;

		case 4719: // es
		case 4724: // ca
		case 4725: // en
		case 4726: // fr
			return "nievesmar";
			break;

		case 10226: // es
		case 10269: // ca
		case 10271: // en
		case 10272: // fr
			return "menorca";
			break;	

		case 10495: // es
		case 10478: // ca
		case 10496: // en
		case 10497: // fr
			return "nautic";
			break;						

		case 12864: // es
		case 12946: // ca
		case 12947: // en
		case 12948: // fr
			return "hotelgolf";
			break;				
	}
}

/* asignar el motor al hotel correspondiente */

/* ATENCION: La app promosapp.js de cada hotel, esta configurada para un motor */
/* Si un hotel cambia de motor de reservas, se tendran que hacer los cambios pertinentes en el lector js del correspondiente hotel */
/* para cambiar el motor de reservas a Ametlla o a Condes, simplemente habra que mirar el fragmento de codigo js que corresponde a donde
se monta el boton de reservar (se puede copiar de el mismo archivo en otro hotel que sea siteminder ;) la unica diferenci es que unos
montan un link con params get y otros montan un formulario con boton submit */

function resuelveMotor($esteHotel) {
	switch ($esteHotel) {
		//case "gr92":
		//case "sesarrels":
		//case "palau":
		//	return "bookingbutton"; //siteminder
		//	break;
			
		case "condes":
			//return "acinet";
			//break;

		//case "sesarrels":
		case "palau":
		case "gr92":	
		case "ametlla":
		case "nievesmar":
		case "tuca":
		case "orri":
		case "menorca":
		case "nautic":
		case "hotelgolf":
			return "bookings";
			break;
	}
}

// elimina de las promos algunos shortcodes antiguos de joomla
function replace_content($content)
{
$content = str_replace('{loadposition notapacks}', '<div style="display:none;" class="alert alert-warning nota-suplementos text-small"></div>',$content);
$content = str_replace('{loadposition comparteix}', '<div style="display:none;" class="alert alert-warning nota-suplementos text-small"></div>',$content);
return $content;
}
add_filter('the_content','replace_content');

// redireccion de formulario de contacto 7 a la thank you page
function jsContactForm7_wp_footer() {
	?>
	<script type="text/javascript">
	document.addEventListener( 'wpcf7mailsent', function( event ) {
	   
	   // FORMULARIO CONTACTO CAT
	   if ( '508' == event.detail.contactFormId ) {
		location.replace('https://www.rvhotels.es/ca/gracies');
		}
		// FORMULARIO CONTACTO ES
		if ( '70' == event.detail.contactFormId ) {
			location.replace('https://www.rvhotels.es/gracias');
		}
		// FORMULARIO CONTACTO EN
		if ( '513' == event.detail.contactFormId ) {
			location.replace('https://www.rvhotels.es/en/thankyou');
		}
		// FORMULARIO CONTACTO FR
		if ( '515' == event.detail.contactFormId ) {
			location.replace('https://www.rvhotels.es/fr/merci');
		}
	}, false );
	</script>
	<?php
	}


/* ---------------------------------------------------------------------------
 * Set hreflang="x-default" according to Google content guidelines with WPML
 * Put into your functions.php - don't forget to use a child-theme ;-)
 * --------------------------------------------------------------------------- */
add_filter('wpml_alternate_hreflang', 'wps_head_hreflang_xdefault', 10, 2);
function wps_head_hreflang_xdefault($url, $lang_code) {

    if($lang_code == apply_filters('wpml_default_language', NULL )) {

        echo '<link rel="alternate" href="' . $url . '" hreflang="x-default" />';
    }

    return $url;
}

/* ------------------------------ */
/* Paginació dels posts del Blog  */
/* ------------------------------ */

function blog_paginacio($pages = '', $range = 2){  
	$showitems = ($range * 2)+1;
	global $paged;
	if(empty($paged)) $paged = 1;
	if($pages == ''){
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages){
			$pages = 1;
		}
	}
	if(1 != $pages){
		echo "<div class='pagination'>";
		if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'><<</a>";
		if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'><</a>";
		for ($i=1; $i <= $pages; $i++){
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
				echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
			}
		}
		if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>></a>";  
		if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>>></a>";
		echo "</div>\n";
	}
}

/* --------------------------------------------------------------------------- */
/* Soluciona el tema dels Apartaments (E-mail enviat per Albert el 02-12-2018) */
/* --------------------------------------------------------------------------- */

add_filter( 'sf-filter-args', 'show_all_posts' );
function show_all_posts( $args ) {
	$args['posts_per_page'] = -1;
	return $args;
}

?>