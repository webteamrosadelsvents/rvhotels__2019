<?php

/*
Template Name: HARCODE CATEGORIZADO, YA CAMBIARMEOS

$args = array(
    'posts_per_page'   => 5,
    'offset'           => 0,
    'category'         => '',
    'orderby'          => 'post_date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'post',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'post_status'      => 'publish',
    'suppress_filters' => true );
*/

$taxonomies =   get_terms("promo_categories", array("hide_empty"=>0, "taxonomy"=>"promo_categories", "suppress_filters"=>false) ); 
$aparts     =   get_posts( array("post_type"=>"apartamento", "posts_per_page"=>-1, "suppress_filters"=>false) ); 
$hotels     =   get_posts( array("post_type"=>"hotel", "posts_per_page"=>-1, "suppress_filters"=>false) );

$queried_object = get_queried_object();
$term_meta = get_option( "taxonomy_".$queried_object->term_id );
$termID = $queried_object->term_id;

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
        return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
    }
add_filter('language_attributes', 'add_opengraph_doctype');

// arreglo en el title meta tag

function my_title($title)   
{  
    global $post, $queried_object, $term_meta;  
    // Do whatever here with your title...  
    //$title = $queried_object->name.' - '.__("Ofertas").': '.__("Hoteles y apartamentos");
	$title = $term_meta['meta_tit'];
      
    return $title;
}
add_filter('aioseop_title', 'my_title', 1);

//Lets add Open Graph Meta Info

if ( get_field('cat_img', $queried_object!="") ) {
    //$ogpicture = get_field('cat_img', 'term_' .$queried_object->term_id);
    $ogpicture = get_field('cat_img', $termID);
} else {
    $ogpicture = get_bloginfo('template_url')."/images/".$term_meta['term_large_img_url'];
}

// OPENGRAPH Y TWEET CARD DE TAXONOMIA, NO DE POST
// DESHABILITAR EN ESTAS PAGINAS EL PLUGIN FB OPENGRAPH, MEDIANTE PLUGIN ORGANIZER
function insert_fb_in_head() {
    global $queried_object;
    global $ogpicture;
	global $term_meta;
	
    $cleanDescription = $queried_object->description;
    $cleanDescription = str_replace("<p>", "", $cleanDescription);
    $cleanDescription = str_replace("</p>", " ", $cleanDescription);
    $cleanDescription = substr($cleanDescription, 0, 130)."...";

    //$sharingtitle = $queried_object->name.' - '.__("Ofertas").': '.__("Hoteles y apartamentos");
	
    // <meta property="fb:admins" content="YOUR USER ID"/>;
    ?>

	<meta name="title" content="<?php echo ($term_meta['meta_tit']); ?>" />
	<meta name="description" content="<?php echo ($term_meta['meta_des']); ?>" />
    <meta property="og:title" content="<?php echo $sharingtitle;?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="<?php echo __("https://www.rvhotels.es/promos/").$queried_object->slug;?>"/>
    <meta property="og:site_name" content="RVHotels &amp; apartments"/>
    <?php
    //the post does not have featured image, use a default image
    if(!has_post_thumbnail( $post->ID )) { 
        //replace this with a default image on your server or an image in your media library
        $default_image= get_bloginfo('template_url').'/images/promos_familia.jpg'; 
        echo '<meta property="og:image" content="' . $default_image . '"/>';
    }
    else{
        $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
        echo '<meta property="og:image" content="'.$ogpicture.'"/>';
    }
    echo "
    ";?>
    <meta name="twitter:card" value="summary_large_image" />
    <meta name="twitter:url" value="<?php echo __("https://www.rvhotels.es/promos/").$queried_object->slug;?>" />
    <meta name="twitter:title" value="<?php echo $sharingtitle;?>" />
    <meta name="twitter:description" value="<?php echo ($term_meta['meta_des']); ?>" />
    <?php
    //the post does not have featured image, use a default image
    if(!has_post_thumbnail( $post->ID )) { 
        //replace this with a default image on your server or an image in your media library
        $default_image= get_bloginfo('template_url').'/images/promos_familia.jpg'; 
        echo '<meta name="twitter:image" value="' . $default_image . '"/>';
    }
    else{
        $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
        echo '<meta name="twitter:image" value="'.$ogpicture.'"/>';
    }?>
    <meta name="twitter:site" value="@Rv_hotels" />
    <?php
}
add_filter( 'wp_head', 'insert_fb_in_head', 5 );

get_header();
$mostrarLog=false;
$log = "<pre style='display:none;' id=jordi>";
/*
<script>
    console.log('<?php echo print_r($term_meta, true);?>');
</script>
*/
?>
<script src="https://www.rvhotels.es/wp-content/themes/rvhotels/js/isotope.pkgd.min.js"></script>
<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js" ></script>


<?php include("includes/modal-promo-apto-v2.inc.php");?>




<section class="bg bgcategorias">
    <?php /*
    echo "<pre class='hidden'>";
    print_r($queried_object);
    echo "</pre>";
    */
    ?>
    <article class="container-categorias">
        <div class="container fullscreen">
            <div class="col-md-12 container-chillon padding5">
              <div class="col-md-12 slide-chillon padding0">
                <div class="position-relative container-categoria-big">
                        <?php
                        if($term_meta['term_large_img_url'] && $term_meta['term_large_img_url']!=""){
                        ?>
                        <figure><img src="<?php echo get_bloginfo('template_url');?>/images/<?php echo $term_meta['term_large_img_url'];?>" alt="<?php echo $queried_object->name;?>"></figure>
                        <?php // NO FUNCIONA NO SE POR QUE NO PILLA LA IMAGEN, VER CODIGO ARRIBA, OPEN GRAPH
                        } else if ( get_field('cat_img', $termID) ) {
                        ?>
                        <figure><img src="<?php echo $ogpicture;?>" alt="<?php echo $queried_object->name;?>"></figure>
                        <?php
                        } else {
                        ?>
                        <!-- IMAGEN POR DEFAULT DE CATEGORIAS -->
                        <figure><img src="<?php echo get_bloginfo('template_url');?>/images/promos_familia.jpg" alt="<?php echo $queried_object->name;?>"></figure>
                        <?php
                        }
                        ?>
                        <div class="categoria-title position-absolute zeroabs bg-blue-alpha">
                          <h1 class="text-center text-bold blanco text-uppercase"><?php echo __("Ofertas");?> <?php echo $queried_object->name;?></h1>
                          <h2 class="text-center subtitle blanco">
                            <?php
                            if($term_meta['promoCategoryTagline'] && $term_meta['promoCategoryTagline']!="") {
                            
                                echo $term_meta['promoCategoryTagline'];

                            } else {
                                echo __("Promociones especiales de Hoteles y Apartamentos en la Costa Brava, Costa Dorada y en el Pirineo");
                            }
                          ?></h2>
                        </div>

                </div>
              </div>
            </div>
        </div>
    </article>
</section>


<section class="bgofertas">       
    <div class="container" >
            <section class="categoria-intro text-center blanco subtitle padding30"></section>
    </div>

    <?php  if( ($termID == 18) || ($termID == 24) || ($termID == 26) || ($termID == 25) ) {
        // FILTRO EN CATEGORIA OFERTAS ESPECIAL APARTAMENTO - el temid cambia segun idioma
        include('includes/filtro-ofertas-apartamentos.php');
    }?>

    <?php  if( ($termID == 11) || ($termID == 10) || ($termID == 12) || ($termID == 9) || ($termID == 22) || ($termID == 21) || ($termID == 23) || ($termID == 14) ) {
        // FILTRO EN CATEGORIA OFERTAS  -FIN DE SEMANA-  Y  -EN FAMILIA-
        include('includes/filtro-ofertas-establecimiento-destino.php');
    }?>

    <div class="container">
        <div class="container-ofertas padding0 clear">

            <div class="row-ofertas grid">

                <div class="grid-sizer"></div>
                <div class="gutter-sizer"></div>
                <article class="sf-result-head text-center noresults hidden margin-bottom-30 animate-slide-top"><?php echo __("NO HAY RESULTADOS");?></article>


                <?php
                $count=0;

                // echo "<pre>".print_r($_POST, true)."</pre>";
                if ($mostrarLog) $log.="inicio la revision<br/>";

                while(have_posts()) {

                    the_post();
                    
                    $metas          = get_post_meta($post->ID);
                    // echo "<pre>".print_r($metas, true)."</pre>";
                    
                    if ($mostrarLog) $log.="POST ".print_r($_POST, true)."<br/>";

                    $pasar=false;
                    if ($_POST["destino"] <> -1) {
                        if ($_POST["destino"] == $metas['custom_oferta_en_zona'][0]) {
                            $pasar=true;
                        }
                    }
                    
                    if ($_POST["form_apartamento"] <> -1) {
                        if ($_POST["form_apartamento"] == $metas['custom_oferta_en_apart'][0]) {
                            $pasar=true;
                        }
                    }
                    
                    if ($_POST["form_hotel"] <> -1) {
                        if ($_POST["form_hotel"] == $metas['custom_oferta_en_hotel'][0]) {
                            $pasar=true;
                        }
                    }

                    if (($_POST["form_hotel"] == -1) and ($_POST["destino"] == -1) and  ($_POST["form_apartamento"] == -1)) $pasar=true;

                    if (!isset($_POST["form_hotel"]) and !isset($_POST["destino"]) and !isset($_POST["form_apartamento"])) $pasar=true;
                    
                    if ($pasar) {
                        $titulo         = get_the_title();
                        $imagen         = wp_get_attachment_image_src( $metas['_thumbnail_id'][0],"thumbnail" );
                        $precio         = ($metas['custom_precio'][0])?$metas['custom_precio'][0]:"0";
                        $dto            = $metas['custom_descuento'][0];
                        $pornoche       = ( ($metas['custom_pornoche'][0]!="0") && ($metas['custom_pornoche'][0]!="") ) ? __("noche") : "";

                        $esHotel        = (isset($metas['custom_oferta_en_apart']) && $metas['custom_oferta_en_apart'][0]!="-")?"nohotel":"hotel";
                        $hotel_apart    = descrimina_hotel_apartamento($metas, $esHotel);
                        $link           = get_custom_link($metas, $post->ID);

                        $idApartWP      = (isset($metas['custom_oferta_en_apart'][0]) ? $metas['custom_oferta_en_apart'][0] : "" );
                        $linkApartPromo = get_permalink($idApartWP);

                        $idAptoNeo      = get_post_meta( $idApartWP, 'custom_bookings', true );
                        $arrival        = (isset($metas['custom_dtinicio'][0]) ? $metas['custom_dtinicio'][0] : "" );
                        $departure      = (isset($metas['custom_dtfin'][0])? $metas['custom_dtfin'][0] : "");
                        $nights         = (isset($metas['custom_noches'][0])? $metas['custom_noches'][0] : "");

                        $zonaClass      = (isset($metas['custom_oferta_en_zona'][0]) && $metas['custom_oferta_en_zona'][0]!="-") ? "zona-".$metas['custom_oferta_en_zona'][0] : "zona-";
                        $zonaClass      = sanitize_title($zonaClass,"default");

                        $postID_class    = "promoid-".$post->ID;
                        $postID_urlencoded = urlencode("?promoid=".$post->ID);

                        $discountType   = "dto-".get_field("apto_tipodescuento");

        //                echo "<pre>".print_r($metas['custom_oferta_en_apart'],true)."</pre>";
                        
                        include('includes/minioferta.inc.php');

                        $count++;
                        
                    }
                }

                if ($mostrarLog) {
                    $log.="</pre>";
                    echo $log;
                }
                ?>
            </div> <!-- row ofertas -->
        </div>
        <!--/OFERTAS -->
        <section class="clear text-center blanco container-description">
        <?php echo $queried_object->description;?>
        </section>

        <script>
            jQuery(function($){
                // Layout - distribuir textos y parrafos de la descripcion de la categoria
                $('.container-description>p:first-of-type').appendTo('.categoria-intro');
                $('.container-description>p:first-of-type').addClass('text-bold');

                // Marcar menu ofertas como actual, bug de page-promos y taxonomy-promo-categories
                $('#menu-menuprincipal li:nth-child(3)').addClass('current-menu-item');
            });
        </script>

        <script src="<?php bloginfo('template_url'); ?>/js/isotope.behavior.js"></script>
    </div><!--.container-->
    <?php include("includes/compartir-share.inc.php");?>

    <div class="catswitcher-container text-center">
    <?php include("includes/catswitcher.inc.php");?>
    </div>
</section>


<?php get_footer(); ?>